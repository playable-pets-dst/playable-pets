
------------------------------------------------------------------
-- Variables
------------------------------------------------------------------

local require = GLOBAL.require
local Ingredient = GLOBAL.Ingredient

local PlayablePets = GLOBAL.PlayablePets
local SETTING = GLOBAL.PP_SETTINGS
local MOBTYPE = GLOBAL.PP_MOBTYPES

PlayablePets.Init(env.modname)

local STRINGS = require("pp_strings")

GLOBAL.PP_FORGE = require("pp_forge")

GLOBAL.ROT_FIX = function(inst) --Remove this after doing the rework
	if inst.components.embarker and inst.components.locomotor then
		inst.components.locomotor:SetAllowPlatformHopping(false)
	end
end

------------------------------------------------------------------
-- Configuration Data
------------------------------------------------------------------

local TUNING = require("pp_tuning") --has to be here to catch configuration settings.

-- For preventing Growth on mobs that grow into disabled mobs

------------------------------------------------------------------
-- Prefabs
------------------------------------------------------------------

PrefabFiles = {
	-------Houses--------
	"mermhouse_player",
	"pighouse_player",
	"igloo_player",
	"spidernest_p",
	"houndmound_p",
	"catcoonden_p",
	"maggothome",
	"lavapond_p",
	"spiderhome",
	"houndhome",
	"cathome",
	"rabbithome",
	"rabbithole_p",
	"molehome",
	"beehome",
	--"froghome",
	"molehole_p",
	"beehive_p",
	--"pond_p",
	"monkeybarrel_p",
	"spiderhole_p",
	"batcave_p",
	"slurtlemound_p",
	-------Tools---------
	"monster_wpn",
	"pigtorch_p",
	"ppbuffs",
	"monkeyhouse",
	"cspiderhome",
	"slurtlehome",
	"rabbithouse_player",
	"bathome",
}

-- Centralizes a number of related functions about playable mobs to one convenient table.
-- Name is the prefab name of the mob.
-- Fancyname is the name of the mob, as seen in-game.
-- Gender is fairly self-explanatory.
-- skins is deprecated and no longer does anything.
-- forge is a boolean used to determine whether or not its able to load up in Reforged.
-- mobtype is used for filter configurations in PP Essentials.
-- mimic_prefab is a table specifically for transform/imposter abilities. I recommend looking at PP Essential's PP_TRANSFORM Action code for more info.
GLOBAL.PP_MobCharacters = {
	houndplayer        = { fancyname = "Hound",           gender = "MALE",   mobtype = {}, skins = {1, 2}, forge = true, mimic_prefab = {"hound"}},
	houndiceplayer     = { fancyname = "Ice Hound",       gender = "MALE",   mobtype = {}, skins = {1}, forge = true, mimic_prefab = {"icehound"}},
	houndredplayer     = { fancyname = "Hell Hound",      gender = "MALE",   mobtype = {}, skins = {1}, forge = true, mimic_prefab = {"firehound"} },
	--hounddarkplayer    = { fancyname = "Nightmare Hound", gender = "MALE",   mobtype = {}, skins = {} }, --Used for testing new features.
	blackspiderplayer  = { fancyname = "Spider",          gender = "MALE",   mobtype = {}, skins = {1}, forge = true, mimic_prefab = {"spider"}},
	warriorp           = { fancyname = "Spider Warrior",  gender = "MALE",   mobtype = {}, skins = {1}, forge = true, mimic_prefab = {"spider_warrior"} },
	whitespiderplayer  = { fancyname = "Depth Dweller",   gender = "MALE",   mobtype = {}, skins = {1}, forge = true, mimic_prefab = {"spider_dropper"} },
	queenspiderplayer  = { fancyname = "Spider Queen",    gender = "FEMALE", mobtype = {MOBTYPE.GIANT}, skins = {1}, mimic_prefab = {"spiderqueen"} },
	--wqueenspiderplayer = { fancyname = "Dweller Queen",   gender = "FEMALE", mobtype = {MOBTYPE.GIANT}, skins = {1},  },
	treeplayer         = { fancyname = "Tree Guard",      gender = "ROBOT",  mobtype = {MOBTYPE.GIANT}, skins = {2}, forge = true, mimic_prefab = {"leif"} },
	vargplayer         = { fancyname = "Varg",            gender = "MALE",   mobtype = {MOBTYPE.GIANT}, skins = {1}, forge = true, mimic_prefab = {"warg"} },
	babygooseplayer    = { fancyname = "Mosling",         gender = "FEMALE", mobtype = {}, 	skins = {1}, mimic_prefab = {"mossling"} },
	moosegooseplayer   = { fancyname = "Moose/Goose",     gender = "FEMALE", mobtype = {MOBTYPE.GIANT}, skins = {1}, mimic_prefab = {"moose"} },
	maggotplayer       = { fancyname = "Lavae",           gender = "MALE",   mobtype = {}, 	skins = {1}, mimic_prefab = {"lavae"} },
	dragonplayer       = { fancyname = "Dragonfly",       gender = "MALE",   mobtype = {MOBTYPE.GIANT}, skins = {1}, mimic_prefab = {"dragonfly"} },
	bearplayer         = { fancyname = "Bearger",         gender = "MALE",   mobtype = {MOBTYPE.GIANT}, skins = {1,6}, forge = true, mimic_prefab = {"bearger"} },
	deerplayer         = { fancyname = "Deerclops",       gender = "MALE",   mobtype = {MOBTYPE.GIANT}, skins = {1}, forge = true, mimic_prefab = {"deerclops"} },
	ghostplayer        = { fancyname = "Ghost",           gender = "MALE",   mobtype = {}, skins = {1}, mimic_prefab = {"ghost"} },
	pigmanplayer       = { fancyname = "Pig",             gender = "MALE",   mobtype = {MOBTYPE.CRAFTY}, skins = {1}, forge = true, mimic_prefab = {"pigman"} },
	pigman_guardp      = { fancyname = "Pig Guard",       gender = "MALE",   mobtype = {MOBTYPE.CRAFTY}, skins = {1}, mimic_prefab = {"pigguard"}},
	mermplayer         = { fancyname = "Merm",            gender = "MALE",   mobtype = {MOBTYPE.CRAFTY}, skins = {1}, forge = true, mimic_prefab = {"merm"} },
	walrusplayer       = { fancyname = "McTusk",          gender = "MALE",   mobtype = {MOBTYPE.CRAFTY}, skins = {1}, forge = true, mimic_prefab = {"spider"} },
	tentaclep          = { fancyname = "Tentacle",        gender = "MALE",   mobtype = {}, skins = {1}, mimic_prefab = {"walrus"} },
	krampusp           = { fancyname = "Krampus",         gender = "MALE",   mobtype = {}, skins = {1,2}, forge = true, mimic_prefab = {"krampus"}},
	catplayer          = { fancyname = "Catcoon",         gender = "FEMALE", mobtype = {}, skins = {1,2}, mimic_prefab = {"catcoon"} },
	beefplayer         = { fancyname = "Beefalo",         gender = "FEMALE", mobtype = {}, skins = {1}, mimic_prefab = {"beefalo", "babybeefalo"} },
	smallbirdp         = { fancyname = "Smallbird",       gender = "FEMALE", mobtype = {}, skins = {1}, mimic_prefab = {"smallbird"} },
	tallbirdplayer     = { fancyname = "Tallbird",        gender = "FEMALE", mobtype = {}, skins = {1}, mimic_prefab = {"tallbird"} },
	goatp              = { fancyname = "Volt Goat",       gender = "MALE",   mobtype = {}, skins = {1}, mimic_prefab = {"lightninggoat"} },
	clockwork1player   = { fancyname = "Knight",          gender = "ROBOT",  mobtype = {}, skins = {1}, mimic_prefab = {"knight"} },
	clockwork2player   = { fancyname = "Bishop",          gender = "ROBOT",  mobtype = {}, skins = {1}, mimic_prefab = {"bishop"} },
	clockwork3player   = { fancyname = "Rook",            gender = "ROBOT",  mobtype = {}, skins = {1}, mimic_prefab = {"rook"} },
	bigfootp  		   = { fancyname = "Big Foot",        gender = "ROBOT",  mobtype = {}, skins = {} },
	--partners
	berniep        = { fancyname = "Bernie",           gender = "ROBOT",   mobtype = {}, skins = {}, forge = true, mimic_prefab = {"bernie"} },
	abbyp        = { fancyname = "Abigail",           gender = "FEMALE",   mobtype = {}, skins = {}, forge = true, mimic_prefab = {"abigail"} },
	balloonp        = { fancyname = "Balloon",           gender = "ROBOT",   mobtype = {}, skins = {}, forge = true, mimic_prefab = {"balloon"} },
	smallghostp        = { fancyname = "Pipspook",           gender = "ROBOT",   mobtype = {}, skins = {}, forge = true, mimic_prefab = {"smallghost"} },
	balloon_partyp        = { fancyname = "Party Balloon",           gender = "ROBOT",   mobtype = {}, skins = {}, mimic_prefab = {"balloonparty"} },
	balloon_speedp        = { fancyname = "Speed Balloon",           gender = "ROBOT",   mobtype = {}, skins = {}, mimic_prefab = {"balloonspeed"} },
	--cute
	rabbitp        = { fancyname = "Rabbit",           gender = "FEMALE",   mobtype = {}, skins = {}, forge = true, mimic_prefab = {"rabbit"}},
	pengullp        = { fancyname = "Pengull",           gender = "FEMALE",   mobtype = {}, skins = {}, mimic_prefab = {"penguin"}},
	beep        = { fancyname = "Bee",           gender = "MALE",   mobtype = {}, skins = {}, mimic_prefab = {"bee", "killerbee"}},
	glommerp        = { fancyname = "Glommer",           gender = "FEMALE",   mobtype = {}, skins = {}, mimic_prefab = {"glommer"}},
	frogp        = { fancyname = "Frog",           gender = "MALE",   mobtype = {}, skins = {}, mimic_prefab = {"frog"}},
	molep        = { fancyname = "Moleworm",           gender = "MALE",   mobtype = {}, skins = {}, mimic_prefab = {"mole"}},
	perdp        = { fancyname = "Gobbler",           gender = "FEMALE",   mobtype = {}, skins = {}, mimic_prefab = {"perd"}},
	flyp        = { fancyname = "Mosquito",           gender = "MALE",   mobtype = {}, skins = {}, mimic_prefab = {"mosquito"}},
	gekkop        = { fancyname = "Grass Gekko",           gender = "MALE",   mobtype = {}, skins = {}, mimic_prefab = {"grassgekko"}},
	elephantplayer        = { fancyname = "Koalefant",           gender = "MALE",   mobtype = {}, skins = {}, mimic_prefab = {"koalefant_summer", "koalefant_winter"}},
	chesterp        = { fancyname = "Chester",           gender = "MALE",   mobtype = {}, skins = {}, mimic_prefab = {"chester"}},
	crowp        = { fancyname = "Crow",           gender = "FEMALE",   mobtype = {}, skins = {}, mimic_prefab = {"crow"}},
	birdp        = { fancyname = "Robin",           gender = "MALE",   mobtype = {}, skins = {}, mimic_prefab = {"robin", "robin_winter"}},
	buzzardp        = { fancyname = "Buzzard",           gender = "MALE",   mobtype = {}, skins = {}, mimic_prefab = {"buzzard"}},
	mandrakep        = { fancyname = "Mandrake",           gender = "MALE",   mobtype = {}, skins = {}, mimic_prefab = {"mandrake"}},
	birchnutterp        = { fancyname = "Birchnutter",           gender = "MALE",   mobtype = {}, skins = {}, mimic_prefab = {"birchnutdrake"}},
	butterflyp        = { fancyname = "Rabbit",           gender = "FEMALE",   mobtype = {}, skins = {}, mimic_prefab = {"butterfly"}},
	shadow2player       	 = { fancyname = "Terrorbeak",       gender = "MALE",   mobtype = {}, skins = {}, mimic_prefab = {"nightmarebeak"}},
	wormp       	 = { fancyname = "Depth Worm",       gender = "FEMALE",   mobtype = {}, skins = {}, mimic_prefab = {"worm"}},
	batp       	 = { fancyname = "Batilisk",       gender = "MALE",   mobtype = {}, skins = {}, mimic_prefab = {"bat"}},
	cspiderp       	 = { fancyname = "Spitter",       gender = "MALE",   mobtype = {}, skins = {}, mimic_prefab = {"spider_spitter"}}, --forge = true },
	cspider2p        = { fancyname = "Hider",           gender = "MALE",   mobtype = {}, skins = {}, mimic_prefab = {"spider_hider"}}, --forge = true },
	hutchp        = { fancyname = "Hutch",           gender = "FEMALE",   mobtype = {}, skins = {}, mimic_prefab = {"hutch"}}, --forge = true },
	monkeyp        = { fancyname = "Splumonkey",           gender = "MALE",   mobtype = {}, skins = {}, mimic_prefab = {"monkey"}}, --forge = true },
	slurperp        = { fancyname = "Slurper",           gender = "FEMALE",   mobtype = {}, skins = {}, mimic_prefab = {"slurper"}}, --forge = true },
	slurtlep        = { fancyname = "Slurtle",           gender = "MALE",   mobtype = {}, skins = {}, mimic_prefab = {"slurtle", "snurtle"}}, --forge = true },
	bunnyp        = { fancyname = "Bunnyman",           gender = "MALE",   mobtype = {}, skins = {}, mimic_prefab = {"bunnyman"}}, --forge = true },
	rocklobsterp        = { fancyname = "Rock Lobster",           gender = "FEMALE",   mobtype = {}, skins = {}, mimic_prefab = {"rocky"}}, --forge = true},
	guardianp        = { fancyname = "Guardian",           gender = "MALE",   mobtype = {}, skins = {}, mimic_prefab = {"minotaur"}}, --forge = true},
	shadowplayer        = { fancyname = "Crawling Horror",           gender = "MALE",   mobtype = {}, skins = {}, mimic_prefab = {"crawlingnightmare"}}, --forge = true},
	--maxpuppetp        = { fancyname = "Maxwell's Shadow",           gender = "ROBOT",   mobtype = {}, skins = {} },
}

-- Necessary to ensure a specific order when adding mobs to the character select screen. This table is iterated and used to index the one above
PP_Character_Order = {
	"rabbitp",
	"butterflyp",
	"crowp",
	"birdp",
	"beep",
	"glommerp",
	"frogp",
	"molep",
	"perdp",
	"flyp",
	"mandrakep",
	"birchnutterp",
	"gekkop",
	"chesterp",
	"buzzardp",
	"pengullp",
	"houndplayer", 
	"houndiceplayer", 
	"houndredplayer", 
	"vargplayer",
	"elephantplayer", 
	"blackspiderplayer", 
	"warriorp", 
	"whitespiderplayer", 
	"berniep", 
	"balloonp", 
	"balloon_partyp", 
	"balloon_speedp",  
	"smallghostp",
	"ghostplayer", 
	"abbyp", 
	"pigmanplayer", 
	"pigman_guardp", 
	"mermplayer", 
	"walrusplayer",
	"bunnyp", 
	"tentaclep", 
	"krampusp", 
	"catplayer", 
	"beefplayer", 
	"smallbirdp", 
	"tallbirdplayer", 
	"goatp",
	"batp",
	"cspiderp",
	"cspider2p",
	"slurperp",
	"slurtlep",
	"wormp",
	"hutchp",
	"monkeyp",
	"rocklobsterp",
	"shadowplayer",
	"shadow2player",
	"clockwork1player", 
	"clockwork2player", 
	"clockwork3player",
	"queenspiderplayer", 
	"treeplayer", 
	"babygooseplayer", 
	"moosegooseplayer", 
	"maggotplayer", 
	"dragonplayer", 
	"bearplayer", 
	"deerplayer",
	"guardianp", 
	"bigfootp",
}

GLOBAL.MOB_DRAGONFLY = PlayablePets.MobEnabled(GLOBAL.PP_MobCharacters["dragonplayer"], env.modname)
GLOBAL.MOB_TALLBIRD = PlayablePets.MobEnabled(GLOBAL.PP_MobCharacters["tallbirdplayer"], env.modname)

------------------------------------------------------------------
-- Assets
------------------------------------------------------------------

Assets = {
	Asset("ANIM", "anim/ghost_monster_build.zip"), -- Might make it not needed on every prefab.
	--------------------------HOMES--------------------------------
	Asset( "IMAGE", "images/inventoryimages/houseplayer.tex" ),
	Asset( "ATLAS", "images/inventoryimages/houseplayer.xml" ),
	Asset( "IMAGE", "images/inventoryimages/rundownplayer.tex" ),
	Asset( "ATLAS", "images/inventoryimages/rundownplayer.xml" ),
	Asset( "IMAGE", "images/inventoryimages/iglooplayer.tex" ),
	Asset( "ATLAS", "images/inventoryimages/iglooplayer.xml" ),
	Asset( "IMAGE", "images/inventoryimages/spiderhome.tex" ),
	Asset( "ATLAS", "images/inventoryimages/spiderhome.xml" ),
	Asset( "IMAGE", "images/inventoryimages/houndhome.tex" ),
	Asset( "ATLAS", "images/inventoryimages/houndhome.xml" ),
	Asset( "IMAGE", "images/inventoryimages/maggothome.tex" ),
	Asset( "ATLAS", "images/inventoryimages/maggothome.xml" ),
	Asset( "IMAGE", "images/inventoryimages/cathome.tex" ),
	Asset( "ATLAS", "images/inventoryimages/cathome.xml" ),
	Asset("IMAGE", "images/inventoryimages/monkeyhouse.tex"),
	Asset("ATLAS", "images/inventoryimages/monkeyhouse.xml"),
	Asset("IMAGE", "images/inventoryimages/bathome.tex"),
	Asset("ATLAS", "images/inventoryimages/cspiderhome.xml"),
	Asset("IMAGE", "images/inventoryimages/slurtlehome.tex"),
	Asset("ATLAS", "images/inventoryimages/slurtlehome.xml"),
	Asset( "IMAGE", "images/inventoryimages/rabbithouse_player.tex" ),
    Asset( "ATLAS", "images/inventoryimages/rabbithouse_player.xml" ),
	--------------------------TOOLS---------------------------------
	Asset( "IMAGE", "images/inventoryimages/monster_wpn.tex" ),
	Asset( "ATLAS", "images/inventoryimages/monster_wpn.xml" ),
	Asset("IMAGE", "images/inventoryimages/cmonster_wpn.tex"),
	Asset("ATLAS", "images/inventoryimages/cmonster_wpn.xml"),
	-----------------------------------------------------------------
}

------------------------------------------------------------------
-- Custom Recipes
------------------------------------------------------------------

-- Pig house
local pigrecipe = AddRecipe("pighouse_player", {Ingredient("boards", 4),Ingredient("cutstone", 2)}, GLOBAL.RECIPETABS.TOWN, GLOBAL.TECH.SCIENCE_ONE, "pighouse_placer", nil, nil, 1, "pig", "images/inventoryimages/houseplayer.xml", "houseplayer.tex")

-- Merm house
local mermrecipe = AddRecipe("mermhouse_player", {Ingredient("boards", 4),Ingredient("cutstone", 2)}, GLOBAL.RECIPETABS.TOWN, GLOBAL.TECH.SCIENCE_ONE, "pighouse_placer", nil, nil, 1,"merm", "images/inventoryimages/rundownplayer.xml", "rundownplayer.tex" )

-- Igloo
local walrusrecipe = AddRecipe("igloo_player", {Ingredient("boards", 2),Ingredient("cutstone", 6), Ingredient("ice", 20)}, GLOBAL.RECIPETABS.TOWN, GLOBAL.TECH.SCIENCE_ONE, "pighouse_placer", nil, nil, 1,"walrusplayer", "images/inventoryimages/iglooplayer.xml", "iglooplayer.tex" )

-- Pig torch
local pigtorchrecipe = AddRecipe("pigtorch_p", {Ingredient("boards", 4),Ingredient("cutgrass", 6)}, GLOBAL.RECIPETABS.LIGHT, GLOBAL.TECH.SCIENCE_TWO, "pigtorch_p_placer", nil, nil, 1, "pigmanplayer", "images/inventoryimages/houseplayer.xml", "houseplayer.tex")

pigrecipe.sortkey = -7.0
mermrecipe.sortkey = -8.0
walrusrecipe.sortkey = -9.0
pigtorchrecipe.sortkey = -10.0

local MobHouseMethod = PlayablePets.GetModConfigData("MobHouseMethod")

-- Add home crafting recipes if enabled
--TODO update these to AddRecipe2
if MobHouseMethod == SETTING.HOUSE_ON_CRAFT or MobHouseMethod == SETTING.HOUSE_BOTH then
	local spiderholerecipe = AddRecipe("spiderhole_p", {Ingredient("rocks", 10), Ingredient("silk", 8)}, GLOBAL.RECIPETABS.TOWN, GLOBAL.TECH.SCIENCE_ONE, "spiderhole_p_placer", nil, nil, 1, nil, "images/inventoryimages/cspiderhome.xml", "cspiderhome.tex" )
	local batcaverecipe = AddRecipe("batcave_p", {Ingredient("rocks", 8), Ingredient("guano", 3), Ingredient("batwing", 3)}, GLOBAL.RECIPETABS.TOWN, GLOBAL.TECH.SCIENCE_ONE, "batcave_p_placer", nil, nil, 1, nil, "images/inventoryimages/bathome.xml", "bathome.tex" )
	local monkeypodrecipe = AddRecipe("monkeybarrel_p", {Ingredient("boards", 4), Ingredient("cutgrass", 6)}, GLOBAL.RECIPETABS.TOWN, GLOBAL.TECH.SCIENCE_ONE, "monkeyhouse_placer", nil, nil, 1, nil, "images/inventoryimages/monkeyhouse.xml", "monkeyhouse.tex" )
	local slurtlemoundrecipe = AddRecipe("slurtlemound_p", {Ingredient("slurtleslime", 7), Ingredient("slurtle_shellpieces", 5)}, GLOBAL.RECIPETABS.TOWN, GLOBAL.TECH.SCIENCE_ONE, "slurtlemound_p_placer", nil, nil, 1, nil, "images/inventoryimages/slurtlehome.xml", "slurtlehome.tex" )
	local bunnyrecipe = AddRecipe("rabbithouse_player", {Ingredient("boards", 4), Ingredient("carrot", 10),Ingredient("foliage", 10)}, GLOBAL.RECIPETABS.TOWN, GLOBAL.TECH.SCIENCE_TWO, "rabbithouse_placer", nil, nil, 1,"bunnyplayer", "images/inventoryimages/rabbithouse_player.xml", "rabbithouse_player.tex" )
	-- Spider nest
	local spiderhomerecipe = AddRecipe("spidernest_p", {Ingredient("silk", 10), Ingredient("spidergland", 3), Ingredient("papyrus", 2)}, GLOBAL.RECIPETABS.TOWN, GLOBAL.TECH.SCIENCE_ONE, "spidereggsack_placer", nil, nil, 1 ,nil, "images/inventoryimages/spiderhome.xml", "spiderhome.tex" )

	-- Hound mound
	local houndhomerecipe = AddRecipe("houndmound_p", {Ingredient("houndstooth", 5), Ingredient("boneshard", 5), Ingredient("monstermeat", 3)}, GLOBAL.RECIPETABS.TOWN, GLOBAL.TECH.SCIENCE_ONE, "houndmound_p_placer", nil, nil, 1 ,nil, "images/inventoryimages/houndhome.xml", "houndhome.tex" )

	-- Catcoon den
	local cathomerecipe = AddRecipe("catcoonden_p", {Ingredient("log", 8), Ingredient("silk", 5), Ingredient("rope", 3)}, GLOBAL.RECIPETABS.TOWN, GLOBAL.TECH.SCIENCE_ONE, "catcoonden_p_placer", nil, nil, 1 ,nil, "images/inventoryimages/cathome.xml", "cathome.tex" )

	-- Lava pond
	local maggothomerecipe = AddRecipe("lavapond_p", {Ingredient("shovel", 1), Ingredient("charcoal", 10), Ingredient("rocks", 10)}, GLOBAL.RECIPETABS.TOWN, GLOBAL.TECH.SCIENCE_TWO, "lavapond_p_placer", nil, nil, 1 ,nil, "images/inventoryimages/maggothome.xml", "maggothome.tex" )

	--Cute Recipes
	local rabbitholerecipe = AddRecipe("rabbithole_p", {Ingredient("shovel", 1),Ingredient("carrot", 2), Ingredient("cutgrass", 6)}, GLOBAL.RECIPETABS.TOWN, GLOBAL.TECH.SCIENCE_ONE, "rabbithole_p_placer", nil, nil, 1, nil, "images/inventoryimages/rabbithome.xml", "rabbithome.tex")
	local molerecipe = AddRecipe("molehole_p", {Ingredient("shovel", 1),Ingredient("flint", 2), Ingredient("rocks", 6)}, GLOBAL.RECIPETABS.TOWN, GLOBAL.TECH.SCIENCE_ONE, "molehole_p_placer", nil, nil, 1, nil, "images/inventoryimages/molehome.xml", "molehome.tex")
	local beerecipe = AddRecipe("beehive_p", {Ingredient("honeycomb", 1),Ingredient("honey", 3), Ingredient("cutgrass", 6)}, GLOBAL.RECIPETABS.TOWN, GLOBAL.TECH.SCIENCE_ONE, "beehive_p_placer", nil, nil, 1, nil, "images/inventoryimages/beehome.xml", "beehome.tex")
end

------------------------------------------------------------------
-- Component Overrides
------------------------------------------------------------------

-- Prevent mob spiders from attacking player spiders
local comb_rep = require("components/combat_replica")
local old_IsAlly = comb_rep.IsAlly
function comb_rep:IsAlly(guy,...)
	if self.inst:HasTag("spiderwhisperer") and not self.inst.prefab == "webber" and guy:HasTag("spider") and not guy:HasTag("player") then
		return true
	end
	return old_IsAlly(self,guy,...)
end

------------------------------------------------------------------
-- PostInits
------------------------------------------------------------------
local function FriendlyWaspInit(prefab)
	local function OnNearBy(prefab, target)
	if prefab.components.childspawner ~= nil and not target:HasTag("bee") then
        prefab.components.childspawner:ReleaseAllChildren(target, "killerbee")
    end

	end
	
	if prefab.components.playerprox then
	prefab.components.playerprox:SetOnPlayerNear(OnNearBy)
	end
end

AddPrefabPostInit("wasphive", FriendlyWaspInit)--function(inst)

-- Allows tallbird players to hatch their eggs.
AddComponentPostInit("hatchable", function (hatchable, inst)
	hatchable.oldupdatefn = hatchable.OnUpdate
	local FindEntity = GLOBAL.FindEntity
	local function IsMother(guy)
		return guy:HasTag("tallbird") and guy:HasTag("mommy")
	end
	
	function hatchable:OnUpdate(dt)
		if self.delay then
			return
		end
		
		local has_heater = FindEntity(self.inst, 1.5, IsMother) ~= nil or FindEntity(self.inst, TUNING.HATCH_CAMPFIRE_RADIUS, IsExothermic, { "campfire", "fire"}) ~= nil
		local wants_heater = self:GetHeaterPref()
		--local has_heater = FindEntity(self.inst, TUNING.HATCH_CAMPFIRE_RADIUS, IsMother) ~= nil

		self.toohot = false
		self.toocold = false

		if wants_heater ~= nil then
			if has_heater and not wants_heater then
				self.toohot = true
			elseif not has_heater and wants_heater then
				self.toocold = true
			end
		end

		if self.state == "unhatched" then
			if has_heater then
				self.progress = self.progress + dt
				if self.progress >= self.cracktime then
					self.progress = 0
					self:OnState("crack")
				end
			else
				self.progress = 0
			end
			return
		end

		local oldstate = self.state

		if self.toohot or self.toocold then
			self:OnState("uncomfy")
		else
			self:OnState("comfy")
		end

		if self.state == "comfy" then
			self.discomfort = math.max(self.discomfort - dt, 0)
			if self.discomfort <= 0 then
				self.progress = self.progress + dt
			end

			if self.progress >= self.hatchtime then
				self:StopUpdating()
				self:OnState("hatch")
			end
		else
			self.discomfort = self.discomfort + dt
			if self.discomfort >= self.hatchfailtime then
				self:StopUpdating()
				self:OnState("dead")
			end
		end
	end
 
end)

_G = GLOBAL
local params={} 
params.monkeybarrel_p =
{
    widget =
    {
        slotpos = {},
        --animbank = "ui_chester_shadow_3x4",
        --animbuild = "ui_chester_shadow_3x4",
        pos = _G.Vector3(350, 350, 0),
        side_align_tip = 80, --160
    },
    type = "chest",
}
-- y and x make the slots. 0 counts as a row.
for y = 0, 5 do --6.5, -0.5, -1
    for x = 0, 5 do
        table.insert(params.monkeybarrel_p.widget.slotpos, _G.Vector3(75 * x - 75 * 12 + 75, 75 * y - 75 * 6 + 75, 0))
    end
end

local containers = _G.require "containers"
containers.MAXITEMSLOTS = math.max(containers.MAXITEMSLOTS, params.monkeybarrel_p.widget.slotpos ~= nil and #params.monkeybarrel_p.widget.slotpos or 0)
local old_widgetsetup = containers.widgetsetup
function containers.widgetsetup(container, prefab, data)
        local pref = prefab or container.inst.prefab
        if pref == "monkeybarrel_p" then
                local t = params[pref]
                if t ~= nil then
                        for k, v in pairs(t) do
                                container[k] = v
                        end
                        container:SetNumSlots(container.widget.slotpos ~= nil and #container.widget.slotpos or 0)
                end
        else
                return old_widgetsetup(container, prefab)
    end
end

-------------------------------------------------------
--Wardrobe stuff--

--Skin Puppet Stuff
local MobPuppets = require("pp_puppets")
local MobSkins = require("pp_skins")
PlayablePets.RegisterPuppetsAndSkins(PP_Character_Order, MobPuppets, MobSkins)
------------------------------------------------------------------
-- Commands
------------------------------------------------------------------

------------------------------------------------------------------
-- Asset Population
------------------------------------------------------------------

local assetPaths = { "bigportraits/", "images/map_icons/", "images/avatars/avatar_", "images/avatars/avatar_ghost_" }
local assetTypes = { {"IMAGE", "tex"}, {"ATLAS", "xml"} }

-- Iterate through the player mob table and do the following:
-- 1. Populate the PrefabFiles table with the mob prefab names and their skin prefabs (if applicable)
-- 2. Add an atlas and image for the mob's following assets:
-- 2.1 Character select screen portraits
-- 2.2 Character map icons
-- 2.3 ??? FIXME
-- 2.4 ??? FIXME
--for prefab, mob in pairs(GLOBAL.PP_MobCharacters) do
for _, prefab in ipairs(PP_Character_Order) do
	local mob = GLOBAL.PP_MobCharacters[prefab]
	if PlayablePets.MobEnabled(mob, env.modname) then
		table.insert(PrefabFiles, prefab)
	end
	
	-- Add custom skin prefabs, if available
	-- Example: "dragonplayer_formal"
	
	for _, path in ipairs(assetPaths) do
		for _, assetType in ipairs(assetTypes) do
			--print("Adding asset: "..assetType[1], path..prefab.."."..assetType[2])
			table.insert( Assets, Asset( assetType[1], path..prefab.."."..assetType[2] ) )
		end
	end
end
------------------------------------------------------------------
-- Mob Character Instantiation
------------------------------------------------------------------

-- Adds a mod character based on an individual mob
-- prefab is the prefab name (e.g. clockwork1player)
-- mob.fancyname is the mob's ingame name (e.g. Knight)
-- mob.gender is fairly self-explanatory
--for prefab, mob in pairs(GLOBAL.PP_MobCharacters) do
for _, prefab in ipairs(PP_Character_Order) do
	local mob = GLOBAL.PP_MobCharacters[prefab]
	PlayablePets.SetGlobalData(prefab, mob)
	if PlayablePets.MobEnabled(mob, env.modname) then
		AddMinimapAtlas("images/map_icons/"..prefab..".xml")
		AddModCharacter(prefab, mob.gender)
	end
end