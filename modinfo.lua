-- This information tells other players more about the mod
name = "Playable Pets"
author = "Leonardo Coxington"
version = "3.52.6"
description = "Play as Pets/Monsters! \nPress the z,h,j,k,l keys when playing as mobs, they might do something special! \n\nNOTE: You will need Playable Pets Essentials enabled to be able to run this mod. \nVersion:"..version
-- This is the URL name of the mod's thread on the forum; the part after the ? and before the first & in the url
forumthread = "/topic/73911-playable-pets/"

folder_name = folder_name or "workshop-"
if not folder_name:find("workshop-") then
    name = name.." Dev."
end

-- This lets other players know if your mod is out of date, update it to match the current version in the game
api_version = 10

dst_compatible = true
dont_starve_compatible = false
reign_of_giants_compatible = false
all_clients_require_mod = true
forge_compatible = true

icon_atlas = "modicon.xml"
icon = "modicon.tex"

priority = -308

server_filter_tags = {
"mob", "mobs", "playable", "monsters", "PP", "playable pets", "original"
}

---------------------------------
--            DATA             --
---------------------------------

-- For initializing configuration settings to disable each mob
local availableMobs = {
	"Batilisk",
	"Spitter",
	"Hider",
	"Depth Worm",
	"Slurper",
	"Slurtle",
	"Hutch",
	"Splumonkey", 
	"Bunnyman",
	"Rock Lobster",
	"Crawling Horror",
	"Terrorbeak",
	"Guardian",
	"Rabbit",
	"Butterfly",
	"Moleworm",
	"Bee",
	"Crow",
	"Robin",
	"Buzzard",
	"Birchnutter",
	"Mosquito",
	"Gobbler",
	"Chester",
	"Mandrake",
	"Glommer",
	"Frog",
	"Pengull",
	"Grass Gekko",
	"Koalefant",
	"Hound", 
	"Ice Hound", 
	"Hell Hound",
	"Spider", 
	"Spider Warrior", 
	"Depth Dweller", 
	"Spider Queen",  
	"Tree Guard", 
	"Varg", 
	"Mosling", 
	"Moose/Goose", 
	"Lavae", 
	"Dragonfly", 
	"Bearger", 
	"Deerclops", 
	"Ghost", 
	"Pig", 
	"Pig Guard",
	"Merm", 
	"McTusk", 
	"Tentacle", 
	"Krampus", 
	"Catcoon", 
	"Beefalo", 
	"Smallbird", 
	"Tallbird", 
	"Volt Goat", 
	"Knight", 
	"Bishop", 
	"Rook",
	"Bernie", 
	--"Maxwell's Shadow", 
	"Abigail", 
	"Balloon", 
	"Pipspook",
	"Party Balloon",
	"Speed Balloon",
}

---------------------------------
--       CONFIG OPTIONS        --
---------------------------------

configuration_options = {
	{
		name = "Big Foot",
		label = "Big Foot",
		options = 
		{
			{description = "Enable", data = "Enable"},
			{description = "Disable",  data = "Disable"},
			--{description = "Locked", data = SETTING.LOCKED, hover = "Coming Soon?"},
		},
		default = "Disable"
	},
}

---------------------------------
-- TABLE POPULATION CODE BELOW --
---------------------------------

-- Automatically populate enable/disable configuration settings for mobs
local settingEnable = {
	{description = "Enabled", data = "Enable"},
	{description = "Disabled", data = "Disable"},
}

for i = 1, #availableMobs do	
	local configOption = {}
	configOption.name = availableMobs[i]
	configOption.label = availableMobs[i]
	configOption.options = settingEnable
	configOption.default = "Enable"
	
	configuration_options[#configuration_options + 1] = configOption
end