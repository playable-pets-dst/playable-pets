local hound_dark_skin_data = {0.3, 0.3, 0.6, 1}
local houndred_other_skin_data = {0.8, 0, 0, 1}
local houndice_nrm_skin_data = 0.65
local SKINS = {
	--[[
	prefab = {
		skinname = {
			name = "", --this is for the purpose of printing
			fname = "Name of Skin",
			build = "build name",
			teen_build = "",
			adult_build = "adult build name",
			etc_build = "blah blah build", --add as many as these as you need
			fn = functionhere,
			owners = {}, --userids go here
			locked = true, --this skin can't be used.
		},
	},
	]]
	houndplayer = {
		dark = {
			name = "dark",
			fname = "Dark Hound",
			build = "hound_ocean",
			symbol_data = {
				{
					symbol = "hound_lowerjaw",
					colour = hound_dark_skin_data,
				},
				{
					symbol = "hound_upperjaw",
					colour = hound_dark_skin_data,
				},
				{
					symbol = "hound_body",
					colour = hound_dark_skin_data,
				},
				{
					symbol = "hound_leg",
					colour = hound_dark_skin_data,
				},
				{
					symbol = "hound_ear",
					colour = hound_dark_skin_data,
				},
				{
					symbol = "hound_tail",
					colour = hound_dark_skin_data,
				},
				{
					symbol = "hound_maw",
					hue = 0.675,
				},
				{
					symbol = "hound_nose",
					hue = 0.7,
				},
				{
					symbol = "hound_eye",
					colour = {1, 0, 0, 1},
					lightoverride = 1,
				},
			}
		},
		rgb = {
			name = "rgb",
			fname = "RGB Hound",
			build = "hound_red_ocean",
			on_equip = function(inst)
				PlayablePets.MakeRainbow(inst)
			end,
			on_remove = function(inst)
				PlayablePets.MakeRainbow(inst, true)
			end,
			locked = true,
		},
	},
	houndiceplayer = {
		other = {
			name = "other",
			fname = "Other Ice Hound",
			build = "hound_red_ocean",
			symbol_data = {
				{
					symbol = "hound_lowerjaw",
					hue = houndice_nrm_skin_data,
				},
				{
					symbol = "hound_upperjaw",
					hue = houndice_nrm_skin_data,
				},
				{
					symbol = "hound_body",
					hue = houndice_nrm_skin_data,
				},
				{
					symbol = "hound_leg",
					hue = houndice_nrm_skin_data,
				},
				{
					symbol = "hound_ear",
					hue = houndice_nrm_skin_data,
				},
				{
					symbol = "hound_tail",
					hue = houndice_nrm_skin_data,
				},
				{
					symbol = "hound_nose",
					hue = houndice_nrm_skin_data,
				},
			}
		},
	},
	deerplayer = {
		hf = {
			name = "hf",
			fname = "Deerclops Skeleton",
			build = "deerclops_skeleton_build",
		},
		demon = {
			name = "demon",
			fname = "Demonclops",
			build = "deerclops_shiny_build_02",
			locked = true
		},
		dark = {
			name = "dark",
			fname = "Dark Deerclops",
			build = "deerclops_shiny_build_03",
			hue = 0.5
		},
		odd = {
			name = "odd",
			fname = "Odd Deerclops",
			build = "deerclops_shiny_build_01",
		},
	},
	catplayer = {
		brown = {
			name = "brown",
			fname = "Brown Catcoon",
			build = "catcoon_shiny_build_01",
		},
		black = {
			name = "black",
			fname = "Black Catcoon",
			build = "catcoon_shiny_build_03",
		},
	},
	dragonplayer = {
		red = {
			name = "red",
			fname = "Red Dragonfly",
			build = "dragonfly_build",
			rage_build = "dragonfly_fire_build",
			hue = 0.7,
		},
		blue = {
			name = "blue",
			fname = "Blue Dragonfly",
			build = "dragonfly_build",
			rage_build = "dragonfly_fire_build",
			hue = 0.4,
			sat = 1.5,
		},
		pink = {
			name = "pink",
			fname = "Pink Dragonfly",
			build = "dragonfly_shiny_build_02",
			rage_build = "dragonfly_fire_shiny_build_02",
		},
		rgb = {
			name = "rgb",
			fname = "RGB Dragon Fly",
			build = "dragonfly_build",
			rage_build = "dragonfly_fire_build",
			on_equip = function(inst)
				PlayablePets.MakeRainbow(inst)
			end,
			on_remove = function(inst)
				PlayablePets.MakeRainbow(inst, true)
			end,
			locked = true,
		},
	},
	bearplayer = {
		brown = {
			name = "brown",
			fname = "Brown Bearger",
			build = "bearger_shiny_build_01",
		},
		odd = {
			name = "odd",
			fname = "Odd Bearger",
			build = "bearger_shiny_build_02",
		},
		catcoon = {
			name = "catcoon",
			fname = "Catcoon Bearger",
			build = "bearger_shiny_build_06",
		},
	},
	krampusp = {
		blue = {
			name = "blue",
			fname = "Blue Krampus",
			build = "krampus_build",
			hue = 0.62
		},
		green = {
			name = "green",
			fname = "Green Krampus",
			build = "krampus_build",
			hue = 0.3,
		},
		purple = {
			name = "purple",
			fname = "Purple Krampus",
			build = "krampus_build",
			hue = 0.7,
		},
		teal = {
			name = "teal",
			fname = "Teal Krampus",
			build = "krampus_build",
			hue = 0.55
		},
		dark = {
			name = "dark",
			fname = "Dark Krampus",
			build = "krampus_build",
			sat = 0,
			symbol_data = {
				{
					symbol = "krampus_bag",
					colour = {0.3, 0.3, 0.3, 1}
				},
			}
		},
		rgb = {
			name = "rgb",
			fname = "RGB Krampus",
			build = "hound_krampus_ocean",
			on_equip = function(inst)
				PlayablePets.MakeRainbow(inst)
			end,
			on_remove = function(inst)
				PlayablePets.MakeRainbow(inst, true)
			end,
			locked = true,
		},
	},
	ghostplayer = {
		purple = {
			name = "purple",
			fname = "Purple Ghost",
			build = "ghost_build",
			symbol_data = {
				{
					symbol = "ghost_tail",
					colour = {102/255, 52/255, 227/255, 1}
				},
				{
					symbol = "ghost_body",
					colour = {102/255, 52/255, 227/255, 1}
				},
				{
					symbol = "ghost_FX",
					colour = {102/255, 52/255, 227/255, 1}
				},
				{
					symbol = "ghost_eyes",
					colour = {245/255, 220/255, 34/255, 1},
					lightoverride = 1,
				},
			}
		},
		green = {
			name = "green",
			fname = "Green Ghost",
			build = "ghost_build",
			symbol_data = {
				{
					symbol = "ghost_tail",
					colour = {126/255, 237/255, 159/255, 1}
				},
				{
					symbol = "ghost_body",
					colour = {126/255, 237/255, 159/255, 1}
				},
				{
					symbol = "ghost_FX",
					colour = {126/255, 237/255, 159/255, 1}
				},
			}
		},
		dark = {
			name = "dark",
			fname = "Dark Ghost",
			build = "ghost_build",
			symbol_data = {
				{
					symbol = "ghost_tail",
					colour = {0, 0, 0, 1}
				},
				{
					symbol = "ghost_body",
					colour = {0, 0, 0, 1}
				},
				{
					symbol = "ghost_FX",
					colour = {0, 0, 0, 1}
				},
				{
					symbol = "ghost_eyes",
					colour = {1, 0, 0, 1},
					lightoverride = 1,
				},
			}
		},
	},
	moosegooseplayer = {
		odd = {
			name = "odd",
			fname = "Odd MooseGoose",
			build = "goosemoose_shiny_build_01",
		},
		dark = {
			name = "dark",
			fname = "Dark MooseGoose",
			build = "goosemoose_shiny_build_03",
		},
	},
	clockwork1player = {
		nm = {
			name = "nm",
			fname = "Nightmare Knight",
			build = "knight_nightmare",
			kind = "_nightmare",
		},
		odd = {
			name = "odd",
			fname = "Odd Knight",
			build = "knight_shiny_build_01",
			kind = "",
		},
		odd2 = {
			name = "odd2",
			fname = "Odd Nightmare Knight",
			build = "knight_nightmare_shiny_build_01",
			kind = "_nightmare",
		},
		blue = {
			name = "blue",
			fname = "Blue Knight",
			kind = "",
			build = "knight_build",
			hue = 0.5,
		},
		green = {
			name = "green",
			fname = "Green Knight",
			kind = "",
			build = "knight_build",
			hue = 0.5,
		},
		rgb = {
			name = "rgb",
			fname = "RGB Knight",
			build = "knight_build",
			on_equip = function(inst)
				PlayablePets.MakeRainbow(inst)
			end,
			on_remove = function(inst)
				PlayablePets.MakeRainbow(inst, true)
			end,
			locked = true,
		},
	},
	clockwork2player = {
		nm = {
			name = "nm",
			fname = "Nightmare Bishop",
			build = "bishop_nightmare",
			kind = "_nightmare",
		},
		odd = {
			name = "odd",
			fname = "Odd Bishop",
			build = "bishop_shiny_build_01",
			kind = "",
		},
		odd2 = {
			name = "odd2",
			fname = "Odd Nightmare Bishop",
			build = "bishop_nightmare_shiny_build_01",
			kind = "_nightmare",
		},
		blue = {
			name = "blue",
			fname = "Blue Bishop",
			kind = "",
			build = "bishop_build",
			hue = 0.5,
		},
		green = {
			name = "green",
			fname = "Green Bishop",
			kind = "",
			build = "bishop_build",
			hue = 0.2,
		},
		rgb = {
			name = "rgb",
			fname = "RGB Bishop",
			build = "bishop_build",
			on_equip = function(inst)
				PlayablePets.MakeRainbow(inst)
			end,
			on_remove = function(inst)
				PlayablePets.MakeRainbow(inst, true)
			end,
			locked = true,
		},
	},
	clockwork3player = {
		nm = {
			name = "nm",
			fname = "Nightmare Rook",
			build = "rook_nightmare",
			kind = "_nightmare",
		},
		odd = {
			name = "odd",
			fname = "Odd Rook",
			kind = "",
			build = "rook_shiny_build_01",
		},
		odd2 = {
			name = "odd2",
			fname = "Odd Nightmare Rook",
			build = "rook_nightmare_shiny_build_01",
			kind = "_nightmare",
		},
		green = {
			name = "green",
			fname = "Green Rook",
			kind = "",
			build = "rook_build",
			hue = 0.5,
		},
		blue = {
			name = "blue",
			fname = "Blue Rook",
			kind = "",
			build = "rook_build",
			hue = 0.6,
		},
		rgb = {
			name = "rgb",
			fname = "RGB Rook",
			build = "rook_build",
			on_equip = function(inst)
				PlayablePets.MakeRainbow(inst)
			end,
			on_remove = function(inst)
				PlayablePets.MakeRainbow(inst, true)
			end,
			locked = true,
		},
	},
	tentaclep = {
		root = {
			name = "Root",
			fname = "Root Tentacle",
			kind = "",
			build = "quagmire_tentacle_root",
		},
		rgb = {
			name = "rgb",
			fname = "RGB Tentacle",
			build = "tentacle",
			on_equip = function(inst)
				PlayablePets.MakeRainbow(inst)
			end,
			on_remove = function(inst)
				PlayablePets.MakeRainbow(inst, true)
			end,
			locked = true,
		},			
	},
	elephantplayer = {
		cat = {
			name = "cat",
			fname = "Cat Koalefant",
			build = "koalefant_cat",
			build2 = "koalefant_cat",
		},
		dino = {
			name = "dino",
			fname = "Dinosaur Koalefant",
			build = "koalefant_dinosaur",
		},
		goldarmor = {
			name = "goldarmor",
			fname = "Gold Hauberk Koalefant",
			build = "koalefant_goldhauberk",
		},
		armor = {
			name = "armor",
			fname = "Hauberk Koalefant",
			build = "koalefant_hauberk",
		},
		moth = {
			name = "moth",
			fname = "Moth Koalefant",
			build = "koalefant_moth",
		},
		tapir = {
			name = "tapir",
			fname = "Tapir Koalefant",
			build = "koalefant_tapir",
		},
		rgb = {
			name = "rgb",
			fname = "RGB Koalefant",
			build = "koalefant_summer_build",
			on_equip = function(inst)
				PlayablePets.MakeRainbow(inst)
			end,
			on_remove = function(inst)
				PlayablePets.MakeRainbow(inst, true)
			end,
			locked = true,
		},
	},
	frogp = {
		odd = {
			name = "odd",
			fname = "Odd Frog",
			build = "frog_shiny_build_01",
		},
		rgb = {
			name = "rgb",
			fname = "RGB Frog",
			build = "frog",
			on_equip = function(inst)
				PlayablePets.MakeRainbow(inst)
			end,
			on_remove = function(inst)
				PlayablePets.MakeRainbow(inst, true)
			end,
			locked = true,
		},
	},
	glommerp = {
		odd = {
			name = "odd",
			fname = "Odd Glommer",
			build = "glommer",
			hue = 0.3, 
		},
		yellow = {
			name = "yellow",
			fname = "Yellow Glommer",
			build = "glommer",
			hue = 0.5, 
		},
		green = {
			name = "green",
			fname = "Green Glommer",
			build = "glommer",
			hue = 0.8, 
		},
		rgb = {
			name = "rgb",
			fname = "RGB Frog",
			build = "glommer",
			on_equip = function(inst)
				PlayablePets.MakeRainbow(inst)
			end,
			on_remove = function(inst)
				PlayablePets.MakeRainbow(inst, true)
			end,
			locked = true,
		},
	},
	bunnyp = {
		enforcer = {
			name = "enforcer",
			fname = "Enforcer Rabbit",
			build = "manrabbit_enforcer_build",
			on_equip = function(inst)
				inst.AnimState:OverrideSymbol("armblur", "manrabbit_beard_build", "armblur")
			end,
			on_remove = function(inst)
				inst.AnimState:OverrideSymbol("armblur", "manrabbit_build", "armblur")
			end,
		},
		rgb = {
			name = "rgb",
			fname = "RGB Rabbit",
			build = "rabbit_build",
			on_equip = function(inst)
				PlayablePets.MakeRainbow(inst)
			end,
			on_remove = function(inst)
				PlayablePets.MakeRainbow(inst, true)
			end,
			locked = true,
		},
	},
	rabbitp = {
		odd = {
			name = "odd",
			fname = "Odd Rabbit",
			build = "rabbit_shiny_build_01",
		},
		rf = {
			name = "rf",
			fname = "Reforged Rabbit",
			build = "rabbit_shiny_build_08",
		},
		rgb = {
			name = "rgb",
			fname = "RGB Rabbit",
			build = "rabbit_build",
			on_equip = function(inst)
				PlayablePets.MakeRainbow(inst)
			end,
			on_remove = function(inst)
				PlayablePets.MakeRainbow(inst, true)
			end,
			locked = true,
		},
	},
	slurperp = {
		odd = {
			name = "odd",
			fname = "Odd Slurper",
			build = "slurper_shiny_build_01",
		},
	},
	vargplayer = {
		polar = {
			name = "polar",
			fname = "Polar Varg",
			build = "warg_polar",
		},
	},
}

return SKINS