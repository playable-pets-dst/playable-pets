
------------------------------------------------------------------
-- Recipe strings
------------------------------------------------------------------

STRINGS.RECIPE_DESC.PIGHOUSE_PLAYER = "A house for best pigs"
STRINGS.RECIPE_DESC.MERMHOUSE_PLAYER = "A house for the fancy swamp dwellers"
STRINGS.RECIPE_DESC.IGLOO_PLAYER = "A COOL home for a COOL dude"
STRINGS.RECIPE_DESC.SPIDERNEST_P = "A nest for your spider buddies"
STRINGS.RECIPE_DESC.HOUNDMOUND_P = "Pile of bones for your best friend"
STRINGS.RECIPE_DESC.CATCOONDEN_P = "Help keep your kitty out of the rain"
STRINGS.RECIPE_DESC.LAVAPOND_P = "A hot pit of lava for your cute bubble of lava"
--STRINGS.RECIPE_DESC.GRAVE_P = "Finally you can rest in peace"
STRINGS.RECIPE_DESC.PIGTORCH_P = "Show off your superior pig craftsmanship"

------------------------------------------------------------------
-- Character select screen strings
------------------------------------------------------------------
STRINGS.CHARACTER_TITLES.berniep = "Bernie"
STRINGS.CHARACTER_NAMES.berniep = "BERNIE"
STRINGS.CHARACTER_DESCRIPTIONS.berniep = "*Is Bernie\n*Can do it\n*Believe in him"
STRINGS.CHARACTER_QUOTES.berniep = "Help me Bernie!"

STRINGS.CHARACTER_TITLES.abbyp = "Abigail"
STRINGS.CHARACTER_NAMES.abbyp = "ABIGAIL"
STRINGS.CHARACTER_DESCRIPTIONS.abbyp = "*Is Abigail\n*Can't do it\n*Won't stop holding F"
STRINGS.CHARACTER_QUOTES.abbyp = "Noob Abby."

STRINGS.CHARACTER_TITLES.smallghostp = STRINGS.NAMES.SMALLGHOST
STRINGS.CHARACTER_NAMES.smallghostp = STRINGS.NAMES.SMALLGHOST
STRINGS.CHARACTER_DESCRIPTIONS.smallghostp = "*Is a ghost\n*Can't attack\n*Immune to most things"
STRINGS.CHARACTER_QUOTES.smallghostp = "It can't find its lost toys."

STRINGS.CHARACTER_TITLES.maxpuppetp = "Maxwell's Minion"
STRINGS.CHARACTER_NAMES.maxpuppetp = "Maxwell's Minion"
STRINGS.CHARACTER_DESCRIPTIONS.maxpuppetp = "*Is a shadow\n*Is pretty much just a human\n*Immune to most things"
STRINGS.CHARACTER_QUOTES.maxpuppetp = "Perfect for slave labor."

STRINGS.CHARACTER_TITLES.balloonp = "Balloon"
STRINGS.CHARACTER_NAMES.balloonp = "BALLOON"
STRINGS.CHARACTER_DESCRIPTIONS.balloonp = "*1 HP\n*Does damage when popped\n*#1 Most requested thing"
STRINGS.CHARACTER_QUOTES.balloonp = "Don't let your dreams be memes."

STRINGS.CHARACTER_TITLES.balloon_partyp = "Balloon"
STRINGS.CHARACTER_NAMES.balloon_partyp = "BALLOON"
STRINGS.CHARACTER_DESCRIPTIONS.balloon_partyp = "*1 HP\n*Does damage when popped\n*Leaves confetti behind"
STRINGS.CHARACTER_QUOTES.balloon_partyp = "Why?"

STRINGS.CHARACTER_TITLES.balloon_speedp = "Balloon"
STRINGS.CHARACTER_NAMES.balloon_speedp = "BALLOON"
STRINGS.CHARACTER_DESCRIPTIONS.balloon_speedp = "*1 HP\n*Does damage when popped\n*Is pretty fast"
STRINGS.CHARACTER_QUOTES.balloon_speedp = "No really, Why?"

STRINGS.CHARACTER_TITLES.bigfootp = "Big Foot"
STRINGS.CHARACTER_NAMES.bigfootp = "Big Foot"
STRINGS.CHARACTER_DESCRIPTIONS.bigfootp = "*Is HUGE\n*Causes destruction with every step."
STRINGS.CHARACTER_QUOTES.bigfootp = "haha get it?"
STRINGS.CHARACTER_SURVIVABILITY.bigfootp = "Very Good"

STRINGS.CHARACTER_TITLES.houndplayer = "Hound"
STRINGS.CHARACTER_NAMES.houndplayer = "HOUND"
STRINGS.CHARACTER_DESCRIPTIONS.houndplayer = "*Is a monster\n*Has more health than other hounds\n*Weaker bite"
STRINGS.CHARACTER_QUOTES.houndplayer = "Survivors' worst friend"
STRINGS.CHARACTER_SURVIVABILITY.houndplayer = "Slim"

STRINGS.CHARACTER_TITLES.houndiceplayer = "Ice Hound"
STRINGS.CHARACTER_NAMES.houndiceplayer = "ICE HOUND"
STRINGS.CHARACTER_DESCRIPTIONS.houndiceplayer = "*Is a monster\n*Loves the cold\n* Frosty bite"
STRINGS.CHARACTER_QUOTES.houndiceplayer = "Chilly dog"
STRINGS.CHARACTER_SURVIVABILITY.houndiceplayer = "Slim"

STRINGS.CHARACTER_TITLES.houndredplayer = "Hell Hound"
STRINGS.CHARACTER_NAMES.houndredplayer = "FIRE HOUND"
STRINGS.CHARACTER_DESCRIPTIONS.houndredplayer = "*Is a monster\n*Can take the heat\n*Has nightvision"
STRINGS.CHARACTER_QUOTES.houndredplayer = "Hot Dog"
STRINGS.CHARACTER_SURVIVABILITY.houndredplayer = "Slim"

STRINGS.CHARACTER_TITLES.hounddarkplayer = "Nightmare Hound"
STRINGS.CHARACTER_NAMES.hounddarkplayer = "DARK HOUND"
STRINGS.CHARACTER_DESCRIPTIONS.hounddarkplayer = "*Is a test monster\n*Has sanity functions\n*Sleep to keep your sanity up!"
STRINGS.CHARACTER_QUOTES.hounddarkplayer = ""

STRINGS.CHARACTER_TITLES.whitespiderplayer = "Depth Dweller"
STRINGS.CHARACTER_NAMES.whitespiderplayer = "DEPTH DWELLER"
STRINGS.CHARACTER_DESCRIPTIONS.whitespiderplayer = "*Can lunge at enemies \n* Special nightvision \n*Kind of creepy"
STRINGS.CHARACTER_QUOTES.whitespiderplayer = "*stare*"

STRINGS.CHARACTER_TITLES.blackspiderplayer = "Spider"
STRINGS.CHARACTER_NAMES.blackspiderplayer = "SPIDER"
STRINGS.CHARACTER_DESCRIPTIONS.blackspiderplayer = "*Can make silk\n*Very adorable\n*Not very strong..."
STRINGS.CHARACTER_QUOTES.blackspiderplayer = "The itsy bitsy spider"

STRINGS.CHARACTER_TITLES.warriorp = "Spider Warrior"
STRINGS.CHARACTER_NAMES.warriorp = "SPIDER WARRIOR"
STRINGS.CHARACTER_DESCRIPTIONS.warriorp = "*Can make spider glands\n*Better health regen\n* Can lunge at enemies"
STRINGS.CHARACTER_QUOTES.warriorp = "RAH"

STRINGS.CHARACTER_TITLES.queenspiderplayer = "Spider Queen"
STRINGS.CHARACTER_NAMES.queenspiderplayer = "SPIDERQUEEN"
STRINGS.CHARACTER_DESCRIPTIONS.queenspiderplayer = "*Is queen of spiders\n*Can spawn minions\n*Minions aren't really friendly..."
STRINGS.CHARACTER_QUOTES.queenspiderplayer = "PEASANTS"

STRINGS.CHARACTER_TITLES.wqueenspiderplayer = "Dweller Queen"
STRINGS.CHARACTER_NAMES.wqueenspiderplayer = "DWELLER QUEEN"
STRINGS.CHARACTER_DESCRIPTIONS.wqueenspiderplayer = "*Can make glands\n*Can spawn dwellers\n* Special nightvision"
STRINGS.CHARACTER_QUOTES.wqueenspiderplayer = "DIRTY SURFACE PEASANTS"

STRINGS.CHARACTER_TITLES.moosegooseplayer = "Moose/Goose"
STRINGS.CHARACTER_NAMES.moosegooseplayer = "MOOSEGOOSE"
STRINGS.CHARACTER_DESCRIPTIONS.moosegooseplayer = "*Is a Giant\n*Can lay eggs\n* Sheds feathers"
STRINGS.CHARACTER_QUOTES.moosegooseplayer = "*Perpetual honking*"

STRINGS.CHARACTER_TITLES.babygooseplayer = "Mosling"
STRINGS.CHARACTER_NAMES.babygooseplayer = "Mosling"
STRINGS.CHARACTER_DESCRIPTIONS.babygooseplayer = "*Is very adorable\n*Has a temper \n* Gets hungry..."
STRINGS.CHARACTER_QUOTES.babygooseplayer = "*Perpetual meeping*"

STRINGS.CHARACTER_TITLES.dragonplayer = "Dragonfly"
STRINGS.CHARACTER_NAMES.dragonplayer = "DRAGONFLY"
STRINGS.CHARACTER_DESCRIPTIONS.dragonplayer = "*Is a Giant\n*Short temper.\n*Gets very tired at night"
STRINGS.CHARACTER_QUOTES.dragonplayer = "*Heavy Buzzing*"

STRINGS.CHARACTER_TITLES.maggotplayer = "Lavae"
STRINGS.CHARACTER_NAMES.maggotplayer = "MAGGOT"
STRINGS.CHARACTER_DESCRIPTIONS.maggotplayer = "*Covered in warm lava! \n*Can grow\n* Not strong at all..."
STRINGS.CHARACTER_QUOTES.maggotplayer = "MAGGOTS"

STRINGS.CHARACTER_TITLES.bearplayer = "Bearger"
STRINGS.CHARACTER_NAMES.bearplayer = "BEAR"
STRINGS.CHARACTER_DESCRIPTIONS.bearplayer = "*Is a Giant\n*Can pound the ground \n*Sheds fur"
STRINGS.CHARACTER_QUOTES.bearplayer = "Unbearably strong"

STRINGS.CHARACTER_TITLES.deerplayer = "Deerclops"
STRINGS.CHARACTER_NAMES.deerplayer = "DEERCLOPS"
STRINGS.CHARACTER_DESCRIPTIONS.deerplayer = "*Is a Giant\n*Destructive \n*Scary..."
STRINGS.CHARACTER_QUOTES.deerplayer = ""

STRINGS.CHARACTER_TITLES.catplayer = "Catcoon"
STRINGS.CHARACTER_NAMES.catplayer = "CATCOON"
STRINGS.CHARACTER_DESCRIPTIONS.catplayer = "*Great at hunting\n*Can harvest \n*Is adorable"
STRINGS.CHARACTER_QUOTES.catplayer = "Meow"

STRINGS.CHARACTER_TITLES.ghostplayer = "Ghost"
STRINGS.CHARACTER_NAMES.ghostplayer = "GHOST"
STRINGS.CHARACTER_DESCRIPTIONS.ghostplayer = "*Can craft \n*Emits light \n*Can turn invisible"
STRINGS.CHARACTER_QUOTES.ghostplayer = "OOOoooOOOoooo"

STRINGS.CHARACTER_TITLES.mermplayer = "Merm"
STRINGS.CHARACTER_NAMES.mermplayer = "MERM"
STRINGS.CHARACTER_DESCRIPTIONS.mermplayer = "*Can craft \n*Loves rain \n*Is a monster"
STRINGS.CHARACTER_QUOTES.mermplayer = "GURGLE!"

STRINGS.CHARACTER_TITLES.pigmanplayer = "Pig"
STRINGS.CHARACTER_NAMES.pigmanplayer = "PIG"
STRINGS.CHARACTER_DESCRIPTIONS.pigmanplayer = "*Can craft \n*Is a pig \n*Has a horrible curse..."
STRINGS.CHARACTER_QUOTES.pigmanplayer = "Oink!"

STRINGS.CHARACTER_TITLES.pigman_guardp = "Pig Guard"
STRINGS.CHARACTER_NAMES.pigman_guardp = "Pig Guard"
STRINGS.CHARACTER_DESCRIPTIONS.pigman_guardp = "*Can craft \n*Is a guard pig \n*Has a horrible curse..."
STRINGS.CHARACTER_QUOTES.pigman_guardp = "PROTECT KING!"


STRINGS.CHARACTER_TITLES.walrusplayer = "MacTusk"
STRINGS.CHARACTER_NAMES.walrusplayer = "MacTusk"
STRINGS.CHARACTER_DESCRIPTIONS.walrusplayer = "*Can craft \n*Loves winter, hates the heat \n*Hounds like him"
STRINGS.CHARACTER_QUOTES.walrusplayer = "The winter hunter"

STRINGS.CHARACTER_TITLES.beefplayer = "Beefalo"
STRINGS.CHARACTER_NAMES.beefplayer = "BEEFALO"
STRINGS.CHARACTER_DESCRIPTIONS.beefplayer = "*Is beefy \n*Makes babies \n*Can poop"
STRINGS.CHARACTER_QUOTES.beefplayer = "100% Grade-A Beefalo"

STRINGS.CHARACTER_TITLES.vargplayer = "Varg"
STRINGS.CHARACTER_NAMES.vargplayer = "VARG"
STRINGS.CHARACTER_DESCRIPTIONS.vargplayer = "*Is a hound\n*Summons hounds \n*Can only eat meat"
STRINGS.CHARACTER_QUOTES.vargplayer = "Hunter of Hunters"

STRINGS.CHARACTER_TITLES.smallbirdp = "Smallbird"
STRINGS.CHARACTER_NAMES.smallbirdp = "SMALLBIRD"
STRINGS.CHARACTER_DESCRIPTIONS.smallbirdp = "*Is adorable \n*Can grow up \n*Not very strong..."
STRINGS.CHARACTER_QUOTES.smallbirdp = "Smaller than your average bird"

STRINGS.CHARACTER_TITLES.tallbirdplayer = "Tallbird"
STRINGS.CHARACTER_NAMES.tallbirdplayer = "TALLBIRD"
STRINGS.CHARACTER_DESCRIPTIONS.tallbirdplayer = "*Very fast \n*Very angry \n*Is proud parent"
STRINGS.CHARACTER_QUOTES.tallbirdplayer = "Taller than your average bird"

STRINGS.CHARACTER_TITLES.clockwork1player = "Knight"
STRINGS.CHARACTER_NAMES.clockwork1player = "KNIGHT"
STRINGS.CHARACTER_DESCRIPTIONS.clockwork1player = "*Is a clockwork \n*Is pretty bulky \n*Can perform basic base maintence"
STRINGS.CHARACTER_QUOTES.clockwork1player = "*furiously springing*"

STRINGS.CHARACTER_TITLES.clockwork2player = "Bishop"
STRINGS.CHARACTER_NAMES.clockwork2player = "BISHOP"
STRINGS.CHARACTER_DESCRIPTIONS.clockwork2player = "*Is a clockwork \n*Has a ranged attack\n*Can perform basic base maintence"
STRINGS.CHARACTER_QUOTES.clockwork2player = "It has bright ideas on how to murder you"

STRINGS.CHARACTER_TITLES.clockwork3player = "Rook"
STRINGS.CHARACTER_NAMES.clockwork3player = "ROOK"
STRINGS.CHARACTER_DESCRIPTIONS.clockwork3player = "*Is bulky \n*Can charge \n*Very destructive"
STRINGS.CHARACTER_QUOTES.clockwork3player = "Most things can be solved with a charge"

STRINGS.CHARACTER_TITLES.treeplayer = "Tree Guard"
STRINGS.CHARACTER_NAMES.treeplayer = "TREEGUARD"
STRINGS.CHARACTER_DESCRIPTIONS.treeplayer = "*Is bulky \n* Helps nature \n*Is slow..."
STRINGS.CHARACTER_QUOTES.treeplayer = "Even the trees are deadly"

STRINGS.CHARACTER_TITLES.krampusp = "Krampus"
STRINGS.CHARACTER_NAMES.krampusp = "KRAMPUS"
STRINGS.CHARACTER_DESCRIPTIONS.krampusp = "*Is bulky \n*Can teleport \n*Is a theif"
STRINGS.CHARACTER_QUOTES.krampusp = "He'll solve your storage issues for sure"

STRINGS.CHARACTER_TITLES.goatp = "Volt Goat"
STRINGS.CHARACTER_NAMES.goatp = "VOLTGOAT"
STRINGS.CHARACTER_DESCRIPTIONS.goatp = "*Is a lightning rod \n*Can store lightning \n*Is fast"
STRINGS.CHARACTER_QUOTES.goatp = "BAAAAAAAHP"

STRINGS.CHARACTER_TITLES.tentaclep = "Tentacle"
STRINGS.CHARACTER_NAMES.tentaclep = "TENTACLE"
STRINGS.CHARACTER_DESCRIPTIONS.tentaclep = "*Is sneaky \n*Frail, but can deal some hurt \n*Will keep attacking until Z is pressed"
STRINGS.CHARACTER_QUOTES.tentaclep = "We've seen enough hentai to know where this is going..."

STRINGS.CHARACTER_TITLES.butterflyp = "Butterfly"
STRINGS.CHARACTER_NAMES.butterflyp = "Butterfly"
STRINGS.CHARACTER_DESCRIPTIONS.butterflyp = "*Is a butterfly.\n*Is flying.\n*Can't attack despite it trying to."
STRINGS.CHARACTER_QUOTES.butterflyp = "After 7 years in development, hopefully it will be worth the wait."

STRINGS.CHARACTER_TITLES.rabbitp = "Rabbit"
STRINGS.CHARACTER_NAMES.rabbitp = "RABBIT"
STRINGS.CHARACTER_DESCRIPTIONS.rabbitp = "*Is cute.\n*Grows white fur in the winter..\n*Can't attack."
STRINGS.CHARACTER_QUOTES.rabbitp = "AAAAAAAAAAAA"

STRINGS.CHARACTER_TITLES.pengullp = "Pengull"
STRINGS.CHARACTER_NAMES.pengullp = "PENGULL"
STRINGS.CHARACTER_DESCRIPTIONS.pengullp = "*Is loud.\n*Lays eggs.\n*Loves winter."
STRINGS.CHARACTER_QUOTES.pengullp = "SQUAWK"

STRINGS.CHARACTER_TITLES.glommerp = "Glommer"
STRINGS.CHARACTER_NAMES.glommerp = "GLOMMER"
STRINGS.CHARACTER_DESCRIPTIONS.glommerp = "*Is cute.\n*Poops special poop.\n*Can't Attack..."
STRINGS.CHARACTER_QUOTES.glommerp = "ZZZRP"

STRINGS.CHARACTER_TITLES.frogp = "Frog"
STRINGS.CHARACTER_NAMES.frogp = "FROG"
STRINGS.CHARACTER_DESCRIPTIONS.frogp = "*Is a Frog.\n*Makes enemies drop items.\n*Is hated"
STRINGS.CHARACTER_QUOTES.frogp = "ribbit"

STRINGS.CHARACTER_TITLES.molep = "Mole"
STRINGS.CHARACTER_NAMES.molep = "MOLE"
STRINGS.CHARACTER_DESCRIPTIONS.molep = "*Hides underground.\n*Loves rocks.\n*Can't attack..."
STRINGS.CHARACTER_QUOTES.molep = "..."

STRINGS.CHARACTER_TITLES.beep = "Bee"
STRINGS.CHARACTER_NAMES.beep = "BEE"
STRINGS.CHARACTER_DESCRIPTIONS.beep = "*Is a bee.\n*buzzes around.\n*Has a nasty sting."
STRINGS.CHARACTER_QUOTES.beep = "BZZZZZZ"

STRINGS.CHARACTER_TITLES.flyp = "Mosquito"
STRINGS.CHARACTER_NAMES.flyp = "MOSTQUITO"
STRINGS.CHARACTER_DESCRIPTIONS.flyp = "*Is a bloodsucker.\n*Is superfast.\n* Annoying..."
STRINGS.CHARACTER_QUOTES.flyp = "BZZZZZZZZZZ"

STRINGS.CHARACTER_TITLES.gekkop = "Grass Gekko"
STRINGS.CHARACTER_NAMES.gekkop = "GRASS GEKKO"
STRINGS.CHARACTER_DESCRIPTIONS.gekkop = "*Sheds grass.\n*Is superfast.\n*Is frail..."
STRINGS.CHARACTER_QUOTES.gekkop = "BZZZZZZZZZZ"

STRINGS.CHARACTER_TITLES.elephantplayer = "Koalefant"
STRINGS.CHARACTER_NAMES.elephantplayer = "KOALEFANT"
STRINGS.CHARACTER_DESCRIPTIONS.elephantplayer = "*Is Bulky.\n*Nice and Warm.\n*Is Adorable!"
STRINGS.CHARACTER_QUOTES.elephantplayer = "HUH?"

STRINGS.CHARACTER_TITLES.perdp = "Gobbler"
STRINGS.CHARACTER_NAMES.perdp = "GOBBLER"
STRINGS.CHARACTER_DESCRIPTIONS.perdp = "*Is fast.\n*Loves Berries.\n*ONLY loves Berries."
STRINGS.CHARACTER_QUOTES.perdp = "gobblegobble"

STRINGS.CHARACTER_TITLES.chesterp = "Chester"
STRINGS.CHARACTER_NAMES.chesterp = "CHESTER"
STRINGS.CHARACTER_DESCRIPTIONS.chesterp = "*Is Chester!\n*Is Helpful.\n*Can Transform."
STRINGS.CHARACTER_QUOTES.chesterp = "*pant*"

STRINGS.CHARACTER_TITLES.mandrakep = "Mandrake"
STRINGS.CHARACTER_NAMES.mandrakep = "MANDRAKE"
STRINGS.CHARACTER_DESCRIPTIONS.mandrakep = "*Is annoying!\n*It won't stop.\n*AAAAAAAAAAAAAAA"
STRINGS.CHARACTER_QUOTES.mandrakep = "MEEPMEEPMEEPMEEP"

STRINGS.CHARACTER_TITLES.crowp = "Crow"
STRINGS.CHARACTER_NAMES.crowp = "CROW"
STRINGS.CHARACTER_DESCRIPTIONS.crowp = "*Is adorable!\n*Can fly.\n*Is pretty frail"
STRINGS.CHARACTER_QUOTES.crowp = "CAW CAW"

STRINGS.CHARACTER_TITLES.birdp = "Robin"
STRINGS.CHARACTER_NAMES.birdp = "ROBIN"
STRINGS.CHARACTER_DESCRIPTIONS.birdp = "*Is adorable!\n*Can fly.\n*Is pretty frail"
STRINGS.CHARACTER_QUOTES.birdp = "CAW CAW"

STRINGS.CHARACTER_TITLES.buzzardp = "Buzzard"
STRINGS.CHARACTER_NAMES.buzzardp = "BUZZARD"
STRINGS.CHARACTER_DESCRIPTIONS.buzzardp = "*Is NOT adorable!\n*Can fly.\n*Can attack."
STRINGS.CHARACTER_QUOTES.buzzardp = "SQUAWK"

STRINGS.CHARACTER_TITLES.birchnutterp = "Birchnutter"
STRINGS.CHARACTER_NAMES.birchnutterp = "BIRCHNUTTER"
STRINGS.CHARACTER_DESCRIPTIONS.birchnutterp = "*Is NOT adorable!\n*Is annoying.\n*Hates fire."
STRINGS.CHARACTER_QUOTES.birchnutterp = "BAP"

STRINGS.CHARACTER_TITLES.shadow2player = "Terrorbeak"
STRINGS.CHARACTER_NAMES.shadow2player = "TERRORBEAK"
STRINGS.CHARACTER_DESCRIPTIONS.shadow2player = "*One of Them\n*Transforms when reaching the ocean\n*Is always watching..."
STRINGS.CHARACTER_QUOTES.shadow2player = "Them..."

STRINGS.CHARACTER_TITLES.shadowplayer = "Crawling Horror"
STRINGS.CHARACTER_NAMES.shadowplayer = "CRAWLING HORROR"
STRINGS.CHARACTER_DESCRIPTIONS.shadowplayer = "*One of Them\n*Is always watching..."
STRINGS.CHARACTER_QUOTES.shadowplayer = "Them..."

STRINGS.CHARACTER_TITLES.wormp = "Depths Worm"
STRINGS.CHARACTER_NAMES.wormp = "DEPTHS WORM"
STRINGS.CHARACTER_DESCRIPTIONS.wormp = "*Digs underground\n*Has a lure"
STRINGS.CHARACTER_QUOTES.wormp = "Worms are never a good thing."

STRINGS.CHARACTER_TITLES.batp = "Batilisk"
STRINGS.CHARACTER_NAMES.batp = "BATILISK"
STRINGS.CHARACTER_DESCRIPTIONS.batp = "*Is flying\n*Poops\n*Not very strong..."
STRINGS.CHARACTER_QUOTES.batp = "Flying rats."

STRINGS.CHARACTER_TITLES.cspiderp = "Spitter"
STRINGS.CHARACTER_NAMES.cspiderp = "SPITTER"
STRINGS.CHARACTER_DESCRIPTIONS.cspiderp = "*Is a Spider\n*Can spit at a range\n*Not very good in melee combat"
STRINGS.CHARACTER_QUOTES.cspiderp = "Spitting spitter spit"

STRINGS.CHARACTER_TITLES.cspider2p = "Hider"
STRINGS.CHARACTER_NAMES.cspider2p = "HIDER"
STRINGS.CHARACTER_DESCRIPTIONS.cspider2p = "*Is a Spider\n*Can hide in their protective shell\n*Is bulky for a spider"
STRINGS.CHARACTER_QUOTES.cspider2p = "Its as tough as it is edgy."

STRINGS.CHARACTER_TITLES.monkeyp = "Splumonkey"
STRINGS.CHARACTER_NAMES.monkeyp = "SPLUMONKEY"
STRINGS.CHARACTER_DESCRIPTIONS.monkeyp = "*Is a Monkey\n*Can throw poop\n*Is a vegetarian"
STRINGS.CHARACTER_QUOTES.monkeyp = "Where are my monkey barrels Klei!?"

STRINGS.CHARACTER_TITLES.slurperp = "Slurper"
STRINGS.CHARACTER_NAMES.slurperp = "SLURPER"
STRINGS.CHARACTER_DESCRIPTIONS.slurperp = "*Is a glutton\n*Gives light \n*Can steal hunger on hit"
STRINGS.CHARACTER_QUOTES.slurperp = "Careful Gordon, she might try to couple with your head."

STRINGS.CHARACTER_TITLES.slurtlep = "Slurtle"
STRINGS.CHARACTER_NAMES.slurtlep = "SLURTLE"
STRINGS.CHARACTER_DESCRIPTIONS.slurtlep = "*Is very slow\n*Can hide in their protective shell\n*Is prone to exploding\n*Can only eat rocks"
STRINGS.CHARACTER_QUOTES.slurtlep = "He will eat all the rocks... eventually."

STRINGS.CHARACTER_TITLES.bunnyp = "Bunnyman"
STRINGS.CHARACTER_NAMES.bunnyp = "BUNNYMAN"
STRINGS.CHARACTER_DESCRIPTIONS.bunnyp = "*Is a vegetarian\n*Can see in the dark \n*Transforms when low on sanity"
STRINGS.CHARACTER_QUOTES.bunnyp = "MEAT IS MURDER!!!1!"
STRINGS.CHARACTERS.BUNNYP = require "speech_bunnyp"

STRINGS.CHARACTER_TITLES.rocklobsterp = "Rock Lobster"
STRINGS.CHARACTER_NAMES.rocklobsterp = "ROCKLOBSTER"
STRINGS.CHARACTER_DESCRIPTIONS.rocklobsterp = "*Super bulky and slow \n*Can protect themselves\n*Can only eat rocks"
STRINGS.CHARACTER_QUOTES.rocklobsterp = "Reminds me of that one song..."

STRINGS.CHARACTER_TITLES.guardianp = "Ancient Guardian"
STRINGS.CHARACTER_NAMES.guardianp = "GUARDIAN"
STRINGS.CHARACTER_DESCRIPTIONS.guardianp = "*Is a Giant\n*Can ram"
STRINGS.CHARACTER_QUOTES.guardianp = "The REAL and ORIGINAL Guardian."

STRINGS.CHARACTER_TITLES.hutchp = "Hutch"
STRINGS.CHARACTER_NAMES.hutchp = "HUTCH"
STRINGS.CHARACTER_DESCRIPTIONS.hutchp = "*Is adorable \n*Can transform when holding certain items\n*Has a light"
STRINGS.CHARACTER_QUOTES.hutchp = "Chester but better?"

------------------------------------------------------------------
-- The prefab names as they appear in-game
------------------------------------------------------------------
STRINGS.RECIPE_DESC.RABBITHOUSE_PLAYER = "Classic House of Carrots."
STRINGS.RECIPE_DESC.SPIDERHOLE_P = "For that hardcore spider."
STRINGS.RECIPE_DESC.BATCAVE_P = "Bat Man not included."
STRINGS.RECIPE_DESC.MONKEYBARREL_P = "For the leader of your DS crew"
STRINGS.RECIPE_DESC.SLURTLEMOUND_P = "Hard and slimy... don't look at me like that."

STRINGS.NAMES.MONKEYBARREL_P = "Splumonkey Pod"
STRINGS.NAMES.MONKEYHOUSE = "Splumonkey Pod"

STRINGS.NAMES.SPIDERHOLE_P = "Spider Mound"
STRINGS.NAMES.CSPIDERHOME = "Spider House"

STRINGS.NAMES.SLURTLEMOUND_P = "Slurtle Mound"
STRINGS.NAMES.SLURTLEHOME = "Slurtle House"

STRINGS.NAMES.BATCAVE_P = "Batilisk Cave"
STRINGS.NAMES.BATHOME = "Bat House"

STRINGS.NAMES.RABBITHOUSE_PLAYER = "Rabbit Hutch"

STRINGS.NAMES.MERMHOUSE_PLAYER = "Merm House"
STRINGS.NAMES.PIGHOUSE_PLAYER = "Pig House"
STRINGS.NAMES.IGLOO_PLAYER = "Walrus Camp"
STRINGS.NAMES.PIGTORCH_P = "Pig Torch"

STRINGS.NAMES.SPIDERNEST_P = "Spider Den"
STRINGS.NAMES.SPIDERNEST2_P = "Spider Den"
STRINGS.NAMES.SPIDERNEST3_P = "Spider Den"
STRINGS.NAMES.SPIDERHOME = "Spider Home"

STRINGS.NAMES.HOUNDMOUND_P = "Hound Mound"
STRINGS.NAMES.HOUNDHOME = "Hound Home"

STRINGS.NAMES.CATCOONDEN_P = "Catcoon Den"
STRINGS.NAMES.CATHOME = "Catcoon Home"

STRINGS.NAMES.LAVAPOND_P = "Lava Pond"
STRINGS.NAMES.MAGGOTHOME = "Lavae Home"

STRINGS.NAMES.MONSTER_WPN = "Mysterious Power"

STRINGS.NAMES.RABBITP = "Rabbit"
STRINGS.NAMES.PENGULLP = "Pengull"
STRINGS.NAMES.GLOMMERP = "Glommer"
STRINGS.NAMES.FROGP = "Frog"
STRINGS.NAMES.MOLEP = "Mole"
STRINGS.NAMES.BEEP = "Bee"
STRINGS.NAMES.CHESTERP = "Chester"
STRINGS.NAMES.MANDRAKEP = "Mandrake"

STRINGS.NAMES.RABBITHOME = "Rabbit Home"
STRINGS.NAMES.RABBITHOLE_P = "Rabbit Hole"

STRINGS.NAMES.MOLEHOME = "Mole Home"
STRINGS.NAMES.MOLEHOLE_P = "Moleworm Hole"

STRINGS.NAMES.BEEHOME = "Bee Home"
STRINGS.NAMES.BEEHIVE_P = "Bee Hive"
STRINGS.NAMES.BEEHIVE2_P = "Killer Bee Hive"

STRINGS.NAMES.FROGHOME = "Frog Home"
STRINGS.NAMES.POND_P = "Pond"

------------------------------------------------------------------
--Reforged Strings
------------------------------------------------------------------
STRINGS.LAVAARENA_CHARACTER_DESCRIPTIONS.houndplayer = "*Is very fast\n*Can taunt by pressing x \n*Not very bulky\n\nExpertise:\nMelees, Darts"
STRINGS.LAVAARENA_CHARACTER_DESCRIPTIONS.houndiceplayer = "*Is very fast\n*Can taunt by pressing x \n*Not very bulky\n\nExpertise:\nBooks, Staves"
STRINGS.LAVAARENA_CHARACTER_DESCRIPTIONS.houndredplayer = "*Is very fast\n*Can taunt by pressing x \n*Not very bulky\n\nExpertise:\nBooks, Staves"
STRINGS.LAVAARENA_CHARACTER_DESCRIPTIONS.pigmanplayer = "*20% more damage with weapons\n*Can taunt by pressing x \n*Transforms when low on health\n\nExpertise:\nMelees"
STRINGS.LAVAARENA_CHARACTER_DESCRIPTIONS.pigman_guardp = "*20% more damage with weapons\n*Can taunt by pressing x \n*Transforms when low on health\n\nExpertise:\nMelees"
STRINGS.LAVAARENA_CHARACTER_DESCRIPTIONS.mermplayer = "*Is fast\n*Can taunt by pressing x \n*attackers can get wet\n\nExpertise:\nMelees"
STRINGS.LAVAARENA_CHARACTER_DESCRIPTIONS.walrusplayer = "*Has thick blubber\n*Is a master of darts \n*Brings his pups\n\nExpertise:\nDarts"
STRINGS.LAVAARENA_CHARACTER_DESCRIPTIONS.blackspiderplayer = "*Can revive allies like Wilson \n*Can be revived twice as quickly \n\nExpertise:\nDarts, Staves"
STRINGS.LAVAARENA_CHARACTER_DESCRIPTIONS.whitespiderplayer = "*Increased attack speed\n*Boosts attacks of nearby allies \n\nExpertise:\nDarts"
STRINGS.LAVAARENA_CHARACTER_DESCRIPTIONS.warriorp = "*Increased attack speed \n*Boosts defense of nearby allies \n\nExpertise:\nDarts"
STRINGS.LAVAARENA_CHARACTER_DESCRIPTIONS.krampusp = "*Isn't well liked\n*Can taunt by pressing x \n*Can steal items by pressing c\n\nExpertise:\nDarts, Books, Staves"
STRINGS.LAVAARENA_CHARACTER_DESCRIPTIONS.vargplayer = "*Is bulky\n*Can summon hounds with x \n*Buffs nearby hound players\n\nExpertise:\nNone"
STRINGS.LAVAARENA_CHARACTER_DESCRIPTIONS.treeplayer = "*Is very bulky\n*Gains more health from healing \n*Rez others slower, but retuns more health\n\nExpertise:\nNone"
STRINGS.LAVAARENA_CHARACTER_DESCRIPTIONS.bearplayer = "*Is a giant\n*Can groundpound by pressing x \n*Can rage by pressing c\n\nExpertise:\nNone"
STRINGS.LAVAARENA_CHARACTER_DESCRIPTIONS.deerplayer = "*Is a giant\n*Can taunt by pressing x \n*Is pretty strong\n\nExpertise:\nNone"

STRINGS.LAVAARENA_CHARACTER_DESCRIPTIONS.berniep = "*Is bulky\n*Grabs aggro with X\n*Starts out big\n\nExpertise:\nNone"
STRINGS.LAVAARENA_CHARACTER_DESCRIPTIONS.balloonp = "*1 HP\n*Popping kills everything in one hit\n*Cannot be revived\n\nExpertise:\nNone"
STRINGS.LAVAARENA_CHARACTER_DESCRIPTIONS.abbyp = "*Is a ghost\n*Auto-revives\n*Revives other instantly, but loses health\n\nExpertise:\nMelee, Darts, Books, Staves"
STRINGS.LAVAARENA_CHARACTER_DESCRIPTIONS.smallghostp = "*Is a ghost\n*Auto-revives\n*Revives other instantly, but loses health\n\nExpertise:\nDarts, Books, Staves"
STRINGS.LAVAARENA_CHARACTER_DESCRIPTIONS.maxpuppetp = "*Is immune to debuffs\n*Brings a nightmare sword with it\n*Can't use much else.\n\nExpertise:\nMelee, Darts, Books, Staves"

STRINGS.LAVAARENA_CHARACTER_DESCRIPTIONS.rabbitp = "*Is very weak\n*Less aggro\n*Can be revived 3x faster\n\nExpertise:\nMelee"
------------------------------------------------------------------
-- Pigman-specific speech strings
------------------------------------------------------------------

STRINGS.CHARACTERS.PIGMANPLAYER = require "speech_pigmanplayer"

------------------------------------------------------------------
-- The default responses of characters examining the prefabs
------------------------------------------------------------------
STRINGS.CHARACTERS.GENERIC.DESCRIBE.RABBITHOME = 
{
	GENERIC = "A rabbit must've dropped this.",

}

STRINGS.CHARACTERS.GENERIC.DESCRIBE.RABBITHOLE_P = 
{
	GENERIC = "There must be a rabbit around here.",

}

STRINGS.CHARACTERS.GENERIC.DESCRIBE.MOLEHOME = 
{
	GENERIC = "A mole probably dropped this.",

}

STRINGS.CHARACTERS.GENERIC.DESCRIBE.MOLEHOLE_P = 
{
	GENERIC = "I better keep an eye on my rocks.",

}

STRINGS.CHARACTERS.GENERIC.DESCRIBE.BEEHOME = 
{
	GENERIC = "A bee probably dropped this.",

}

STRINGS.CHARACTERS.GENERIC.DESCRIBE.BEEHIVE_P = 
{
	GENERIC = "That one seems pretty quiet.",

}

STRINGS.CHARACTERS.GENERIC.DESCRIBE.BEEHIVE2_P = 
{
	GENERIC = "Looks as angry as its owner.",

}

STRINGS.CHARACTERS.GENERIC.DESCRIBE.FROGHOME = 
{
	GENERIC = "A frog probably dropped this.",

}

STRINGS.CHARACTERS.GENERIC.DESCRIBE.POND_P = 
{
	GENERIC = "Theres no fish in there.",

}

STRINGS.CHARACTERS.GENERIC.DESCRIBE.MERMHOUSE_PLAYER = 
{
	GENERIC = "What a smelly house.",
	BUNRT = "Now its useless and smelly.",
}

STRINGS.CHARACTERS.GENERIC.DESCRIBE.PIGHOUSE_PLAYER = 
{
	GENERIC = "A house for pigs.",
	BURNT = "No one is sleeping there tonight.",
}

STRINGS.CHARACTERS.GENERIC.DESCRIBE.IGLOO_PLAYER = 
{
	GENERIC = "I hope whoever built this is friendly.",
	BURNT = "I hope whoever built this is still friendly after this.",
}

STRINGS.CHARACTERS.GENERIC.DESCRIBE.PIGTORCH_P = 
{
	GENERIC = "An over-decorated torch",
}

STRINGS.CHARACTERS.GENERIC.DESCRIBE.MONSTER_WPN = 
{
	GENERIC = "I really shouldn't have this...",
}

STRINGS.CHARACTERS.GENERIC.DESCRIBE.SPIDERNEST_P = 
{
	GENERIC = "Cozy for 6-legged freaks",
}

STRINGS.CHARACTERS.GENERIC.DESCRIBE.SPIDERNEST2_P = 
{
	GENERIC = "It grew quite a bit",
}

STRINGS.CHARACTERS.GENERIC.DESCRIBE.SPIDERNEST3_P = 
{
	GENERIC = "A mansion for spiders",
}

STRINGS.CHARACTERS.GENERIC.DESCRIBE.HOUNDMOUND_P = 
{
	GENERIC = "Pretty noisy in there",
}

STRINGS.CHARACTERS.GENERIC.DESCRIBE.CATCOONDEN_P = 
{
	GENERIC = "Theres a lot of cathair inside.",
}

STRINGS.CHARACTERS.GENERIC.DESCRIBE.LAVAPOND_P = 
{
	GENERIC = "Perfect for overheating",
}

STRINGS.CHARACTERS.GENERIC.DESCRIBE.SPIDERHOME = 
{
	GENERIC = "Its for our spider friends.",
}

STRINGS.CHARACTERS.GENERIC.DESCRIBE.CATHOME = 
{
	GENERIC = "Its for our catcoon friends.",
}

STRINGS.CHARACTERS.GENERIC.DESCRIBE.HOUNDHOME = 
{
	GENERIC = "Its for our hound friends.",
}

STRINGS.CHARACTERS.GENERIC.DESCRIBE.MAGGOTHOME = 
{
	GENERIC = "Its for our lavae friends.",
}

STRINGS.CHARACTERS.GENERIC.DESCRIBE.RABBITHOUSE_PLAYER = 
{
	GENERIC = "Smells like carrots.",
	BURNT = "Smells like burnt carrots... and rabbits.",
}

STRINGS.CHARACTERS.GENERIC.DESCRIBE.BATCAVE_P = 
{
	GENERIC = "Home of Batman.",
}

STRINGS.CHARACTERS.GENERIC.DESCRIBE.SPIDERHOLE_P = 
{
	GENERIC = "Doesn't look very comfy.",
}

STRINGS.CHARACTERS.GENERIC.DESCRIBE.CMONSTER_WPN = 
{
	GENERIC = "I shouldn't have this...",
}

return STRINGS