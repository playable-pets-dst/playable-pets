require("stategraphs/commonstates")
require("stategraphs/ppstates")

local function GetExcludeTags(inst)
	if TheNet:GetPVPEnabled() then
		return {"INLIMBO", "playerghost", "notarget", "shadow", "tentacle"}
	elseif TheNet:GetServerGameMode() == "lavaarena" then
		return {"player", "companion", "playerghost", "INLIMBO"}
	else	
		return {"player", "companion", "INLIMBO", "playerghost", "notarget", "shadow", "tentacle"}
	end
end

local function ShakeIfClose(inst)
    ShakeAllCameras(CAMERASHAKE.VERTICAL, .15, .02, .5, inst, 20)
end

local longaction = "action_custom"
local shortaction = "action_custom"
local workaction = "action_custom"
local otheraction = "action_custom"

local actionhandlers = PP_CommonStates.GetCommonActions(longaction, shortaction, workaction, otheraction)

local actionhandler_overrides = 
{ 
	ActionHandler(ACTIONS.ATTACK, "attack_pre"),
}

if #actionhandler_overrides > 0 then
	for i, v in ipairs(actionhandler_overrides) do
		table.insert(actionhandlers, v)
	end
end

local events=
{
    EventHandler("attacked", function(inst) if not inst.components.health:IsDead() and not inst.sg:HasStateTag("attack") and not inst.sg:HasStateTag("nointerrupt") then inst.sg:GoToState("hit") end end),
    EventHandler("death", function(inst) inst.sg:GoToState("death") end),
    EventHandler("doattack", function(inst, data) if not inst.components.health:IsDead() and (inst.sg:HasStateTag("hit") or not inst.sg:HasStateTag("busy")) then inst.sg:GoToState("attack", data.target) end end),
    CommonHandlers.OnSleep(),
    PP_CommonHandlers.OpenGift(),
	PP_CommonHandlers.OnSink(),
	PP_CommonHandlers.OnKnockback(),
	PP_CommonHandlers.OnDeath(),
	PP_CommonHandlers.AddCommonHandlers(),
	CommonHandlers.OnHop(),
	PP_CommonHandlers.OnLocomoteAdvanced(),  
    CommonHandlers.OnFreeze(),
}


 local states=
{
	
	 State{
        name = "rumble",
        tags = {"idle", "invisible"},
        onenter = function(inst)
            inst.SoundEmitter:PlaySound("dontstarve/tentacle/tentacle_rumble_LP", "tentacle")
            inst.SoundEmitter:SetParameter( "tentacle", "state", 0)
            inst.AnimState:PlayAnimation("ground_pre")
            inst.AnimState:PushAnimation("ground_loop", true)
            inst.sg:SetTimeout(GetRandomWithVariance(10, 5) )
        end,
        ontimeout = function(inst)
            inst.AnimState:PushAnimation("ground_pst", false)
            inst.sg:GoToState("idle")
        end,

        onexit = function(inst)
            inst.SoundEmitter:KillSound("tentacle")
        end,
    },
   
    State{
        name = "idle",
        tags = {"idle", "invisible"},
        onenter = function(inst)
            inst.AnimState:PushAnimation("idle", true)
            --inst.sg:SetTimeout(GetRandomWithVariance(10, 5) )
            inst.SoundEmitter:KillAllSounds()
        end,
                
        ontimeout = function(inst)
			--inst.sg:GoToState("rumble")
        end,
    },


    State{
        name = "run_start",
        tags = {"moving", "canrotate"},

        onenter = function(inst) 
            inst.components.locomotor:RunForward() 
			inst.sg:GoToState("run")
        end,

        events =
        {   
            --EventHandler("animover", function(inst) inst.sg:GoToState("run") end ),        
        },
    },
        
    State{            
        name = "run",
        tags = {"moving", "canrotate"},
        
        onenter = function(inst) 
				inst.components.locomotor:RunForward() 
        end,

        events=
        {   
            EventHandler("animover", function(inst) inst.sg:GoToState("run") end ),        
        },
    },      
    
    State{            
        name = "run_stop",
        tags = {"idle", "canrotate"},
        
        onenter = function(inst) 
        inst.components.locomotor:StopMoving()
		inst.sg:GoToState("idle")
        end,

        events=
        {   
            
            EventHandler("animover", function(inst) inst.sg:GoToState("idle") end ),        
        },
    },

    State{
        name = "special_atk1",
        tags = {"busy"},
        
        onenter = function(inst)
            inst.Physics:Stop()
            inst.SoundEmitter:PlaySound("dontstarve/tentacle/tentacle_rumble_LP", "tentacle")
            inst.SoundEmitter:SetParameter( "tentacle", "state", 0)
            inst.AnimState:PlayAnimation("ground_pre")
            inst.AnimState:PushAnimation("ground_loop", true)
            inst.sg:SetTimeout(2)

        end,
		
		ontimeout = function(inst)
            inst.AnimState:PushAnimation("ground_pst", false)
            inst.sg:GoToState("idle")
        end,

        onexit = function(inst)
            inst.SoundEmitter:KillSound("tentacle")
        end,
    },
	
	State{
        name = "action_custom",
        tags = {"busy"},
        
        onenter = function(inst)
            inst.Physics:Stop()
            inst.SoundEmitter:PlaySound("dontstarve/tentacle/tentacle_rumble_LP", "tentacle")
            inst.SoundEmitter:SetParameter( "tentacle", "state", 0)
            inst.AnimState:PlayAnimation("ground_pre")
            inst.AnimState:PushAnimation("ground_loop", true)
            inst.sg:SetTimeout(1)

        end,
		
		ontimeout = function(inst)
			inst:PerformBufferedAction()
            inst.AnimState:PushAnimation("ground_pst", false)
            inst.sg:GoToState("idle")
        end,

        onexit = function(inst)
            inst.SoundEmitter:KillSound("tentacle")
        end,
    },

    State{
        name = "special_atk2",
        tags = {"busy"},
        onenter = function(inst)
			inst.Physics:Stop()
			inst.SoundEmitter:PlaySound("dontstarve/tentacle/tentacle_rumble_LP", "tentacle")
			inst.SoundEmitter:SetParameter( "tentacle", "state", 0)
            inst:RemoveTag("notarget")
            inst.AnimState:PlayAnimation("breach_pre")
            inst.AnimState:PushAnimation("breach_loop", true)
			inst.sg:SetTimeout(2)
        end,

        ontimeout = function(inst)           
            inst.AnimState:PlayAnimation("breach_pst")
            inst.sg:GoToState("idle")        

        end,
        onexit = function(inst)
            inst.SoundEmitter:KillSound("tentacle")
			inst:AddTag("notarget")
        end,
    },
	
	State{
        name ="attack_pre",
        tags = {"attack", "attacking", "busy"},
        onenter = function(inst)
			inst.Physics:Stop()
            inst.SoundEmitter:PlaySound("dontstarve/tentacle/tentacle_emerge")
            inst.components.combat:StartAttack()
			inst:RemoveTag("notarget")
            inst.AnimState:PlayAnimation("atk_pre")
			if not inst.SoundEmitter:PlayingSound("tentacle") then
				inst.SoundEmitter:PlaySound("dontstarve/tentacle/tentacle_rumble_LP", "tentacle")
			end      
			inst.SoundEmitter:SetParameter( "tentacle", "state", 1)      
        end,
        events=
        {
            EventHandler("animover", function(inst) inst.sg:GoToState("attack") end),
        },
        timeline=
        {
            TimeEvent(20*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/tentacle/tentacle_emerge_VO") end),
        }
        
    },
    
    State{ 
        name = "attack",
        tags = {"attack", "attacking", "busy"},
        onenter = function(inst)
			local buffaction = inst:GetBufferedAction()
			local target = buffaction ~= nil and buffaction.target or nil
			inst.components.combat:SetTarget(target)
			if target ~= nil then
            if target:IsValid() then
                inst:FacePoint(target:GetPosition())
                inst.sg.statemem.attacktarget = target
				end
			end
			inst.Physics:Stop()
            inst.AnimState:PlayAnimation("atk_loop")
            inst.AnimState:PushAnimation("atk_idle", false)
        end,
        
        timeline=
        {
            TimeEvent(2*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/tentacle/tentacle_attack") end),
			TimeEvent(7*FRAMES, function(inst) inst.components.combat:DoAttack(inst.sg.statemem.target) end),
            TimeEvent(15*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/tentacle/tentacle_attack") end),
            TimeEvent(17*FRAMES, function(inst) inst.components.combat:DoAttack(inst.sg.statemem.target) end),
            TimeEvent(18*FRAMES, function(inst) inst.sg:RemoveStateTag("attack") end),
        },
        
        events=
        {
            EventHandler("animqueueover", function(inst) 
                if inst.wants_to_sleep == false then
                    inst.sg:GoToState("attack") 
                else
                    inst.sg:GoToState("attack_post") 
                end
            end),
        },
    },
    
    State{
        name ="attack_post",
		tags = {"busy", "nointerrupt"},
        onenter = function(inst)
            inst.SoundEmitter:PlaySound("dontstarve/tentacle/tentacle_disappear")
            inst.AnimState:PlayAnimation("atk_pst")
			inst.wants_to_sleep = false
        end,
        events=
        {
            EventHandler("animover", function(inst) inst.SoundEmitter:KillAllSounds() inst.sg:GoToState("idle") end),
        },
		
		onexit = function(inst) inst:AddTag("notarget") end,
    },
	
	State{
        name = "hit",
        tags = {"busy", "hit"},
        
        onenter = function(inst)
			inst.Physics:Stop()
			inst:RemoveTag("notarget")
            inst.AnimState:PlayAnimation("hit")
            inst.SoundEmitter:PlaySound("dontstarve/tentacle/tentacle_hurt_VO")
        end,
        
        events=
        {
            EventHandler("animover", function(inst) if inst.wants_to_sleep == false then
			inst.sg:GoToState("attack")
			else
			inst.sg:GoToState("attack_post")
			end end),
        },
        
    },   

    State{
        name = "death",
        tags = {"busy"},

        onenter = function(inst)
            inst.SoundEmitter:PlaySound("dontstarve/tentacle/tentacle_death_VO")
            inst.AnimState:PlayAnimation("death")
            inst.Physics:Stop()
            RemovePhysicsColliders(inst)
            inst.components.lootdropper:DropLoot(inst:GetPosition())
			inst.components.inventory:DropEverything(true)
			
            if inst.components.playercontroller ~= nil then
                inst.components.playercontroller:RemotePausePrediction()
            end
        end,
		
		timeline = 
		{
		
		},
		
        events =
        {
            EventHandler("animover", function(inst)
				inst.SoundEmitter:PlaySound("dontstarve/tentacle/tentacle_splat")
                if inst.AnimState:AnimDone() then
                    PlayablePets.DoDeath(inst)
                end
            end),
        },
    },
}
  

CommonStates.AddFrozenStates(states)
local simpleanim = "ground_pst"
local idleanim = "idle"
PP_CommonStates.AddKnockbackState(states, nil, "hit") --(states, timeline, anim, sounds, fns)
PP_CommonStates.AddActionStates(states, 
	{
		TimeEvent(0 * FRAMES, function(inst)
            inst:PerformBufferedAction()
        end),
	}, 
	"ground_pst", nil, nil, "ground_loop", "ground_pst") --(states, timelines, anim, anim2, enteranim, loopanim, exitanim, noanim, events)
PP_CommonStates.AddCorpseStates(states, true, 
	--timelines = 
	{
		corpse =
		{
			TimeEvent(0*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/tentacle/tentacle_death_VO") end),
		},
		
		corpse_taunt =
		{
			TimeEvent(0*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/tentacle/tentacle_disappear") end),
		},
	
	},
	--anims = 
	{
		corpse = "death",
		corpse_taunt = "atk_pst"
	},
	--sounds =
	{
		--corpse = "dontstarve_DLC003/creatures/enemy/grabbing_vine/death"
	},
	--fns =
	{

	}
	) ---(states, isflying, timelines, anims, sounds, fns, events, nofx)
PP_CommonStates.AddJumpInStates(states, nil, "ground_pst")
PP_CommonStates.AddOpenGiftStates(states, "idle_loop")
CommonStates.AddHopStates(states, false, {pre = "ground_pre", loop = "ground_loop", pst = "ground_pst"}, nil, "death")
PP_CommonStates.AddSailorStates(states, nil, 
{
	plank_idle = simpleanim,
	plank_idle_loop = idleanim,
	plank_idle_pst = simpleanim,
	
	plank_hop_pre = "ground_pre",
	plank_hop = "ground_loop",
	
	steer_pre = simpleanim,
	steer_idle = idleanim,
	steer_turning = simpleanim,
	stop_steering = simpleanim,
	
	row = simpleanim,
}
)
PP_CommonStates.AddAoeStates(states, nil, 
{
	generic = "ground_pst",
	
	leap_pre = "ground_pre",
	leap_loop = "ground_loop",
	leap_pst = "ground_pst",
	
	lunge_pre = "ground_pre",
	lunge_loop = "ground_loop",
	lunge_pst = "ground_pst",
	
	superjump_pre = "ground_pre",
	superjump_loop = "ground_loop",
	superjump_pst = "ground_pst",
	
	castspelltime = 10,
})



    
return StateGraph("tentaclep", states, events, "idle", actionhandlers)

