require("stategraphs/commonstates")
require("stategraphs/ppstates")

local function GetExcludeTags(inst)
	if TheNet:GetPVPEnabled() then
		return {"INLIMBO", "playerghost", "notarget", "shadow", "groundpoundimmune"}
	elseif TheNet:GetServerGameMode() == "lavaarena" then
		return {"player", "companion", "playerghost", "INLIMBO", "groundpoundimmune"}
	else	
		return {"player", "companion", "INLIMBO", "playerghost", "notarget", "shadow", "groundpoundimmune"}
	end
end

local function ShakeIfClose(inst)
    ShakeAllCameras(CAMERASHAKE.VERTICAL, .15, .02, .5, inst, 20)
end

local longaction = "action"
local shortaction = "action"
local workaction = "action"
local otheraction = "action"

local actionhandlers = PP_CommonStates.GetCommonActions(longaction, shortaction, workaction, otheraction)

local actionhandler_overrides = 
{ 
	--ActionHandler(ACTIONS.CASTAOE, function(inst)
		--local hands = inst.components.inventory:GetEquippedItem(EQUIPSLOTS.HANDS) or nil
		--if TheNet:GetServerGameMode() == "lavaarena" and hands and (hands:HasTag("magicweapon") or hands:HasTag("book")) then
			--return "castaoe"
		--else
			--return "attack"
		--end
	--end),
	ActionHandler(ACTIONS.REVIVE_CORPSE, "revive_other"),
}

if #actionhandler_overrides > 0 then
	for i, v in ipairs(actionhandler_overrides) do
		table.insert(actionhandlers, v)
	end
end

local events=
{
	EventHandler("attacked", function(inst, data) 
		if data.stimuli and data.stimuli == "strong" and not (inst.sg:HasStateTag("transform") or inst.sg:HasStateTag("nointerrupt")) and not inst.components.health:IsDead() then
			inst.sg:GoToState("hit")
		elseif data.stimuli and (data.stimuli == "electric" or data.stimuli == "explosive") and not inst.components.health:IsDead() then
			inst.sg:GoToState("hit", data.stimuli)
		elseif not inst.components.health:IsDead() and not inst.sg:HasStateTag("busy") and not inst.sg:HasStateTag("nointerrupt") then 
			inst.sg:GoToState("hit") 
		end 
	end),
	PP_CommonHandlers.AddCommonHandlers(),
    EventHandler("doattack", function(inst, data) if not inst.components.health:IsDead() and (inst.sg:HasStateTag("hit") or not inst.sg:HasStateTag("busy")) then inst.sg:GoToState("attack", data.target) end end),
    CommonHandlers.OnSleep(),
    --CommonHandlers.OnLocomote(true,false),
    CommonHandlers.OnFreeze(),
	EventHandler("emote", function(inst, data)
        if not (inst.sg:HasStateTag("busy") or
                inst.sg:HasStateTag("nopredict") or
                inst.sg:HasStateTag("sleeping"))
            and not inst.components.inventory:IsHeavyLifting()
            and (data.mounted or not inst.components.rider:IsRiding())
            and (data.beaver or not inst:HasTag("beaver"))
            and (not data.requires_validation or TheInventory:CheckClientOwnership(inst.userid, data.item_type)) then
            inst.sg:GoToState("emote", data)
        end
    end),
	---
	PP_CommonHandlers.OpenGift(),
	PP_CommonHandlers.OnKnockback(),
	PP_CommonHandlers.OnLocomoteAdvanced(), --if mob uses toggeable walk or homes
	PP_CommonHandlers.OnDeath(),
		
	EventHandler("respawnfromghost", function(inst)  
			if inst.components.playercontroller ~= nil then
                inst.components.playercontroller:Enable(true)
            end

            inst.components.health:SetInvincible(false)
            inst:ShowHUD(true)
            inst:SetCameraDistance()

            SerializeUserSession(inst) end),	
}

if CurrentRelease.GreaterOrEqualTo(ReleaseID.IDs.R08_ROT_TURNOFTIDES) then
	table.insert(events, PP_CommonHandlers.OnSink())
end

local function startaura(inst)
    inst.Light:SetColour(255/255, 32/255, 32/255)
    if inst:HasTag("girl") then
        inst.SoundEmitter:PlaySound("dontstarve/ghost/ghost_girl_attack_LP", "angry")
    else
        inst.SoundEmitter:PlaySound("dontstarve/ghost/ghost_attack_LP", "angry")
    end
	--inst.components.health:DoDelta(3, false)
    inst.AnimState:SetMultColour(207/255,92/255,92/255,1)

end

local function stopaura(inst)
    inst.Light:SetColour(180/255, 195/255, 225/255)
    inst.SoundEmitter:KillSound("angry")
    inst.AnimState:SetMultColour(1,1,1,1)
end

local function getidleanim(inst)
    if inst.ismad and not TheNet:GetServerGameMode() == "lavaarena" then
        return "angry"
    elseif inst.components.health:GetPercent() < .25 then
        return "shy"
    else
        return "idle"
    end
end

 local states=
{
    State
    {
        name = "idle",
        tags = {"idle", "canrotate", "canslide"},
        onenter = function(inst)
            inst.AnimState:PlayAnimation(getidleanim(inst), true)
        end,
    },
	
	State{
        name = "castaoe",
        tags = {"busy"},
        
        onenter = function(inst,data)            
            inst.Physics:Stop()
            inst:PerformBufferedAction()
			inst.AnimState:PlayAnimation("angry")
        end,
        
        timeline=
        {
            TimeEvent(40*FRAMES, function(inst) inst:PerformBufferedAction() end),
            ---TimeEvent(21*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/pog/bark") end),  
        },

        events=
        {
            EventHandler("animover", function(inst)                
                inst.sg:GoToState("idle")
            end),
        },
    },      
	
	
	State{
        name = "revive_other",
        tags = {"busy"},
        
        onenter = function(inst,data)            
            inst.Physics:Stop()
            inst:PerformBufferedAction()
			inst.components.health:DoDelta(-30)
			inst.AnimState:PlayAnimation("hit")
        end,
        
        timeline=
        {
            ---TimeEvent(21*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve_DLC003/creatures/pog/bark") end),  
        },

        events=
        {
            EventHandler("animover", function(inst)                
                inst.sg:GoToState("idle")
            end),
        },
    },      
	
	State{
        name = "hit",
        tags = {"canrotate", "canslide", "busy"},
        
        onenter = function(inst)
            if inst:HasTag("girl") then
                inst.SoundEmitter:PlaySound("dontstarve/ghost/ghost_girl_howl")
            else
                inst.SoundEmitter:PlaySound("dontstarve/ghost/ghost_howl")
            end

            inst.AnimState:PlayAnimation("hit")   
        end,
        
        events =
        {
            EventHandler("animover", function(inst)
                if inst.AnimState:AnimDone() then
                    inst.sg:GoToState("idle")
                end
            end),
        },
    },
	
	State{
        name = "attack",
        tags = {"attack", "busy"},
        
        onenter = function(inst)
            inst.AnimState:PlayAnimation("idle")   
			inst.AnimState:SetTime(55 * FRAMES)
        end,
		
		timeline =
		{
			TimeEvent(2*FRAMES, function(inst)
				if not inst.noactions then
					inst:PerformBufferedAction()
				end
			end),
		},
        
        events =
        {
            EventHandler("animover", function(inst)
                if inst.AnimState:AnimDone() then
                    inst.sg:GoToState("idle")
                end
            end),
        },
    },
	
	State{
        name= "special_atk2",
        tags = {"idle", "busy"},
        
        onenter = function(inst)
			inst.Physics:Stop()
			if inst.noactions then 
				inst.sg:GoToState("idle", true) 
			else 
				inst.AnimState:PlayAnimation("angry")
				if inst.ismad then
					inst.ismad = false
					inst.components.aura:Enable(false)
				else
					inst.ismad = true
					inst.components.aura:Enable(true)
				end
			end
        end,
        
        events=
        {
            EventHandler("animover", function(inst) inst.sg:GoToState("idle") end ),
        },        
    },

    State{
        name = "death",
        tags = {"busy"},

        onenter = function(inst)
			inst.AnimState:PlayAnimation("dissipate")
			inst.SoundEmitter:PlaySound("dontstarve/ghost/ghost_howl")
			inst.components.aura:Enable(false)
            inst.Physics:Stop()
            RemovePhysicsColliders(inst)     
			inst.components.lootdropper:DropLoot(Vector3(inst.Transform:GetWorldPosition()))
			inst.components.inventory:DropEverything(true)
        end,
		
		timeline=
        {
		
        },

        events =
        {
            EventHandler("animover", function(inst)
                if inst.AnimState:AnimDone() then
					inst.noskeleton = true
					PlayablePets.DoDeath(inst)
                end
            end),
        },

    },
	
	State
    {
        name = "special_sleep",
		tags = {"busy", "hiding"},
        onenter = function(inst)
            inst.Physics:Stop()
            inst.AnimState:PlayAnimation("dissipate")
			inst:AddTag("invisible")
			inst:AddTag("notarget")
			inst.components.health:SetInvincible(true)
			inst.noactions = true
			inst:RemoveTag("scarytoprey") 
            if inst:HasTag("girl") then
                inst.SoundEmitter:PlaySound("dontstarve/ghost/ghost_girl_howl")
            else
                inst.SoundEmitter:PlaySound("dontstarve/ghost/ghost_howl")
            end
			inst.SoundEmitter:KillSound("howl")
			inst.Light:Enable(false)
			inst.components.aura:Enable(false)            
        end,
		
		events =
        {
            EventHandler("animover", function(inst)
                if inst.AnimState:AnimDone() then
					inst.components.aura:Enable(false)
					inst:Hide()
					inst.specialsleep = false
					inst.sg:GoToState("idle")					
                end
            end),
        },

        
    },
	
	State
    {
        name = "special_wake",
		tags = {"busy"},
        onenter = function(inst)
			----print ("A wild ghost appears")
			inst:Show()
			inst.components.health:SetInvincible(false)
			--inst.HUD:Hide(false)
            inst.AnimState:PlayAnimation("appear")
			inst.specialsleep = true
			inst.noactions = nil
			inst.Light:Enable(true)
			inst:RemoveTag("invisible")
			inst:RemoveTag("notarget")
			inst:AddTag("scarytoprey") 
            if inst:HasTag("girl") then
                inst.SoundEmitter:PlaySound("dontstarve/ghost/ghost_girl_howl")
            else
                inst.SoundEmitter:PlaySound("dontstarve/ghost/ghost_howl")
            end
			inst.SoundEmitter:PlaySound("dontstarve/ghost/ghost_howl_LP", "howl")
        end,

        events =
        {
            EventHandler("animover", function(inst)
                if inst.AnimState:AnimDone() then
                    inst.components.aura:Enable(true)
                    inst.sg:GoToState("idle")
					inst.CanExamine = nil
                end
            end),
        },
    }, 
}

CommonStates.AddFrozenStates(states)
CommonStates.AddSimpleWalkStates(states, getidleanim)
CommonStates.AddSimpleRunStates(states, getidleanim)
PP_CommonStates.AddKnockbackState(states, nil, "hit") --(states, timeline, anim, sounds, fns)
PP_CommonStates.AddActionStates(states, 
	{
		TimeEvent(0*FRAMES, function(inst) 
			if inst.noactions then 
				inst.sg:GoToState("idle", true) 
			else 
				PlayablePets.DoWork(inst, 2.5)
				inst.sg:GoToState("idle", true) 
			end 
		end),
	}, 
	"idle", nil, nil, "idle", "idle") --(states, timelines, anim, anim2, enteranim, loopanim, exitanim, noanim, events)
PP_CommonStates.AddCorpseStates(states, nil, 
	--timelines = 
	{
		corpse =
		{
			TimeEvent(0*FRAMES, function(inst) 
				inst.SoundEmitter:PlaySound("dontstarve/ghost/ghost_howl")
				inst.components.inventory:DropEverything(true)
				inst.Light:Enable(false)
				inst.SoundEmitter:KillSound("howl")
				inst:RemoveTag("corpse")
			end),
		},
		
		corpse_taunt =
		{
			TimeEvent(0*FRAMES, function(inst) 
				inst.SoundEmitter:PlaySound("dontstarve/ghost/ghost_howl")
				inst.SoundEmitter:PlaySound("dontstarve/ghost/ghost_howl_LP", "howl")
				inst.Light:Enable(true)
			end),
		},
	
	},
	--anims = 
	{
		corpse = "dissipate",
		corpse_taunt = "appear"
	},
	--sounds =
	{
		--corpse = "dontstarve_DLC003/creatures/enemy/grabbing_vine/death"
	},
	--fns =
	{
		corpse = function(inst)
			inst:DoTaskInTime(15 * (inst.revive_delay or 1), function(inst)
				inst:AddTag("corpse")
				inst.components.health:SetPercent(0.2)
				inst:PushEvent("respawnfromcorpse", { source = nil, user = nil })
				inst.sg:GoToState("corpse_rebirth")			
			end)
			inst.revive_delay = inst.revive_delay + 0.2
		end,
	}
	) ---(states, isflying, timelines, anims, sounds, fns, events, nofx)
PP_CommonStates.AddJumpInStates(states, nil, "idle")
PP_CommonStates.AddOpenGiftStates(states, "idle")
PP_CommonStates.AddSailStates(states, {}, "idle", "idle")
local simpleanim = "idle"
local simpleidle = "idle"
local simplemove = "idle"
if CurrentRelease.GreaterOrEqualTo(ReleaseID.IDs.R08_ROT_TURNOFTIDES) then
CommonStates.AddHopStates(states, false, {pre = simplemove, loop = simplemove, pst = simplemove}, nil, "death")
end
PP_CommonStates.AddSailorStates(states, nil, 
{
	plank_idle = simpleanim,
	plank_idle_loop = simpleidle,
	plank_idle_pst = simpleanim,
	
	plank_hop_pre = simplemove,
	plank_hop = simplemove,
	
	steer_pre = simpleanim,
	steer_idle = simpleidle,
	steer_turning = simpleanim,
	stop_steering = simpleanim,
	
	row = "idle",
}
)

PP_CommonStates.AddAoeStates(states, nil, 
{
	generic = "hit",
	
	leap_pre = simplemove,
	leap_loop = simplemove,
	leap_pst = simplemove,
	
	lunge_pre = simpleanim,
	lunge_loop = simpleanim,
	lunge_pst = simpleanim,
	
	superjump_pre = simpleanim,
	superjump_loop = simpleanim,
	superjump_pst = simpleanim,
	
	castspelltime = 10,
})

    
return StateGraph("ghostplayer", states, events, "idle", actionhandlers)

