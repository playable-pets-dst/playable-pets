require("stategraphs/commonstates")
require("stategraphs/ppstates")

local function GetExcludeTags(inst)
	if TheNet:GetPVPEnabled() then
		return {"INLIMBO", "playerghost", "notarget", "shadow", "groundpoundimmune"}
	elseif PP_FORGE_ENABLED then
		return {"player", "companion", "playerghost", "INLIMBO", "groundpoundimmune"}
	else	
		return {"player", "companion", "INLIMBO", "playerghost", "notarget", "shadow", "groundpoundimmune"}
	end
end

local function ShakeIfClose(inst)
    ShakeAllCameras(CAMERASHAKE.VERTICAL, .15, .02, .5, inst, 20)
end

local longaction = "action"
local shortaction = "action"
local workaction = "action"
local otheraction = "action"

local actionhandlers = PP_CommonStates.GetCommonActions(longaction, shortaction, workaction, otheraction)

local actionhandler_overrides = 
{ 
	--ActionHandler(ACTIONS.CAST_NET, longaction),
}

local WALK_SPEED = 4
local RUN_SPEED = 7

local function SetSleeperAwakeState(inst)
    if inst.components.grue ~= nil then
        inst.components.grue:RemoveImmunity("sleeping")
    end
    if inst.components.talker ~= nil then
        inst.components.talker:StopIgnoringAll("sleeping")
    end
    if inst.components.firebug ~= nil then
        inst.components.firebug:Enable()
    end
    if inst.components.playercontroller ~= nil then
        inst.components.playercontroller:EnableMapControls(true)
        inst.components.playercontroller:Enable(true)
    end
    inst:OnWakeUp()
    inst.components.inventory:Show()
    inst:ShowActions(true)
	--inst.sg:GoToState("taunt")
end

local function SetSleeperSleepState(inst)
    if inst.components.grue ~= nil then
        inst.components.grue:AddImmunity("sleeping")
    end
    if inst.components.talker ~= nil then
        inst.components.talker:IgnoreAll("sleeping")
    end
    if inst.components.firebug ~= nil then
        inst.components.firebug:Disable()
    end
    if inst.components.playercontroller ~= nil then
        inst.components.playercontroller:EnableMapControls(false)
        inst.components.playercontroller:Enable(false)
    end
    inst:OnSleepIn()
    inst.components.inventory:Hide()
    inst:PushEvent("ms_closepopups")
    inst:ShowActions(false)
end

local function SetFlyingState(inst)
	inst.Physics:SetCollisionGroup(COLLISION.FLYERS)
	if TheNet:GetServerGameMode() ~= "lavaarena" and TheNet:GetServerGameMode() ~= "quagmire" then
		inst.Physics:ClearCollidesWith(COLLISION.LIMITS)
	end
	inst.components.locomotor:SetExternalSpeedMultiplier(inst, 1.5, 1.5)
	inst.DynamicShadow:Enable(false)
	inst.components.health:SetInvincible(true)
	inst:AddTag("notarget")
    inst.noactions = true
end

local function SetLandState(inst)
	inst.Physics:SetCollisionGroup(COLLISION.CHARACTERS)
	inst.Physics:CollidesWith(COLLISION.LIMITS)
	inst.components.locomotor:RemoveExternalSpeedMultiplier(inst, 1.5)
	inst.DynamicShadow:Enable(true)
	inst.components.health:SetInvincible(false)
	inst:RemoveTag("notarget")
    inst.noactions = false
end

local events=
{
    EventHandler("attacked", function(inst) if not inst.components.health:IsDead() and not inst.sg:HasStateTag("attack") then inst.sg:GoToState("hit") end end),
    EventHandler("doattack", function(inst, data) inst.sg:GoToState("attack", data.target) end),
    PP_CommonHandlers.OnDeath(),
    CommonHandlers.OnSleep(),
    CommonHandlers.OnFreeze(),
	PP_CommonHandlers.AddCommonHandlers(),
	PP_CommonHandlers.OnLocomoteAdvanced(), 
	PP_CommonHandlers.OnSink(),
	PP_CommonHandlers.OnKnockback(),
	PP_CommonHandlers.OpenGift(),
}

local states=
{
    
    
    State{
        name = "death",
        tags = {"busy"},
        
        onenter = function(inst)
            inst.AnimState:PlayAnimation("death")
            inst.Physics:Stop()
            RemovePhysicsColliders(inst)            
            inst.components.lootdropper:DropLoot(Vector3(inst.Transform:GetWorldPosition())) --no loot, instead we spawn its corpse.
			inst.components.inventory:DropEverything(true)
         
			if inst.components.playercontroller ~= nil then
               inst.components.playercontroller:RemotePausePrediction()
            end
        end,
		
		timeline = 
		{
		
		},
		
        events =
        {
            EventHandler("animover", function(inst)
                if inst.AnimState:AnimDone() then
					PlayablePets.DoDeath(inst)
                end
            end),
        },
    },    
	
	State {
		name = "frozen",
		tags = {"busy"},
		
        onenter = function(inst)
            inst.AnimState:PlayAnimation("frozen")
            inst.Physics:Stop()
        end,
		
    },
	
	 State{
        name = "special_atk1",
        tags = {"busy"},
        onenter= function(inst)
            inst.AnimState:PlayAnimation("caw")
            inst.Physics:Stop()
			inst.components.locomotor:StopMoving()
            inst.SoundEmitter:PlaySound("dontstarve/birds/chirp_"..inst.soundsname)
        end,
        
		events =
        {
            EventHandler("animover", function(inst)
				inst.sg:GoToState("idle")
            end),
        },

    },
    
	State{
        name = "idle",
        tags = {"idle", "canrotate"},
        
        onenter = function(inst, pushanim)
            inst.Physics:Stop()
            inst.AnimState:PlayAnimation("idle")
        end,
        
        timeline = 
        {

        },
        events=
        {
            EventHandler("animover", function(inst) inst.sg:GoToState("idle") end),
        },
   },

	 State{
        name = "run_start",
        tags = {"moving", "canrotate", "hopping"},
        
        onenter = function(inst) 
            inst.AnimState:PlayAnimation("hop")
            inst.components.locomotor:RunForward()
        end,
        
        timeline=
        {
            TimeEvent(8*FRAMES, function(inst) 
				if inst._isflying == false then
					inst.Physics:Stop() 
				end
            end),
        },
        
        events=
        {
            EventHandler("animover", function(inst) inst.sg:GoToState("idle") end ),
        }
    },
        
    State{            
        name = "run",
        tags = {"moving", "canrotate","running"},
        
        onenter = function(inst) 
            inst.components.locomotor:RunForward()
            inst.AnimState:PlayAnimation("walk_loop")
        end,          
        events=
        {
            EventHandler("animover", function(inst) inst.sg:GoToState("run") end ),        
        },
        timeline=
        {

        },
    },      
    
    State{            
        name = "run_stop",
        tags = {"canrotate"},
        
        onenter = function(inst) 
            inst.components.locomotor:StopMoving()
           -- inst.AnimState:PlayAnimation("idle")            
        end,

        events=
        {   
            EventHandler("animover", function(inst) inst.sg:GoToState("idle") end ),        
        },
    },    
	
	State{            
        name = "attack",
        tags = {"busy"},
        
        onenter = function(inst) 
			if inst._isflying == true then
				inst.sg:GoToState("idle")
			else
                inst.components.locomotor:StopMoving()
                inst.AnimState:PlayAnimation("peck")    
                inst:PerformBufferedAction()
			end
        end,

        events=
        {   
            EventHandler("animover", function(inst) inst.sg:GoToState("idle") end ),        
        },
    },   
	 
	State{
        name = "land",
        tags = {"busy", "flight"},
        onenter= function(inst)
			local pos = inst:GetPosition()
			if inst._isflying and PlayablePets.CheckWaterTile(pos) then
				inst._cannot_land = true
				inst.sg:GoToState("glide")
			else
				inst.components.locomotor:StopMoving()
				inst:Show()
				local pt = Point(inst.Transform:GetWorldPosition())
				pt.y = 25
				inst.Physics:Stop()
				inst.components.locomotor:StopMoving()
				inst.Physics:Teleport(pt.x,pt.y,pt.z)
				inst.AnimState:PlayAnimation("glide", true)
				inst.sg:SetTimeout(inst.AnimState:GetCurrentAnimationLength())
			
				inst:RemoveTag("invisible")
				inst:RemoveTag("shadowcreature")
				inst:RemoveTag("shadow")
				inst.components.locomotor:StopMoving()
				inst.components.locomotor.disable = true
				inst.Physics:SetMotorVel(0,-20+math.random()*10,0)
			end
        end,
		
		onexit = function(inst)
			if not inst._cannot_land then
			inst._isflying = false
			inst:DoTaskInTime(10, function(inst) inst.taunt2 = true end)
			inst.components.health:SetInvincible(false)
			inst.components.locomotor.disable = false
			inst.Physics:CollidesWith(COLLISION.LIMITS)
            SetLandState(inst)
			else
				inst._cannot_land = nil
			end
		end,	
		
		 timeline = 
        {
            TimeEvent(5, function(inst) --added as a failsafe. Teleports to the ground if takes too long.
				local pt = Point(inst.Transform:GetWorldPosition())
				pt.y = 0
                inst.components.locomotor:StopMoving()
                inst.Physics:Teleport(pt.x,pt.y,pt.z)
                inst.AnimState:PlayAnimation("land")
	            SetLandState(inst)
                inst.sg:GoToState("idle", true)
			end),
        },
        
        onupdate= function(inst)
            local pt = Point(inst.Transform:GetWorldPosition())
            inst.Physics:SetMotorVel(0,-20+math.random()*10,0)
            if pt.y <= 1 then
                pt.y = 0
                inst.components.locomotor:StopMoving()
                inst.Physics:Teleport(pt.x,pt.y,pt.z)
                inst.AnimState:PlayAnimation("land")
                SetLandState(inst)
                inst.sg:GoToState("idle", true)
            end
        end,
		

        ontimeout = function(inst)
             --inst.sg:GoToState("idle")
        end,
    },    
	 
	 State{
        name = "special_atk2",
        tags = {"flight", "busy"}, --sliding happens
        onenter = function(inst)
			if not inst.taunt2 then
				inst.sg:GoToState("idle")
			else
				inst.taunt2 = false
				inst.components.locomotor:StopMoving()
				inst.sg:SetTimeout(.1+math.random()*.2)
				inst.sg.statemem.vert = math.random() > .5
                SetFlyingState(inst)
				inst.AnimState:PlayAnimation("takeoff_vertical_pre")
			end
        end,
        
        ontimeout= function(inst)
             inst.AnimState:PushAnimation("takeoff_vertical_loop", true)
			 inst.components.locomotor.disable = true
			 inst.components.locomotor:StopMoving()
             inst.Physics:SetMotorVel(0,15+math.random()*5,0)
        end,

        timeline = 
        {
            TimeEvent(1.5, function(inst) 
				inst:Hide()
				inst.components.locomotor.disable = false
				if TheNet:GetServerGameMode() == "lavaarena" then
					inst:DoTaskInTime(5, function(inst) if inst._isflying == true and not inst.sg:HasStateTag("landing") then inst.sg:GoToState("land") end end)
				end	
				inst.sg:GoToState("glide") 
			end)
        }
        
    },	
		
	State{
        name = "glide",
        tags = {"flight2", "canrotate"},
        onenter= function(inst)
			
			local pt = Point(inst.Transform:GetWorldPosition())
			if pt.y > .1 then
                pt.y = 0
                inst.Physics:Stop()
                inst.Physics:Teleport(pt.x,pt.y,pt.z)
				
			end	
			inst._isflying = true
			--inst.Physics:SetMotorVel(8,0,0)
            inst.AnimState:PlayAnimation("idle", true)
        end,
        
        onupdate= function(inst)
        end,
		

        ontimeout = function(inst)
             inst.SoundEmitter:PlaySound("dontstarve/birds/flyin", "flyin")
        end,
    },    
		
	State{
        name = "hit",
        tags = {"busy"},
        
        onenter = function(inst)
            inst.AnimState:PlayAnimation("hit")
            inst.Physics:Stop()
            local pt = Vector3(inst.Transform:GetWorldPosition())
            if pt.y > 1 then
                inst.sg:GoToState("fall")
            end
        end,
        
        events=
        {
            EventHandler("animover", function(inst) 
                    inst.sg:GoToState("idle") 
            end ),
        },        
    },    	
	
	 State{
        name = "fall",
        tags = {"busy"},
        onenter = function(inst)
            inst.Physics:Stop()
            inst.AnimState:PlayAnimation("fall_loop", true)
        end,
        
        onupdate = function(inst)
            local pt = Vector3(inst.Transform:GetWorldPosition())
            if pt.y <= .2 then
                pt.y = 0
                inst.Physics:Stop()
                inst.Physics:Teleport(pt.x,pt.y,pt.z)
	            inst.DynamicShadow:Enable(false)
                inst.sg:GoToState("stunned")
            end
        end,
    },    
		
	State{
        name = "stunned",
        tags = {"busy"},
        
        onenter = function(inst) 
            inst.Physics:Stop()
            inst.AnimState:PlayAnimation("stunned_loop", true)
            inst.sg:SetTimeout(GetRandomWithVariance(6, 2) )
        end,
        
        ontimeout = function(inst) inst.sg:GoToState("idle") end,
    },	
		
	State {
        name = "sleep",
        tags = { "busy", "sleeping" }, --add tag "busy" if you hate sliding

        onenter = function(inst)
            if inst.components.locomotor ~= nil then
                inst.components.locomotor:StopMoving()
            end
            inst.AnimState:PlayAnimation("sleep_pre")
			--inst.SoundEmitter:PlaySound("dontstarve/creatures/spiderqueen/fallasleep")
            --if fns ~= nil and fns.onsleep ~= nil then
                --fns.onsleep(inst)
            --end
        end,

        --timeline = timelines ~= nil and timelines.starttimeline or nil,
		
		timeline = 
		{
			--TimeEvent(15*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/creatures/together/grass_gekko/sleep_pre") end)
		},
		
        events =
        {
            EventHandler("animqueueover", function(inst) inst.sg:GoToState("sleeping") end ),
            EventHandler("onwakeup", function(inst) inst.sg:GoToState("wake") end),
        },
    },

    State
    {
        name = "sleeping",
        tags = { "sleeping" },

        --onenter = onentersleeping,
		
		onenter = function(inst)
				inst.components.locomotor:StopMoving()
				PlayablePets.SleepHeal(inst)
				--inst.SoundEmitter:PlaySound("dontstarve_DLC001/creatures/bearger/sleep")
				inst.AnimState:PlayAnimation("sleep_loop")
			end,

        --timeline = timelines ~= nil and timelines.sleeptimeline or nil,
		timeline = 
		{
           --TimeEvent(15*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/creatures/together/grass_gekko/sleep") end),
		   --TimeEvent(28*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/creatures/together/grass_gekko/sleep") end)
        },

        events =
        {
            EventHandler("animover", function(inst) inst.sg:GoToState("sleeping") end ),
			EventHandler("onwakeup", function(inst) inst.sg:GoToState("wake") end),
        },
    },

    State
    {
        name = "wake",
        tags = { "busy", "waking" },

        onenter = function(inst)
            if inst.components.locomotor ~= nil then
                inst.components.locomotor:StopMoving()
            end
			--inst.SoundEmitter:PlaySound("dontstarve/creatures/spiderqueen/wakeup")
            inst.AnimState:PlayAnimation("sleep_pst")
            if inst.components.sleeper ~= nil and inst.components.sleeper:IsAsleep() then
                inst.components.sleeper:WakeUp()
            end
            --if fns ~= nil and fns.onwake ~= nil then
                --fns.onwake(inst)
            --end
        end,

        --timeline = timelines ~= nil and timelines.waketimeline or nil,
		
		timeline = 
		{
			
		},
		
		events =
        {
            EventHandler("animover", function(inst) inst.sg:GoToState("idle") end),
        },
    },
    
}

--CommonStates.AddFrozenStates(states)
PP_CommonStates.AddKnockbackState(states, nil, "hit") --(states, timeline, anim, sounds, fns)
PP_CommonStates.AddActionStates(states, 
	{
		TimeEvent(0 * FRAMES, function(inst)
            inst:PerformBufferedAction()
        end),
	}, 
	"peck", nil, nil, "peck", "peck") --(states, timelines, anim, anim2, enteranim, loopanim, exitanim, noanim, events)
PP_CommonStates.AddCorpseStates(states, true, 
	--timelines = 
	{
		corpse =
		{

		},
	
	},
	--anims = 
	{
		corpse = "death",
		corpse_taunt = "distress"
	},
	--sounds =
	{
		--corpse = "dontstarve_DLC003/creatures/enemy/grabbing_vine/death"
	},
	--fns =
	{

	}
	) ---(states, isflying, timelines, anims, sounds, fns, events, nofx)
PP_CommonStates.AddJumpInStates(states, nil, "hop")
PP_CommonStates.AddOpenGiftStates(states, "idle")
CommonStates.AddHopStates(states, false, {pre = "hop", loop = "hop", pst = "idle"}, nil, "death")
PP_CommonStates.AddSailorStates(states, nil, 
{
	plank_idle = "hop",
	plank_idle_loop = "idle",
	plank_idle_pst = "hop",
	
	plank_hop_pre = "hop",
	plank_hop = "hop",
	
	steer_pre = "hop",
	steer_idle = "idle",
	steer_turning = "idle",
	stop_steering = "hop",
	
	row = "hop",
}
)
    
PP_CommonStates.AddAoeStates(states, nil, 
{
	generic = "hop",
	
	castspelltime = 10,
})	
    
return StateGraph("bird2p", states, events, "idle", actionhandlers)

