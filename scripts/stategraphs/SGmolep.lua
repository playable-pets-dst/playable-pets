require("stategraphs/commonstates")
require("stategraphs/ppstates")

local longaction = "action"
local shortaction = "action"
local workaction = "action"
local otheraction = "action"

local actionhandlers = PP_CommonStates.GetCommonActions(longaction, shortaction, workaction, otheraction)

local actionhandler_overrides = 
{ 
	ActionHandler(ACTIONS.ATTACK, "idle"),
	ActionHandler(ACTIONS.PICKUP, "steal_pre_under"),
	ActionHandler(ACTIONS.PICK, "steal_pre_under"),
}

if #actionhandler_overrides > 0 then
	for i, v in ipairs(actionhandler_overrides) do
		table.insert(actionhandlers, v)
	end
end

local WALK_SPEED = 4
local RUN_SPEED = 7

local MOLE_PEEK_INTERVAL = 20
local MOLE_PEEK_VARIANCE = 5

local function SetSleeperAwakeState(inst)
    if inst.components.grue ~= nil then
        inst.components.grue:RemoveImmunity("sleeping")
    end
    if inst.components.talker ~= nil then
        inst.components.talker:StopIgnoringAll("sleeping")
    end
    if inst.components.firebug ~= nil then
        inst.components.firebug:Enable()
    end
    if inst.components.playercontroller ~= nil then
        inst.components.playercontroller:EnableMapControls(true)
        inst.components.playercontroller:Enable(true)
    end
    inst:OnWakeUp()
    inst.components.inventory:Show()
    inst:ShowActions(true)
	--inst.sg:GoToState("taunt")
end

local function SetSleeperSleepState(inst)
    if inst.components.grue ~= nil then
        inst.components.grue:AddImmunity("sleeping")
    end
    if inst.components.talker ~= nil then
        inst.components.talker:IgnoreAll("sleeping")
    end
    if inst.components.firebug ~= nil then
        inst.components.firebug:Disable()
    end
    if inst.components.playercontroller ~= nil then
        inst.components.playercontroller:EnableMapControls(false)
        inst.components.playercontroller:Enable(false)
    end
    inst:OnSleepIn()
    inst.components.inventory:Hide()
    inst:PushEvent("ms_closepopups")
    inst:ShowActions(false)
end

local events=
{
    EventHandler("attacked", function(inst) if not inst.components.health:IsDead() and not inst.sg:HasStateTag("attack") then inst.sg:GoToState("hit") end end),
    EventHandler("doattack", function(inst, data) inst.sg:GoToState("attack", data.target) end),
    PP_CommonHandlers.OnDeath(),
    CommonHandlers.OnSleep(),
    CommonHandlers.OnFreeze(),
	PP_CommonHandlers.AddCommonHandlers(),
    --CommonHandlers.OnLocomote(true,false),
	EventHandler("locomote", function(inst, data)
        if inst.sg:HasStateTag("busy") or inst.sg:HasStateTag("sleeping") and not inst.sg:HasStateTag("home") then
            return
        end
        local is_moving = inst.sg:HasStateTag("moving")
        local should_move = inst.components.locomotor:WantsToMoveForward()

        if inst.sg:HasStateTag("home") or inst.sg:HasStateTag("home_waking") then -- wakeup on locomote
            if inst.sleepingbag ~= nil and inst.sg:HasStateTag("sleeping") and inst.sg:HasStateTag("home") then
                inst.sleepingbag.components.sleepingbag:DoWakeUp()
                inst.sleepingbag = nil
				inst:Show()
				--inst.AnimState:PushAnimation("pig_pickup")
				SetSleeperAwakeState(inst)
				--inst.AnimState:PushAnimation("pig_pickup")
				--inst.sg:GoToState("idle")
				inst.AnimState:PlayAnimation("idle_under")
                inst.sg:GoToState("idle", true)
            
				
            end 
			elseif is_moving and not should_move then
            inst.sg:GoToState("run_stop")
        elseif not is_moving and should_move then
            inst.sg:GoToState("run_start")
        elseif data.force_idle_state and not (is_moving or should_move or inst.sg:HasStateTag("idle")) then
            inst.sg:GoToState("idle")
        end
    end),
	PP_CommonHandlers.OnKnockback(),
	PP_CommonHandlers.OnSink(),
	CommonHandlers.OnHop(), 
	PP_CommonHandlers.OpenGift(),
}

local function SpawnMoveFx(inst)
    SpawnPrefab("mole_move_fx").Transform:SetPosition(inst.Transform:GetWorldPosition())
end

local function PlayStunnedSound(inst)
    if not inst.SoundEmitter:PlayingSound("stunned") then
        inst.SoundEmitter:PlaySound("dontstarve_DLC001/creatures/mole/sleep", "stunned")
    end
end

local function KillStunnedSound(inst)
    inst.SoundEmitter:KillSound("stunned")
end

local states=
{
    
    
    State{
        name = "death",
        tags = {"busy"},
        
        onenter = function(inst)
            inst.SoundEmitter:KillSound("move")
            inst.SoundEmitter:KillSound("sniff")
            inst.SoundEmitter:PlaySound("dontstarve_DLC001/creatures/mole/death")
            inst.AnimState:PlayAnimation("death")
            inst.Physics:Stop()
            RemovePhysicsColliders(inst)            
            inst.components.lootdropper:DropLoot(Vector3(inst.Transform:GetWorldPosition()))
			inst.components.inventory:DropEverything(true)
         
			if inst.components.playercontroller ~= nil then
               inst.components.playercontroller:RemotePausePrediction()
            end
        end,
		
		timeline = 
		{
			
		},
		
        events =
        {
            EventHandler("animover", function(inst)
                if inst.AnimState:AnimDone() then
					PlayablePets.DoDeath(inst)
                end
            end),
        },

        

    },    
    
   State
    {
        name = "enter",
        tags = { "busy" },
        onenter = function(inst)
            inst.Physics:Stop()
            inst:SetAbovePhysics()
            inst.AnimState:PlayAnimation("enter")
            inst.SoundEmitter:KillSound("move")
        end,

        events =
        {
            EventHandler("animover", function(inst)
                inst.sg:GoToState("idle")
            end),
        },
    },

    State
    {
        name = "peek",
        tags = { "busy" },
        onenter = function(inst)
            inst.Physics:Stop()
            inst:SetAbovePhysics()
            inst.SoundEmitter:KillSound("move")
            inst.AnimState:PlayAnimation("enter")

            inst.peek_interval = GetRandomWithVariance(MOLE_PEEK_INTERVAL, MOLE_PEEK_VARIANCE)
            inst.last_above_time = GetTime()
            inst:PerformBufferedAction()
        end,

        timeline =
        {
            TimeEvent(1*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve_DLC001/creatures/mole/emerge") end),
            TimeEvent(3*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve_DLC001/creatures/mole/emerge_voice") end),
        },

        events =
        {
            EventHandler("animover", function(inst)
                inst.sg:GoToState("exit")
            end),
        },
    },

    State
    {
        name = "steal_pre_under",
        tags = { "busy" },
        onenter = function(inst, data)
            inst.Physics:Stop()
            inst:SetAbovePhysics()
            inst.SoundEmitter:KillSound("move")
            inst.AnimState:PlayAnimation("enter")
            inst.AnimState:PushAnimation("idle", false)
        end,

        timeline =
        {
            TimeEvent(1*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve_DLC001/creatures/mole/emerge") end),
            TimeEvent(3*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve_DLC001/creatures/mole/emerge_voice") end),
            TimeEvent(26*FRAMES, function(inst)
                if not inst.SoundEmitter:PlayingSound("sniff") then
                    inst.SoundEmitter:PlaySound("dontstarve_DLC001/creatures/mole/sniff", "sniff")
                end
            end),
            TimeEvent(77*FRAMES, function(inst) inst.SoundEmitter:KillSound("sniff") end),
        },

        events =
        {
            EventHandler("animqueueover", function(inst)
                inst.sg:GoToState("steal")
            end),
        },

        onexit = function(inst)
            inst.SoundEmitter:KillSound("sniff")
        end,
    },

    State
    {
        name = "steal_pre_above",
        tags = { "busy" },
        onenter = function(inst, data)
            inst.Physics:Stop()
            inst:SetAbovePhysics()
            inst.SoundEmitter:KillSound("move")
            inst.AnimState:PlayAnimation("idle", false)
        end,

        timeline =
        {
            TimeEvent(1*FRAMES, function(inst)
                if not inst.SoundEmitter:PlayingSound("sniff") then
                    inst.SoundEmitter:PlaySound("dontstarve_DLC001/creatures/mole/sniff", "sniff")
                end
            end),
            TimeEvent(52*FRAMES, function(inst) inst.SoundEmitter:KillSound("sniff") end),
        },

        events =
        {
            EventHandler("animqueueover", function(inst)
                inst.sg:GoToState("steal")
            end)
        },

        onexit = function(inst)
            inst.SoundEmitter:KillSound("sniff")
        end,
    },

    State
    {
        name = "steal",
        tags = { "busy", "canrotate" },
        onenter = function(inst, playanim)
            inst.Physics:Stop()
            inst:SetAbovePhysics()
            inst.AnimState:PlayAnimation("action")
            inst.AnimState:PushAnimation("idle", false)
        end,

        timeline =
        {
            TimeEvent(9*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve_DLC001/creatures/mole/pickup") end),
            TimeEvent(12*FRAMES, function(inst) inst:PerformBufferedAction() end),
            TimeEvent(27*FRAMES, function(inst)
                if not inst.SoundEmitter:PlayingSound("sniff") then
                    inst.SoundEmitter:PlaySound("dontstarve_DLC001/creatures/mole/sniff", "sniff")
                end
            end),
            TimeEvent(78*FRAMES, function(inst) inst.SoundEmitter:KillSound("sniff") end),
        },

        events =
        {
            EventHandler("animqueueover", function(inst)
                inst.sg:GoToState("exit")
            end),
        },

        onexit = function(inst)
            inst.SoundEmitter:KillSound("sniff")
        end,
    },

    State
    {
        name = "exit",
        tags = { "busy" },
        onenter = function(inst)
            inst.Physics:Stop()
            inst:SetAbovePhysics()
            inst.AnimState:PlayAnimation("exit")
            -- if inst.components.burnable:IsBurning() then
            --     inst.components.burnable:Extinguish()
            -- end
        end,

        timeline =
        {
            TimeEvent(8*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve_DLC001/creatures/mole/jump") end),
            --TimeEvent(24*FRAMES, function(inst) end),
            TimeEvent(26*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve_DLC001/creatures/mole/retract") end),
            TimeEvent(43*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve_DLC001/creatures/mole/retract") end),
        },

        events =
        {
            EventHandler("animover", function(inst)
                inst:SetUnderPhysics()
                inst.last_above_time = GetTime()
                inst.sg:GoToState("idle")
            end),
        },
    },

    State
    {
        name = "idle",
        tags = { "idle", "canrotate" },
        onenter = function(inst, playanim)
            inst.Physics:Stop()
            inst.SoundEmitter:KillSound("move")
            if inst.isunder then
                inst.sg:AddStateTag("noattack")
            end
       
            inst.AnimState:PlayAnimation(inst.isunder and "idle_under" or "idle", true)
            
        end,

        timeline =
        {
            TimeEvent(1*FRAMES, function(inst)
                if not (inst.isunder or inst.SoundEmitter:PlayingSound("sniff")) then
                    inst.SoundEmitter:PlaySound("dontstarve_DLC001/creatures/mole/sniff", "sniff")
                end
            end),
            TimeEvent(52*FRAMES, function(inst) inst.SoundEmitter:KillSound("sniff") end),
        },
    },

    State
    {
        name = "run_start",
        tags = { "moving", "canrotate", "noattack", "invisible" },

        onenter = function(inst)
            inst:SetUnderPhysics()
            inst.AnimState:PlayAnimation("walk_pre")
            if not inst.SoundEmitter:PlayingSound("move") then
                inst.SoundEmitter:PlaySound("dontstarve_DLC001/creatures/mole/move", "move")
            end
            inst.components.locomotor:WalkForward()
        end,

        events =
        {
            EventHandler("animover", function(inst)
                inst.sg:GoToState("run")
            end),
        }
    },

    State
    {
        name = "run",
        tags = { "moving", "canrotate", "noattack", "invisible" },

        onenter = function(inst)
            inst:SetUnderPhysics()
            inst.AnimState:PlayAnimation("walk_loop")
            inst.components.locomotor:WalkForward()
        end,

        timeline =
        {
            TimeEvent(0*FRAMES,  SpawnMoveFx),
            TimeEvent(5*FRAMES,  SpawnMoveFx),
            TimeEvent(10*FRAMES, SpawnMoveFx),
            TimeEvent(15*FRAMES, SpawnMoveFx),
            TimeEvent(20*FRAMES, SpawnMoveFx),
            TimeEvent(25*FRAMES, SpawnMoveFx),
        },

        events =
        {
            EventHandler("animover", function(inst)
                inst.sg:GoToState("run")
            end),
        }
    },

    State
    {
        name = "run_stop",
        tags = { "canrotate", "noattack", "invisible" },

        onenter = function(inst)
            inst:SetUnderPhysics()
            inst.components.locomotor:StopMoving()

            --local should_softstop = false
            --if should_softstop then
                --inst.AnimState:PushAnimation("walk_pst")
            --else
                inst.AnimState:PlayAnimation("walk_pst")
            --end

            inst.SoundEmitter:KillSound("move")
        end,

        events =
        {
            EventHandler("animover", function(inst)
                inst.sg:GoToState("idle")
            end),
        },
    },
	
    State
    {
        name = "gohome",
        tags = { "canrotate" },

        onenter = function(inst, playanim)
            inst.Physics:Stop()
            if inst.isunder then
                inst.sg:AddStateTag("noattack")
                inst.AnimState:PlayAnimation("idle_under")
            else
                inst.AnimState:PlayAnimation("idle")
            end
            inst:PerformBufferedAction()
        end,

        events =
        {
            EventHandler("animover", function (inst, data)
                inst.sg:GoToState("idle")
            end),
        },
    },

    State
    {
        name = "make_molehill",
        tags = { "busy", "noattack" },

        onenter = function(inst, playanim)
            inst.Physics:Stop()
            inst:SetUnderPhysics()
            inst.AnimState:PlayAnimation("mound")
        end,

        timeline =
        {
            TimeEvent(16*FRAMES, function(inst)
                inst:SetAbovePhysics()
                inst.sg:RemoveStateTag("noattack")
                inst.SoundEmitter:PlaySound("dontstarve_DLC001/creatures/mole/emerge")
            end),
            TimeEvent(30*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve_DLC001/creatures/mole/emerge_voice") end),
        },

        events =
        {
            EventHandler("animover", function(inst, data)
                inst:PerformBufferedAction()
                inst:SetUnderPhysics()
                inst.last_above_time = GetTime()
                inst.sg:GoToState("idle")
            end),
        },
    },
	
	State
    {
        name = "hit",
        tags = { "busy" },

        onenter = function(inst)
            inst.SoundEmitter:KillSound("move")
            inst.SoundEmitter:KillSound("sniff")
            inst.AnimState:PlayAnimation("hit")
            inst.Physics:Stop()
            inst:SetAbovePhysics()
        end,

        events =
        {
            EventHandler("animover", function(inst) inst.sg:GoToState("idle") end),
        },
    },
    
	State
    {
        name = "sleep",
        tags = { "busy", "sleeping" },
        
        onenter = function(inst)
            inst.components.locomotor:StopMoving()
            if inst.isunder then
                inst:SetAbovePhysics()
                inst.AnimState:PlayAnimation("enter")
                inst.SoundEmitter:PlaySound("dontstarve_DLC001/creatures/mole/emerge")
                inst.AnimState:PushAnimation("sleep_pre", false)
            else
                inst.AnimState:PlayAnimation("sleep_pre")
            end
        end,

        timeline =
        {
            TimeEvent(FRAMES, function(inst)
                inst.SoundEmitter:KillSound("sniff")
                inst.SoundEmitter:KillSound("stunned")
            end),
        },

        events =
        {
            EventHandler("animqueueover", function(inst)
                inst.sg:GoToState("sleeping")
            end),
            EventHandler("onwakeup", function(inst)
                inst.sg:GoToState("wake")
            end),
        },
    },

    State
    {
        name = "sleeping",
        tags = { "sleeping" },
        onenter = function(inst)
            inst:SetAbovePhysics()
            inst.AnimState:PlayAnimation("sleep_loop")
			PlayablePets.SleepHeal(inst)
        end,

        timeline =
        {
            TimeEvent(27*FRAMES, function(inst)
                if not inst.SoundEmitter:PlayingSound("sleep") then
                    inst.SoundEmitter:PlaySound("dontstarve_DLC001/creatures/mole/sleep", "sleep")
                end
            end),
            TimeEvent(42*FRAMES, function(inst)
                inst.SoundEmitter:KillSound("sleep")
            end),
        },

        events =
        {   
            EventHandler("animover", function(inst)
                inst.sg:GoToState("sleeping")
            end),
            EventHandler("onwakeup", function(inst)
                inst.sg:GoToState("wake")
            end),
        },
    },

    State
    {
        name = "wake",
        tags = { "busy", "waking" },
        
        onenter = function(inst)
            inst:SetAbovePhysics()
            inst.components.locomotor:StopMoving()
            inst.AnimState:PlayAnimation("sleep_pst")
            if inst.components.sleeper ~= nil and inst.components.sleeper:IsAsleep() then
                inst.components.sleeper:WakeUp()
            end
        end,

        timeline =
        {
            TimeEvent(FRAMES, function(inst)
                inst.SoundEmitter:KillSound("sleep")
            end)
        },

        events =
        {
            EventHandler("animover", function(inst)
                inst.sg:GoToState("exit")
            end),
        },
    },
	
	------------------HOME SLEEPING-----------------

        State{
        name = "home",
        tags = {"busy", "silentmorph", "invisible" },

        onenter = function(inst)
            inst.components.locomotor:Stop()

            local target = inst:GetBufferedAction().target
            --local siesta = HasTag("siestahut")
            local failreason =
               -- (siesta ~= TheWorld.state.isday and
                    --(siesta
                    --and (TheWorld:HasTag("cave") and "ANNOUNCE_NONIGHTSIESTA_CAVE" or "ANNOUNCE_NONIGHTSIESTA")
                    --or (TheWorld:HasTag("cave") and "ANNOUNCE_NODAYSLEEP_CAVE" or "ANNOUNCE_NODAYSLEEP"))
               -- )or
			   (target.components.burnable ~= nil and
                    target.components.burnable:IsBurning() and
                    "ANNOUNCE_NOSLEEPONFIRE") 
                --or (IsNearDanger(inst) and "ANNOUNCE_NODANGERSLEEP")
                -- you can still sleep if your hunger will bottom out, but not absolutely
                or (inst.components.hunger.current < TUNING.CALORIES_MED and "ANNOUNCE_NOHUNGERSLEEP")
                or (inst.components.beaverness ~= nil and inst.components.beaverness:IsStarving() and "ANNOUNCE_NOHUNGERSLEEP")
                or nil

            if failreason ~= nil then
                inst:PushEvent("performaction", { action = inst.bufferedaction })
                inst:ClearBufferedAction()
                inst.sg:GoToState("idle")
                if inst.components.talker ~= nil then
                    inst.components.talker:Say(GetString(inst, failreason))
                end
                return
            end

            inst.AnimState:PlayAnimation("idle_under")
            inst.sg:SetTimeout(11 * FRAMES)

            SetSleeperSleepState(inst)
        end,

        ontimeout = function(inst)
            local bufferedaction = inst:GetBufferedAction()
            if bufferedaction == nil then
                inst.AnimState:PlayAnimation("pig_pickup")
                inst.sg:GoToState("idle", true)
                return
            end
            local home = bufferedaction.target
            if home == nil or
                not home:HasTag("molehouse") or
                --home:HasTag("hassleeper") or
                --home:HasTag("siestahut") ~= TheWorld.state.isday or
                (home.components.burnable ~= nil and home.components.burnable:IsBurning()) then
                --Edge cases, don't bother with fail dialogue
                --Also, think I will let smoldering pass this one
                inst:PushEvent("performaction", { action = inst.bufferedaction })
                inst:ClearBufferedAction()
                inst.AnimState:PlayAnimation("idle_under")
                inst.sg:GoToState("idle", true)
            else
                inst:PerformBufferedAction()
				inst.SoundEmitter:KillSound("move")
                inst.components.health:SetInvincible(true)
                inst:Hide()
                if inst.Physics ~= nil then
                    inst.Physics:Teleport(inst.Transform:GetWorldPosition())
                end
                if inst.DynamicShadow ~= nil then
                    inst.DynamicShadow:Enable(false)
                end
                inst.sg:AddStateTag("sleeping")
				inst.sg:AddStateTag("home")
                inst.sg:RemoveStateTag("busy")
                if inst.components.playercontroller ~= nil then
                    inst.components.playercontroller:Enable(true)
                end
            end
        end,

        onexit = function(inst)
            inst.components.health:SetInvincible(false)
            inst:Show()
            if inst.DynamicShadow ~= nil then
                inst.DynamicShadow:Enable(true)
            end
            if inst.sleepingbag ~= nil then
                --Interrupted while we are "sleeping"
                inst.sleepingbag.components.sleepingbag:DoWakeUp(true)
                inst.sleepingbag = nil				
                SetSleeperAwakeState(inst)
            elseif not inst.sg.statemem.iswaking then
                --Interrupted before we are "sleeping"
                SetSleeperAwakeState(inst)
            end
        end,
    },
    

    State
    {
        name = "home_sleeping",
        tags = { "sleeping", "home" },

        --onenter = onentersleeping,
		
		onenter = function(inst)
				inst.components.locomotor:StopMoving()
				inst.components.health:DoDelta(3.5, false)
				--inst.SoundEmitter:PlaySound("dontstarve_DLC001/creatures/bearger/sleep")
				inst.AnimState:PlayAnimation("sleep_loop")
			end,

        --timeline = timelines ~= nil and timelines.sleeptimeline or nil,
		timeline =
		{
			TimeEvent(35*FRAMES, function(inst) inst.SoundEmitter:PlaySound(SoundPath(inst, "sleeping")) end ),
		},

        events =
        {
            EventHandler("animover", function(inst) inst.sg:GoToState("sleeping") end ),
			EventHandler("onwakeup", function(inst) inst.sg:GoToState("wake") end),
        },
    },

    State
    {
        name = "home_wake",
        tags = { "busy", "waking", "home" },

        onenter = function(inst)
            if inst.components.locomotor ~= nil then
                inst.components.locomotor:StopMoving()
            end
			--inst.SoundEmitter:PlaySound("dontstarve/creatures/spiderqueen/wakeup")
            inst.AnimState:PlayAnimation("sleep_pst")
            if inst.components.sleeper ~= nil and inst.components.sleeper:IsAsleep() then
                inst.components.sleeper:WakeUp()
            end
            --if fns ~= nil and fns.onwake ~= nil then
                --fns.onwake(inst)
            --end
        end,

        --timeline = timelines ~= nil and timelines.waketimeline or nil,
		
		timeline = 
		{
		TimeEvent(0*FRAMES, function(inst) inst.SoundEmitter:PlaySound(SoundPath(inst, "wakeUp")) end ),
		},
		
		events =
        {
            EventHandler("animover", function(inst) inst.sg:GoToState("idle") end),
        },
    },
    
}

CommonStates.AddFrozenStates(states)
PP_CommonStates.AddActionStates(states, 
	{
		TimeEvent(0 * FRAMES, function(inst)
            inst:PerformBufferedAction()
        end),
	}, 
	"walk_pst", nil, nil, "idle_loop", "walk_pst") --(states, timelines, anim, anim2, enteranim, loopanim, exitanim, noanim, events)
PP_CommonStates.AddCorpseStates(states, nil, 
	--timelines = 
	{
		corpse =
		{
			TimeEvent(0, function(inst) inst.SoundEmitter:PlaySound("dontstarve_DLC001/creatures/mole/death") end),
		},
		
		corpse_taunt =
		{
			 TimeEvent(8*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve_DLC001/creatures/mole/jump") end),
            --TimeEvent(24*FRAMES, function(inst) end),
            TimeEvent(26*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve_DLC001/creatures/mole/retract") end),
            TimeEvent(43*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve_DLC001/creatures/mole/retract") end),
		},
	
	},
	--anims = 
	{
		corpse = "death",
		corpse_taunt = "exit"
	},
	--sounds =
	{
		--corpse = "dontstarve_DLC003/creatures/enemy/grabbing_vine/death"
	},
	--fns =
	{

	}
) ---(states, isflying, timelines, anims, sounds, fns, events, nofx)
PP_CommonStates.AddJumpInStates(states, nil, "walk_pst")
PP_CommonStates.AddOpenGiftStates(states, "idle_loop")
PP_CommonStates.AddSailStates(states, {}, "walk_pst", "idle_loop")
local simpleanim = "walk_pst"
local simpleidle = "idle_under"
local simplemove = "walk"
CommonStates.AddHopStates(states, false, {pre = simplemove, loop = simplemove, pst = simplemove}, nil, "death")
PP_CommonStates.AddSailorStates(states, nil, 
{
	plank_idle = simpleanim,
	plank_idle_loop = simpleidle,
	plank_idle_pst = simpleanim,
	
	plank_hop_pre = simplemove.."_pre",
	plank_hop = simplemove.."_loop",
	
	steer_pre = simpleanim,
	steer_idle = simpleidle,
	steer_turning = simpleanim,
	stop_steering = simpleanim,
	
	row = "idle_under",
}
)
    
PP_CommonStates.AddAoeStates(states, nil, 
{
	generic = "idle_under",
	
	castspelltime = 10,
})
    
return StateGraph("molep", states, events, "idle", actionhandlers)

