require("stategraphs/commonstates")
require("stategraphs/ppstates")

local function GetExcludeTags(inst)
	if TheNet:GetPVPEnabled() then
		return {"INLIMBO", "playerghost", "notarget", "shadow", "groundpoundimmune"}
	elseif PP_FORGE_ENABLED then
		return {"player", "companion", "playerghost", "INLIMBO", "groundpoundimmune"}
	else	
		return {"player", "companion", "INLIMBO", "playerghost", "notarget", "shadow", "groundpoundimmune"}
	end
end

local function ShakeIfClose(inst)
    ShakeAllCameras(CAMERASHAKE.VERTICAL, .15, .02, .5, inst, 20)
end

local longaction = "peck"
local shortaction = "action"
local workaction = "attack"
local otheraction = "action"

local actionhandlers = PP_CommonStates.GetCommonActions(longaction, shortaction, workaction, otheraction)

local actionhandler_overrides = 
{ 
	--ActionHandler(ACTIONS.CAST_NET, longaction),
}

if #actionhandler_overrides > 0 then
	for i, v in ipairs(actionhandler_overrides) do
		table.insert(actionhandlers, v)
	end
end

local events=
{
    EventHandler("attacked", function(inst) if not inst.components.health:IsDead() and not inst.sg:HasStateTag("attack") then inst.sg:GoToState("hit") end end),
    EventHandler("doattack", function(inst, data) if inst._isflying == false then inst.sg:GoToState("attack", data.target) end end),
    PP_CommonHandlers.OnDeath(),
    CommonHandlers.OnSleep(),
    CommonHandlers.OnFreeze(),
	PP_CommonHandlers.AddCommonHandlers(),
    --CommonHandlers.OnLocomote(true,false),
	EventHandler("locomote", function(inst, data)
        if inst.sg:HasStateTag("busy") or inst.sg:HasStateTag("sleeping") and not inst.sg:HasStateTag("home") then
            return
        end
        local is_moving = inst.sg:HasStateTag("moving")
        local should_move = inst.components.locomotor:WantsToMoveForward()

        if inst.sg:HasStateTag("home") or inst.sg:HasStateTag("home_waking") then -- wakeup on locomote
            if inst.sleepingbag ~= nil and inst.sg:HasStateTag("sleeping") and inst.sg:HasStateTag("home") then
                inst.sleepingbag.components.sleepingbag:DoWakeUp()
                inst.sleepingbag = nil
				inst:Show()
				--inst.AnimState:PushAnimation("pig_pickup")
				SetSleeperAwakeState(inst)
				--inst.AnimState:PushAnimation("pig_pickup")
				--inst.sg:GoToState("idle")
				inst.AnimState:PlayAnimation("idle_under")
                inst.sg:GoToState("idle", true)
            
				
            end 
			elseif is_moving and not should_move then
            inst.sg:GoToState("run_stop")
        elseif not is_moving and should_move then
            inst.sg:GoToState("run_start")
        elseif data.force_idle_state and not (is_moving or should_move or inst.sg:HasStateTag("idle")) then
            inst.sg:GoToState("idle")
        end
    end),
	PP_CommonHandlers.OnSink(),
	PP_CommonHandlers.OnKnockback(),
	PP_CommonHandlers.OpenGift(),
}

local states=
{
    
    
    State{
        name = "death",
        tags = {"busy"},
        
        onenter = function(inst)
			inst.SoundEmitter:PlaySound("dontstarve_DLC001/creatures/buzzard/death")   
            inst.AnimState:PlayAnimation("death")
            inst.Physics:Stop()
            RemovePhysicsColliders(inst)            
            inst.components.lootdropper:DropLoot(Vector3(inst.Transform:GetWorldPosition())) --no loot, instead we spawn its corpse.
			inst.components.inventory:DropEverything(true)
         
			if inst.components.playercontroller ~= nil then
               inst.components.playercontroller:RemotePausePrediction()
            end
        end,
		
		timeline = 
		{

		},
		
        events =
        {
            EventHandler("animover", function(inst)
                if inst.AnimState:AnimDone() then
					PlayablePets.DoDeath(inst)
                end
            end),
        },
    },    
	
	    State {
		name = "frozen",
		tags = {"busy"},
		
        onenter = function(inst)
            inst.AnimState:PlayAnimation("frozen")
            inst.Physics:Stop()
        end,
		
    },
	
	 State{
        name = "special_atk1",
        tags = {"busy"},
        onenter= function(inst)
			inst.components.locomotor:Stop()
            inst.AnimState:PlayAnimation("taunt")
			inst:PerformBufferedAction()
            inst.SoundEmitter:PlaySound("dontstarve_DLC001/creatures/buzzard/taunt")
        end,
        
		events =
        {
            EventHandler("animover", function(inst)
				inst.sg:GoToState("idle")
            end),
        },

    },
    
	State{
        name = "idle",
        tags = {"idle", "canrotate"},
        
        onenter = function(inst, pushanim)
            inst.Physics:Stop()
            inst.AnimState:PlayAnimation("idle", true)
        end,
        
        timeline = 
        {

        },
        events=
        {
            EventHandler("animover", function(inst) inst.sg:GoToState("idle") end),
        },
   },

	 State{
        name = "run_start",
        tags = {"moving", "canrotate", "hopping"},
        
        onenter = function(inst) 
            inst.AnimState:PlayAnimation("hop")
            inst.components.locomotor:RunForward()
        end,
        
        timeline=
        {
            TimeEvent(8*FRAMES, function(inst) 
				if inst._isflying == false then
					inst.SoundEmitter:PlaySound("dontstarve_DLC001/creatures/buzzard/hurt")
					inst.Physics:Stop() 
				end
            end),
        },
        
        events=
        {
            EventHandler("animover", function(inst) inst.sg:GoToState("idle") end ),
        }
    },
        
    State{            
        name = "run",
        tags = {"moving", "canrotate","running"},
        
        onenter = function(inst) 
            inst.components.locomotor:RunForward()
            inst.AnimState:PlayAnimation("walk_loop")
        end,          
        events=
        {
            EventHandler("animover", function(inst) inst.sg:GoToState("run") end ),        
        },
        timeline=
        {

        },
    },      
    
    State{            
        name = "run_stop",
        tags = {"canrotate"},
        
        onenter = function(inst) 
            inst.components.locomotor:StopMoving()
           -- inst.AnimState:PlayAnimation("idle")            
        end,

        events=
        {   
            EventHandler("animover", function(inst) inst.sg:GoToState("idle") end ),        
        },
    },    
	
	State{            
        name = "peck",
        tags = {"busy"},
        
        onenter = function(inst) 
			if inst._isflying == true then
				inst.sg:GoToState("idle")
			else
            inst.components.locomotor:StopMoving()
            inst.AnimState:PlayAnimation("peck")    
			inst:PerformBufferedAction()
			end
        end,

        events=
        {   
            EventHandler("animover", function(inst) inst.sg:GoToState("idle") end ),        
        },
    }, 
	
	State{            
        name = "attack",
        tags = {"busy", "attack"},
        
        onenter = function(inst) 
			if inst._isflying == true then
				inst.sg:GoToState("idle")
			else
            inst.components.locomotor:StopMoving()
            inst.AnimState:PlayAnimation("atk")   
			end
        end,
		
		timeline = {
			TimeEvent(15*FRAMES, function(inst)
				inst:PerformBufferedAction()
				inst.SoundEmitter:PlaySound("dontstarve_DLC001/creatures/buzzard/attack")
			end),
		},

        events=
        {   
            EventHandler("animover", function(inst) inst.sg:GoToState("idle") end ),        
        },
    }, 
	
	State{            
        name = "action",
        tags = {"busy"},
        
        onenter = function(inst) 
			if inst._isflying == true then
				inst.sg:GoToState("idle")
			else
            inst.components.locomotor:StopMoving()
            inst.AnimState:PlayAnimation("peck")    
			inst:PerformBufferedAction()
			end
        end,

        events=
        {   
            EventHandler("animover", function(inst) inst.sg:GoToState("idle") end ),        
        },
    },    

	State{
        name = "land",
        tags = {"busy", "flight"},
        onenter= function(inst)
			inst.components.locomotor:StopMoving()
			inst:Show()
			local pt = Point(inst.Transform:GetWorldPosition())
			pt.y = 25
			inst.Physics:Stop()
			inst.components.locomotor:StopMoving()
            inst.Physics:Teleport(pt.x,pt.y,pt.z)
			inst.components.locomotor:StopMoving()
            inst.AnimState:PlayAnimation("glide", true)
            inst.sg:SetTimeout(inst.AnimState:GetCurrentAnimationLength())
			
			inst:RemoveTag("invisible")
			inst:RemoveTag("shadowcreature")
			inst:RemoveTag("shadow")
            inst.components.locomotor:StopMoving()
			inst.components.locomotor.disable = true
            inst.Physics:SetMotorVel(0,-20+math.random()*10,0)
            inst.SoundEmitter:PlaySound("dontstarve/birds/flyin", "flyin")
        end,
		
		onexit = function(inst)
			inst._isflying = false
			inst:DoTaskInTime(10, function(inst) inst.taunt2 = true end)
			inst.components.health:SetInvincible(false)
			inst.components.locomotor.disable = false
			inst.Physics:CollidesWith(COLLISION.LIMITS)
		end,	
		
		 timeline = 
        {
            TimeEvent(5, function(inst) --added as a failsafe. Teleports to the ground if takes too long.
				local pt = Point(inst.Transform:GetWorldPosition())
				pt.y = 0
                inst.components.locomotor:StopMoving()
                inst.Physics:Teleport(pt.x,pt.y,pt.z)
                inst.AnimState:PlayAnimation("land")
	            inst.DynamicShadow:Enable(true)
				inst.components.health:SetInvincible(false)
                inst.sg:GoToState("idle", true)
			end),
        },
        
        onupdate= function(inst)
            local pt = Point(inst.Transform:GetWorldPosition())
            inst.Physics:SetMotorVel(0,-20+math.random()*10,0)
            if pt.y <= 1 then
                pt.y = 0
                inst.components.locomotor:StopMoving()
                inst.Physics:Teleport(pt.x,pt.y,pt.z)
                inst.AnimState:PlayAnimation("land")
	            inst.DynamicShadow:Enable(true)
				inst.components.health:SetInvincible(false)
                inst.sg:GoToState("idle", true)
            end
        end,
		

        ontimeout = function(inst)
             inst.SoundEmitter:PlaySound("dontstarve/birds/flyin", "flyin")
             --inst.sg:GoToState("idle")
        end,
    },    
	 
	 State{
        name = "special_atk2",
        tags = {"flight", "busy"}, --sliding happens
        onenter = function(inst)
			if not inst.taunt2 then
			    inst.sg:GoToState("idle")
            else
                inst.taunt2 = false
                inst.components.locomotor:StopMoving()
                inst.sg:SetTimeout(.1+math.random()*.2)
                inst.sg.statemem.vert = math.random() > .5
                
                if TheNet:GetServerGameMode() ~= "lavaarena" and TheNet:GetServerGameMode() ~= "quagmire" then
                    inst.Physics:ClearCollidesWith(COLLISION.LIMITS)
                end
                
                inst.DynamicShadow:Enable(false)
                inst.components.health:SetInvincible(true)
                inst.SoundEmitter:PlaySound("dontstarve_DLC001/creatures/buzzard/flyout")
                inst.AnimState:PlayAnimation("takeoff_vertical_pre")
            end
        end,
        
        ontimeout= function(inst)
             inst.AnimState:PushAnimation("takeoff_vertical_loop", true)
			 inst.components.locomotor.disable = true
			 inst.components.locomotor:StopMoving()
             inst.Physics:SetMotorVel(0,15+math.random()*5,0)
        end,

        timeline = 
        {
            TimeEvent(1.5, function(inst) 
				inst:Hide()
				inst.components.locomotor.disable = false
				if TheNet:GetServerGameMode() == "lavaarena" then
					inst:DoTaskInTime(5, function(inst) if inst._isflying == true and not inst.sg:HasStateTag("landing") then inst.sg:GoToState("land") end end)
				end	
				inst.sg:GoToState("glide") 
			end)
        }
        
    },	
		
	State{
        name = "glide",
        tags = {"flight2", "canrotate"},
        onenter= function(inst)
			
			local pt = Point(inst.Transform:GetWorldPosition())
			if pt.y > .1 then
                pt.y = 0
                inst.Physics:Stop()
                inst.Physics:Teleport(pt.x,pt.y,pt.z)
				
			end	
			inst._isflying = true
			--inst.Physics:SetMotorVel(8,0,0)
            inst.AnimState:PlayAnimation("idle", true)
        end,
        
        onupdate= function(inst)
        end,
		

        ontimeout = function(inst)
             inst.SoundEmitter:PlaySound("dontstarve/birds/flyin", "flyin")
        end,
    }, 
		
	 State{
        name = "hit",
        tags = {"busy"},
        
        onenter = function(inst)
            inst.AnimState:PlayAnimation("hit")
            inst.Physics:Stop()
            local pt = Vector3(inst.Transform:GetWorldPosition())
            if pt.y > 1 then
                inst.sg:GoToState("fall")
            end
        end,
        
        events=
        {
            EventHandler("animover", function(inst) 
                    inst.sg:GoToState("idle") 
            end ),
        },        
    },    	
	
	 State{
        name = "fall",
        tags = {"busy"},
        onenter = function(inst)
            inst.Physics:Stop()
            inst.AnimState:PlayAnimation("fall_loop", true)
        end,
        
        onupdate = function(inst)
            local pt = Vector3(inst.Transform:GetWorldPosition())
            if pt.y <= .2 then
                pt.y = 0
                inst.Physics:Stop()
                inst.Physics:Teleport(pt.x,pt.y,pt.z)
	            inst.DynamicShadow:Enable(true)
                inst.sg:GoToState("stunned")
            end
        end,
    },    
		
	State{
        name = "stunned",
        tags = {"busy"},
        
        onenter = function(inst) 
            inst.Physics:Stop()
            inst.AnimState:PlayAnimation("stunned_loop", true)
            inst.sg:SetTimeout(GetRandomWithVariance(6, 2) )
        end,
        
        ontimeout = function(inst) inst.sg:GoToState("idle") end,
    },	
		
	State {
        name = "sleep",
        tags = { "busy", "sleeping" }, --add tag "busy" if you hate sliding

        onenter = function(inst)
            if inst.components.locomotor ~= nil then
                inst.components.locomotor:StopMoving()
            end
            inst.AnimState:PlayAnimation("sleep_pre")
			--inst.SoundEmitter:PlaySound("dontstarve/creatures/spiderqueen/fallasleep")
            --if fns ~= nil and fns.onsleep ~= nil then
                --fns.onsleep(inst)
            --end
        end,

        --timeline = timelines ~= nil and timelines.starttimeline or nil,
		
		timeline = 
		{
			--TimeEvent(15*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/creatures/together/grass_gekko/sleep_pre") end)
		},
		
        events =
        {
            EventHandler("animqueueover", function(inst) inst.sg:GoToState("sleeping") end ),
            EventHandler("onwakeup", function(inst) inst.sg:GoToState("wake") end),
        },
    },

    State
    {
        name = "sleeping",
        tags = { "sleeping" },

        --onenter = onentersleeping,
		
		onenter = function(inst)
				inst.components.locomotor:StopMoving()
				PlayablePets.SleepHeal(inst)
				--inst.SoundEmitter:PlaySound("dontstarve_DLC001/creatures/bearger/sleep")
				inst.AnimState:PlayAnimation("sleep_loop")
			end,

        --timeline = timelines ~= nil and timelines.sleeptimeline or nil,
		timeline = 
		{
           --TimeEvent(15*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/creatures/together/grass_gekko/sleep") end),
		   --TimeEvent(28*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/creatures/together/grass_gekko/sleep") end)
        },

        events =
        {
            EventHandler("animover", function(inst) inst.sg:GoToState("sleeping") end ),
			EventHandler("onwakeup", function(inst) inst.sg:GoToState("wake") end),
        },
    },

    State
    {
        name = "wake",
        tags = { "busy", "waking" },

        onenter = function(inst)
            if inst.components.locomotor ~= nil then
                inst.components.locomotor:StopMoving()
            end
			--inst.SoundEmitter:PlaySound("dontstarve/creatures/spiderqueen/wakeup")
            inst.AnimState:PlayAnimation("sleep_pst")
            if inst.components.sleeper ~= nil and inst.components.sleeper:IsAsleep() then
                inst.components.sleeper:WakeUp()
            end
            --if fns ~= nil and fns.onwake ~= nil then
                --fns.onwake(inst)
            --end
        end,

        --timeline = timelines ~= nil and timelines.waketimeline or nil,
		
		timeline = 
		{
			
		},
		
		events =
        {
            EventHandler("animover", function(inst) inst.sg:GoToState("idle") end),
        },
    },
    
}
PP_CommonStates.AddKnockbackState(states, nil, "hit") --(states, timeline, anim, sounds, fns)
PP_CommonStates.AddActionStates(states, 
	{
		TimeEvent(0 * FRAMES, function(inst)
            inst:PerformBufferedAction()
        end),
	}, 
	"peck", nil, nil, "peck", "peck") --(states, timelines, anim, anim2, enteranim, loopanim, exitanim, noanim, events)
PP_CommonStates.AddCorpseStates(states, true, 
	--timelines = 
	{
		corpse =
		{

		},
	
	},
	--anims = 
	{
		corpse = "death",
		corpse_taunt = "distress"
	},
	--sounds =
	{
		--corpse = "dontstarve_DLC003/creatures/enemy/grabbing_vine/death"
	},
	--fns =
	{

	}
	) ---(states, isflying, timelines, anims, sounds, fns, events, nofx)
PP_CommonStates.AddJumpInStates(states, nil, "hop")
PP_CommonStates.AddOpenGiftStates(states, "idle")
CommonStates.AddHopStates(states, false, {pre = "hop", loop = "hop", pst = "idle"}, nil, "death")
PP_CommonStates.AddSailorStates(states, nil, 
{
	plank_idle = "hop",
	plank_idle_loop = "idle",
	plank_idle_pst = "hop",
	
	plank_hop_pre = "hop",
	plank_hop = "hop",
	
	steer_pre = "hop",
	steer_idle = "idle",
	steer_turning = "idle",
	stop_steering = "hop",
	
	row = "hop",
}
)
    
PP_CommonStates.AddAoeStates(states, nil, 
{
	generic = "hop",
	
	castspelltime = 10,
})
CommonStates.AddFrozenStates(states)



    
return StateGraph("buzzardp", states, events, "idle", actionhandlers)

