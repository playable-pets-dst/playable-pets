require("stategraphs/commonstates")
require("stategraphs/ppstates")

local longaction = "pick"
local shortaction = "action"
local workaction = "action"
local otheraction = "action"

local actionhandlers = PP_CommonStates.GetCommonActions(longaction, shortaction, workaction, otheraction)

local actionhandler_overrides = 
{ 
	ActionHandler(ACTIONS.PICKUP, "pick"),
	ActionHandler(ACTIONS.PICK, "pick"),
	ActionHandler(ACTIONS.ATTACK, "idle"),
}

if #actionhandler_overrides > 0 then
	for i, v in ipairs(actionhandler_overrides) do
		table.insert(actionhandlers, v)
	end
end

local events=
{
    EventHandler("attacked", function(inst) if not inst.components.health:IsDead() and not inst.sg:HasStateTag("attack") then inst.sg:GoToState("idle") end end),
    EventHandler("doattack", function(inst, data) inst.sg:GoToState("attack", data.target) end),
    PP_CommonHandlers.OnDeath(),
    CommonHandlers.OnSleep(),
    CommonHandlers.OnFreeze(),
	PP_CommonHandlers.OnSink(),
	CommonHandlers.OnHop(), 
	PP_CommonHandlers.OpenGift(),
	PP_CommonHandlers.AddCommonHandlers(),
	PP_CommonHandlers.OnLocomoteAdvanced(),
}

local function Gobble(inst)
    --if not inst.SoundEmitter:PlayingSound("gobble") then
        inst.SoundEmitter:PlaySound("dontstarve/creatures/perd/gobble")--, "gobble")
    --end
end

local states=
{
    
    
    State{
        name = "death",
        tags = {"busy"},
        
        onenter = function(inst)
            inst.SoundEmitter:PlaySound("dontstarve/creatures/perd/death")
            --inst.SoundEmitter:PlaySound("dontstarve/creatures/pengull/death")
            inst.AnimState:PlayAnimation("death")
            inst.Physics:Stop()
            RemovePhysicsColliders(inst)            
            inst.components.lootdropper:DropLoot(Vector3(inst.Transform:GetWorldPosition()))
			inst.components.inventory:DropEverything(true)
         
			if inst.components.playercontroller ~= nil then
               inst.components.playercontroller:RemotePausePrediction()
            end
        end,
		
		timeline = 
		{
		},
		
        events =
        {
            EventHandler("animover", function(inst)
                if inst.AnimState:AnimDone() then
					PlayablePets.DoDeath(inst)
                end
            end),
        },

        

    },    
    
	State{
        name= "idle",
        tags = {"idle"},
        
        onenter = function(inst)
			inst.Physics:Stop()
            --Gobble(inst)
            
            inst.AnimState:PlayAnimation("idle_loop")
        end,
        
        events=
        {
            EventHandler("animover", function(inst) inst.sg:GoToState("idle") end ),
        },        
    },
	
	State{
        name = "attack",
        tags = {"attack"},
        
        onenter = function(inst)
            inst.SoundEmitter:PlaySound("dontstarve/creatures/perd/attack")
            inst.SoundEmitter:PlaySound("dontstarve/wilson/attack_whoosh")
            inst.components.combat:StartAttack()
            inst.Physics:Stop()
            inst.AnimState:PlayAnimation("atk")
        end,
        
        timeline =
        {
            TimeEvent(20*FRAMES, function(inst) inst.components.combat:DoAttack() end),
        },
        
        events =
        {
            EventHandler("animover", function(inst) inst.sg:GoToState("idle") end),
        },
    },
    
    State{
        name = "eat",
        
        tags = {"busy"},
        
        onenter = function(inst)
            inst.AnimState:PlayAnimation("eat")
            inst.Physics:Stop()            
        end,
        
        timeline=
        {
            TimeEvent(10*FRAMES, function(inst)
                inst:PerformBufferedAction()
                inst.sg:RemoveStateTag("busy")
                inst.sg:AddStateTag("idle")
            end),
        },
        
        events=
        {
            EventHandler("animover", function(inst) inst.sg:GoToState("idle") end ),
        },        
    },
    
    State{
        name = "pick",
        
        tags = {"busy"},
        
        onenter = function(inst)
            inst.AnimState:PlayAnimation("take")
            inst.Physics:Stop()            
        end,
        
        timeline=
        {
            TimeEvent(9*FRAMES, function(inst)
                inst:PerformBufferedAction()
                inst.sg:RemoveStateTag("busy")
                inst.sg:AddStateTag("idle")
            end),
        },
        
        events=
        {
            EventHandler("animover", function(inst) inst.sg:GoToState("idle") end ),
        },        
    },
    State{
        name = "hit",
        tags = {"busy"},
        
        onenter = function(inst)
            inst.SoundEmitter:PlaySound("dontstarve/creatures/perd/hurt")
            inst.AnimState:PlayAnimation("hit")
            inst.Physics:Stop()
        end,
        
        events=
        {
            EventHandler("animover", function(inst) inst.sg:GoToState("idle") end ),
        },        
    },    
	
	State{
        name = "special_atk2",
        tags = {"busy"},
        
        onenter = function(inst)
            inst.AnimState:PlayAnimation("walk_pst")
            inst.Physics:Stop()
			if inst.shouldwalk then
				inst.shouldwalk = false
			else
				inst.shouldwalk = true
			end
        end,
        
        events=
        {
            EventHandler("animover", function(inst) inst.sg:GoToState("idle") end ),
        },        
    },    
		
		
	State {
        name = "sleep",
        tags = { "busy", "sleeping" }, --add tag "busy" if you hate sliding

        onenter = function(inst)
            if inst.components.locomotor ~= nil then
                inst.components.locomotor:StopMoving()
            end
            inst.AnimState:PlayAnimation("sleep_pre")
			--inst.SoundEmitter:PlaySound("dontstarve/creatures/spiderqueen/fallasleep")
            --if fns ~= nil and fns.onsleep ~= nil then
                --fns.onsleep(inst)
            --end
        end,

        --timeline = timelines ~= nil and timelines.starttimeline or nil,
		
		timeline = 
		{
			TimeEvent(0*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/creatures/perd/sleep") end ),
		},
		
        events =
        {
            EventHandler("animqueueover", function(inst) inst.sg:GoToState("sleeping") end ),
            EventHandler("onwakeup", function(inst) inst.sg:GoToState("wake") end),
        },
    },

    State
    {
        name = "sleeping",
        tags = { "sleeping" },

        --onenter = onentersleeping,
		
		onenter = function(inst)
				inst.components.locomotor:StopMoving()
				PlayablePets.SleepHeal(inst)
				--inst.SoundEmitter:PlaySound("dontstarve_DLC001/creatures/bearger/sleep")
				inst.AnimState:PlayAnimation("sleep_loop")
			end,

        --timeline = timelines ~= nil and timelines.sleeptimeline or nil,
		timeline = 
		{
           TimeEvent(40*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/creatures/perd/sleep") end),
        },

        events =
        {
            EventHandler("animover", function(inst) inst.sg:GoToState("sleeping") end ),
			EventHandler("onwakeup", function(inst) inst.sg:GoToState("wake") end),
        },
    },

    State
    {
        name = "wake",
        tags = { "busy", "waking" },

        onenter = function(inst)
            if inst.components.locomotor ~= nil then
                inst.components.locomotor:StopMoving()
            end
			--inst.SoundEmitter:PlaySound("dontstarve/creatures/spiderqueen/wakeup")
            inst.AnimState:PlayAnimation("sleep_pst")
            if inst.components.sleeper ~= nil and inst.components.sleeper:IsAsleep() then
                inst.components.sleeper:WakeUp()
            end
            --if fns ~= nil and fns.onwake ~= nil then
                --fns.onwake(inst)
            --end
        end,

        --timeline = timelines ~= nil and timelines.waketimeline or nil,
		
		timeline = 
		{

		},
		
		events =
        {
            EventHandler("animover", function(inst) inst.sg:GoToState("idle") end),
        },
    },
    
}
CommonStates.AddWalkStates(states,
{
    starttimeline =
    {
        TimeEvent(0, Gobble),
    },

    walktimeline =
    {
        TimeEvent(0, PlayFootstep),
        TimeEvent(12 * FRAMES, PlayFootstep),
    },
})

CommonStates.AddRunStates(states,
{
    starttimeline =
    {
        TimeEvent(0, function(inst) inst.SoundEmitter:PlaySound("dontstarve/creatures/perd/run") end),
    },

    runtimeline =
    {
        TimeEvent(0, PlayFootstep),
        TimeEvent(5 * FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/creatures/perd/run") end),
        TimeEvent(10 * FRAMES, PlayFootstep),
    },
})

CommonStates.AddFrozenStates(states)
CommonStates.AddSimpleActionState(states, "pick", "take", 9*FRAMES, {"busy"})
local moveanim = "run"
local idleanim = "idle_loop"
local actionanim = "run_pst"
CommonStates.AddFrozenStates(states)
PP_CommonStates.AddKnockbackState(states, nil, "hit") --(states, timeline, anim, sounds, fns)
PP_CommonStates.AddActionStates(states, 
	{
		TimeEvent(0 * FRAMES, function(inst)
            inst:PerformBufferedAction()
        end),
	}, 
	actionanim, nil, nil, "idle_loop", actionanim) --(states, timelines, anim, anim2, enteranim, loopanim, exitanim, noanim, events)
PP_CommonStates.AddCorpseStates(states, true, 
	--timelines = 
	{
		corpse =
		{
			TimeEvent(0*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/creatures/perd/death") end),
		},
		
		corpse_taunt =
		{
			TimeEvent(0*FRAMES, function(inst)  end),
		},
	
	},
	--anims = 
	{
		corpse = "death",
		corpse_taunt = "idle_loop"
	},
	--sounds =
	{
		--corpse = "dontstarve_DLC003/creatures/enemy/grabbing_vine/death"
	},
	--fns =
	{

	}
) ---(states, isflying, timelines, anims, sounds, fns, events, nofx)
PP_CommonStates.AddJumpInStates(states, nil, actionanim)
PP_CommonStates.AddOpenGiftStates(states, "idle_loop")
--PP_CommonStates.AddHomeState(states, nil, "eat", "taunt", true)
CommonStates.AddHopStates(states, false, {pre = moveanim.."_pre", loop = moveanim.."_loop", pst = moveanim.."_pst"}, nil, "death")
PP_CommonStates.AddSailorStates(states, nil, 
{
	plank_idle = actionanim,
	plank_idle_loop = idleanim,
	plank_idle_pst = actionanim,
	
	plank_hop_pre = moveanim.."_pre",
	plank_hop = moveanim.."_loop",
	
	steer_pre = actionanim,
	steer_idle = idleanim,
	steer_turning = actionanim,
	stop_steering = actionanim,
	
	row = actionanim,
}
)
PP_CommonStates.AddAoeStates(states, nil, 
{
	generic = actionanim,
	
	leap_pre = moveanim.."_pre",
	leap_loop = moveanim.."_loop",
	leap_pst = moveanim.."_pst",
	
	lunge_pre = moveanim.."_pre",
	lunge_loop = moveanim.."_loop",
	lunge_pst = moveanim.."_pst",
	
	superjump_pre = moveanim.."_pre",
	superjump_loop = moveanim.."_loop",
	superjump_pst = moveanim.."_pst",
	
	castspelltime = 10,
})

    
return StateGraph("perdp", states, events, "idle", actionhandlers)

