require("stategraphs/commonstates")
require("stategraphs/ppstates")

local longaction = "action"
local shortaction = "action"
local workaction = "attack"
local otheraction = "action"

local actionhandlers = PP_CommonStates.GetCommonActions(longaction, shortaction, workaction, otheraction)

local actionhandler_overrides = 
{ 

}

if #actionhandler_overrides > 0 then
	for i, v in ipairs(actionhandler_overrides) do
		table.insert(actionhandlers, v)
	end
end

local function GetExcludeTagsp(inst)
	if TheNet:GetPVPEnabled() then
		return  {"notarget", "wall", "lavae"}
	elseif TheNet:GetServerGameMode() == "lavaarena" then
		return {"player", "companion", "INLIMBO", "notarget", "lavae"}
	else	
		return {"player", "companion", "INLIMBO", "notarget", "wall", "battlestandard", "lavae"}
	end
end

local function onattackfn(inst)
	if inst.components.health and not inst.components.health:IsDead()
	   and (inst.sg:HasStateTag("hit") or not inst.sg:HasStateTag("busy")) then
			inst.sg:GoToState("attack")
	end
end

local function DoDebuff(inst)
	local pos = inst:GetPosition()
	local ents = TheSim:FindEntities(pos.x,0,pos.z, 8, nil, GetExcludeTagsp(inst))
	local targets = {}
	if ents and #ents > 0 then
		for i, v in ipairs(ents) do
			if v:IsValid() and v.components.health and not v.components.health:IsDead() and v.components.combat and v.components.debuffable and v ~= inst then
				PlayablePets.KnockbackOther(inst, v, 8)
				v:PushEvent("flipped", {flipper = inst})
				v.components.combat:GetAttacked(inst, 20)
				v.AnimState:SetMultColour(1, 0.2, 0.2, 1)
				v.components.combat:AddDamageBuff("moose_debuff", 0.5, false)
				v:DoTaskInTime(8, function(v) v.components.combat:RemoveDamageBuff("moose_debuff") v.AnimState:SetMultColour(1, 1, 1, 1) end)
			end
		end
	end	
end

local events=
{
	EventHandler("locomote",
	function(inst)
		if (not inst.sg:HasStateTag("idle") and not inst.sg:HasStateTag("moving")) then return end

		if not inst.components.locomotor:WantsToMoveForward() then
			if not inst.sg:HasStateTag("idle") then
				inst.sg:GoToState("idle", {softstop = true})
			end
		else
			if not inst.sg:HasStateTag("hopping") then
				if inst.components.combat and not inst.components.health:IsDead() and inst.components.combat.target then
				inst.sg:GoToState("goosestep")
				else
				inst.sg:GoToState("hop")
				end
			end
		end
	end),
	PP_CommonHandlers.AddCommonHandlers(),
	CommonHandlers.OnSleep(),
	CommonHandlers.OnFreeze(),
	EventHandler("doattack", onattackfn),
	CommonHandlers.OnAttacked(),
	PP_CommonHandlers.OnDeath(),
	CommonHandlers.OnHop(),
	PP_CommonHandlers.OnSink(),
	PP_CommonHandlers.OnKnockback(),

	EventHandler("flyaway", function(inst)
		if inst.components.health:GetPercent() > 0 and not inst.sg:HasStateTag("busy") then
			inst.sg:GoToState("flyaway")
		end
	end),
	
	EventHandler("ms_opengift",
        function(inst)
            if not inst.sg:HasStateTag("busy") then
                inst.sg:GoToState("opengift")
            end
        end),
}

local function ShakeIfClose(inst)
	for i, v in ipairs(AllPlayers) do
		v:ShakeCamera(CAMERASHAKE.FULL, .35, .02, 1.25, inst, 40)
	end
end

local function DeathCollapseShake(inst)
	for i, v in ipairs(AllPlayers) do
		v:ShakeCamera(CAMERASHAKE.FULL, .7, .02, 3., inst, 40)
	end
end

local states=
{
	State{
		name = "idle",
		tags = {"idle", "canrotate"},

		onenter = function(inst, data)
			inst.Physics:Stop()
			inst.AnimState:PlayAnimation("idle", true)
			inst.sg:SetTimeout(math.random()*10+2)
		end,

		timeline = {},

		ontimeout= function(inst)
			inst.sg:GoToState((math.random() < 0.5 and "twitch" or "preen"))
		end,
	},

	State{
		name = "twitch",
		tags = {"idle"},

		onenter = function(inst, playanim)
			inst.Physics:Stop()
			--inst:PerformBufferedAction()
			inst.AnimState:PlayAnimation("idle_2")
		end,

		events =
		{
			EventHandler("animqueueover", function(inst) inst.sg:GoToState("idle") end)
		}
	},
	
	State{
		name = "twitch2",
		tags = {"busy"},

		onenter = function(inst, playanim)
			inst.Physics:Stop()
			inst:PerformBufferedAction()
			inst.AnimState:PlayAnimation("idle_2")
		end,

		events =
		{
			EventHandler("animqueueover", function(inst) inst.sg:GoToState("idle") end)
		}
	},

	State{
		name = "preen",
		tags = {"idle"},

		onenter = function(inst)
			inst.Physics:Stop()
			--inst:PerformBufferedAction()
			inst.AnimState:PlayAnimation("idle_3")
			
		end,

		events =
		{
			EventHandler("animover", function(inst) inst.sg:GoToState("idle") end)
		},

		timeline =
		{
			TimeEvent(11*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve_DLC001/creatures/moose/preen") end),
			TimeEvent(14*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve_DLC001/creatures/moose/preen_feathers") end),
		},
	},
	
	State{
		name = "preen2",
		tags = {"busy"},

		onenter = function(inst)
			inst.Physics:Stop()
			inst:PerformBufferedAction()
			inst.AnimState:PlayAnimation("idle_3")
			
		end,

		events =
		{
			EventHandler("animover", function(inst) inst.sg:GoToState("idle") end)
		},

		timeline =
		{
			TimeEvent(11*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve_DLC001/creatures/moose/preen") end),
			TimeEvent(14*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve_DLC001/creatures/moose/preen_feathers") end),
		},
	},

	State{
		name = "hop",
		tags = {"moving", "canrotate", "hopping"},

		onenter = function(inst)
			inst.AnimState:PlayAnimation("hop")
			PlayFootstep(inst)
			inst.components.locomotor:WalkForward()
			inst.sg:SetTimeout(math.random()+.5)
		end,

		onupdate= function(inst)
			if not inst.components.locomotor:WantsToMoveForward() then
				inst.sg:GoToState("idle")
			end
		end,

		timeline=
		{
			TimeEvent(1*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve_DLC001/creatures/moose/attack") end),
			TimeEvent(9*FRAMES, function(inst)
				inst.Physics:Stop()
				ShakeIfClose(inst)
				inst.SoundEmitter:PlaySound("dontstarve_DLC001/creatures/dragonfly/land")
			end),
		},

		ontimeout= function(inst)
			inst.sg:GoToState("hop")
		end,
	},
	
	State{
		name = "goosestep",
		tags = {"moving", "canrotate", "hopping"},

		onenter = function(inst)
			inst.AnimState:PlayAnimation("hop")
			PlayFootstep(inst)
			inst.components.locomotor:RunForward()
		end,

		onupdate= function(inst)
			if not inst.components.locomotor:WantsToMoveForward() then
				inst.sg:GoToState("idle")
			end
		end,

		timeline=
		{
			TimeEvent(1*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve_DLC001/creatures/moose/attack") end),
			TimeEvent(9*FRAMES, function(inst)
				inst.Physics:Stop()
				ShakeIfClose(inst)
				inst.SoundEmitter:PlaySound("dontstarve_DLC001/creatures/dragonfly/land")
			end),
		},

		events =
		{
			EventHandler("animqueueover", function(inst) if inst.components.combat.target then inst.sg:GoToState("goosestep") else inst.sg:GoToState("idle") end end)
		},
	},

	State{
		name = "glide",
		tags = {"flying", "busy"},

		onenter= function(inst)
			inst.AnimState:PlayAnimation("glide", true)
			inst.Physics:SetMotorVelOverride(0,-11,0)
			inst.flapSound = inst:DoPeriodicTask(6*FRAMES,
				function(inst)
					inst.SoundEmitter:PlaySound("dontstarve_DLC001/creatures/moose/flap")
				end)
		end,

		onupdate= function(inst)
			inst.Physics:SetMotorVelOverride(0,-15,0)
			local pt = Point(inst.Transform:GetWorldPosition())
			if pt.y < 2 then
				inst.Physics:ClearMotorVelOverride()
				pt.y = 0
				inst.Physics:Stop()
				inst.Physics:Teleport(pt.x,pt.y,pt.z)
				inst.AnimState:PlayAnimation("land")
				inst.DynamicShadow:Enable(true)
				inst.sg:GoToState("idle", {softstop = true})
				ShakeIfClose(inst)
			end
		end,

		onexit = function(inst)
			if inst.flapSound then
				inst.flapSound:Cancel()
				inst.flapSound = nil
			end
			if inst:GetPosition().y > 0 then
				local pos = inst:GetPosition()
				pos.y = 0
				inst.Transform:SetPosition(pos:Get())
			end
			inst.components.knownlocations:RememberLocation("landpoint", inst:GetPosition())
		end,
	},

	State{
		name = "flyaway",
		tags = {"flying", "busy"},

		onenter = function(inst)
			inst.Physics:Stop()
			inst.DynamicShadow:Enable(false)
			inst.AnimState:PlayAnimation("takeoff_pre_vertical")
			inst.SoundEmitter:PlaySound("dontstarve_DLC001/creatures/moose/flap")
			inst.sg.statemem.flapSound = 9*FRAMES
		end,

		onupdate = function(inst, dt)
			inst.sg.statemem.flapSound = inst.sg.statemem.flapSound - dt
			if inst.sg.statemem.flapSound <= 0 then
				inst.sg.statemem.flapSound = 6*FRAMES
				inst.SoundEmitter:PlaySound("dontstarve_DLC001/creatures/moose/flap")
			end
		end,

		timeline =
		{
			TimeEvent(9*FRAMES, function(inst)
				inst.AnimState:PushAnimation("takeoff_vertical", true)
				inst.Physics:SetMotorVel(math.random()*4,7+math.random()*2,math.random()*4)
			end),
			TimeEvent(10, function(inst) inst:Remove() end)
		}
	},

	State{
		name = "honk",
		tags = {"busy"},

		onenter = function(inst)
			inst.is_doing_special = true
			inst.Physics:Stop()
			inst.AnimState:PlayAnimation("honk")
		end,
		
		onexit = function(inst)
			inst.taunt2_event = false
			inst.is_doing_special = nil
			inst:DoTaskInTime(PP_FORGE.GOOSE_YULE.SPECIAL_CD, function(inst) inst.taunt2_event = true end)
		end,

		timeline =
		{
			TimeEvent(2*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve_DLC001/creatures/moose/flap") end),
			TimeEvent(10*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve_DLC001/creatures/moose/flap") end),
			TimeEvent(11*FRAMES, function(inst)
				PlayFootstep(inst)
				inst.SoundEmitter:PlaySound("dontstarve_DLC001/creatures/moose/swhoosh")
			end),
			TimeEvent(12*FRAMES, function(inst)
				inst.SoundEmitter:PlaySound("dontstarve_DLC001/creatures/moose/honk")
				DoDebuff(inst)
				if inst.components.combat.target and inst.components.combat.target.ShakeCamera then
					inst.components.combat.target:ShakeCamera(CAMERASHAKE.FULL, 0.75, 0.01, 2, 40)
				end
			end),
			--TimeEvent(15*FRAMES, function(inst) DisarmTarget(inst, inst.components.combat.target) end),
			TimeEvent(29*FRAMES, function(inst) PlayFootstep(inst) end),
		},

		events=
		{
			EventHandler("animqueueover", function(inst)
				inst.sg:GoToState("idle")
			end ),
		},

	},
	
	State{
		name = "attack",
		tags = {"attack", "busy"},
     
		onenter = function(inst, target)
        local buffaction = inst:GetBufferedAction()
        local target = buffaction ~= nil and buffaction.target or nil
        inst.components.combat:SetTarget(target)
        inst.components.combat:StartAttack()
        inst.components.locomotor:Stop()
        inst.Physics:Stop()
        inst.AnimState:PlayAnimation("honk")
    end,
     
    onexit = function(inst)

    end,
     
    timeline =
    {
			TimeEvent(2*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve_DLC001/creatures/moose/flap") end),
			TimeEvent(10*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve_DLC001/creatures/moose/flap") end),
			TimeEvent(11*FRAMES, function(inst)
				PlayFootstep(inst)
				inst.SoundEmitter:PlaySound("dontstarve_DLC001/creatures/moose/swhoosh")
				inst:PerformBufferedAction()
			end),
			TimeEvent(12*FRAMES, function(inst)
				inst.SoundEmitter:PlaySound("dontstarve_DLC001/creatures/moose/honk")
			end),
			--TimeEvent(15*FRAMES, function(inst) DisarmTarget(inst, inst.components.combat.target) end),
			TimeEvent(29*FRAMES, function(inst) PlayFootstep(inst) end),
			--TimeEvent(13*FRAMES, function(inst) inst:PerformBufferedAction() end),
    },
     
    events =
    {
        EventHandler("animover", function(inst) inst.sg:GoToState("idle") end),
    },
},

	
	
	State{
		name = "special_atk2",
		tags = {"busy"},

		onenter = function(inst)
			if TheNet:GetServerGameMode() == "lavaarena" then
				inst.sg:GoToState("honk")
			else
				if inst.taunt2_event == false then
					inst.sg:GoToState("idle")
				else
					inst.sg:GoToState("layegg2")
				end	
			end	
		end,
		
		onexit = function(inst)
            
			--if inst.components.timer:TimerExists(SpecialTimer) then
            --inst.components.timer:StartTimer(SpecialTimer, 10)
			--end
        end,
		
		timeline=
        {
           
        },

		events=
		{
			EventHandler("animqueueover", function(inst)
				inst.sg:GoToState("idle")
				--inst:AddTag("nospecial")
			end ),
		},

	},
	
	State{
		name = "special_atk1",
		tags = {"busy"},

		onenter = function(inst)
			inst.Physics:Stop()
			inst.AnimState:PlayAnimation("taunt_pre")
			inst.AnimState:PushAnimation("taunt")
			inst.AnimState:PushAnimation("taunt_pst", false)
		end,

		timeline=
		{
			TimeEvent(9*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve_DLC001/creatures/moose/flap") end),
			TimeEvent(11*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve_DLC001/creatures/moose/taunt") end),
			TimeEvent(13*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve_DLC001/creatures/moose/flap") end),
			TimeEvent(17*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve_DLC001/creatures/moose/flap") end),
			TimeEvent(21*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve_DLC001/creatures/moose/flap") end),
			TimeEvent(27*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve_DLC001/creatures/moose/flap") end),
			TimeEvent(37*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve_DLC001/creatures/moose/flap") end),
		},

		events=
		{
			EventHandler("animqueueover", function(inst) inst.sg:GoToState("idle") end),
		},
	},
	
	State{
        name = "death",
        tags = {"busy", "pausepredict", "nomorph"},

        onenter = function(inst)
            inst.components.locomotor:Stop()
            inst.components.locomotor:Clear()
            inst:ClearBufferedAction()
            inst.AnimState:PlayAnimation("death")
			--inst.components.lootdropper:DropLoot(Vector3(inst.Transform:GetWorldPosition()))
			inst.components.lootdropper:DropLoot(inst:GetPosition())
			inst.components.inventory:DropEverything(true)
            if inst.components.playercontroller ~= nil then
                inst.components.playercontroller:RemotePausePrediction()
            end
        end,
		
		timeline =
	{
		TimeEvent(0*FRAMES, function(inst)
			inst.SoundEmitter:PlaySound("dontstarve_DLC001/creatures/moose/death")
		end),

		TimeEvent(22*FRAMES, function(inst)
			inst.components.inventory:DropEverything(true)
			DeathCollapseShake(inst)
		end),
		
		TimeEvent(50*FRAMES, function(inst)
			if MOBGHOST == "Enable" then
                     inst:PushEvent(inst.ghostenabled and "makeplayerghost" or "playerdied", { skeleton = false })
					else
						TheWorld:PushEvent("ms_playerdespawnanddelete", inst)
				end
		end),
	},

        events =
        {
           
        },
	},	
	
	State{
		name = "layegg2",
		tags = {"busy"},

		onenter = function(inst)
			inst.Physics:Stop()
			inst.taunt2_event = false
			inst.userfunctions.StartTimer(inst, inst.charge_time + 8640*2)
			inst.AnimState:PlayAnimation("honk")
			inst.AnimState:PushAnimation("idle", false)
			
		end,

		timeline =
		{
			TimeEvent(2*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve_DLC001/creatures/moose/flap") end),
			TimeEvent(10*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve_DLC001/creatures/moose/flap") end),
			TimeEvent(12*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve_DLC001/creatures/moose/honk") end),

			TimeEvent(15*FRAMES, function(inst) TheWorld:PushEvent("ms_forceprecipitation", true) end),

			TimeEvent(50*FRAMES,
				function(inst)
					local egg = SpawnPrefab("mooseegg")
					local offset = FindWalkableOffset(inst:GetPosition(), math.random() * 2 * math.pi, 4, 12) or Vector3(0,0,0)
					local pt = offset + inst:GetPosition()
					local isname = inst:GetDisplayName()
					local selfpos = inst:GetPosition()
					print("LOGGED: "..isname.." spawned a mossling egg at "..selfpos.x..","..selfpos.y..","..selfpos.z.." as "..inst.prefab)

					egg.Transform:SetPosition(pt:Get())
					TheWorld:PushEvent("ms_sendlightningstrike", pt)
					inst.components.entitytracker:TrackEntity("egg", egg)
					egg.components.entitytracker:TrackEntity("mother", inst)
					egg:InitEgg()
				end)
		},

		events=
		{
			EventHandler("animqueueover", function(inst)
				inst.sg:GoToState("idle")
				--inst:DoTaskInTime(9640, function() inst:RemoveTag("nospecial") end)
			end ),
		},

	},
	
	State {
        name = "sleep",
        tags = { "sleeping" }, 

        onenter = function(inst)
            if inst.components.locomotor ~= nil then
                inst.components.locomotor:StopMoving()
            end
            inst.AnimState:PlayAnimation("sleep_pre")
            --if fns ~= nil and fns.onsleep ~= nil then
                --fns.onsleep(inst)
            --end
        end,

        --timeline = timelines ~= nil and timelines.starttimeline or nil,

        events =
        {
            EventHandler("animqueueover", function(inst) inst.sg:GoToState("sleeping") end ),
            EventHandler("onwakeup", function(inst) inst.sg:GoToState("wake") end),
        },
    },

    State
    {
        name = "sleeping",
        tags = { "sleeping" },

        --onenter = onentersleeping,
		
		onenter = function(inst)
				inst.components.locomotor:StopMoving()
				PlayablePets.SleepHeal(inst)
				inst.AnimState:PlayAnimation("sleep_loop")
			end,

        --timeline = timelines ~= nil and timelines.sleeptimeline or nil,
		timeline=
        {
			TimeEvent(22*FRAMES, function(inst)
			inst.SoundEmitter:PlaySound("dontstarve_DLC001/creatures/moose/sleep")
			end),
        },

        events =
        {
            EventHandler("animover", function(inst) inst.sg:GoToState("sleeping") end ),
			EventHandler("onwakeup", function(inst) inst.sg:GoToState("wake") end),
        },
    },

    State
    {
        name = "wake",
        tags = { "busy", "waking" },

        onenter = function(inst)
            if inst.components.locomotor ~= nil then
                inst.components.locomotor:StopMoving()
            end
            inst.AnimState:PlayAnimation("sleep_pst")
            if inst.components.sleeper ~= nil and inst.components.sleeper:IsAsleep() then
                inst.components.sleeper:WakeUp()
            end
            --if fns ~= nil and fns.onwake ~= nil then
                --fns.onwake(inst)
            --end
        end,

        --timeline = timelines ~= nil and timelines.waketimeline or nil,

        events =
        {
            EventHandler("animover", function(inst) inst.sg:GoToState("idle") end),
        },
    },

}

CommonStates.AddCombatStates(states,
{
	attacktimeline =
	{
		TimeEvent(0*FRAMES, function(inst) PlayFootstep(inst) end),
		TimeEvent(13*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve_DLC001/creatures/moose/swhoosh") end),
		TimeEvent(19*FRAMES, function(inst) PlayFootstep(inst) end),
		TimeEvent(20*FRAMES, function(inst)
			PlayablePets.DoWork(inst, 8)
			inst.SoundEmitter:PlaySound("dontstarve_DLC001/creatures/moose/attack")
		end),
		TimeEvent(25*FRAMES, function(inst) inst.sg:RemoveStateTag("attack") end),
	},

	deathtimeline =
	{
		TimeEvent(0*FRAMES, function(inst)
			inst.SoundEmitter:PlaySound("dontstarve_DLC001/creatures/moose/death")
		end),

		TimeEvent(22*FRAMES, function(inst)
			inst.components.inventory:DropEverything(true)
			DeathCollapseShake(inst)
		end),
		
		TimeEvent(50*FRAMES, function(inst)
			PlayablePets.SetSkeleton(inst, "pp_skeleton_giant")
			PlayablePets.DoDeath(inst)
		end),
	},
})

CommonStates.AddFrozenStates(states)
PP_CommonStates.AddActionStates(states, 
	{
		TimeEvent(0 * FRAMES, function(inst)
            inst:PerformBufferedAction()
        end),
	}, 
	"idle_3", nil, nil, "idle", "idle_3") --(states, timelines, anim, anim2, enteranim, loopanim, exitanim, noanim, events)
PP_CommonStates.AddCorpseStates(states, nil, 
	--timelines = 
	{
		corpse =
		{
			TimeEvent(0*FRAMES, function(inst)
				inst.SoundEmitter:PlaySound("dontstarve_DLC001/creatures/moose/death")
			end),

			TimeEvent(22*FRAMES, function(inst)
				DeathCollapseShake(inst)
			end),
		},
		
		corpse_taunt =
		{
			TimeEvent(0*FRAMES, function(inst) 
				inst.AnimState:PushAnimation("taunt")
				inst.AnimState:PushAnimation("taunt_pst", false)
			end),
			TimeEvent(9*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve_DLC001/creatures/moose/flap") end),
			TimeEvent(11*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve_DLC001/creatures/moose/taunt") end),
			TimeEvent(13*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve_DLC001/creatures/moose/flap") end),
			TimeEvent(17*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve_DLC001/creatures/moose/flap") end),
			TimeEvent(21*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve_DLC001/creatures/moose/flap") end),
			TimeEvent(27*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve_DLC001/creatures/moose/flap") end),
			TimeEvent(37*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve_DLC001/creatures/moose/flap") end),
		},
	
	},
	--anims = 
	{
		corpse = "death",
		corpse_taunt = "taunt_pre"
	},
	--sounds =
	{

	},
	--fns =
	{

	}
	) ---(states, isflying, timelines, anims, sounds, fns, events, nofx)
PP_CommonStates.AddJumpInStates(states, nil, "walk_pst")
PP_CommonStates.AddOpenGiftStates(states, "idle")
PP_CommonStates.AddSailStates(states, {}, "walk_pst", "idle")
local simpleanim = "taunt_pst"
local simpleidle = "idle"
local simplemove = "hop"
if CurrentRelease.GreaterOrEqualTo(ReleaseID.IDs.R08_ROT_TURNOFTIDES) then
	CommonStates.AddHopStates(states, false, {pre = simplemove, loop = simplemove, pst = simplemove}, nil, "death")
end
PP_CommonStates.AddSailorStates(states, nil, 
{
	plank_idle = simpleanim,
	plank_idle_loop = simpleidle,
	plank_idle_pst = simpleanim,
	
	plank_hop_pre = simplemove,
	plank_hop = simplemove,
	
	steer_pre = simpleanim,
	steer_idle = simpleidle,
	steer_turning = simpleanim,
	stop_steering = simpleanim,
	
	row = "taunt_pst",
}
)

return StateGraph("moosegooseplayer", states, events, "idle", actionhandlers)
