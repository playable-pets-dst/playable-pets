require("stategraphs/commonstates")
require("stategraphs/ppstates")

local function GetExcludeTags(inst)
	if TheNet:GetPVPEnabled() then
		return {"INLIMBO", "playerghost", "notarget", "shadow", "groundpoundimmune"}
	elseif TheNet:GetServerGameMode() == "lavaarena" then
		return {"player", "companion", "playerghost", "INLIMBO", "groundpoundimmune"}
	else	
		return {"player", "companion", "INLIMBO", "playerghost", "notarget", "shadow", "groundpoundimmune"}
	end
end

local function ShakeIfClose(inst)
    ShakeAllCameras(CAMERASHAKE.VERTICAL, .15, .02, .5, inst, 20)
end

local longaction = "action"
local shortaction = "action"
local workaction = "action"
local otheraction = "action"

local actionhandlers = PP_CommonStates.GetCommonActions(longaction, shortaction, workaction, otheraction)

local actionhandler_overrides = 
{ 

}

if #actionhandler_overrides > 0 then
	for i, v in ipairs(actionhandler_overrides) do
		table.insert(actionhandlers, v)
	end
end

local events=
{
    EventHandler("attacked", function(inst) if not inst.components.health:IsDead() and not inst.sg:HasStateTag("attack") then inst.sg:GoToState("hit") end end),
    EventHandler("doattack", function(inst, data) if not inst.components.health:IsDead() and (inst.sg:HasStateTag("hit") or not inst.sg:HasStateTag("busy")) then inst.sg:GoToState("attack", data.target) end end),
    CommonHandlers.OnSleep(),
    CommonHandlers.OnFreeze(),
	PP_CommonHandlers.OpenGift(),
	PP_CommonHandlers.OnSink(),
	PP_CommonHandlers.AddCommonHandlers(),
	PP_CommonHandlers.OnKnockback(),
	PP_CommonHandlers.OnDeath(),
	CommonHandlers.OnHop(),
	PP_CommonHandlers.OnLocomoteAdvanced(),
}


 local states=
{
	
	State{
        
        name = "idle",
        tags = {"idle", "canrotate"},
        onenter = function(inst, playanim)
            inst.Physics:Stop()
            inst.AnimState:PlayAnimation("idle_loop", true)
        end,
        
        timeline = 
        {
            TimeEvent(1*FRAMES, function(inst) inst.SoundEmitter:PlaySound(inst.soundpath .. "idle") end ),
        },
        
        events=
        {
            EventHandler("animover", function(inst) inst.sg:GoToState("idle") end),
        },
    },

   State{
        name = "taunt",
        tags = {"busy"},
        
        onenter = function(inst)
            inst.Physics:Stop()
            inst.AnimState:PlayAnimation("taunt")
        end,
        
        timeline = 
        {
            TimeEvent(19*FRAMES, function(inst) inst.SoundEmitter:PlaySound(inst.soundpath .. "voice") end ),
        },
        
        events=
        {
            EventHandler("animover", function(inst) inst.sg:GoToState("idle") end),
        },
    },
	
	State{
        name = "special_atk1",
        tags = {"busy"},
        
        onenter = function(inst)
            inst.Physics:Stop()
            inst.AnimState:PlayAnimation("taunt")
        end,
        
        timeline = 
        {
            TimeEvent(19*FRAMES, function(inst) inst.SoundEmitter:PlaySound(inst.soundpath .. "voice") end ),
        },
        
        events=
        {
            EventHandler("animover", function(inst) inst.sg:GoToState("idle") end),
        },
    },
	
    State{
        name = "attack",
        tags = {"attack", "busy"},
        
        onenter = function(inst, cb)
			local buffaction = inst:GetBufferedAction()
			local target = buffaction ~= nil and buffaction.target or nil
			if target ~= nil then
				if target:IsValid() then
					inst:ForceFacePoint(target:GetPosition())
					inst.sg.statemem.attacktarget = target
				end
			end
			inst.components.combat:StartAttack()
            inst.Physics:Stop()
            inst.AnimState:PlayAnimation("atk", false)
        end,
        
		timeline = 
		{
			TimeEvent(0*FRAMES, function(inst) inst.SoundEmitter:PlaySound(inst.soundpath .. "charge") end ),
			TimeEvent(15*FRAMES, function(inst) inst.SoundEmitter:PlaySound(inst.soundpath .. "shoot") end ),
			TimeEvent(24*FRAMES, function(inst) 
				if inst.sg.statemem.attacktarget then
					inst.FireProjectile(inst, inst.sg.statemem.attacktarget) 
				end
			end),

		},
        
        events=
        {
            EventHandler("animqueueover", function(inst) inst.sg:GoToState("idle") end),
        },
    },
	
	State{
        name = "hit",
        tags = {"hit"},
        
        onenter = function(inst, cb)
            inst.Physics:Stop()
            inst.AnimState:PlayAnimation("hit", false)
        end,
        
		timeline = 
		{
			TimeEvent(0*FRAMES, function(inst) inst.SoundEmitter:PlaySound(inst.soundpath .. "hurt") end),
		},
        
        events=
        {
            EventHandler("animqueueover", function(inst) inst.sg:GoToState("idle") end),
        },
    },

	State {
        name = "sleep",
        tags = { "sleeping" }, --add tag "busy" if you hate sliding

        onenter = function(inst)
            if inst.components.locomotor ~= nil then
                inst.components.locomotor:StopMoving()
            end
            inst.AnimState:PlayAnimation("sleep_pre")
            --if fns ~= nil and fns.onsleep ~= nil then
                --fns.onsleep(inst)
            --end
        end,

        --timeline = timelines ~= nil and timelines.starttimeline or nil,
		
		timeline = 
		{
			TimeEvent(11*FRAMES, function(inst) inst.SoundEmitter:PlaySound(inst.soundpath .. "liedown") end ),
		},

        events =
        {
            EventHandler("animqueueover", function(inst) inst.sg:GoToState("sleeping") end ),
            EventHandler("onwakeup", function(inst) inst.sg:GoToState("wake") end),
        },
    },

    State
    {
        name = "sleeping",
        tags = { "sleeping" },

        --onenter = onentersleeping,
		
		onenter = function(inst)
			inst.components.locomotor:StopMoving()
			PlayablePets.SleepHeal(inst)
			inst.AnimState:PlayAnimation("sleep_loop")
		end,

        
		timeline = 
		{
			TimeEvent(18*FRAMES, function(inst) inst.SoundEmitter:PlaySound(inst.soundpath .. "sleep") end),
		},

        events =
        {
            EventHandler("animover", function(inst) inst.sg:GoToState("sleeping") end ),
			EventHandler("onwakeup", function(inst) inst.sg:GoToState("wake") end),
        },
    },

    State
    {
        name = "wake",
        tags = { "busy", "waking" },

        onenter = function(inst)
            if inst.components.locomotor ~= nil then
                inst.components.locomotor:StopMoving()
            end
            inst.AnimState:PlayAnimation("sleep_pst")
            if inst.components.sleeper ~= nil and inst.components.sleeper:IsAsleep() then
                inst.components.sleeper:WakeUp()
            end
            --if fns ~= nil and fns.onwake ~= nil then
                --fns.onwake(inst)
            --end
        end,
		
        events =
        {
            EventHandler("animover", function(inst) inst.sg:GoToState("idle") end),
        },
    },
	
	

    State{
        name = "death",
        tags = {"busy"},

        onenter = function(inst)
            inst.AnimState:PlayAnimation("death")
			
            inst.Physics:Stop()
            RemovePhysicsColliders(inst)
            inst.components.lootdropper:DropLoot(inst:GetPosition())
			inst.components.inventory:DropEverything(true)
			
            if inst.components.playercontroller ~= nil then
                inst.components.playercontroller:RemotePausePrediction()
            end
        end,
		
		timeline = 
		{
			TimeEvent(0*FRAMES, function(inst) inst.SoundEmitter:PlaySound(inst.soundpath .. "death") end),
		},
		
        events =
        {
            EventHandler("animover", function(inst)
                if inst.AnimState:AnimDone() then
                    PlayablePets.DoDeath(inst)
                end
            end),
        },
    },
	
	State
    {
        name = "run_start",
        tags = { "moving", "running", "canrotate" },

        onenter = function(inst)
			inst.components.locomotor:WalkForward()
            inst.AnimState:PlayAnimation("walk_pre")
			
        end,
		
		timeline = 
		{
			TimeEvent(0*FRAMES, function(inst) inst.Physics:Stop() end ),
		},
		
        events =
        {
            EventHandler("animqueueover", function(inst) inst.sg:GoToState("run") end ),
        },
    },
	
	State
    {
        name = "run",
        tags = { "moving", "running", "canrotate" },

        onenter = function(inst)
			inst.components.locomotor:WalkForward()
			inst.AnimState:PlayAnimation("walk_loop")
        end,
		
		timeline = {
		    TimeEvent(0*FRAMES, function(inst) inst.Physics:Stop() end ),
            TimeEvent(7*FRAMES, function(inst) 
                inst.SoundEmitter:PlaySound(inst.soundpath .. "bounce")
                inst.components.locomotor:WalkForward()
            end ),
            TimeEvent(20*FRAMES, function(inst)
                inst.SoundEmitter:PlaySound(inst.soundpath .. "land")
		        inst.SoundEmitter:PlaySound(inst.effortsound)
                inst.Physics:Stop()
            end ),
		},
		
        events=
			{
				EventHandler("animqueueover", function(inst) inst.sg:GoToState("run") end ),
			},

    },
	
	State
    {
        name = "run_stop",
        tags = { "idle" },

        onenter = function(inst) 
            inst.components.locomotor:StopMoving()
			inst.AnimState:PlayAnimation("walk_pst")
			
            
        end,


        events =
        {
            EventHandler("animqueueover", function(inst) inst.sg:GoToState("idle") end ),
        },
    },
	
}
  

CommonStates.AddFrozenStates(states)
local simpleanim = "walk_pst"
local idleanim = "idle_loop"
PP_CommonStates.AddKnockbackState(states, nil, "hit") --(states, timeline, anim, sounds, fns)
PP_CommonStates.AddActionStates(states, 
	{
		TimeEvent(0 * FRAMES, function(inst)
            inst:PerformBufferedAction()
        end),
	}, 
	simpleanim, nil, nil, "idle_loop", simpleanim) --(states, timelines, anim, anim2, enteranim, loopanim, exitanim, noanim, events)
PP_CommonStates.AddCorpseStates(states, true, 
	--timelines = 
	{
		corpse =
		{
			TimeEvent(0*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/creatures/knight"..inst.kind.."/death") end),
		},
		
		corpse_taunt =
		{
			TimeEvent(0*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/creatures/knight"..inst.kind.."/voice") end),
			TimeEvent(10*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/creatures/knight"..inst.kind.."/pawground") end ),
		    TimeEvent(28*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/creatures/knight"..inst.kind.."/pawground") end ),
		},
	
	},
	--anims = 
	{
		corpse = "death",
		corpse_taunt = "taunt"
	},
	--sounds =
	{
		--corpse = "dontstarve_DLC003/creatures/enemy/grabbing_vine/death"
	},
	--fns =
	{

	}
	) ---(states, isflying, timelines, anims, sounds, fns, events, nofx)
PP_CommonStates.AddJumpInStates(states, nil, "walk_pst")
PP_CommonStates.AddOpenGiftStates(states, "idle_loop")
CommonStates.AddHopStates(states, false, {pre = "walk_pre", loop = "walk_loop", pst = "walk_pst"}, nil, "death")
PP_CommonStates.AddSailorStates(states, nil, 
{
	plank_idle = "walk_pst",
	plank_idle_loop = "idle_loop",
	plank_idle_pst = simpleanim,
	
	plank_hop_pre = "walk_pre",
	plank_hop = "walk_loop",
	
	steer_pre = simpleanim,
	steer_idle = idleanim,
	steer_turning = simpleanim,
	stop_steering = simpleanim,
	
	row = simpleanim,
}
)
PP_CommonStates.AddAoeStates(states, nil, 
{
	generic = "walk_pst",
	
	leap_pre = "walk_pre",
	leap_loop = "walk_loop",
	leap_pst = "walk_pst",
	
	lunge_pre = "walk_pre",
	lunge_loop = "walk_loop",
	lunge_pst = "walk_pst",
	
	superjump_pre = "walk_pre",
	superjump_loop = "walk_loop",
	superjump_pst = "walk_pst",
	
	castspelltime = 10,
})
    
return StateGraph("clockwork2player", states, events, "idle", actionhandlers)

