require("stategraphs/commonstates")
require("stategraphs/ppstates")

local longaction = "action"
local shortaction = "action"
local workaction = "work"
local otheraction = "action"

local actionhandlers = PP_CommonStates.GetCommonActions(longaction, shortaction, workaction, otheraction)

local actionhandler_overrides = 
{ 
	ActionHandler(ACTIONS.REVIVE_CORPSE, "idle"), --no reviving
	ActionHandler(ACTIONS.PP_DESTROY, "attack"), --no reviving
}

if #actionhandler_overrides > 0 then
	for i, v in ipairs(actionhandler_overrides) do
		table.insert(actionhandlers, v)
	end
end

local SHAKE_DIST = 40

local function OnDestroy(inst)
	inst.components.health:DoDelta(50)
	inst.components.sanity:DoDelta(25)
end

local function DeerclopsFootstep(inst)
    inst.SoundEmitter:PlaySound("dontstarve/creatures/deerclops/step")
    for i, v in ipairs(AllPlayers) do
        v:ShakeCamera(CAMERASHAKE.VERTICAL, .25, .03, 1, inst, SHAKE_DIST)
    end
end

local AREAATTACK_MUST_TAGS = { "_combat" }
local AREA_EXCLUDE_TAGS = { "INLIMBO", "notarget", "noattack", "flight", "invisible", "playerghost" }
local ICESPAWNTIME =  0.25

local function GetExcludeTagsp(inst)
	if TheNet:GetPVPEnabled() then
		return  {"notarget", "wall"}
	elseif TheNet:GetServerGameMode() == "lavaarena" then
		return {"player", "companion", "INLIMBO", "notarget"}
	else	
		return {"player", "companion", "INLIMBO", "notarget", "wall", "battlestandard"}
	end
end
    
local function DoSpawnIceSpike(inst, x, z)
    SpawnPrefab("icespike_fx_"..tostring(math.random(1, 4))).Transform:SetPosition(x, 0, z)

    local ents = TheSim:FindEntities(x,0,z,1.5,AREAATTACK_MUST_TAGS,GetExcludeTagsp(inst))
    if #ents > 0 then
        for i,ent in ipairs(ents)do
            if ent ~= inst then
                if not inst._icespikeshit_targets[ent.GUID] and inst.components.combat:CanTarget(ent) and not ent.deerclopsattacked then
					inst.components.combat:DoAttack(ent)
					inst._icespikeshit = true
	                inst._icespikeshit_targets[ent.GUID] = true
                end
                ent.deerclopsattacked = true
                ent:DoTaskInTime(ICESPAWNTIME +0.03,function() ent.deerclopsattacked = nil end)
            end
        end
    end
end

local function CheckForIceSpikesMiss(inst)
	if inst._icespikeshit_task ~= nil then
		inst._icespikeshit_task:Cancel()
		inst._icespikeshit_task = nil
	end

	if not inst._icespikeshit then
        inst:PushEvent("onmissother") -- for ChaseAndAttack
	end
end

local function SpawnIceFx(inst, target)
	inst._icespikeshit_targets = {}

	local AOEarc = 35

    local x, y, z = inst.Transform:GetWorldPosition()
    local angle = inst.Transform:GetRotation()

    local num = 3
    for i=1,num do
        local newarc = 180 - AOEarc
        local theta =  inst.Transform:GetRotation()*DEGREES
        local radius = TUNING.DEERCLOPS_ATTACK_RANGE - ( (TUNING.DEERCLOPS_ATTACK_RANGE/num)*i )
        local offset = Vector3(radius * math.cos( theta ), 0, -radius * math.sin( theta ))
        inst:DoTaskInTime(math.random() * .25, DoSpawnIceSpike, x+offset.x, z+offset.z)
    end

    for i=math.random(12,17),1,-1 do
        local theta =  ( angle + math.random(AOEarc *2) - AOEarc ) * DEGREES
        local radius = TUNING.DEERCLOPS_ATTACK_RANGE * math.sqrt(math.random())
        local offset = Vector3(radius * math.cos( theta ), 0, -radius * math.sin( theta ))
        inst:DoTaskInTime(math.random() * ICESPAWNTIME, DoSpawnIceSpike, x+offset.x, z+offset.z)
    end

    for i=math.random(5,8),1,-1 do
        local newarc = 180 - AOEarc
        local theta =  ( angle -180 + math.random(newarc *2) - newarc ) * DEGREES
        local radius = 2 * math.random() +1
        local offset = Vector3(radius * math.cos( theta ), 0, -radius * math.sin( theta ))
        inst:DoTaskInTime(math.random() * ICESPAWNTIME, DoSpawnIceSpike, x+offset.x, z+offset.z)
    end 

	inst._icespikeshit = false
	if inst._icespikeshit_task ~= nil then
		inst._icespikeshit_task:Cancel()
	end
	inst._icespikeshit_task = inst:DoTaskInTime(ICESPAWNTIME + FRAMES, CheckForIceSpikesMiss)
end

local events=
{
    CommonHandlers.OnLocomote(false,true),
    CommonHandlers.OnSleep(),
    CommonHandlers.OnFreeze(),
    CommonHandlers.OnAttack(),
    CommonHandlers.OnAttacked(),
	CommonHandlers.OnHop(),
	PP_CommonHandlers.OnSink(),
	PP_CommonHandlers.OnKnockback(),
	PP_CommonHandlers.AddCommonHandlers(),
    PP_CommonHandlers.OnDeath(),
	EventHandler("ms_opengift",
        function(inst)
            if not inst.sg:HasStateTag("busy") then
                inst.sg:GoToState("opengift")
            end
        end),
}

local states=
{  
	
	State{
        name = "taunt",
        tags = {"busy"},
        
        onenter = function(inst)
            inst.Physics:Stop()
            inst.AnimState:PlayAnimation("taunt")            
        end,
        
        timeline=
        {
            TimeEvent(5*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/creatures/deerclops/taunt_grrr") end),
            TimeEvent(16*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/creatures/deerclops/taunt_howl") end),
        },
        
        events=
        {
            EventHandler("animover", function(inst) inst.sg:GoToState("idle") end),
        },
    },
	
	State{
        name = "special_atk1",
        tags = {"busy"},
        
        onenter = function(inst)
            inst.Physics:Stop()
            inst.AnimState:PlayAnimation("taunt")            
        end,
        
        timeline=
        {
            TimeEvent(5*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/creatures/deerclops/taunt_grrr") end),
            TimeEvent(16*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/creatures/deerclops/taunt_howl") end),
        },
        
		onexit = function(inst)
			
		end,
		
        events=
        {
            EventHandler("animover", function(inst) inst.sg:GoToState("idle") end),
        },
    },
	
	State{
        name = "hit",
        tags = {"busy"},
        
        onenter = function(inst)
            inst.Physics:Stop()
            inst.AnimState:PlayAnimation("hit")            
        end,
        
        timeline=
        {
            TimeEvent(0*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/creatures/deerclops/hurt") end),
        },
        
        events=
        {
            EventHandler("animover", function(inst) inst.sg:GoToState("idle") end),
        },
    },
	
	State{
        name = "attack",
        tags = {"attack", "busy"},
        
        onenter = function(inst)
            local buffaction = inst:GetBufferedAction()
            local target = buffaction ~= nil and buffaction.target or nil
            inst.components.combat:StartAttack()
            if target ~= nil then
                if target:IsValid() then
                    inst:FacePoint(target:GetPosition())
                    inst.sg.statemem.attacktarget = target
                end
            end
            if target ~= nil and target:HasTag("wall") then
                local isname = inst:GetDisplayName()
                local selfpos = inst:GetPosition()
                print("LOGGED: "..isname.." attacked at "..selfpos.x..","..selfpos.y..","..selfpos.z.." as "..inst.prefab)
            end
            inst.components.locomotor:Stop()
            inst.Physics:Stop()
            inst.AnimState:PlayAnimation("atk")
        end,
        
        onexit = function(inst)
            
        end,
        
        timeline =
		{
			TimeEvent(0*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/creatures/deerclops/attack") end),
			TimeEvent(29*FRAMES, function(inst) 
                SpawnIceFx(inst, inst.sg.statemem.attacktarget) 
            end),
			TimeEvent(35*FRAMES, function(inst)
				inst.SoundEmitter:PlaySound("dontstarve/creatures/deerclops/swipe")
                --buffered action checks no longer work for some reason. Figure out why.
                --[[
                local bufferedaction = inst:GetBufferedAction()
				if bufferedaction and (bufferedaction.action == ACTIONS.HAMMER or bufferedaction.action == ACTIONS.PP_DESTROY) then
					bufferedaction.action = ACTIONS.HAMMER
                    print("Hammer check passed")
					PlayablePets.DoWork(inst, 99)
					OnDestroy(inst) --assuming it'll be destroyed
				elseif bufferedaction and bufferedaction ~= ACTIONS.ATTACK then
                    print("Non Attack check passed")
					PlayablePets.DoWork(inst, 8)
				end]]
				for i, v in ipairs(AllPlayers) do
					v:ShakeCamera(CAMERASHAKE.FULL, .5, .05, 2, inst, SHAKE_DIST)
				end
			end),
			TimeEvent(36*FRAMES, function(inst) inst.sg:RemoveStateTag("attack") end),
		},
     
		events =
		{
			EventHandler("animover", function(inst) inst.sg:GoToState("idle") end),
		},
	},

    State{
        name = "work",
        tags = {"attack", "busy"},
        
        onenter = function(inst)
            local buffaction = inst:GetBufferedAction()
            local target = buffaction ~= nil and buffaction.target or nil
            inst.components.combat:StartAttack()
            if target ~= nil then
                if target:IsValid() then
                    inst:FacePoint(target:GetPosition())
                    inst.sg.statemem.attacktarget = target
                end
            end
            if target ~= nil and target:HasTag("wall") then
                local isname = inst:GetDisplayName()
                local selfpos = inst:GetPosition()
                print("LOGGED: "..isname.." attacked at "..selfpos.x..","..selfpos.y..","..selfpos.z.." as "..inst.prefab)
            end
            inst.components.locomotor:Stop()
            inst.Physics:Stop()
            inst.AnimState:PlayAnimation("atk")
        end,
        
        onexit = function(inst)
            
        end,
        
        timeline =
		{
			TimeEvent(0*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/creatures/deerclops/attack") end),
			TimeEvent(29*FRAMES, function(inst) 
                SpawnIceFx(inst, inst.sg.statemem.attacktarget) 
            end),
			TimeEvent(35*FRAMES, function(inst)
				inst.SoundEmitter:PlaySound("dontstarve/creatures/deerclops/swipe")
                --buffered action checks no longer work for some reason. Figure out why.
				PlayablePets.DoWork(inst, 99)
				OnDestroy(inst) --assuming it'll be destroyed
				for i, v in ipairs(AllPlayers) do
					v:ShakeCamera(CAMERASHAKE.FULL, .5, .05, 2, inst, SHAKE_DIST)
				end
			end),
			TimeEvent(36*FRAMES, function(inst) inst.sg:RemoveStateTag("attack") end),
		},
     
		events =
		{
			EventHandler("animover", function(inst) inst.sg:GoToState("idle") end),
		},
	},

	State 
	{
        name = "sleep",
        tags = { "sleeping" }, --add tag "busy" if you hate sliding

        onenter = function(inst)
            if inst.components.locomotor ~= nil then
                inst.components.locomotor:StopMoving()
            end
            inst.AnimState:PlayAnimation("sleep_pre")
        end,

        events =
        {
            EventHandler("animqueueover", function(inst) inst.sg:GoToState("sleeping") end ),
            EventHandler("onwakeup", function(inst) inst.sg:GoToState("wake") end),
        },
    },

    State
    {
        name = "sleeping",
        tags = { "sleeping" },

        --onenter = onentersleeping,
		
		onenter = function(inst)
				inst.components.locomotor:StopMoving()
				PlayablePets.SleepHeal(inst)
				inst.AnimState:PlayAnimation("sleep_loop")
			end,

		timeline =
		{
			
		},

        events =
        {
            EventHandler("animover", function(inst) inst.sg:GoToState("sleeping") end ),
			EventHandler("onwakeup", function(inst) inst.sg:GoToState("wake") end),
        },
    },

    State
    {
        name = "wake",
        tags = { "busy", "waking" },

        onenter = function(inst)
            if inst.components.locomotor ~= nil then
                inst.components.locomotor:StopMoving()
            end
			--inst.SoundEmitter:PlaySound("dontstarve/creatures/spiderqueen/wakeup")
            inst.AnimState:PlayAnimation("sleep_pst")
            if inst.components.sleeper ~= nil and inst.components.sleeper:IsAsleep() then
                inst.components.sleeper:WakeUp()
            end
        end,

        --timeline = timelines ~= nil and timelines.waketimeline or nil,
		
		events =
        {
            EventHandler("animover", function(inst) inst.sg:GoToState("idle") end),
        },
    },
	State{
        name = "death",
        tags = {"busy", "pausepredict", "nomorph"},

        onenter = function(inst)
            inst.components.locomotor:Stop()
            inst.components.locomotor:Clear()
            inst:ClearBufferedAction()

            inst.AnimState:PlayAnimation("death")
			--inst.components.lootdropper:DropLoot(Vector3(inst.Transform:GetWorldPosition()))
			inst.components.lootdropper:DropLoot(inst:GetPosition())
            if inst.components.playercontroller ~= nil then
                inst.components.playercontroller:RemotePausePrediction()
            end
        end,
		
		 timeline=
    {
        TimeEvent(0*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/creatures/deerclops/death") end),
        TimeEvent(50*FRAMES, function(inst)
            if TheWorld.state.snowlevel > 0.02 then
                inst.SoundEmitter:PlaySound("dontstarve/creatures/deerclops/bodyfall_snow")
            else
                inst.SoundEmitter:PlaySound("dontstarve/creatures/deerclops/bodyfall_dirt")
            end
            for i, v in ipairs(AllPlayers) do
                v:ShakeCamera(CAMERASHAKE.FULL, .7, .02, 3, inst, SHAKE_DIST)
            end
        end),
    },
		
        events =
        {
            EventHandler("animover", function(inst)
                if inst.AnimState:AnimDone() then
					PlayablePets.DoDeath(inst)			
                end
            end),
        },
}

}
CommonStates.AddIdle(states)
CommonStates.AddFrozenStates(states)
CommonStates.AddWalkStates( states,
{
	starttimeline =
	{
        TimeEvent(7*FRAMES, DeerclopsFootstep),
	},
    walktimeline = 
    { 
        TimeEvent(23*FRAMES, DeerclopsFootstep),
        TimeEvent(42*FRAMES, DeerclopsFootstep),
    },
    endtimeline=
    {
        TimeEvent(5*FRAMES, DeerclopsFootstep),
    },
})

PP_CommonStates.AddActionStates(states, 
	{
		TimeEvent(0 * FRAMES, function(inst)
            inst:PerformBufferedAction()
        end),
	}, 
	"walk_pst", nil, nil, "idle_loop", "walk_pst") --(states, timelines, anim, anim2, enteranim, loopanim, exitanim, noanim, events)
PP_CommonStates.AddCorpseStates(states, nil, 
	--timelines = 
	{
		corpse =
		{
			TimeEvent(0*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/creatures/deerclops/death") end),
			TimeEvent(50*FRAMES, function(inst)
            if TheWorld.state.snowlevel > 0.02 then
                inst.SoundEmitter:PlaySound("dontstarve/creatures/deerclops/bodyfall_snow")
            else
                inst.SoundEmitter:PlaySound("dontstarve/creatures/deerclops/bodyfall_dirt")
            end
            for i, v in ipairs(AllPlayers) do
                v:ShakeCamera(CAMERASHAKE.FULL, .7, .02, 3, inst, SHAKE_DIST)
            end
        end),
		},
		
		corpse_taunt =
		{
			TimeEvent(5*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/creatures/deerclops/taunt_grrr") end),
            TimeEvent(16*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/creatures/deerclops/taunt_howl") end),
		},
	
	},
	--anims = 
	{
		corpse = "death",
		corpse_taunt = "taunt"
	},
	--sounds =
	{

	},
	--fns =
	{

	}
	) ---(states, isflying, timelines, anims, sounds, fns, events, nofx)
PP_CommonStates.AddJumpInStates(states, nil, "walk_pst")
PP_CommonStates.AddOpenGiftStates(states, "idle_loop")
PP_CommonStates.AddSailStates(states, {}, "walk_pst", "idle_loop")
local simpleanim = "walk_pst"
local simpleidle = "idle_loop"
local simplemove = "walk"
CommonStates.AddHopStates(states, false, {pre = simplemove.."_pre", loop = simplemove.."_loop", pst = simplemove.."_pst"}, nil, "death")
PP_CommonStates.AddSailorStates(states, nil, 
{
	plank_idle = simpleanim,
	plank_idle_loop = simpleidle,
	plank_idle_pst = simpleanim,
	
	plank_hop_pre = simplemove.."_pre",
	plank_hop = simplemove.."_loop",
	
	steer_pre = simpleanim,
	steer_idle = simpleidle,
	steer_turning = simpleanim,
	stop_steering = simpleanim,
	
	row = "walk_pst",
}
)

return StateGraph("deerplayer", states, events, "idle", actionhandlers)

