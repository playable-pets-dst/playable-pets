require("stategraphs/commonstates")
require("stategraphs/ppstates")

local DESTROYSTUFF_IGNORE_TAGS = { "INLIMBO", "mushroomsprout", "NET_workable" }
local BOUNCESTUFF_MUST_TAGS = { "_inventoryitem" }
local BOUNCESTUFF_CANT_TAGS = { "locomotor", "INLIMBO" }

local function DestroyStuff(inst)
    local x, y, z = inst.Transform:GetWorldPosition()
    local ents = TheSim:FindEntities(x, y, z, 3, nil, DESTROYSTUFF_IGNORE_TAGS)
    for i, v in ipairs(ents) do
        if v:IsValid() and
            v.components.workable ~= nil and
            v.components.workable:CanBeWorked() and
            v.components.workable.action ~= ACTIONS.NET then
            SpawnPrefab("collapse_small").Transform:SetPosition(v.Transform:GetWorldPosition())
            v.components.workable:Destroy(inst)
        end
    end
end

local function ClearRecentlyBounced(inst, other)
    inst.sg.mem.recentlybounced[other] = nil
end

local function SmallLaunch(inst, launcher, basespeed)
    local hp = inst:GetPosition()
    local pt = launcher:GetPosition()
    local vel = (hp - pt):GetNormalized()
    local speed = basespeed * 2 + math.random() * 2
    local angle = math.atan2(vel.z, vel.x) + (math.random() * 20 - 10) * DEGREES
    inst.Physics:Teleport(hp.x, .1, hp.z)
    inst.Physics:SetVel(math.cos(angle) * speed, 1.5 * speed + math.random(), math.sin(angle) * speed)

    launcher.sg.mem.recentlybounced[inst] = true
    launcher:DoTaskInTime(.6, ClearRecentlyBounced, inst)
end

local function BounceStuff(inst)
    if inst.sg.mem.recentlybounced == nil then
        inst.sg.mem.recentlybounced = {}
    end
    local x, y, z = inst.Transform:GetWorldPosition()
    local ents = TheSim:FindEntities(x, y, z, 6, BOUNCESTUFF_MUST_TAGS, BOUNCESTUFF_CANT_TAGS)
    for i, v in ipairs(ents) do
        if v:IsValid() and not (v.components.inventoryitem.nobounce or inst.sg.mem.recentlybounced[v]) and v.Physics ~= nil and v.Physics:IsActive() then
            local distsq = v:GetDistanceSqToPoint(x, y, z)
            local intensity = math.clamp((36 - distsq) / 27, 0, 1)
            SmallLaunch(v, inst, intensity)
        end
    end
end

local function hit_recovery_delay(inst, delay, max_hitreacts, skip_cooldown_fn)
    local on_cooldown = false
    if (inst._last_hitreact_time ~= nil and inst._last_hitreact_time + (delay or inst.hit_recovery or TUNING.DEFAULT_HIT_RECOVERY) >= GetTime()) then   -- is hit react is on cooldown?
        max_hitreacts = max_hitreacts or inst._max_hitreacts
        if max_hitreacts then
            if inst._hitreact_count == nil then
                inst._hitreact_count = 2
                return false
            elseif inst._hitreact_count < max_hitreacts then
                inst._hitreact_count = inst._hitreact_count + 1
                return false
            end
        end

        skip_cooldown_fn = skip_cooldown_fn or inst._hitreact_skip_cooldown_fn
        if skip_cooldown_fn ~= nil then
            on_cooldown = not skip_cooldown_fn(inst, inst._last_hitreact_time, delay)
        elseif inst.components.combat ~= nil then
            on_cooldown = not (inst.components.combat:InCooldown() and inst.sg:HasStateTag("idle"))     -- skip the hit react cooldown if the creature is ready to attack
        else
            on_cooldown = true
        end
    end

    if inst._hitreact_count ~= nil and not on_cooldown then
        inst._hitreact_count = 1
    end
    return on_cooldown
end

local function GetExcludeTags(inst)
	if TheNet:GetPVPEnabled() then
		return {"INLIMBO", "playerghost", "notarget", "shadow", "groundpoundimmune"}
	elseif TheNet:GetServerGameMode() == "lavaarena" then
		return {"player", "companion", "playerghost", "INLIMBO", "groundpoundimmune"}
	else	
		return {"player", "companion", "INLIMBO", "playerghost", "notarget", "shadow", "groundpoundimmune"}
	end
end

local function ShakeIfClose(inst)
    ShakeAllCameras(CAMERASHAKE.VERTICAL, .15, .02, .5, inst, 20)
end

local longaction = "action"
local shortaction = "action"
local workaction = "attack"
local otheraction = "action"

local actionhandlers = PP_CommonStates.GetCommonActions(longaction, shortaction, workaction, otheraction)

local actionhandler_overrides = 
{ 
    ActionHandler(ACTIONS.ATTACK, function(inst)
        if inst.atphase2 and inst.altattack then
            return "leap_attack_pre"
        else
            return "attack"
        end
    end),
}

if #actionhandler_overrides > 0 then
	for i, v in ipairs(actionhandler_overrides) do
		table.insert(actionhandlers, v)
	end
end

local events=
{
    --EventHandler("attacked", function(inst) if not inst.components.health:IsDead() and not inst.sg:HasStateTag("attack") and not inst.sg:HasStateTag("running") then inst.sg:GoToState("hit") end end),
    EventHandler("doattack", function(inst, data) if not inst.components.health:IsDead() and (inst.sg:HasStateTag("hit") or not inst.sg:HasStateTag("busy")) then inst.sg:GoToState("attack", data.target) end end),
    CommonHandlers.OnSleep(),
    CommonHandlers.OnFreeze(),
	CommonHandlers.OnHop(),
	PP_CommonHandlers.AddCommonHandlers(),
	PP_CommonHandlers.OpenGift(),
	PP_CommonHandlers.OnSink(),
	PP_CommonHandlers.OnKnockback(),
	PP_CommonHandlers.OnLocomoteAdvanced(), 
	PP_CommonHandlers.OnDeath(),
    EventHandler("collision_stun", function(inst,data)
        
        if data.light_stun == true then
            inst.sg:GoToState("hit")
        elseif data.land_stun == true then
            inst.sg:GoToState("stun",{land_stun=true})
        else
            inst.sg:GoToState("stun")
        end
    end),

    EventHandler("land_stun", function(inst,data)
        inst.sg:GoToState("land_stun")
    end),

    EventHandler("attacked", function(inst,data)    
        if inst.components.health ~= nil and not inst.components.health:IsDead()
            and not hit_recovery_delay(inst)
            and (not inst.sg:HasStateTag("busy")
            or inst.sg:HasStateTag("caninterrupt")
            or inst.sg:HasStateTag("frozen")) and not inst.sg:HasStateTag("cannotinterrupt") then    
                inst.sg:GoToState("hit")
        elseif inst.sg:HasStateTag("stunned") then
            inst:PushEvent("stunned_hit")
        end
    end),

    EventHandler("dostomp", function(inst, data)
        if not (inst.sg:HasStateTag("busy") or inst.components.health:IsDead()) then
            inst.sg:GoToState("stomp", data.time)
        end
    end), 

    EventHandler("doleapattack", function(inst,data)
        if inst.components.health and not inst.components.health:IsDead()  then -- and not inst.sg:HasStateTag("busy")
            inst.sg:GoToState("leap_attack_pre", data.target)
        end
    end), 
}


 local states=
{
	
	State{
        
        name = "idle",
        tags = {"idle", "canrotate"},
        onenter = function(inst, playanim)
            inst.Physics:Stop()
            inst.SoundEmitter:KillSound("charge")
            inst.AnimState:PlayAnimation("idle", true)
			inst.SoundEmitter:PlaySound("dontstarve/creatures/rook_minotaur/voice")
            
        end,
        
        events=
        {
            EventHandler("animover", function(inst) inst.sg:GoToState("idle") end),
        },
    },

    State{
        name = "attack",
        tags = { "attack", "busy" },

        onenter = function(inst)
            inst.components.combat:StartAttack()
            inst.components.locomotor:StopMoving()
            inst.AnimState:PlayAnimation("bite")
        end,

        timeline =

        { 
            TimeEvent(13*FRAMES, function(inst) inst.SoundEmitter:PlaySound("ancientguardian_rework/minotaur2/scuff") end),
            TimeEvent(10*FRAMES, function(inst) inst.SoundEmitter:PlaySound("ancientguardian_rework/minotaur2/bite") end),
            TimeEvent(16 * FRAMES, function(inst) 
                PlayablePets.DoWork(inst, 5)
            end),
        },

        events =
        {
            EventHandler("animover", function(inst)
                inst.sg:GoToState("idle")
            end),
        },
    },

    State{
        name = "stomp",
        tags = { "busy" },

        onenter = function(inst, time)
            inst.components.timer:StartTimer("stomptimer", 30 + (math.random()*10))
            inst.components.locomotor:StopMoving()
            inst.AnimState:PlayAnimation("jump_atk_pre")
            inst.AnimState:PushAnimation("jump_atk_loop",false)
            inst.AnimState:PushAnimation("jump_atk_pst",false)
        end,

        timeline =
        {
            TimeEvent(39*FRAMES, function(inst)
                inst.components.groundpounder:GroundPound()
                BounceStuff(inst)
                inst.SoundEmitter:PlaySound("ancientguardian_rework/minotaur2/groundpound")
            end),
        },

        onexit = function(inst)
            inst.altattack = false
            if inst._altattack_task then
                inst._altattack_task:Cancel()
                inst._altattack_task = nil
            end
            inst._altattack_task = inst:DoTaskInTime(15, function(inst) inst.altattack = true end)
        end,

        events =
        {
            EventHandler("animqueueover", function(inst)
                inst.sg:GoToState("idle")
            end),
        },
    },

    State{
        name = "leap_attack_pre",
        tags = {"attack", "busy","leapattack"},
        
        onenter = function(inst)
            inst.hasrammed = true
            inst.components.locomotor:Stop()
            inst.AnimState:PlayAnimation("jump_atk_pre")
            inst.sg.statemem.startpos = Vector3(inst.Transform:GetWorldPosition())
            inst:DoTaskInTime(1,function()
                if inst:IsValid() and not inst.components.health:IsDead() and inst.sg and inst.sg:HasStateTag("leapattack") then
                    local target = inst.components.combat.target or nil
                    if target then
                        inst.sg.statemem.targetpos = Vector3(inst.components.combat.target.Transform:GetWorldPosition())
                        inst:ForceFacePoint(inst.sg.statemem.targetpos)
                    else
                        local range = 6 -- overshoot range
                        local theta = inst.Transform:GetRotation()*DEGREES
                        local offset = Vector3(range * math.cos( theta ), 0, -range * math.sin( theta ))            
                        inst.sg.statemem.targetpos = Vector3(inst.sg.statemem.startpos.x + offset.x, 0, inst.sg.statemem.startpos.z + offset.z)
                    end
                end
            end)
            inst.sg:SetTimeout(1.5)
        end,

        onexit = function(inst)
            
        end,

        ontimeout = function(inst, target)
            inst.sg:GoToState("leap_attack",{targetpos = inst.sg.statemem.targetpos}) 
        end,
    },

    State{
        name = "leap_attack",
        tags = {"attack", "busy", "leapattack"},
        
        onenter = function(inst,data)
			if inst.components.timer:TimerExists("leapattack_cooldown") then
				inst.components.timer:SetTimeLeft("leapattack_cooldown", TUNING.MINOTAUR_LEAP_CD)
			else
				inst.components.timer:StartTimer("leapattack_cooldown", TUNING.MINOTAUR_LEAP_CD)
			end

            inst.sg.statemem.targetpos = data.targetpos
            
            inst.AnimState:PlayAnimation("jump_atk_loop")
            inst.components.locomotor:Stop()

            inst.sg.statemem.startpos = Vector3(inst.Transform:GetWorldPosition())

            inst.components.combat:StartAttack()

            inst:ForceFacePoint(inst.sg.statemem.targetpos)
            
            local range = 2
            local theta = inst.Transform:GetRotation()*DEGREES
            local offset = Vector3(range * math.cos( theta ), 0, -range * math.sin( theta ))
            local newloc = Vector3(inst.sg.statemem.targetpos.x + offset.x, 0, inst.sg.statemem.targetpos.z + offset.z)

            local time = inst.AnimState:GetCurrentAnimationLength()
            local dist = math.sqrt(distsq(inst.sg.statemem.startpos.x, inst.sg.statemem.startpos.z, newloc.x, newloc.z))
            local vel = dist/time

            inst.sg.statemem.vel = vel

            inst.components.locomotor:EnableGroundSpeedMultiplier(false)
            inst.Physics:SetMotorVelOverride(vel,0,0)

            inst.Physics:ClearCollisionMask()
            inst.Physics:CollidesWith(COLLISION.WORLD)
        end,

        onexit = function(inst)

            inst.Physics:ClearMotorVelOverride()

            inst.components.locomotor:Stop()
            inst.components.locomotor:EnableGroundSpeedMultiplier(true)
            inst.sg.statemem.startpos = nil
            inst.sg.statemem.targetpos = nil

            inst.OnChangeToObstacle(inst)
        end,
        
        events=
        {
            EventHandler("animover", function(inst)
                
                inst.components.groundpounder:GroundPound()
                BounceStuff(inst)

                if inst.jumpland(inst) then
                    inst.sg:GoToState("leap_attack_pst")
                    inst.SoundEmitter:PlaySound("ancientguardian_rework/minotaur2/groundpound")
                else
                    inst.sg:GoToState("stun",{land_stun=true})
                end
            end),
        },
    },

    State{

        name = "leap_attack_pst",
        tags = {"busy"},
        
        onenter = function(inst)
            inst.components.locomotor:Stop()
            inst.AnimState:PlayAnimation("jump_atk_pst")
            inst.SoundEmitter:PlaySound("ancientguardian_rework/minotaur2/groundpound")
        end,

        onexit = function(inst)
            inst.components.groundpounder.numRings = 3
        end,

        events=
        {
            EventHandler("animover", function(inst) inst.sg:GoToState("taunt") end),
        },
    },

   State{
        name = "taunt",
        tags = {"busy"},
        
        onenter = function(inst)
            inst.Physics:Stop()
            inst.AnimState:PlayAnimation("taunt")
            inst.SoundEmitter:PlaySound("dontstarve/creatures/rook_minotaur/voice")
        end,
        
        timeline = 
        {
		    TimeEvent(10*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/creatures/rook_minotaur/voice") end ),
		    TimeEvent(27*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/creatures/rook_minotaur/voice") end ),
        },
        
        events=
        {
            EventHandler("animover", function(inst) inst.sg:GoToState("idle") end),
        },
    },
	
	State{
        name = "special_atk1",
        tags = {"busy"},
        
        onenter = function(inst)
            inst.Physics:Stop()
            inst.AnimState:PlayAnimation("taunt")
            inst.SoundEmitter:PlaySound("dontstarve/creatures/rook_minotaur/voice")
        end,
        
        timeline = 
        {
		    TimeEvent(10*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/creatures/rook_minotaur/voice") end ),
		    TimeEvent(27*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/creatures/rook_minotaur/voice") end ),
        },
        
        events=
        {
            EventHandler("animover", function(inst) inst.sg:GoToState("idle") end),
        },
    },
	
	State{
        name = "special_atk2",
        tags = {"busy"},
        
        onenter = function(inst)
            inst.Physics:Stop()
            inst.AnimState:PlayAnimation("walk_pst")
			if inst.shouldwalk then
				inst.shouldwalk = false
			else
				inst.shouldwalk = true
			end            
        end,
        
        timeline = 
        {
		    
        },
        
        events=
        {
            EventHandler("animover", function(inst) inst.sg:GoToState("idle") end),
        },
    },
	
	State{  name = "runningattack",
            tags = {"runningattack"},
            
            onenter = function(inst)
                inst.SoundEmitter:KillSound("charge")
                inst.components.combat:StartAttack()
                inst.components.locomotor:StopMoving()
		        inst.AnimState:PlayAnimation("gore")
            end,
            
            timeline =
            {
                TimeEvent(1*FRAMES, function(inst)
                    inst:PerformBufferedAction()
                end),
            },
            
            events =
            {
                EventHandler("animqueueover", function(inst) inst.sg:GoToState("idle") end),
            },
        },
	
	State{
        name = "hit",
        tags = {"hit"},
        
        onenter = function(inst, cb)
            inst.Physics:Stop()
			inst.SoundEmitter:KillSound("charge")
            inst.AnimState:PlayAnimation("hit", false)
        end,
        
		timeline = 
		{
			TimeEvent(0*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/creatures/rook_minotaur/hurt") end),
		},
        
        events=
        {
            EventHandler("animqueueover", function(inst) inst.sg:GoToState("idle") end),
        },
    },  

	State {
        name = "sleep",
        tags = { "sleeping" }, --add tag "busy" if you hate sliding

        onenter = function(inst)
            if inst.components.locomotor ~= nil then
                inst.components.locomotor:StopMoving()
            end
            inst.AnimState:PlayAnimation("sleep_pre")
        end,
		
		timeline = 
		{
			TimeEvent(11*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/creatures/rook_minotaur/liedown") end ),
		},

        events =
        {
            EventHandler("animqueueover", function(inst) inst.sg:GoToState("sleeping") end ),
            EventHandler("onwakeup", function(inst) inst.sg:GoToState("wake") end),
        },
    },

    State
    {
        name = "sleeping",
        tags = { "sleeping", "busy" },

        --onenter = onentersleeping,
		
		onenter = function(inst)
				inst.components.locomotor:StopMoving()
				PlayablePets.SleepHeal(inst)
				inst.AnimState:PlayAnimation("sleep_loop")
			end,

        
		timeline = 
		{
			TimeEvent(18*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/creatures/rook_minotaur/sleep") end),
		},

        events =
        {
            EventHandler("animover", function(inst) inst.sg:GoToState("sleeping") end ),
			EventHandler("onwakeup", function(inst) inst.sg:GoToState("wake") end),
        },
    },

    State
    {
        name = "wake",
        tags = { "busy", "waking" },

        onenter = function(inst)
            if inst.components.locomotor ~= nil then
                inst.components.locomotor:StopMoving()
            end
            inst.AnimState:PlayAnimation("sleep_pst")
            if inst.components.sleeper ~= nil and inst.components.sleeper:IsAsleep() then
                inst.components.sleeper:WakeUp()
            end
        end,
		
        events =
        {
            EventHandler("animover", function(inst) inst.sg:GoToState("idle") end),
        },
    },
	
	

    State{
        name = "death",
        tags = {"busy"},

        onenter = function(inst)
            inst.AnimState:PlayAnimation("death")
			
            inst.Physics:Stop()
            RemovePhysicsColliders(inst)
            inst.components.lootdropper:DropLoot(inst:GetPosition())
			inst.components.inventory:DropEverything(true)

            inst.atphase2 = nil
            if inst.tentacletask then
                inst.tentacletask:Cancel()
                inst.tentacletask = nil
            end
			
            if inst.components.playercontroller ~= nil then
                inst.components.playercontroller:RemotePausePrediction()
            end
        end,
		
		timeline = 
		{
			TimeEvent(0*FRAMES, function(inst) 
				inst.SoundEmitter:PlaySound("dontstarve/creatures/rook_minotaur/death")
				inst.SoundEmitter:PlaySound("dontstarve/creatures/rook_minotaur/death_voice")
			end),
		},
		
        events =
        {
            EventHandler("animover", function(inst)
                if inst.AnimState:AnimDone() then
                    PlayablePets.DoDeath(inst)
                end
            end),
        },
    },
	
	State
    {
        name = "walk_start",
        tags = { "moving", "running", "canrotate" },

        onenter = function(inst)
			inst.components.locomotor:WalkForward()
            inst.AnimState:PlayAnimation("walk_pre")
        end,
		
		timeline = 
		{
			TimeEvent(0*FRAMES, function(inst) inst.Physics:Stop() end ),
		},
		
        events =
        {
            EventHandler("animqueueover", function(inst) inst.sg:GoToState("walk") end ),
        },
    },
	
	State
    {
        name = "walk",
        tags = { "moving", "running", "canrotate" },

        onenter = function(inst)
			inst.components.locomotor:WalkForward()
			inst.AnimState:PlayAnimation("walk_loop")
        end,
		
		timeline = {
		    TimeEvent(0*FRAMES, function(inst) inst.Physics:Stop() end ),
            TimeEvent(7*FRAMES, function(inst) 
                inst.components.locomotor:WalkForward()
            end ),
            TimeEvent(20*FRAMES, function(inst)
                inst.SoundEmitter:PlaySound("dontstarve/creatures/rook_minotaur/step")
                ShakeAllCameras(CAMERASHAKE.FULL, .5, .05, .1, inst, 40)
                inst.Physics:Stop()
            end ),
		},
		
        events=
			{
				EventHandler("animqueueover", function(inst) inst.sg:GoToState("walk") end ),
			},

    },
	
	State
    {
        name = "walk_stop",
        tags = { "idle" },

        onenter = function(inst) 
            inst.components.locomotor:StopMoving()
			inst.AnimState:PlayAnimation("walk_pst")
        end,


        events =
        {
            EventHandler("animqueueover", function(inst) inst.sg:GoToState("idle") end ),
        },
    },
	
	State{  name = "run_start",
            tags = {"moving", "running", "busy", "atk_pre", "canrotate"},
            
            onenter = function(inst)
                -- inst.components.locomotor:RunForward()
                inst.Physics:Stop()
                inst.SoundEmitter:PlaySound("dontstarve/creatures/rook_minotaur/pawground")
                inst.SoundEmitter:PlaySound("dontstarve/creatures/rook_minotaur/voice")
                inst.AnimState:PlayAnimation("atk_pre")
                inst.AnimState:PlayAnimation("paw_loop", true)
                inst.sg:SetTimeout(1.5)
                inst.chargecount = 0
            end,
            
            ontimeout= function(inst)
                inst.sg:GoToState("run")
            end,
            
            timeline=
            {
				TimeEvent(12*FRAMES, function(inst)
                    inst.SoundEmitter:PlaySound("dontstarve/creatures/rook_minotaur/pawground")
				end ),

				TimeEvent(30*FRAMES, function(inst)
                    inst.SoundEmitter:PlaySound("dontstarve/creatures/rook_minotaur/pawground")
                end),
			},        

            onexit = function(inst)
                --inst.SoundEmitter:PlaySound(inst.soundpath .. "charge_LP","charge")
            end,
        },

    State{  name = "run",
            tags = {"moving", "running", "cannotinterrupt", "charging"},
            
            onenter = function(inst) 
                inst.components.locomotor:RunForward()
                
                if not inst.AnimState:IsCurrentAnimation("atk") then
                    inst.AnimState:PlayAnimation("atk", true)
                end
                inst.sg:SetTimeout(inst.AnimState:GetCurrentAnimationLength())
				inst.SoundEmitter:PlaySound("dontstarve/creatures/rook_minotaur/step")
            end,

            onupdate = function(inst, dt)
                inst.chargecount = inst.chargecount + dt
            end,
            
            timeline=
            {
		        TimeEvent(5*FRAMES, function(inst)
                    inst.SoundEmitter:PlaySound("dontstarve/creatures/rook_minotaur/step")                                        
                end ),
            },
            
            ontimeout = function(inst)
                inst.sg:GoToState("run")
            end,
        },
    
    State{  name = "run_stop",
            tags = {"canrotate", "idle"},
            
            onenter = function(inst) 
                inst.SoundEmitter:KillSound("charge")
                inst.components.locomotor:Stop()
                inst.AnimState:PlayAnimation("gore")
		        --inst.SoundEmitter:PlaySound(inst.effortsound)
            end,
			
			timeline =
            {
                TimeEvent(5*FRAMES, function(inst) inst:PerformBufferedAction() end),
            },
            
            events=
            {   
                EventHandler("animover", function(inst) inst.sg:GoToState("idle") end ),        
            },
        },
	
        State{
            name = "stun",
            tags = {"busy","stunned"},
            
            onenter = function(inst, data)
                inst.components.locomotor:Stop()
                if data and data.land_stun then
                    inst.AnimState:PlayAnimation("stun_jump_pre")
                else
                    inst.sg.statemem.playlandsound = true
                    inst.AnimState:PlayAnimation("stun_pre")
                end
                local stuntime = math.max(1.5,Remap(inst.chargecount,0, 1, 0, 6 ) )
                inst.components.timer:StartTimer("endstun", stuntime)
            end,
    
            timeline=
            { 
                TimeEvent(11*FRAMES, function(inst) 
                    if inst.sg.statemem.playlandsound then
                        inst.SoundEmitter:PlaySound("ancientguardian_rework/minotaur2/step")
                    end
                 end), 
            },
    
            events=
            {
                EventHandler("stunned_hit", function(inst) inst.sg:GoToState("stun_hit") end),
                EventHandler("animover", function(inst) inst.sg:GoToState("stun_loop") end),
            },    
        },
    
        State{
            name = "stun_loop",
            tags = {"busy","stunned"},
            
            onenter = function(inst)
                inst.AnimState:PlayAnimation("stun_loop")
            end,
            
            timeline=
            { 
                TimeEvent(8*FRAMES, function(inst)
                    inst.SoundEmitter:PlaySound("ancientguardian_rework/minotaur2/recover")
                 end), 
            },
    
            events=
            {
                EventHandler("stunned_hit", function(inst) inst.sg:GoToState("stun_hit") end),
                EventHandler("animover", function(inst) inst.sg:GoToState("stun_loop") end),
            },
        },
    
        State{
            name = "stun_hit",
            tags = {"busy","stunned"},
            
            onenter = function(inst)
                inst.AnimState:PlayAnimation("stun_hit")
            end,
    
            events=
            {
                EventHandler("animover", function(inst) inst.sg:GoToState("stun_loop") end),
            },
        },
    
        State{
            name = "stun_pst",
            tags = {"busy"},
            
            onenter = function(inst)
                inst.components.locomotor:Stop()
                inst.AnimState:PlayAnimation("stun_pst")
            end,
    
            events=
            {
                EventHandler("animover", function(inst) inst.sg:GoToState("idle") end),
            },
    
            timeline =
            {
                TimeEvent(8 * FRAMES, function(inst) inst.SoundEmitter:PlaySound("ancientguardian_rework/minotaur2/scuff") end),
                TimeEvent(27 * FRAMES, function(inst) inst.SoundEmitter:PlaySound("ancientguardian_rework/minotaur2/scuff") end),
                TimeEvent(30 * FRAMES, function(inst) inst.SoundEmitter:PlaySound("ancientguardian_rework/minotaur2/step") end),
                TimeEvent(38 * FRAMES, function(inst) inst.SoundEmitter:PlaySound("ancientguardian_rework/minotaur2/scuff") end),
            },
        },
}

CommonStates.AddFrozenStates(states)
PP_CommonStates.AddKnockbackState(states, nil, "hit") --(states, timeline, anim, sounds, fns)
PP_CommonStates.AddActionStates(states, 
	{
		TimeEvent(0 * FRAMES, function(inst)
            inst:PerformBufferedAction()
        end),
	}, 
	"walk_pst", nil, nil, "idle", "walk_pst") --(states, timelines, anim, anim2, enteranim, loopanim, exitanim, noanim, events)
PP_CommonStates.AddCorpseStates(states, true, 
	--timelines = 
	{
		corpse =
		{
			TimeEvent(0*FRAMES, function(inst) 
				inst.SoundEmitter:PlaySound("dontstarve/creatures/rook_minotaur/death")
				inst.SoundEmitter:PlaySound("dontstarve/creatures/rook_minotaur/death_voice")
			end),
		},
		
		corpse_taunt =
		{
			TimeEvent(0*FRAMES, function(inst) inst.SoundEmitter:PlaySound(SoundPath(inst, "scream")) end),
		},
	
	},
	--anims = 
	{
		corpse = "death",
		corpse_taunt = "taunt"
	},
	--sounds =
	{
		--corpse = "dontstarve_DLC003/creatures/enemy/grabbing_vine/death"
	},
	--fns =
	{

	}
	) ---(states, isflying, timelines, anims, sounds, fns, events, nofx)
PP_CommonStates.AddJumpInStates(states, nil, "walk_pst")
PP_CommonStates.AddOpenGiftStates(states, "taunt")
CommonStates.AddHopStates(states, false, {pre = "walk_pre", loop = "walk_loop", pst = "walk_pst"}, nil, "death")
PP_CommonStates.AddSailorStates(states, nil, 
{
	plank_idle = "idle",
	plank_idle_loop = "idle",
	plank_idle_pst = "walk_pst",
	
	plank_hop_pre = "walk_pre",
	plank_hop = "walk_loop",
	
	steer_pre = "walk_pst",
	steer_idle = "idle",
	steer_turning = "taunt",
	stop_steering = "walk_pst",
	
	row = "walk_pst",
}
)
PP_CommonStates.AddAoeStates(states, nil, 
{
	generic = "walk_pst",
	
	leap_pre = "walk_pre",
	leap_loop = "walk_loop",
	leap_pst = "walk_pst",
	
	castspelltime = 10,
})    
    
return StateGraph("guardianp", states, events, "idle", actionhandlers)

