require("stategraphs/commonstates")
require("stategraphs/ppstates")

local function GetExcludeTags(inst)
	if TheNet:GetPVPEnabled() then
		return {"INLIMBO", "playerghost", "notarget", "shadow", "groundpoundimmune"}
	elseif TheNet:GetServerGameMode() == "lavaarena" then
		return {"player", "companion", "playerghost", "INLIMBO", "groundpoundimmune"}
	else	
		return {"player", "companion", "INLIMBO", "playerghost", "notarget", "shadow", "groundpoundimmune"}
	end
end

local longaction = "action"
local shortaction = "action"
local workaction = "attack"
local otheraction = "eat_pre"

local actionhandlers = PP_CommonStates.GetCommonActions(longaction, shortaction, workaction, otheraction)

local actionhandler_overrides = 
{ 
	ActionHandler(ACTIONS.PICKUP, "attack"),
	ActionHandler(ACTIONS.HARVEST, "attack"),
}

if #actionhandler_overrides > 0 then
	for i, v in ipairs(actionhandler_overrides) do
		table.insert(actionhandlers, v)
	end
end

local function NumHoundsToSpawn(inst)
    local numHounds = TUNING.WARG_BASE_HOUND_AMOUNT

    local pt = Vector3(inst.Transform:GetWorldPosition())
    local ents = TheSim:FindEntities(pt.x, pt.y, pt.z, TUNING.WARG_NEARBY_PLAYERS_DIST, {"player"}, {"playerghost"})
    for i,player in ipairs(ents) do
        local playerAge = player.components.age:GetAgeInDays()
        local addHounds = 1
        numHounds = numHounds + addHounds
    end
    local numFollowers = inst.components.leader:CountFollowers()
    local num = math.min(numFollowers+numHounds/2, numHounds) -- only spawn half the hounds per howl
    num = (math.log(num)/0.4)+1 -- 0.4 is approx log(1.5)

    num = RoundToNearest(num, 1)

	if numFollowers == (MOBPVPMODE == "Disable" and 2 or 4) then
		return 0
	else
		return num - numFollowers
	end
end

local function RetargetForgeFn(inst)
    if (inst.components.follower.leader and inst.components.follower.leader.components.health:IsDead()) or (inst.daddy and inst.daddy.components.health:IsDead()) then
		return FindEntity(inst, 30, function(guy) return inst.components.combat:CanTarget(guy) and not guy.components.health:IsDead() end, nil, { "structure", "player", "companion" })
	elseif inst.components.follower.leader and inst.components.follower.leader.components.combat.target then	
		return inst.components.follower.leader.components.combat.target
	end
end

local function KeepForgeTarget(inst, target)
    return target:IsValid() and 
		target.sg ~= nil and not 
		((target.sg:HasStateTag("sleeping") or target:HasTag("_isinheals")) or target.sg:HasStateTag("fossilized")) and not target:HasTag("player")
end

local function RelinktoLeader(inst)
	if inst.daddy and inst.daddy.components.leader then
		inst.daddy.components.leader:AddFollower(inst)
	end
end

local function NoDmgFromPlayers(inst, amount, overtime, cause, ignore_invincible, afflicter, ignore_absorb)
    return afflicter ~= nil and (afflicter:HasTag("player") or afflicter:HasTag("companion"))
end

local function OnForgeAttacked(inst, data)
	if not (data.attacker:HasTag("player") or data.attacker:HasTag("companion") or data.attacker:HasTag("hound")) then
    inst.components.combat:SetTarget(data.attacker)
    inst.components.combat:ShareTarget(data.attacker, SHARE_TARGET_DIST,
        function(dude)
            return not (dude.components.health ~= nil and dude.components.health:IsDead())
                and (dude:HasTag("hound") or dude:HasTag("houndfriend"))
                and data.attacker ~= (dude.components.follower ~= nil and dude.components.follower.leader or nil)
        end, 5)
	end	
end

--old
local function OnAttacked(inst, data)
    inst.components.combat:SetTarget(data.attacker)
    inst.components.combat:ShareTarget(data.attacker, SHARE_TARGET_DIST,
        function(dude)
            return not (dude.components.health ~= nil and dude.components.health:IsDead())
                and (dude:HasTag("hound") or dude:HasTag("houndfriend"))
                and data.attacker ~= (dude.components.follower ~= nil and dude.components.follower.leader or nil)
        end, 5)
end

local function SpawnHound(inst)
	if not TheWorld:HasTag("cave") and not inst.components.revivablecorpse then
		local hounded = TheWorld.components.hounded
		if hounded ~= nil then
			local num = NumHoundsToSpawn(inst)
			local pt = inst:GetPosition()
			for i = 1, num do
				local hound = hounded:SummonSpawn(pt)
				if hound ~= nil then
					hound.components.follower:SetLeader(inst)
				end
			end
		end
	elseif inst.components.revivablecorpse then --Forge
		for i = 1, PP_FORGE.VARG.AMOUNT or 1 do
			local theta = math.random() * 2 * _G.PI
			local pt = inst:GetPosition()
			local radius = math.random(1, 2)
			local offset = _G.FindWalkableOffset(pt, theta, radius, 1.5, true, true)
				if offset ~= nil then
					pt.x = pt.x + offset.x
					pt.z = pt.z + offset.z
				end
			local fx = SpawnPrefab("lavaarena_portal_player_fx").Transform:SetPosition(pt.x, 0, pt.z)
			local pet = SpawnPrefab("hound")
			pet.Transform:SetPosition(pt.x, 0, pt.z)
			pet:Hide()
			pet:DoTaskInTime(0.3, function(inst) inst:Show() end)
			pet:AddTag("companion")
			pet.daddy = inst
			pet.components.health.redirect = NoDmgFromPlayers
			
			inst.components.leader:AddFollower(pet)
			pet.components.follower:KeepLeaderOnAttacked()
			pet.components.follower.keepleader = true
			
			pet.components.combat:SetDamageType(1)	
			pet.components.combat:SetDefaultDamage(15)
			pet.components.combat:SetRetargetFunction(1, RetargetForgeFn)
			pet.components.combat:SetKeepTargetFunction(KeepForgeTarget)
			
			pet:AddComponent("debuffable")
			pet.components.debuffable:IsEnabled(true)
			pet:AddComponent("colouradder")
			pet.components.lootdropper:SetChanceLootTable({})
			
			pet:RemoveEventCallback("onattacked", OnAttacked)
			pet:ListenForEvent("onattacked", OnForgeAttacked)
			--if inst.components.stat_tracker then inst.components.stat_tracker:InitializePetStats(pet) end
		end
		
	end	
end

local events=
{
    EventHandler("attacked", function(inst)
        if not inst.components.health:IsDead() and
            (not inst.sg:HasStateTag("busy") or inst.sg:HasStateTag("sleeping")) and
            (inst.sg.mem.last_hit_time or 0) + inst.hit_recovery < GetTime() then
				inst.sg:GoToState("hit")
        end
    end),
    EventHandler("doattack", function(inst, data) if not inst.components.health:IsDead() and (inst.sg:HasStateTag("hit") or not inst.sg:HasStateTag("busy")) then inst.sg:GoToState("attack", data.target) end end),
    CommonHandlers.OnSleep(),
    CommonHandlers.OnFreeze(),
	EventHandler("chomp", function(inst, data)
        if data and data.target then
            inst.sg:GoToState("eat_pre", data.target)
        end
    end),
	PP_CommonHandlers.OpenGift(),
	PP_CommonHandlers.OnSink(),
	PP_CommonHandlers.OnKnockback(),
	PP_CommonHandlers.OnDeath(),
	PP_CommonHandlers.AddCommonHandlers(),
	CommonHandlers.OnHop(),
	PP_CommonHandlers.OnLocomoteAdvanced(), 
}


 local states=
{
	
	State
	{
		name = "idle",
		tags = {"idle"},

		onenter = function(inst)
			inst.Physics:Stop()
			inst.AnimState:PlayAnimation("idle_loop")
			inst.SoundEmitter:PlaySound("dontstarve_DLC001/creatures/vargr/idle")
		end,

		events = 
		{
			EventHandler("animover", function(inst) 
				inst.sg:GoToState("idle") 
			end)
		},
	},
	
	 State{
        name = "attack",
        tags = {"busy", "attack"},

        onenter = function(inst)
            inst.components.locomotor:StopMoving()
            inst.components.combat:StartAttack()
            inst.AnimState:PlayAnimation("atk")
        end,

        timeline =
        {
            TimeEvent(12*FRAMES, function(inst) PlayablePets.DoWork(inst, 5) end),
			TimeEvent(0*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve_DLC001/creatures/vargr/attack") end)
        },

        events =
        {
            EventHandler("animover", function(inst)
                inst.sg:GoToState("idle")
            end),
        },
    },
	
	State{
        name = "hit",
        tags = {"busy", "hit", "canrotate"},

        onenter = function(inst, cb)
            inst.Physics:Stop()
			inst.sg.mem.last_hit_time = GetTime()
            inst.AnimState:PlayAnimation("hit")
        end,

        timeline =
        {
            TimeEvent(0*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve_DLC001/creatures/vargr/hit") end)
        },

        events=
        {
            EventHandler("animover", function(inst) inst.sg:GoToState("idle") end),
        },
    },
	
	State{
		name = "eat_pre",
        tags = { "chewing", "busy" },
		onenter = function(inst, target)
			inst.components.locomotor:StopMoving()
			inst.components.combat:StartAttack()
			inst.AnimState:PlayAnimation("eat_pre")
			inst.SoundEmitter:PlaySound("dontstarve_DLC001/creatures/vargr/attack")
			if target and target:HasTag("meat_carcass") then
                inst.sg.statemem.carcass = target
            end
			local buffaction = inst:GetBufferedAction()
			local target = buffaction and buffaction.target or inst.sg.statemem.carcass
			if target ~= nil and target:IsValid() then
				inst.components.combat:StartAttack()
				inst:ForceFacePoint(target.Transform:GetWorldPosition())
			end
		end,

        timeline =
		{

		},

		events =
		{
			EventHandler("animover", function(inst)
				local carcass = inst.sg.statemem.carcass
                if carcass and carcass:IsValid() then
                    carcass:PushEvent("chomped")
                    inst.components.hunger:DoDelta(10)
                    inst.sg.statemem.carcass = nil
                end
				if inst.AnimState:AnimDone() then
					inst.sg:GoToState("eat_loop")
				end
			end),
		},
	},

	State{
		name = "eat_loop",
		tags = { "chewing", "busy" },

		onenter = function(inst)
			inst.components.locomotor:StopMoving()
			inst.AnimState:PlayAnimation("eat_loop")
			inst.SoundEmitter:PlaySound("rifts3/chewing/warg")
		end,

		timeline =
		{
			FrameEvent(7, function(inst)
				inst.sg:AddStateTag("caninterrupt")
				inst.sg:AddStateTag("wantstoeat")
			end),
		},

		events =
		{
			EventHandler("animover", function(inst)
				if inst.AnimState:AnimDone() then
					inst.sg:GoToState("eat_pst", true)
				end
			end),
		},
	},

	State{
		name = "eat_pst",
		tags = { "busy", "caninterrupt" },

		onenter = function(inst, chewing)
			inst.components.locomotor:StopMoving()
			inst.AnimState:PlayAnimation("eat_pst")
            PlayablePets.DoWork(inst, 2.5)
		end,

		timeline =
		{
			FrameEvent(6, function(inst)

			end),
		},

		events =
		{
			EventHandler("animover", function(inst)
				if inst.AnimState:AnimDone() then
					inst.sg:GoToState("idle")
				end
			end),
		},

		onexit = function(inst)
			inst.SoundEmitter:KillSound("loop")
		end,
	},

	State
	{
		name = "special_atk1",
		tags = {"busy"},

		onenter = function(inst)
			if inst.taunt == false then
				inst.sg:GoToState("idle")
			end
			inst.taunt = false
			inst.Physics:Stop()
			inst.AnimState:PlayAnimation("howl")
			inst.SoundEmitter:PlaySound("dontstarve_DLC001/creatures/vargr/howl")
		end,

		timeline = 
		{
			TimeEvent(10*FRAMES, SpawnHound),
		},

		events = 
		{
			EventHandler("animover", function(inst) 
				inst:DoTaskInTime(inst.components.revivablecorpse and PP_FORGE.VARG.SPECIAL_CD or 8, function() inst.taunt = true end)
				inst.sg:GoToState("idle") 
			end)
		},
	},

	State {
        name = "sleep",
        tags = { "sleeping" }, --add tag "busy" if you hate sliding

        onenter = function(inst)
            if inst.components.locomotor ~= nil then
                inst.components.locomotor:StopMoving()
            end
            inst.AnimState:PlayAnimation("sleep_pre")
            --if fns ~= nil and fns.onsleep ~= nil then
                --fns.onsleep(inst)
            --end
        end,

        --timeline = timelines ~= nil and timelines.starttimeline or nil,

        events =
        {
            EventHandler("animqueueover", function(inst) inst.sg:GoToState("sleeping") end ),
            EventHandler("onwakeup", function(inst) inst.sg:GoToState("wake") end),
        },
    },

    State
    {
        name = "sleeping",
        tags = { "sleeping" },

        --onenter = onentersleeping,
		
		onenter = function(inst)
				inst.components.locomotor:StopMoving()
				PlayablePets.SleepHeal(inst)
				inst.AnimState:PlayAnimation("sleep_loop")
			end,

        --timeline = timelines ~= nil and timelines.sleeptimeline or nil,
		timeline=
        {
			TimeEvent(0*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve_DLC001/creatures/vargr/sleep") end)
        },

        events =
        {
            EventHandler("animover", function(inst) inst.sg:GoToState("sleeping") end ),
			EventHandler("onwakeup", function(inst) inst.sg:GoToState("wake") end),
        },
    },

    State
    {
        name = "wake",
        tags = { "busy", "waking" },

        onenter = function(inst)
            if inst.components.locomotor ~= nil then
                inst.components.locomotor:StopMoving()
            end
            inst.AnimState:PlayAnimation("sleep_pst")
            if inst.components.sleeper ~= nil and inst.components.sleeper:IsAsleep() then
                inst.components.sleeper:WakeUp()
            end
        end,

        events =
        {
            EventHandler("animover", function(inst) inst.sg:GoToState("idle") end),
        },
    },
	
	

    State{
        name = "death",
        tags = {"busy"},

        onenter = function(inst)
            inst.AnimState:PlayAnimation("death")
			
            inst.Physics:Stop()
            RemovePhysicsColliders(inst)
            inst.components.lootdropper:DropLoot(inst:GetPosition())
			inst.components.inventory:DropEverything(true)
			
            if inst.components.playercontroller ~= nil then
                inst.components.playercontroller:RemotePausePrediction()
            end
        end,
		
		timeline=
		{
			TimeEvent(0*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve_DLC001/creatures/vargr/death") end)
		},
		
        events =
        {
            EventHandler("animover", function(inst)
                if inst.AnimState:AnimDone() then
                    PlayablePets.DoDeath(inst)
                end
            end),
        },
    },
	
}





  
CommonStates.AddRunStates(states, 
{	
	starttimeline = {},
    runtimeline = 
    { 
        TimeEvent(5*FRAMES, 
        function(inst) 
        	PlayFootstep(inst)
        	inst.SoundEmitter:PlaySound("dontstarve_DLC001/creatures/vargr/idle")
        end),
    },
	endtimeline = {},
})
CommonStates.AddFrozenStates(states)
local simpleanim = "run_pst"
local idleanim = "idle_loop"
PP_CommonStates.AddKnockbackState(states, nil, "hit") --(states, timeline, anim, sounds, fns)
PP_CommonStates.AddActionStates(states, 
	{
		TimeEvent(0 * FRAMES, function(inst)
            inst:PerformBufferedAction()
        end),
	}, 
	simpleanim, nil, nil, "idle_loop", simpleanim) --(states, timelines, anim, anim2, enteranim, loopanim, exitanim, noanim, events)
PP_CommonStates.AddCorpseStates(states, true, 
	--timelines = 
	{
		corpse =
		{
			TimeEvent(0*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve_DLC001/creatures/vargr/death") end)
		},
		
		corpse_taunt =
		{
			TimeEvent(0*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve_DLC001/creatures/vargr/howl") end),
		},
	
	},
	--anims = 
	{
		corpse = "death",
		corpse_taunt = "howl"
	},
	--sounds =
	{
		--corpse = "dontstarve_DLC003/creatures/enemy/grabbing_vine/death"
	},
	--fns =
	{

	}
	) ---(states, isflying, timelines, anims, sounds, fns, events, nofx)
PP_CommonStates.AddJumpInStates(states, nil, "run_pst")
PP_CommonStates.AddOpenGiftStates(states, "idle_loop")
CommonStates.AddHopStates(states, false, {pre = "run_pre", loop = "run_loop", pst = "run_pst"}, nil, "death")
PP_CommonStates.AddSailorStates(states, nil, 
{
	plank_idle = "run_pst",
	plank_idle_loop = "idle_loop",
	plank_idle_pst = simpleanim,
	
	plank_hop_pre = "run_pre",
	plank_hop = "run_loop",
	
	steer_pre = simpleanim,
	steer_idle = idleanim,
	steer_turning = simpleanim,
	stop_steering = simpleanim,
	
	row = simpleanim,
}
)
PP_CommonStates.AddAoeStates(states, nil, 
{
	generic = "run_pst",
	
	leap_pre = "run_pre",
	leap_loop = "run_loop",
	leap_pst = "run_pst",
	
	lunge_pre = "run_pre",
	lunge_loop = "run_loop",
	lunge_pst = "run_pst",
	
	superjump_pre = "run_pre",
	superjump_loop = "run_loop",
	superjump_pst = "run_pst",
	
	castspelltime = 10,
})   
    
return StateGraph("vargplayer", states, events, "idle", actionhandlers)

