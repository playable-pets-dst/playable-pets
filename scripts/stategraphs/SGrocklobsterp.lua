require("stategraphs/commonstates")
require("stategraphs/ppstates")

local function GetExcludeTags(inst)
	if TheNet:GetPVPEnabled() then
		return {"INLIMBO", "playerghost", "notarget", "shadow", "groundpoundimmune"}
	elseif TheNet:GetServerGameMode() == "lavaarena" then
		return {"player", "companion", "playerghost", "INLIMBO", "groundpoundimmune"}
	else	
		return {"player", "companion", "INLIMBO", "playerghost", "notarget", "shadow", "groundpoundimmune"}
	end
end

local function ShakeIfClose(inst)
    ShakeAllCameras(CAMERASHAKE.VERTICAL, .15, .02, .5, inst, 20)
end

local longaction = "action"
local shortaction = "action"
local workaction = "attack"
local otheraction = "eat"

local actionhandlers = PP_CommonStates.GetCommonActions(longaction, shortaction, workaction, otheraction)

local actionhandler_overrides = 
{ 
	ActionHandler(ACTIONS.PICKUP, "rocklick"),
	ActionHandler(ACTIONS.PICK, "rocklick"),
	ActionHandler(ACTIONS.HARVEST, "rocklick"),
}

if #actionhandler_overrides > 0 then
	for i, v in ipairs(actionhandler_overrides) do
		table.insert(actionhandlers, v)
	end
end

local events=
{
    EventHandler("attacked", function(inst) if not inst.components.health:IsDead() and not inst.sg:HasStateTag("attack") and not inst.sg:HasStateTag("hiding") then inst.sg:GoToState("hit") end end),
    EventHandler("doattack", function(inst, data) if not inst.components.health:IsDead() and (inst.sg:HasStateTag("hit") or not inst.sg:HasStateTag("busy")) then inst.sg:GoToState("attack", data.target) end end),
    CommonHandlers.OnSleep(),
    CommonHandlers.OnLocomote(true,false),
	PP_CommonHandlers.AddCommonHandlers(),
    CommonHandlers.OnFreeze(),
	CommonHandlers.OnHop(),
	PP_CommonHandlers.OpenGift(),
	PP_CommonHandlers.OnSink(),
	PP_CommonHandlers.OnKnockback(),
	PP_CommonHandlers.OnDeath(),
}


 local states=
{
	
	State{
		name = "idle",
		tags = {"idle", "canrotate"},

        onenter = function(inst, playanim)
            inst.Physics:Stop()
			inst.AnimState:PlayAnimation("idle_loop", true)
        end,

        timeline = 
        {
            --TimeEvent(5*FRAMES, function(inst) inst.SoundEmitter:PlaySound( "dontstarve/creatures/rocklobster/idle") end),        
            --TimeEvent(20*FRAMES, function(inst) inst.SoundEmitter:PlaySound( "dontstarve/creatures/rocklobster/idle") end),        
        },

        events=
        {
            EventHandler("animover", function(inst) 
				inst.sg:GoToState(math.random() < 0.33 and "idle_tendril" or "idle") 
			end),
        },
	},
	
	State{
		name = "idle_tendril",
		tags = {"idle", "canrotate"},

        onenter = function(inst, playanim)
            inst.Physics:Stop()
			inst.AnimState:PlayAnimation("idle_tendrils")
        end,

        timeline = 
        {
            TimeEvent(5*FRAMES, function(inst) inst.SoundEmitter:PlaySound( "dontstarve/creatures/rocklobster/idle") end),        
            TimeEvent(20*FRAMES, function(inst) inst.SoundEmitter:PlaySound( "dontstarve/creatures/rocklobster/idle") end),        
        },

        events=
        {
            EventHandler("animover", function(inst) inst.sg:GoToState("idle") end),
        },
	},
	
    State{
        name = "eat",
        tags = {"idle"},

        onenter = function(inst, playanim)
            inst.Physics:Stop()
            inst.AnimState:PlayAnimation("idle_tendrils")
            inst.SoundEmitter:PlaySound("dontstarve/creatures/rocklobster/foley")            
        end,

        timeline = 
        {
            TimeEvent(0*FRAMES, function(inst) inst.SoundEmitter:PlaySound( "dontstarve/creatures/rocklobster/foley") end),        
            TimeEvent(8*FRAMES, function(inst) 
                    inst:PerformBufferedAction() 
                    inst.SoundEmitter:PlaySound("dontstarve/creatures/rocklobster/idle")
                end),
            TimeEvent(20*FRAMES, function(inst) inst.SoundEmitter:PlaySound( "dontstarve/creatures/rocklobster/foley") end),        
        },

        events=
        {
            EventHandler("animover", function(inst) inst.sg:GoToState("idle") end),
        },
    },

    State{
        name = "special_atk1",
        tags = {"busy"},
        
        onenter = function(inst)
            inst.Physics:Stop()
            inst.AnimState:PlayAnimation("taunt")
            inst.SoundEmitter:PlaySound("dontstarve/creatures/rocklobster/foley")
            inst.SoundEmitter:PlaySound( "dontstarve/creatures/rocklobster/taunt")
        end,
        
        timeline = 
        {
            TimeEvent(10*FRAMES, function(inst) inst.SoundEmitter:PlaySound( "dontstarve/creatures/rocklobster/foley") end),        
            TimeEvent(30*FRAMES, function(inst) inst.SoundEmitter:PlaySound( "dontstarve/creatures/rocklobster/foley") end),                    
        },
        
        events=
        {
            EventHandler("animover", function(inst) inst.sg:GoToState("idle") end),
        },
    },

    State{
        name = "rocklick",
        tags = {"busy"},
        
        onenter = function(inst)
            inst.Physics:Stop()
            inst.AnimState:PlayAnimation("rocklick_pre")
            inst.AnimState:PushAnimation("rocklick_loop")
            inst.AnimState:PushAnimation("rocklick_pst", false)
        end,

        timeline = 
        {
            TimeEvent(5*FRAMES, function(inst) inst.SoundEmitter:PlaySound( "dontstarve/creatures/rocklobster/foley") end),        
            TimeEvent(10*FRAMES, function(inst) inst.SoundEmitter:PlaySound( "dontstarve/creatures/rocklobster/attack") end),
            TimeEvent(20*FRAMES, function(inst) inst.SoundEmitter:PlaySound( "dontstarve/creatures/rocklobster/foley") end),        
            TimeEvent(25*FRAMES, function(inst) inst:PerformBufferedAction() end ),
            TimeEvent(35*FRAMES, function(inst) inst.SoundEmitter:PlaySound( "dontstarve/creatures/rocklobster/foley") end),        
        },
        
        events=
        {
            EventHandler("animqueueover", function(inst) inst.sg:GoToState("idle") end),
        },
    }, 



    State{
        name = "hide_pre",
        tags = {"busy", "hiding"},

        onenter = function(inst)
            inst.AnimState:PlayAnimation("hide")
            inst.SoundEmitter:PlaySound( "dontstarve/creatures/rocklobster/foley")
            inst.SoundEmitter:PlaySound( "dontstarve/creatures/rocklobster/hide")
            inst.Physics:Stop()
        end,

        events=
        {
            EventHandler("animover", function(inst) inst.sg:GoToState("shield") end ),
        },
    },

    State{
        name = "hide_loop",
        tags = {"busy", "hiding"},

        onenter = function(inst)
			inst.components.health:SetAbsorptionAmount(TUNING.ROCKY_ABSORB)
            inst.AnimState:PlayAnimation("hide_loop")
        end,

        onexit = function(inst)
            inst.components.health:SetAbsorptionAmount(0)
        end,

        timeline = 
        {
            TimeEvent(20*FRAMES, function(inst) inst.SoundEmitter:PlaySound( "dontstarve/creatures/rocklobster/sleep") end),
        },


    },

    State{
        name = "hide_pst",
        tags = {"busy", "hiding"},

        onenter = function(inst)
            inst.AnimState:PlayAnimation("unhide")
            inst.SoundEmitter:PlaySound( "dontstarve/creatures/rocklobster/foley")
        end,

        timeline = 
        {
            TimeEvent(10*FRAMES, function(inst) inst.SoundEmitter:PlaySound( "dontstarve/creatures/rocklobster/foley") end),        
        },

        events=
        {
            EventHandler("animqueueover", function(inst) inst.sg:GoToState("idle") end ),
        },
    },
	
	State{
        name = "attack",
        tags = {"attack", "busy"},
        
        onenter = function(inst)
			inst.AnimState:PlayAnimation("atk")
        end,

        timeline = 
        {
            TimeEvent(0*FRAMES, function(inst) inst.SoundEmitter:PlaySound( "dontstarve/creatures/rocklobster/foley") end),        
        TimeEvent(0*FRAMES, function(inst) inst.SoundEmitter:PlaySound( "dontstarve/creatures/rocklobster/attack") end),
        TimeEvent(5*FRAMES, function(inst) inst.SoundEmitter:PlaySound( "dontstarve/creatures/rocklobster/foley") end),        
        TimeEvent(8*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/creatures/rocklobster/clawsnap_small") end),
        TimeEvent(12*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/creatures/rocklobster/clawsnap_small") end),
        TimeEvent(13*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/creatures/rocklobster/attack_whoosh") end),
        TimeEvent(20*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/creatures/rocklobster/clawsnap") end),
        TimeEvent(20*FRAMES, function(inst) PlayablePets.DoWork(inst, 6) end),
        TimeEvent(25*FRAMES, function(inst) inst.SoundEmitter:PlaySound( "dontstarve/creatures/rocklobster/foley") end),                
        TimeEvent(30*FRAMES, function(inst) inst.SoundEmitter:PlaySound( "dontstarve/creatures/rocklobster/foley") end),  
        
        },
        
        events=
        {
            EventHandler("animover", function(inst) inst.sg:GoToState("idle") end),
        },
    },
	
	State{
        name = "hit",
        tags = {"hit", "busy"},
        
        onenter = function(inst)
            inst.components.locomotor:StopMoving()    
			if inst:HasTag("hiding") then
			inst.AnimState:PlayAnimation("hide_hit") else
            inst.AnimState:PlayAnimation("hit")
			end
        end,

        timeline = 
        {
            TimeEvent(0*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/creatures/rocklobster/hurt") end),
			TimeEvent(0*FRAMES, function(inst) inst.SoundEmitter:PlaySound( "dontstarve/creatures/rocklobster/foley") end), 
        
        },
        
        events=
        {
            EventHandler("animover", function(inst) inst.sg:GoToState("idle") end),
        },
    },

	State {
        name = "sleep",
        tags = { "sleeping", "busy" }, 

        onenter = function(inst)
            if inst.components.locomotor ~= nil then
                inst.components.locomotor:StopMoving()
            end
            inst.AnimState:PlayAnimation("sleep_pre")
        end,

		timeline = 
		{
			TimeEvent(0*FRAMES, function(inst) inst.SoundEmitter:PlaySound( "dontstarve/creatures/rocklobster/foley") end),        
		},
		
        events =
        {
            EventHandler("animqueueover", function(inst) inst.sg:GoToState("sleeping") end ),
            EventHandler("onwakeup", function(inst) inst.sg:GoToState("wake") end),
        },
    },

    State
    {
        name = "sleeping",
        tags = { "sleeping", "busy" },

        --onenter = onentersleeping,
		
		onenter = function(inst)
				inst.components.locomotor:StopMoving()
				PlayablePets.SleepHeal(inst)
				--inst.SoundEmitter:PlaySound("dontstarve_DLC001/creatures/bearger/sleep")
				inst.AnimState:PlayAnimation("sleep_loop")
			end,

        --timeline = timelines ~= nil and timelines.sleeptimeline or nil,
		timeline = {
			TimeEvent(0*FRAMES, function(inst) inst.SoundEmitter:PlaySound( "dontstarve/creatures/rocklobster/sleep") end),
			TimeEvent(20*FRAMES, function(inst) inst.SoundEmitter:PlaySound( "dontstarve/creatures/rocklobster/foley") end),        
		},

        events =
        {
            EventHandler("animover", function(inst) inst.sg:GoToState("sleeping") end ),
			EventHandler("onwakeup", function(inst) inst.sg:GoToState("wake") end),
        },
    },

    State
    {
        name = "wake",
        tags = { "busy", "waking" },

        onenter = function(inst)
            if inst.components.locomotor ~= nil then
                inst.components.locomotor:StopMoving()
            end
            inst.AnimState:PlayAnimation("sleep_pst")
            if inst.components.sleeper ~= nil and inst.components.sleeper:IsAsleep() then
                inst.components.sleeper:WakeUp()
            end
        end,

		timeline =
		{
			TimeEvent(0*FRAMES, function(inst) inst.SoundEmitter:PlaySound( "dontstarve/creatures/rocklobster/foley") end),        
        },
		
        events =
        {
            EventHandler("animover", function(inst) inst.sg:GoToState("idle") end),
        },
    },

    State{
        name = "death",
        tags = {"busy"},

        onenter = function(inst)
            inst.AnimState:PlayAnimation("death")
            inst.Physics:Stop()
            RemovePhysicsColliders(inst)
			
            inst.components.lootdropper:DropLoot(inst:GetPosition())
			inst.components.inventory:DropEverything(true)

            if inst.components.playercontroller ~= nil then
                inst.components.playercontroller:RemotePausePrediction()
            end
        end,
		
		timeline =
		{
			TimeEvent(0*FRAMES, function(inst) 
				inst.SoundEmitter:PlaySound( "dontstarve/creatures/rocklobster/death") 
				inst.SoundEmitter:PlaySound( "dontstarve/creatures/rocklobster/explode") 
			end),
		},
		
        events =
        {
            EventHandler("animover", function(inst)
                if inst.AnimState:AnimDone() then
                    PlayablePets.DoDeath(inst)
                end
            end),
        },
    },

	State
    {
        name = "run_start",
        tags = { "moving", "running", "canrotate" },

        onenter = function(inst)
			inst.components.locomotor:WalkForward()
            inst.AnimState:PlayAnimation("walk_pre")
        end,
		
		timeline =  {
			TimeEvent(0*FRAMES, function(inst) inst.SoundEmitter:PlaySound( "dontstarve/creatures/rocklobster/foley") end),
		},

        events =
        {
            EventHandler("animqueueover", function(inst) inst.sg:GoToState("run") end ),
        },
    },
	
	State
    {
        name = "run",
        tags = { "moving", "running", "canrotate" },

        onenter = function(inst)
			inst.components.locomotor:WalkForward()
			inst.AnimState:PlayAnimation("walk_loop")
        end,
		
		timeline =
        {
           TimeEvent(1*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/creatures/rocklobster/footstep") end),
        TimeEvent(8*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/creatures/rocklobster/footstep") end),
        TimeEvent(12*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/creatures/rocklobster/footstep") end),
        TimeEvent(15*FRAMES, function(inst) inst.SoundEmitter:PlaySound( "dontstarve/creatures/rocklobster/foley") end),        
        TimeEvent(26*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/creatures/rocklobster/footstep") end),
        TimeEvent(30*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/creatures/rocklobster/footstep") end),
        },
		
        events=
			{
				EventHandler("animqueueover", function(inst) inst.sg:GoToState("run") end ),
			},

    },
	
	State
    {
        name = "run_stop",
        tags = { "idle" },

        onenter = function(inst) 
            inst.components.locomotor:StopMoving()
			inst.AnimState:PlayAnimation("walk_pst")
            
        end,

		timeline = {
			TimeEvent(0*FRAMES, function(inst) inst.SoundEmitter:PlaySound( "dontstarve/creatures/rocklobster/foley") end),    
		},
		
        events =
        {
            EventHandler("animqueueover", function(inst) inst.sg:GoToState("idle") end ),
        },
    },
}
	
CommonStates.AddFrozenStates(states)
PP_CommonStates.AddKnockbackState(states, nil, "hit") --(states, timeline, anim, sounds, fns)
PP_CommonStates.AddActionStates(states, 
	{
		TimeEvent(0 * FRAMES, function(inst)
			inst.SoundEmitter:PlaySound( "dontstarve/creatures/rocklobster/foley")
            inst:PerformBufferedAction()
        end),
	}, 
	"walk_pst", nil, nil, "idle_loop", "walk_pst") --(states, timelines, anim, anim2, enteranim, loopanim, exitanim, noanim, events)
PP_CommonStates.AddCorpseStates(states, true, 
	--timelines = 
	{
		corpse =
		{
			TimeEvent(0*FRAMES, function(inst) 
				inst.SoundEmitter:PlaySound( "dontstarve/creatures/rocklobster/death") 
				inst.SoundEmitter:PlaySound( "dontstarve/creatures/rocklobster/explode") 
			end),
		},
		
		corpse_taunt =
		{
			TimeEvent(0*FRAMES, function(inst) 
				inst.SoundEmitter:PlaySound("dontstarve/creatures/rocklobster/foley")
				inst.SoundEmitter:PlaySound( "dontstarve/creatures/rocklobster/taunt") 
			end),
			TimeEvent(10*FRAMES, function(inst) inst.SoundEmitter:PlaySound( "dontstarve/creatures/rocklobster/foley") end),        
            TimeEvent(30*FRAMES, function(inst) inst.SoundEmitter:PlaySound( "dontstarve/creatures/rocklobster/foley") end),     
		},
	
	},
	--anims = 
	{
		corpse = "death",
		corpse_taunt = "taunt"
	},
	--sounds =
	{
		--corpse = "dontstarve_DLC003/creatures/enemy/grabbing_vine/death"
	},
	--fns =
	{

	}
	) ---(states, isflying, timelines, anims, sounds, fns, events, nofx)
PP_CommonStates.AddJumpInStates(states, nil, "walk_pst")
PP_CommonStates.AddOpenGiftStates(states, "idle_loop")
CommonStates.AddHopStates(states, false, {pre = "walk_pre", loop = "walk_loop", pst = "walk_pst"}, nil, "death")
PP_CommonStates.AddSailorStates(states, nil, 
{
	plank_idle = "idle_loop",
	plank_idle_loop = "idle_loop",
	plank_idle_pst = "walk_pst",
	
	plank_hop_pre = "walk_pre",
	plank_hop = "walk_loop",
	
	steer_pre = "walk_pst",
	steer_idle = "idle_loop",
	steer_turning = "taunt",
	stop_steering = "walk_pst",
	
	row = "walk_pst",
}
)
PP_CommonStates.AddAoeStates(states, nil, 
{
	generic = "walk_pst",
	
	leap_pre = "walk_pre",
	leap_loop = "walk_loop",
	leap_pst = "walk_pst",
	
	castspelltime = 10,
})    
    
return StateGraph("rocklobsterp", states, events, "idle", actionhandlers)

