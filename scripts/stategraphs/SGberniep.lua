require("stategraphs/commonstates")
require("stategraphs/ppstates")

local function SetSmall(inst)
	inst.big = nil
	inst.AnimState:SetBank("bernie")
	inst:RemoveTag("epic")
	inst.shouldwalk = true
	inst.Physics:SetMass(50)
	
	inst.components.combat:SetAttackPeriod(0)
	inst.components.combat:SetRange(30, 0)
	inst:RemoveTag("epic")
	inst.Transform:SetScale(1, 1, 1)
	inst.components.locomotor.walkspeed = inst.mob_table.walkspeed
    inst.components.locomotor.runspeed = inst.mob_table.runspeed
end

local function SetBig(inst)
	inst.big = true
	inst.AnimState:SetBank("bernie_big")
	inst:AddTag("epic")
	inst.shouldwalk = false
	
	inst.components.combat:SetAttackPeriod(2)
	inst.components.combat:SetRange(5)
	inst.Physics:SetMass(500)	
	
	inst.Transform:SetScale(0.7, 0.7, 0.7)
	inst:AddTag("epic")
	inst.components.locomotor.walkspeed = TUNING.BERNIE_BIG_WALK_SPEED
    inst.components.locomotor.runspeed = TUNING.BERNIE_BIG_RUN_SPEED
end

local function DoFootStep(inst)
    if inst.sg:HasStateTag("running") then
        inst.SoundEmitter:PlaySoundWithParams("dontstarve/creatures/together/bernie_big/footstep", { speed = 1 })
    else
        inst.SoundEmitter:PlaySound("dontstarve/creatures/together/bernie_big/footstep")
    end
    inst.sg.mem.lastfootstep = GetTime()
end

local function DoStopFootStep(inst)
    local t = GetTime()
    if (inst.sg.mem.lastfootstep or 0) + 7 * FRAMES < t then
        inst.SoundEmitter:PlaySound("dontstarve/creatures/together/bernie_big/footstep")
        inst.sg.mem.lastfootstep = t
    end
end

local function EaseShadow(inst, k)
    inst.DynamicShadow:SetSize(inst.sg.statemem.shadowstart[1] + k * (inst.sg.statemem.shadowend[1] - inst.sg.statemem.shadowstart[1]), inst.sg.statemem.shadowstart[2] + k * (inst.sg.statemem.shadowend[2] - inst.sg.statemem.shadowstart[2]))
end

local function EaseOutShadow(inst, k)
    k = 1 - k
    EaseShadow(inst, 1 - k * k)
end

local function EaseInShadow(inst, k)
    EaseShadow(inst, k * k)
end

local function EaseInOutShadow(inst, k)
    if k <= .5 then
        EaseShadow(inst, 2 * k * k)
    else
        k = 2 - 2 * k
        EaseShadow(inst, 1 - .5 * k * k)
    end
end

local function DrawAggro(inst)
	local x, y, z = inst.Transform:GetWorldPosition()
	local ents = TheSim:FindEntities(x, y, z, 30, nil, { "playerghost", "ghost", "INLIMBO", "player" }, {"hostile", "monster", "hound", "epic"} )
	for i, v in ipairs(ents) do
        if v ~= nil and v.components.combat and v.components.health and not v.components.health:IsDead() then
			v.components.combat:SetTarget(inst)
		end
    end

end

local function LoseAggro(inst)
	local x, y, z = inst.Transform:GetWorldPosition()
	local ents = TheSim:FindEntities(x, y, z, 30, nil, { "playerghost", "ghost", "INLIMBO", "player" }, {"hostile", "monster", "hound", "epic"} )
	for i, v in ipairs(ents) do
        if v ~= nil and v.components.combat and v.components.health and not v.components.health:IsDead() then
			if v.components.combat.target and v.components.combat.target == inst then
			v.components.combat:SetTarget(nil)
			end
		end
    end

end

local longaction = "action"
local shortaction = "action"
local workaction = "action"
local otheraction = "action"

local actionhandlers = PP_CommonStates.GetCommonActions(longaction, shortaction, workaction, otheraction)

local actionhandler_overrides = 
{ 

}

if #actionhandler_overrides > 0 then
	for i, v in ipairs(actionhandler_overrides) do
		table.insert(actionhandlers, v)
	end
end

local function SetSleeperAwakeState(inst)
    if inst.components.grue ~= nil then
        inst.components.grue:RemoveImmunity("sleeping")
    end
    if inst.components.talker ~= nil then
        inst.components.talker:StopIgnoringAll("sleeping")
    end
    if inst.components.firebug ~= nil then
        inst.components.firebug:Enable()
    end
    if inst.components.playercontroller ~= nil then
        inst.components.playercontroller:EnableMapControls(true)
        inst.components.playercontroller:Enable(true)
    end
    inst:OnWakeUp()
    inst.components.inventory:Show()
    inst:ShowActions(true)
	--inst.sg:GoToState("taunt")
end

local function SetSleeperSleepState(inst)
    if inst.components.grue ~= nil then
        inst.components.grue:AddImmunity("sleeping")
    end
    if inst.components.talker ~= nil then
        inst.components.talker:IgnoreAll("sleeping")
    end
    if inst.components.firebug ~= nil then
        inst.components.firebug:Disable()
    end
    if inst.components.playercontroller ~= nil then
        inst.components.playercontroller:EnableMapControls(false)
        inst.components.playercontroller:Enable(false)
    end
    inst:OnSleepIn()
    inst.components.inventory:Hide()
    inst:PushEvent("ms_closepopups")
    inst:ShowActions(false)
end

local events=
{
    EventHandler("attacked", function(inst) if not inst.components.health:IsDead() and not inst.sg:HasStateTag("busy") then inst.sg:GoToState("hit") end end),
    EventHandler("doattack", function(inst, data) if not inst.components.health:IsDead() and (inst.sg:HasStateTag("hit") or not inst.sg:HasStateTag("busy")) then inst.sg:GoToState("attack", data.target) end end),
    CommonHandlers.OnSleep(),
    PP_CommonHandlers.OnLocomoteAdvanced(),
	PP_CommonHandlers.OnKnockback(),
	PP_CommonHandlers.OpenGift(),
	PP_CommonHandlers.OnDeath(),
	PP_CommonHandlers.AddCommonHandlers(),
    CommonHandlers.OnFreeze(),
	
	 EventHandler("ms_opengift",
        function(inst)
            if not inst.sg:HasStateTag("busy") then
                inst.sg:GoToState("opengift")
            end
        end),
		
	EventHandler("respawnfromghost", function(inst)  
			if inst.components.playercontroller ~= nil then
                inst.components.playercontroller:Enable(true)
            end

            inst.components.health:SetInvincible(false)
            inst:ShowHUD(true)
            inst:SetCameraDistance()

            SerializeUserSession(inst) end),	
}

if CurrentRelease.GreaterOrEqualTo(ReleaseID.IDs.R08_ROT_TURNOFTIDES) then
	table.insert(events, PP_CommonHandlers.OnSink())
	table.insert(events, CommonHandlers.OnHop())
end

 local states=
{

    State{
        name = "idle",
        tags = { "idle", "canrotate" },

        onenter = function(inst)
            inst.Physics:Stop()
            if not inst.AnimState:IsCurrentAnimation("idle_loop") then
                inst.AnimState:PlayAnimation("idle_loop", true)
            end
            inst.SoundEmitter:PlaySound("dontstarve/creatures/together/bernie/idle")
            inst.sg:SetTimeout(inst.AnimState:GetCurrentAnimationLength())
        end,

        ontimeout = function(inst)
            inst.sg:GoToState("idle")
        end,
    },

	State{
        name = "attack2",
        tags = { "attack", "busy" },

        onenter = function(inst, target)
			local buffaction = inst:GetBufferedAction()
            local target = buffaction ~= nil and buffaction.target or nil
            inst.components.locomotor:Stop()
            inst.AnimState:PlayAnimation("atk")
            inst.components.combat:StartAttack()
            if target ~= nil and target:IsValid() then
                inst.sg.statemem.target = target
                inst:ForceFacePoint(target.Transform:GetWorldPosition())
            end
            inst.SoundEmitter:PlaySound("dontstarve/creatures/together/bernie_big/vo_atk_pre")
        end,

        timeline =
        {
            TimeEvent(FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/creatures/together/bernie_big/attack_pre") end),
            TimeEvent(15 * FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/creatures/together/bernie_big/attack") end),
            TimeEvent(16 * FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/creatures/together/bernie_big/vo_atk") end),
            TimeEvent(23 * FRAMES, function(inst)
                local target = inst.sg.statemem.target
                if target ~= nil and math.abs(anglediff(inst.Transform:GetRotation(), inst:GetAngleToPoint(target.Transform:GetWorldPosition()))) < 90 then
                    inst:PerformBufferedAction()
                end
            end),
            TimeEvent(38 * FRAMES, function(inst)
                inst.sg:AddStateTag("caninterrupt")
            end),
            TimeEvent(39 * FRAMES, function(inst)
                inst.sg:RemoveStateTag("busy")
            end),
        },

        events =
        {
            EventHandler("animover", function(inst)
                if inst.AnimState:AnimDone() then
                    inst.sg:GoToState("idle")
                end
            end),
        },
    },
	
	State{
        name = "attack",
        tags = { "busy" },

        onenter = function(inst)
			if inst.big then
				inst.sg:GoToState("attack2")
			else
				inst.Physics:Stop()
				inst.AnimState:PlayAnimation("taunt")
				DrawAggro(inst)
				--inst.components.timer:StartTimer("taunt_cd", 4)
			end
        end,

        timeline =
        {
            --3, 12, 21, 30
            TimeEvent(FRAMES*3, function(inst) inst.SoundEmitter:PlaySound("dontstarve/creatures/together/bernie/taunt") end),
            TimeEvent(FRAMES*12, function(inst) inst.SoundEmitter:PlaySound("dontstarve/creatures/together/bernie/taunt") end),
            TimeEvent(FRAMES*21, function(inst) inst.SoundEmitter:PlaySound("dontstarve/creatures/together/bernie/taunt") end),
            TimeEvent(FRAMES*30, function(inst) inst.SoundEmitter:PlaySound("dontstarve/creatures/together/bernie/taunt") DrawAggro(inst) end),
            --10, 20, 28, 36
            TimeEvent(FRAMES*10, PlayFootstep),
            TimeEvent(FRAMES*20, PlayFootstep),
            TimeEvent(FRAMES*28, PlayFootstep),
            TimeEvent(FRAMES*36, PlayFootstep),
        },

        events =
        {
            EventHandler("animover", function(inst) inst.sg:GoToState("idle") end),
        },
    },
	
	State{
        name = "hit",
        tags = { "busy" },

        onenter = function(inst)
            inst.Physics:Stop()
            inst.AnimState:PlayAnimation("hit")
			if inst.big then
				inst.SoundEmitter:PlaySound("dontstarve/creatures/together/bernie_big/hit")
				inst.sg.mem.last_hit_time = GetTime()
			else
				inst.SoundEmitter:PlaySound("dontstarve/creatures/together/bernie/hit")
			end
        end,

        events =
        {
            EventHandler("animover", function(inst) inst.sg:GoToState("idle") end),
        },
    },	
	
	State{
        name = "taunt",
        tags = { "busy" },

        onenter = function(inst)
            inst.components.locomotor:Stop()
            inst.AnimState:PlayAnimation("taunt")
        end,

        timeline =
        {
            TimeEvent(3 * FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/creatures/together/bernie_big/vo_taunt") end),
            TimeEvent(4 * FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/creatures/together/bernie_big/taunt") end),
            TimeEvent(16 * FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/creatures/together/bernie_big/taunt") end),
            TimeEvent(28 * FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/creatures/together/bernie_big/taunt") end),
            TimeEvent(30 * FRAMES, function(inst)
                DrawAggro(inst)
            end),
            TimeEvent(40 * FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/creatures/together/bernie_big/taunt") end),
            TimeEvent(52 * FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/creatures/together/bernie_big/taunt") end),
        },

        events =
        {
            EventHandler("animover", function(inst)
                if inst.AnimState:AnimDone() then
                    inst.sg:GoToState("idle")
                end
            end),
        },
    },
	
	State{
		name = "special_atk1",
        tags = {"busy"},

        onenter = function(inst, cb)
			if inst.big then
				inst.sg:GoToState("taunt")
			else
				inst.Physics:Stop()
				inst.AnimState:PlayAnimation("taunt")
			end
        end,
		
			timeline =
        {
            --3, 12, 21, 30
            TimeEvent(FRAMES*3, function(inst) inst.SoundEmitter:PlaySound("dontstarve/creatures/together/bernie/taunt") end),
            TimeEvent(FRAMES*12, function(inst) inst.SoundEmitter:PlaySound("dontstarve/creatures/together/bernie/taunt") end),
            TimeEvent(FRAMES*21, function(inst) inst.SoundEmitter:PlaySound("dontstarve/creatures/together/bernie/taunt") end),
            TimeEvent(FRAMES*30, function(inst) inst.SoundEmitter:PlaySound("dontstarve/creatures/together/bernie/taunt") DrawAggro(inst) end),
            --10, 20, 28, 36
            TimeEvent(FRAMES*10, PlayFootstep),
            TimeEvent(FRAMES*20, PlayFootstep),
            TimeEvent(FRAMES*28, PlayFootstep),
            TimeEvent(FRAMES*36, PlayFootstep),
        },


        events=
        {
			EventHandler("animover", function(inst) inst.sg:GoToState("idle") end),
        },
    },
	
	State{
		name = "special_atk2",
        tags = {"busy", "nointerrupt"},

        onenter = function(inst, cb)
            inst.Physics:Stop()
			inst.sg.statemem.shadowstart = { 1, .5 }
            inst.sg.statemem.shadowend = { .7 + 2.75 * .3, .5 * .7 + 1.3 * .3 }
			if inst.big then
				inst.AnimState:PlayAnimation("walk_pst")
				if inst.shouldwalk == false then
					inst.shouldwalk = true
				elseif inst.shouldwalk == true then
					inst.shouldwalk = false
				end
			else
				--SetBig(inst) --set inst.big onexit so the timeline can play.
				inst.AnimState:SetBank("bernie_big")
				inst.AnimState:PlayAnimation("activate")
			end
        end,
		
		timeline =
        {
			TimeEvent(0, function(inst) if not inst.big then inst.SoundEmitter:PlaySound("dontstarve/creatures/together/bernie_big/activate") end end),
            TimeEvent(FRAMES, function(inst) if not inst.big then EaseOutShadow(inst, .25) end end),
            TimeEvent(2 * FRAMES, function(inst) if not inst.big then EaseOutShadow(inst, .5) end end),
            TimeEvent(3 * FRAMES, function(inst) if not inst.big then EaseOutShadow(inst, .75) end end),
            TimeEvent(4 * FRAMES, function(inst) if not inst.big then inst.DynamicShadow:SetSize(unpack(inst.sg.statemem.shadowend)) end end),
            TimeEvent(15 * FRAMES, function(inst)
				if not inst.big then
                local temp = inst.sg.statemem.shadowstart
                inst.sg.statemem.shadowstart = inst.sg.statemem.shadowend
                inst.sg.statemem.shadowend = temp
                temp[1], temp[2] = (1 + 2.75) * .5, (.5 + 1.3) * .5
                EaseShadow(inst, 1 / 3)
				end
            end),
            TimeEvent(16 * FRAMES, function(inst) if not inst.big then EaseShadow(inst, 2 / 3) end end),
            TimeEvent(17 * FRAMES, function(inst) if not inst.big then inst.DynamicShadow:SetSize(unpack(inst.sg.statemem.shadowend)) end end),
            TimeEvent(35 * FRAMES, function(inst)
				if not inst.big then
					local temp = inst.sg.statemem.shadowstart
					inst.sg.statemem.shadowstart = inst.sg.statemem.shadowend
					inst.sg.statemem.shadowend = temp
					temp[1], temp[2] = 2.75, 1.3
					EaseOutShadow(inst, .2)
				end
            end),
            TimeEvent(36 * FRAMES, function(inst)
				if not inst.big then
					EaseOutShadow(inst, .4)
					inst.SoundEmitter:PlaySound("dontstarve/creatures/together/bernie_big/footstep", nil, .5)
				end
            end),
            TimeEvent(37 * FRAMES, function(inst) if not inst.big then EaseOutShadow(inst, .6) end end),
            TimeEvent(38 * FRAMES, function(inst) if not inst.big then EaseOutShadow(inst, .8) end end),
            TimeEvent(39 * FRAMES, function(inst) if not inst.big then inst.DynamicShadow:SetSize(unpack(inst.sg.statemem.shadowend)) end end),
        },
		
		onexit = function(inst)
			if not inst.big then
				SetBig(inst)
			end
		end,


        events=
        {
			EventHandler("animover", function(inst) inst.sg:GoToState("idle") end),
        },
    },
	
    State{
        name = "death",
        tags = {"busy"},

        onenter = function(inst)
			inst.SoundEmitter:PlaySound("dontstarve/creatures/together/bernie/death")
            inst.AnimState:PlayAnimation("death")
            inst.Physics:Stop()
            RemovePhysicsColliders(inst)     
			--inst.components.lootdropper:DropLoot(Vector3(inst.Transform:GetWorldPosition()))
			inst.components.inventory:DropEverything(true)
        end,

		timeline =
        {
            TimeEvent(4 * FRAMES, function(inst)
				if inst.big then
					inst.SoundEmitter:PlaySound("dontstarve/creatures/together/bernie_big/footstep")
					inst.sg.statemem.shadowstart = { 2.75, 1.3 }
					inst.sg.statemem.shadowend = { 2.75 * .7 + .3, 1.3 * .7 + .5 * .3 }
					EaseInOutShadow(inst, .125)
				end
            end),
            TimeEvent(5 * FRAMES, function(inst)
				if inst.big then
					inst.SoundEmitter:PlaySound("dontstarve/creatures/together/bernie_big/vo_death_drop")
					EaseInOutShadow(inst, .25)
				end
            end),
            TimeEvent(6 * FRAMES, function(inst) if inst.big then EaseInOutShadow(inst, .375) end end),
            TimeEvent(7 * FRAMES, function(inst) if inst.big then EaseInOutShadow(inst, .5) end end),
            TimeEvent(8 * FRAMES, function(inst) if inst.big then EaseInOutShadow(inst, .625) end end),
            TimeEvent(9 * FRAMES, function(inst) if inst.big then EaseInOutShadow(inst, .75) end end),
            TimeEvent(10 * FRAMES, function(inst) if inst.big then EaseInOutShadow(inst, .875) end end),
            TimeEvent(11 * FRAMES, function(inst) if inst.big then inst.DynamicShadow:SetSize(unpack(inst.sg.statemem.shadowend)) end end),
            TimeEvent(27 * FRAMES, function(inst) if inst.big then inst.SoundEmitter:PlaySound("dontstarve/creatures/together/bernie_big/death") end end),
            TimeEvent(31 * FRAMES, function(inst) if inst.big then inst.SoundEmitter:PlaySound("dontstarve/creatures/together/bernie_big/vo_death_collapse") end end),
            TimeEvent(32 * FRAMES, function(inst)
				if inst.big then
					local temp = inst.sg.statemem.shadowstart
					inst.sg.statemem.shadowstart = inst.sg.statemem.shadowend
					inst.sg.statemem.shadowend = temp
					temp[1], temp[2] = 1, .5
					EaseInShadow(inst, .2)
				end
            end),
            TimeEvent(33 * FRAMES, function(inst) if inst.big then EaseInShadow(inst, .4) end end),
            TimeEvent(34 * FRAMES, function(inst) if inst.big then EaseInShadow(inst, .6) end end),
            TimeEvent(35 * FRAMES, function(inst) if inst.big then EaseInShadow(inst, .8) end end),
            TimeEvent(36 * FRAMES, function(inst) if inst.big then inst.DynamicShadow:SetSize(unpack(inst.sg.statemem.shadowend)) end end)
        },
		
		onexit = function(inst)
            --V2C: shouldn't happen
            inst.DynamicShadow:SetSize(2.75, 1.3)
        end,
		
        events =
        {
            EventHandler("animover", function(inst)
                if inst.AnimState:AnimDone() then
					SetSmall(inst)
					PlayablePets.DoDeath(inst)
                end
            end),
        },

    },
	
	State {
        name = "sleep",
        tags = { "sleeping", "busy" }, --add tag "busy" if you hate sliding

        onenter = function(inst)
            if inst.components.locomotor ~= nil then
                inst.components.locomotor:StopMoving()
            end
            inst.AnimState:PlayAnimation("deactivate", false)
			if inst.big then
				--SetSmall(inst)
			end
			LoseAggro(inst)
            --if fns ~= nil and fns.onsleep ~= nil then
                --fns.onsleep(inst)
            --end
        end,
		
		timeline =
        {
			TimeEvent(0*FRAMES, function(inst)
				if inst.big then
					inst.Transform:SetScale(0.7, 0.7, 0.7)
					inst.SoundEmitter:PlaySound("dontstarve/creatures/together/bernie_big/deactivate")
				end
			end),
            TimeEvent(3 * FRAMES, function(inst)
				if inst.big then
					inst.sg.statemem.shadowstart = { 2.75, 1.3 }
					inst.sg.statemem.shadowend = { 2.75 * .7 + .3, 1.3 * .7 + .5 * .3 }
					EaseOutShadow(inst, .2)
				end	
            end),
			TimeEvent(5*FRAMES, function(inst)
				if not inst.big then
					inst.SoundEmitter:PlaySound("dontstarve/creatures/together/bernie/sit_down")
				end
			end),
            TimeEvent(4 * FRAMES, function(inst) if inst.big then EaseOutShadow(inst, .4) end end),
            TimeEvent(5 * FRAMES, function(inst) if inst.big then EaseOutShadow(inst, .6) end end),
            TimeEvent(6 * FRAMES, function(inst) if inst.big then EaseOutShadow(inst, .8) end end),
            TimeEvent(7 * FRAMES, function(inst) if inst.big then inst.DynamicShadow:SetSize(unpack(inst.sg.statemem.shadowend)) end end),
            TimeEvent(18 * FRAMES, function(inst)
				if inst.big then
					local temp = inst.sg.statemem.shadowstart
					inst.sg.statemem.shadowstart = inst.sg.statemem.shadowend
					inst.sg.statemem.shadowend = temp
					temp[1], temp[2] = 1, .5
					EaseInOutShadow(inst, .2)
				end	
            end),
            TimeEvent(19 * FRAMES, function(inst) if inst.big then EaseInOutShadow(inst, .4) end end),
            TimeEvent(20 * FRAMES, function(inst) if inst.big then EaseInOutShadow(inst, .6) end end),
            TimeEvent(21 * FRAMES, function(inst) if inst.big then EaseInOutShadow(inst, .8) end end),
            TimeEvent(22 * FRAMES, function(inst) if inst.big then inst.DynamicShadow:SetSize(unpack(inst.sg.statemem.shadowend)) end end),
        },
		
        
		
		onexit = function(inst)
			SetSmall(inst)
			inst.Transform:SetScale(1, 1, 1)
		end,

        events =
        {
            EventHandler("animqueueover", function(inst) inst.sg:GoToState("sleeping") end ),
            EventHandler("onwakeup", function(inst) inst.sg:GoToState("wake") end),
        },
    },

    State
    {
        name = "sleeping",
        tags = { "sleeping", "busy" },

        --onenter = onentersleeping,
		
		onenter = function(inst)
				inst.components.locomotor:StopMoving()
			end,

        events =
        {
			EventHandler("onwakeup", function(inst) inst.sg:GoToState("wake") end),
        },
    },

    State
    {
        name = "wake",
        tags = { "busy", "waking" },

        onenter = function(inst)
            if inst.components.locomotor ~= nil then
                inst.components.locomotor:StopMoving()
            end
            inst.AnimState:PlayAnimation("activate")
            if inst.components.sleeper ~= nil and inst.components.sleeper:IsAsleep() then
                inst.components.sleeper:WakeUp()
            end
            --if fns ~= nil and fns.onwake ~= nil then
                --fns.onwake(inst)
            --end
        end,

        TimeEvent(5*FRAMES, function(inst)
                inst.SoundEmitter:PlaySound("dontstarve/creatures/together/bernie/sit_up")
        end),

        events =
        {
            EventHandler("animover", function(inst) inst.sg:GoToState("idle") end),
        },
    },
	
	State
    {
        name = "walk_start",
        tags = { "moving", "running", "canrotate" },

        onenter = function(inst)
			inst.components.locomotor:WalkForward()
            inst.AnimState:PlayAnimation("walk_pre")
			
        end,
		
		timeline = {
			TimeEvent(0, function(inst)
				if inst.big then
					DoStopFootStep(inst)
				end	
			end),
			TimeEvent(11 * FRAMES, function(inst)
				if inst.big then
					DoFootStep(inst)
				end	
			end),
		},
		
        events =
        {
            EventHandler("animqueueover", function(inst) inst.sg:GoToState("walk") end ),
        },
    },
	
	State
    {
        name = "walk",
        tags = { "moving", "running", "canrotate" },

        onenter = function(inst)
			inst.components.locomotor:WalkForward()
			inst.AnimState:PlayAnimation("walk_loop")
        end,
		
		timeline = 
		{
			TimeEvent(10*FRAMES, function(inst)
				if not inst.big then
					PlayFootstep(inst) 
					inst.SoundEmitter:PlaySound("dontstarve/creatures/together/bernie/walk")
				end
			end),
			TimeEvent(30*FRAMES, function(inst)
				if not inst.big then
					PlayFootstep(inst)
					inst.SoundEmitter:PlaySound("dontstarve/creatures/together/bernie/walk")
				end	
			end),
			TimeEvent(21 * FRAMES, function(inst) if inst.big then DoFootStep(inst) end end),
			TimeEvent(40 * FRAMES, function(inst) if inst.big then DoFootStep(inst) end end),
			
		},
		
        events=
			{
				EventHandler("animqueueover", function(inst) inst.sg:GoToState("walk") end ),
			},

    },
	
	State
    {
        name = "walk_stop",
        tags = { "idle" },

        onenter = function(inst) 
            inst.components.locomotor:StopMoving()
			inst.AnimState:PlayAnimation("walk_pst")
			
            
        end,
		
		timeline = {
			TimeEvent(0, function(inst) if inst.big then DoStopFootStep(inst) end end),
			TimeEvent(3*FRAMES, function(inst)
				if not inst.big then
					PlayFootstep(inst) 
					inst.SoundEmitter:PlaySound("dontstarve/creatures/together/bernie/walk")
				end	
			end),
		},

        events =
        {
            EventHandler("animqueueover", function(inst) inst.sg:GoToState("idle") end ),
        },
    },	
}

CommonStates.AddRunStates(states,
{
    starttimeline =
    {
        TimeEvent(0, function(inst)
            local t = GetTime()
            if (inst.sg.mem.lastfootstep or 0) + 7 * FRAMES < t then
                inst.SoundEmitter:PlaySound("dontstarve/creatures/together/bernie_big/footstep", nil, .5)
                inst.sg.mem.lastfootstep = t
            end
            if (inst.sg.mem.lastrunvo or 0) + 3 < t then
                inst.SoundEmitter:PlaySound("dontstarve/creatures/together/bernie_big/vo_run_start")
                inst.sg.mem.lastrunvo = t
            end
        end),
    },
    runtimeline =
    {
        TimeEvent(11 * FRAMES, DoFootStep),
        TimeEvent(27 * FRAMES, DoFootStep),
    },
    endtimeline =
    {
        TimeEvent(0 * FRAMES, DoStopFootStep),
    },
})
PP_CommonStates.AddCorpseStates(states, true, 
	--timelines = 
	{
		corpse =
		{
			TimeEvent(4 * FRAMES, function(inst)
				if inst.big then
					inst.SoundEmitter:PlaySound("dontstarve/creatures/together/bernie_big/footstep")
					inst.sg.statemem.shadowstart = { 2.75, 1.3 }
					inst.sg.statemem.shadowend = { 2.75 * .7 + .3, 1.3 * .7 + .5 * .3 }
					EaseInOutShadow(inst, .125)
				end
            end),
            TimeEvent(5 * FRAMES, function(inst)
				if inst.big then
					inst.SoundEmitter:PlaySound("dontstarve/creatures/together/bernie_big/vo_death_drop")
					EaseInOutShadow(inst, .25)
				end
            end),
            TimeEvent(6 * FRAMES, function(inst) if inst.big then EaseInOutShadow(inst, .375) end end),
            TimeEvent(7 * FRAMES, function(inst) if inst.big then EaseInOutShadow(inst, .5) end end),
            TimeEvent(8 * FRAMES, function(inst) if inst.big then EaseInOutShadow(inst, .625) end end),
            TimeEvent(9 * FRAMES, function(inst) if inst.big then EaseInOutShadow(inst, .75) end end),
            TimeEvent(10 * FRAMES, function(inst) if inst.big then EaseInOutShadow(inst, .875) end end),
            TimeEvent(11 * FRAMES, function(inst) if inst.big then inst.DynamicShadow:SetSize(unpack(inst.sg.statemem.shadowend)) end end),
            TimeEvent(27 * FRAMES, function(inst) if inst.big then inst.SoundEmitter:PlaySound("dontstarve/creatures/together/bernie_big/death") end end),
            TimeEvent(31 * FRAMES, function(inst) if inst.big then inst.SoundEmitter:PlaySound("dontstarve/creatures/together/bernie_big/vo_death_collapse") end end),
            TimeEvent(32 * FRAMES, function(inst)
				if inst.big then
					local temp = inst.sg.statemem.shadowstart
					inst.sg.statemem.shadowstart = inst.sg.statemem.shadowend
					inst.sg.statemem.shadowend = temp
					temp[1], temp[2] = 1, .5
					EaseInShadow(inst, .2)
				end
            end),
            TimeEvent(33 * FRAMES, function(inst) if inst.big then EaseInShadow(inst, .4) end end),
            TimeEvent(34 * FRAMES, function(inst) if inst.big then EaseInShadow(inst, .6) end end),
            TimeEvent(35 * FRAMES, function(inst) if inst.big then EaseInShadow(inst, .8) end end),
            TimeEvent(36 * FRAMES, function(inst) if inst.big then inst.DynamicShadow:SetSize(unpack(inst.sg.statemem.shadowend)) end end)
		},
		
		corpse_taunt =
		{
			TimeEvent(0, function(inst) if not inst.big then inst.SoundEmitter:PlaySound("dontstarve/creatures/together/bernie_big/activate") end end),
            TimeEvent(FRAMES, function(inst) if not inst.big then EaseOutShadow(inst, .25) end end),
            TimeEvent(2 * FRAMES, function(inst) if not inst.big then EaseOutShadow(inst, .5) end end),
            TimeEvent(3 * FRAMES, function(inst) if not inst.big then EaseOutShadow(inst, .75) end end),
            TimeEvent(4 * FRAMES, function(inst) if not inst.big then inst.DynamicShadow:SetSize(unpack(inst.sg.statemem.shadowend)) end end),
            TimeEvent(15 * FRAMES, function(inst)
				if not inst.big then
                local temp = inst.sg.statemem.shadowstart
                inst.sg.statemem.shadowstart = inst.sg.statemem.shadowend
                inst.sg.statemem.shadowend = temp
                temp[1], temp[2] = (1 + 2.75) * .5, (.5 + 1.3) * .5
                EaseShadow(inst, 1 / 3)
				end
            end),
            TimeEvent(16 * FRAMES, function(inst) if not inst.big then EaseShadow(inst, 2 / 3) end end),
            TimeEvent(17 * FRAMES, function(inst) if not inst.big then inst.DynamicShadow:SetSize(unpack(inst.sg.statemem.shadowend)) end end),
            TimeEvent(35 * FRAMES, function(inst)
				if not inst.big then
					local temp = inst.sg.statemem.shadowstart
					inst.sg.statemem.shadowstart = inst.sg.statemem.shadowend
					inst.sg.statemem.shadowend = temp
					temp[1], temp[2] = 2.75, 1.3
					EaseOutShadow(inst, .2)
				end
            end),
            TimeEvent(36 * FRAMES, function(inst)
				if not inst.big then
					EaseOutShadow(inst, .4)
					inst.SoundEmitter:PlaySound("dontstarve/creatures/together/bernie_big/footstep", nil, .5)
				end
            end),
            TimeEvent(37 * FRAMES, function(inst) if not inst.big then EaseOutShadow(inst, .6) end end),
            TimeEvent(38 * FRAMES, function(inst) if not inst.big then EaseOutShadow(inst, .8) end end),
            TimeEvent(39 * FRAMES, function(inst) if not inst.big then inst.DynamicShadow:SetSize(unpack(inst.sg.statemem.shadowend)) end end),
		},
	
	},
	--anims = 
	{
		corpse = "death",
		corpse_taunt = "activate"
	},
	--sounds =
	{

	},
	--fns =
	{

	}
	) ---(states, isflying, timelines, anims, sounds, fns, events, nofx)
PP_CommonStates.AddActionStates(states, 
	{
		TimeEvent(0 * FRAMES, function(inst)
            inst:PerformBufferedAction()
        end),
	}, 
	"walk_pst", nil, nil, "taunt", "walk_pst") --(states, timelines, anim, anim2, enteranim, loopanim, exitanim, noanim, events)
PP_CommonStates.AddSailStates(states, {}, "walk_pst", "idle_loop")
local simpleanim = "walk_pst"
local simpleidle = "idle_loop"
local simplemove = "walk"
PP_CommonStates.AddKnockbackState(states, nil, "hit") --(states, timeline, anim, sounds, fns)
PP_CommonStates.AddJumpInStates(states, nil, "walk_pst")
PP_CommonStates.AddOpenGiftStates(states, "idle_loop")
if CurrentRelease.GreaterOrEqualTo(ReleaseID.IDs.R08_ROT_TURNOFTIDES) then
CommonStates.AddHopStates(states, false, {pre = simplemove.."_pre", loop = simplemove.."_loop", pst = simplemove.."_pst"}, nil, "death")
end
PP_CommonStates.AddSailorStates(states, nil, 
{
	plank_idle = simpleanim,
	plank_idle_loop = simpleidle,
	plank_idle_pst = simpleanim,
	
	plank_hop_pre = simplemove.."_pre",
	plank_hop = simplemove.."_loop",
	
	steer_pre = simpleanim,
	steer_idle = simpleidle,
	steer_turning = simpleanim,
	stop_steering = simpleanim,
	
	row = "walk_pst",
}
)


    
return StateGraph("berniep", states, events, "idle", actionhandlers)

