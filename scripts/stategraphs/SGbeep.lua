require("stategraphs/commonstates")
require("stategraphs/ppstates")

local longaction = "action"
local shortaction = "action"
local workaction = "attack"
local otheraction = "action"

local actionhandlers = PP_CommonStates.GetCommonActions(longaction, shortaction, workaction, otheraction)

local actionhandler_overrides = 
{ 

}

if #actionhandler_overrides > 0 then
	for i, v in ipairs(actionhandler_overrides) do
		table.insert(actionhandlers, v)
	end
end

local events=
{
    EventHandler("attacked", function(inst) if not inst.components.health:IsDead() and not inst.sg:HasStateTag("attack") then inst.sg:GoToState("hit") end end),
    EventHandler("doattack", function(inst, data) inst.sg:GoToState("attack", data.target) end),
    PP_CommonHandlers.OnDeath(),
	PP_CommonHandlers.OnKnockback(),
    CommonHandlers.OnSleep(),
    CommonHandlers.OnFreeze(),
	PP_CommonHandlers.AddCommonHandlers(),
    --CommonHandlers.OnLocomote(true,false),
	EventHandler("locomote", function(inst, data)
        if inst.sg:HasStateTag("busy") or inst.sg:HasStateTag("sleeping") and not inst.sg:HasStateTag("home") then
            return
        end
        local is_moving = inst.sg:HasStateTag("moving")
        local should_move = inst.components.locomotor:WantsToMoveForward()

        if is_moving and not should_move then
            inst.sg:GoToState("run_stop")
        elseif not is_moving and should_move then
            inst.sg:GoToState("run_start")
        elseif data.force_idle_state and not (is_moving or should_move or inst.sg:HasStateTag("idle")) then
            inst.sg:GoToState("idle")
        end
        if inst.sg:HasStateTag("home") or inst.sg:HasStateTag("home_waking") then -- wakeup on locomote
            if inst.sleepingbag ~= nil and inst.sg:HasStateTag("sleeping") and inst.sg:HasStateTag("home") then
                if inst.sleepingbag.components.sleepingbag then
                    inst.sleepingbag.components.sleepingbag:DoWakeUp()
                elseif inst.sleepingbag.components.multisleepingbag then
                    inst.sleepingbag.components.multisleepingbag:DoWakeUp(inst)
                end
                inst:Show()
                inst.sg:GoToState("wake")
            end 
        end
    end),
	EventHandler("ms_opengift",
        function(inst)
            if not inst.sg:HasStateTag("busy") then
                inst.sg:GoToState("opengift")
            end
        end),
}

local states=
{
    State{
        name = "death",
        tags = {"busy"},
        
        onenter = function(inst)
            --inst.SoundEmitter:KillSound("slide")
			inst.SoundEmitter:KillSound("buzz")
			inst.SoundEmitter:PlaySound(inst.sounds.death)
            inst.AnimState:PlayAnimation("death")
            inst.Physics:Stop()
            RemovePhysicsColliders(inst)            
            inst.components.lootdropper:DropLoot(Vector3(inst.Transform:GetWorldPosition()))
			inst.components.inventory:DropEverything(true)
         
			if inst.components.playercontroller ~= nil then
               inst.components.playercontroller:RemotePausePrediction()
            end
        end,
		
		timeline = 
		{
			
		},
		
        events =
        {
            EventHandler("animover", function(inst)
                if inst.AnimState:AnimDone() then
					PlayablePets.DoDeath(inst)
                end
            end),
        },

        

    },    
    
    State{
        name = "run_start",
        tags = {"moving", "canrotate"},
        
        onenter = function(inst)
			inst.components.locomotor:WalkForward()
            inst.AnimState:PlayAnimation("walk_pre")
        end,
        
        events=
        {
            EventHandler("animover", function(inst) inst.sg:GoToState("run") end),
        },
    },
    
    State{
        name = "run",
        tags = {"moving", "canrotate"},
        
        onenter = function(inst)
            inst.components.locomotor:WalkForward()
            if not inst.AnimState:IsCurrentAnimation("walk_loop") then
                inst.AnimState:PushAnimation("walk_loop", true)
            end
            inst.sg:SetTimeout(2.5+math.random())
        end,
        
        ontimeout = function(inst)
            if (inst.components.combat and not inst.components.combat.target)
               and not inst:GetBufferedAction() and
               inst:HasTag("worker") then
                inst.sg:GoToState("catchbreath")
            else
                inst.sg:GoToState("run")
            end
        end,
    },    
    
	State{
        name = "run_stop",
        tags = {"idle", "canrotate"},
        
        onenter = function(inst, start_anim)
            inst.Physics:Stop()
            local animname = "idle"

            inst.AnimState:PlayAnimation("idle", true)

        end,
		
		events=
        {
            EventHandler("animover", function(inst) inst.sg:GoToState("idle") end),
        },
    },
    
    State{
        name = "idle",
        tags = {"idle", "canrotate"},
        
        onenter = function(inst, start_anim)
            inst.Physics:Stop()
            local animname = "idle"

            inst.AnimState:PlayAnimation("idle", true)

        end,
    },
    
    State{
        name = "catchbreath",
        tags = {"busy", "landed"},
        
        onenter = function(inst)
            inst.Physics:Stop()
            inst.AnimState:PlayAnimation("land")
            inst.AnimState:PushAnimation("land_idle", true)
            inst.sg:SetTimeout(GetRandomWithVariance(4, 2) )
        end,
        
        timeline=
        {
            TimeEvent(20*FRAMES, function(inst)
                inst.SoundEmitter:KillSound("buzz")
                inst.SoundEmitter:PlaySound("dontstarve/bee/bee_tired_LP", "tired")
            end),
        },
        
        ontimeout = function(inst)
            if not (inst.components.homeseeker and inst.components.homeseeker:HasHome() )
               and inst.components.pollinator
               and inst.components.pollinator:HasCollectedEnough()
               and inst.components.pollinator:CheckFlowerDensity() then
                inst.components.pollinator:CreateFlower()
            end
            inst.sg:GoToState("takeoff")
        end,
        
        onexit = function(inst)
            inst.SoundEmitter:KillSound("tired")
        end,
    },
	
	State{
        name = "special_atk1",
        tags = {"busy", "landed"},
        
        onenter = function(inst)
            inst.Physics:Stop()
            inst.AnimState:PlayAnimation("land")
            inst.AnimState:PushAnimation("land_idle", true)
            inst.sg:SetTimeout(GetRandomWithVariance(4, 2) )
        end,
        
        timeline=
        {
            TimeEvent(20*FRAMES, function(inst)
                inst.SoundEmitter:KillSound("buzz")
                inst.SoundEmitter:PlaySound("dontstarve/bee/bee_tired_LP", "tired")
            end),
        },
        
        ontimeout = function(inst)
            inst.sg:GoToState("takeoff")
        end,
        
        onexit = function(inst)
            inst.SoundEmitter:KillSound("tired")
        end,
    },
    
    
    State{
        name = "land",
        tags = {"busy", "landing"},
        
        onenter = function(inst)
            inst.Physics:Stop()
            inst.AnimState:PlayAnimation("land")
        end,
        
        events=
        {
            EventHandler("animover", function(inst)
                inst.SoundEmitter:KillSound("buzz")
                if inst.bufferedaction and inst.bufferedaction.action == ACTIONS.POLLINATE then
					inst.sg:GoToState("pollinate")
				else
					inst.sg:GoToState("land_idle")
				end
            end),
        },
    },
    
    State{
        name = "land_idle",
        tags = {"busy", "landed"},
        
        onenter = function(inst)
            inst.AnimState:PushAnimation("land_idle", true)
        end,
    },
    
    State{
        name = "pollinate",
        tags = {"busy", "landed"},
        
        onenter = function(inst)
            inst.AnimState:PushAnimation("land_idle", true)
            inst.sg:SetTimeout(GetRandomWithVariance(3, 1) )
        end,
        
        ontimeout = function(inst)
            inst:PerformBufferedAction()
            inst.sg:GoToState("takeoff")
        end,
    },
    
    State{
        name = "takeoff",
        tags = {"busy"},
        
        onenter = function(inst)
            inst.Physics:Stop()
            inst.AnimState:PlayAnimation("take_off")
            inst.SoundEmitter:PlaySound(inst.sounds.takeoff)
        end,
        
        events =
        {
            EventHandler("animover", function(inst) inst.SoundEmitter:PlaySound(inst.sounds.buzz, "buzz") inst.sg:GoToState("idle") end),
        },
        
    },  
    
    State{
        name = "attack",
        tags = {"attack"},
        
        onenter = function(inst, cb)
            inst.Physics:Stop()
            inst.components.combat:StartAttack()
            inst.AnimState:PlayAnimation("atk")
        end,
        
        timeline=
        {
            TimeEvent(10*FRAMES, function(inst) inst.SoundEmitter:PlaySound(inst.sounds.attack) end),
            TimeEvent(15*FRAMES, function(inst) inst:PerformBufferedAction() end),
        },
        
        events=
        {
            EventHandler("animover", function(inst) inst.sg:GoToState("idle") end),
        },
    },

    State{
        name = "hit",
        tags = {"busy"},
        
        onenter = function(inst)
            inst.SoundEmitter:PlaySound(inst.sounds.hit)
            inst.AnimState:PlayAnimation("hit")
            inst.Physics:Stop()            
        end,
        
        events=
        {
            EventHandler("animover", function(inst) inst.sg:GoToState("idle") end ),
        },        
    },    
	    
	State {
        name = "sleep",
        tags = { "busy", "sleeping" }, --add tag "busy" if you hate sliding

        onenter = function(inst)
            if inst.components.locomotor ~= nil then
                inst.components.locomotor:StopMoving()
            end
            inst.AnimState:PlayAnimation("sleep_pre")
			--inst.SoundEmitter:PlaySound("dontstarve/creatures/spiderqueen/fallasleep")
            --if fns ~= nil and fns.onsleep ~= nil then
                --fns.onsleep(inst)
            --end
        end,

        --timeline = timelines ~= nil and timelines.starttimeline or nil,
		
		timeline = 
		{
			TimeEvent(23*FRAMES, function(inst) inst.SoundEmitter:KillSound("buzz") end)
		},
		
        events =
        {
            EventHandler("animqueueover", function(inst) inst.sg:GoToState("sleeping") end ),
            EventHandler("onwakeup", function(inst) inst.sg:GoToState("wake") end),
        },
    },

    State
    {
        name = "sleeping",
        tags = { "sleeping" },

        --onenter = onentersleeping,
		
		onenter = function(inst)
				inst.components.locomotor:StopMoving()
				PlayablePets.SleepHeal(inst)
				--inst.SoundEmitter:PlaySound("dontstarve_DLC001/creatures/bearger/sleep")
				inst.AnimState:PlayAnimation("sleep_loop")
			end,

        --timeline = timelines ~= nil and timelines.sleeptimeline or nil,
		timeline = 
		{
            
        },

        events =
        {
            EventHandler("animover", function(inst) inst.sg:GoToState("sleeping") end ),
			EventHandler("onwakeup", function(inst) inst.sg:GoToState("wake") end),
        },
    },

    State
    {
        name = "wake",
        tags = { "busy", "waking" },

        onenter = function(inst)
            if inst.components.locomotor ~= nil then
                inst.components.locomotor:StopMoving()
            end
			--inst.SoundEmitter:PlaySound("dontstarve/creatures/spiderqueen/wakeup")
            inst.AnimState:PlayAnimation("sleep_pst")
            if inst.components.sleeper ~= nil and inst.components.sleeper:IsAsleep() then
                inst.components.sleeper:WakeUp()
            end
            --if fns ~= nil and fns.onwake ~= nil then
                --fns.onwake(inst)
            --end
        end,

        --timeline = timelines ~= nil and timelines.waketimeline or nil,
		
		timeline = 
		{
			TimeEvent(1*FRAMES, function(inst) inst.SoundEmitter:PlaySound(inst.sounds.buzz, "buzz") end)
		},
		
		events =
        {
            EventHandler("animover", function(inst) inst.sg:GoToState("idle") end),
        },
    },
}

CommonStates.AddFrozenStates(states)
PP_CommonStates.AddHomeState(states, nil, "sleep_pre", "sleep_pst", false, {
    onenter = function(inst)
        inst.SoundEmitter:KillSound("buzz")
    end,

    onexit = function(inst)
        inst.SoundEmitter:PlaySound(inst.sounds.buzz, "buzz")
    end,
})
PP_CommonStates.AddActionStates(states, 
	{
		TimeEvent(0 * FRAMES, function(inst)
            PlayablePets.DoWork(inst, 2.5)
        end),
	}, 
	"idle", nil, nil, "idle", "idle") --(states, timelines, anim, anim2, enteranim, loopanim, exitanim, noanim, events)
PP_CommonStates.AddCorpseStates(states, nil, 
	--timelines = 
	{
		corpse =
		{
			TimeEvent(0*FRAMES, function(inst) 
				inst.SoundEmitter:KillSound("buzz")
				inst.SoundEmitter:PlaySound(inst.sounds.death)
			end ),
		},
		
		corpse_taunt =
		{
			TimeEvent(1*FRAMES, function(inst) inst.SoundEmitter:PlaySound(inst.sounds.buzz, "buzz") end)
		},
	
	},
	--anims = 
	{
		corpse = "death",
		corpse_taunt = "sleep_pst"
	},
	--sounds =
	{
		--corpse = "dontstarve_DLC003/creatures/enemy/grabbing_vine/death"
	},
	--fns =
	{

	}
) ---(states, isflying, timelines, anims, sounds, fns, events, nofx)
PP_CommonStates.AddJumpInStates(states, nil, "idle")
PP_CommonStates.AddOpenGiftStates(states, "idle")
PP_CommonStates.AddSailStates(states, {}, "idle", "idle")
local simpleanim = "idle"
local simpleidle = "idle"
local simplemove = "walk"
CommonStates.AddHopStates(states, false, {pre = simplemove, loop = simplemove.."_loop", pst = simplemove.."_loop"}, nil, "death")
PP_CommonStates.AddSailorStates(states, nil, 
{
	plank_idle = simpleanim,
	plank_idle_loop = simpleidle,
	plank_idle_pst = simpleanim,
	
	plank_hop_pre = simplemove.."_pre",
	plank_hop = simplemove.."_loop",
	
	steer_pre = simpleanim,
	steer_idle = simpleidle,
	steer_turning = simpleanim,
	stop_steering = simpleanim,
	
	row = "idle",
}
)
    
PP_CommonStates.AddAoeStates(states, nil, 
{
	generic = "idle",
	
	castspelltime = 10,
})



    
return StateGraph("beep", states, events, "idle", actionhandlers)

