require("stategraphs/commonstates")
require("stategraphs/ppstates")

local function GetExcludeTags(inst)
	if TheNet:GetPVPEnabled() then
		return {"INLIMBO", "playerghost", "notarget", "shadow", "groundpoundimmune"}
	elseif TheNet:GetServerGameMode() == "lavaarena" then
		return {"player", "companion", "playerghost", "INLIMBO", "groundpoundimmune"}
	else	
		return {"player", "companion", "INLIMBO", "playerghost", "notarget", "shadow", "groundpoundimmune"}
	end
end

local function ShakeIfClose(inst)
    ShakeAllCameras(CAMERASHAKE.VERTICAL, .15, .02, .5, inst, 20)
end

local longaction = "action"
local shortaction = "action"
local workaction = "attack"
local otheraction = "action"

local actionhandlers = PP_CommonStates.GetCommonActions(longaction, shortaction, workaction, otheraction)

local actionhandler_overrides = 
{ 

}

if #actionhandler_overrides > 0 then
	for i, v in ipairs(actionhandler_overrides) do
		table.insert(actionhandlers, v)
	end
end

local function IsAboveLand(inst)
	local pos = inst:GetPosition()
	return not PlayablePets.CheckWaterTile(pos)
end

local events=
{
    --EventHandler("attacked", function(inst) if not inst.components.health:IsDead() and not inst.sg:HasStateTag("attack") then inst.sg:GoToState("hit") end end),
    EventHandler("doattack", function(inst, data) if not inst.components.health:IsDead() and (inst.sg:HasStateTag("hit") or not inst.sg:HasStateTag("busy")) then inst.sg:GoToState("attack", data.target) end end),
    CommonHandlers.OnSleep(),
    CommonHandlers.OnFreeze(),
	PP_CommonHandlers.AddCommonHandlers(),
	PP_CommonHandlers.OpenGift(),
	--PP_CommonHandlers.OnSink(),
	PP_CommonHandlers.OnKnockback(),
	PP_CommonHandlers.OnDeath(),
	--CommonHandlers.OnHop(),
	PP_CommonHandlers.OnLocomoteAdvanced(), 
}

local states=
{
	
	State{
        
        name = "idle",
        tags = {"idle", "canrotate"},
        onenter = function(inst, playanim)
			if not IsAboveLand(inst) then
				inst:Hide()
				inst.components.health:SetInvincible(true)
			end
            inst.Physics:Stop()
            inst.AnimState:PlayAnimation("idle", true)
        end,
		
		onexit = function(inst)
			inst.components.health:SetInvincible(false)
			inst:Show()
		end,
        
        timeline = 
        {

        },
        
        events=
        {
            --EventHandler("animover", function(inst) inst.sg:GoToState("idle") end),
        },
    },
	
    State{
        name = "attack",
        tags = {"attack", "busy"},
        
        onenter = function(inst, cb)
			if not IsAboveLand(inst) then
				inst:Hide()
				inst.components.health:SetInvincible(true)
			end
			local buffaction = inst:GetBufferedAction()
			local target = buffaction ~= nil and buffaction.target or nil
            if target then
                inst.sg.mem.target = target
            end
            inst.Physics:Stop()
            inst.AnimState:PlayAnimation("stomp_pst")
        end,
        
		timeline = 
		{
			TimeEvent(25*FRAMES, function(inst) inst:LeaveStep() end ),
		},
		
		onexit = function(inst)
			inst:Show()
			inst.components.health:SetInvincible(false)
		end,
        
        events=
        {
            EventHandler("animover", function(inst) inst.sg:GoToState("run_stop") end),
        },
    },

    State{
        name = "death",
        tags = {"busy"},

        onenter = function(inst)
			inst:Show()
            inst.AnimState:PlayAnimation("stomp_pst")
			
            inst.Physics:Stop()
            RemovePhysicsColliders(inst)
            inst.components.lootdropper:DropLoot(inst:GetPosition())
			inst.components.inventory:DropEverything(true)
			
            if inst.components.playercontroller ~= nil then
                inst.components.playercontroller:RemotePausePrediction()
            end
        end,
		
		timeline = 
		{

		},
		
        events =
        {
            EventHandler("animover", function(inst)
                if inst.AnimState:AnimDone() then
                    PlayablePets.DoDeath(inst)
                end
            end),
        },
    },
	
	State
    {
        name = "run_start",
        tags = { "moving", "running", "canrotate", "busy" },

        onenter = function(inst)
			if not IsAboveLand(inst) then
				inst:Hide()
				inst.components.health:SetInvincible(true)
				inst.AnimState:PlayAnimation("idle")
			else
				 inst.AnimState:PlayAnimation("stomp_pst")
			end           			
        end,
		
		onexit = function(inst)
			inst.components.health:SetInvincible(false)
			inst:Show()
		end,
		
		timeline = 
		{
			TimeEvent(0*FRAMES, function(inst) inst.Physics:Stop() end ),
			TimeEvent(25*FRAMES, function(inst) inst:LeaveStep() end ),
		},
		
        events =
        {
            EventHandler("animqueueover", function(inst) inst.sg:GoToState("run") end ),
        },
    },
	
	State
    {
        name = "run",
        tags = { "moving", "running", "canrotate" },

        onenter = function(inst)
			inst.components.locomotor:WalkForward()
			inst.AnimState:PlayAnimation("idle")
			inst:Hide()
			inst.sg:SetTimeout(2)
        end,
		
		timeline = {
		},
		
		ontimeout = function(inst)
			inst.sg:GoToState("run_stop")
		end,
		
		onexit = function(inst)
			inst.components.health:SetInvincible(false)
			inst:Show()
		end,
		
        events=
		{
			--EventHandler("animqueueover", function(inst) inst.sg:GoToState("run") end ),
		},

    },
	
	State
    {
        name = "run_stop",
        tags = { "busy" },

        onenter = function(inst) 
			if IsAboveLand(inst) then
				if inst.sg.mem.target and inst.sg.mem.target:IsValid() then
					local pos = inst.sg.mem.target:GetPosition()
					inst.Transform:SetPosition(pos:Get())
					inst.sg.mem.target = nil
				end
				inst.components.locomotor:StopMoving()
				inst.AnimState:PlayAnimation("idle")
				inst:StartStep()
				inst.components.health:SetInvincible(true)
			else
				inst.sg:GoToState("idle")
			end
        end,

		onexit = function(inst)
			inst.components.health:SetInvincible(false)
		end,

        events =
        {
            --EventHandler("animqueueover", function(inst) inst.sg:GoToState("idle") end ),
        },
    },
	
	State{
		name = "stomp",

		tags = {"busy"},

		onenter = function(inst)
			inst:Show()
			inst.components.locomotor:StopMoving()
			inst.AnimState:PlayAnimation("stomp")
		end,
		
		events =
		{
			EventHandler("animover", function(inst) inst.sg:GoToState("idle") end)
		},

		timeline =
		{
			TimeEvent(5*FRAMES, function(inst) 
				inst:DoStep()
				inst:SpawnPrint()
			end)
		},
	},
}  

local simpleanim = "idle"
local idleanim = "idle"
PP_CommonStates.AddKnockbackState(states, nil, "idle") --(states, timeline, anim, sounds, fns)
PP_CommonStates.AddActionStates(states, 
	{
		TimeEvent(0 * FRAMES, function(inst)
            inst:PerformBufferedAction()
        end),
	}, 
	simpleanim, nil, nil, "idle", simpleanim) --(states, timelines, anim, anim2, enteranim, loopanim, exitanim, noanim, events)
PP_CommonStates.AddCorpseStates(states, true, 
	--timelines = 
	{
		corpse =
		{

		},
		
		corpse_taunt =
		{

		},
	
	},
	--anims = 
	{
		corpse = "stomp_pst",
		corpse_taunt = "stomp"
	},
	--sounds =
	{
		--corpse = "dontstarve_DLC003/creatures/enemy/grabbing_vine/death"
	},
	--fns =
	{

	}
	) ---(states, isflying, timelines, anims, sounds, fns, events, nofx)
PP_CommonStates.AddJumpInStates(states, nil, "idle")
PP_CommonStates.AddOpenGiftStates(states, "idle")
CommonStates.AddHopStates(states, false, {pre = "walk_pre", loop = "walk_loop", pst = "walk_pst"}, nil, "death")
PP_CommonStates.AddSailorStates(states, nil, 
{
	plank_idle = "idle",
	plank_idle_loop = "idle",
	plank_idle_pst = "idle",
	
	plank_hop_pre = "idle",
	plank_hop = "idle",
	
	steer_pre = simpleanim,
	steer_idle = idleanim,
	steer_turning = simpleanim,
	stop_steering = simpleanim,
	
	row = simpleanim,
}
)
PP_CommonStates.AddAoeStates(states, nil, 
{
	generic = "walk_pst",
	
	leap_pre = "walk_pre",
	leap_loop = "walk_loop",
	leap_pst = "walk_pst",
	
	lunge_pre = "walk_pre",
	lunge_loop = "walk_loop",
	lunge_pst = "walk_pst",
	
	superjump_pre = "walk_pre",
	superjump_loop = "walk_loop",
	superjump_pst = "walk_pst",
	
	castspelltime = 10,
})
    
return StateGraph("bigfootp", states, events, "idle", actionhandlers)

