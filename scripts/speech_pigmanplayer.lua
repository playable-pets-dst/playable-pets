return {

	ACTIONFAIL =
	{
        REPAIR =
        {
            WRONGPIECE = "BAD PIECE!",
        },
        BUILD =
        {
            MOUNTED = "TOO HIGH!",
            HASPET = "I HAVE PET",
        },
		SHAVE =
		{
			AWAKEBEEFALO = "WAIT TILL SLEEP!",
			GENERIC = "PIG CAN'T SHAVE",
			NOBITS = "NO LEFT TO SHAVE",
		},
		STORE =
		{
			GENERIC = "NO ROOM!",
			NOTALLOWED = "NO!",
			INUSE = "CHEST BUSY!",
		},
		RUMMAGE =
		{	
			GENERIC = "CAN'T DO IT!",
			INUSE = "CHEST BUSY!",
		},
		USEKLAUSSACKKEY =
        {
        	WRONGKEY = "BAD KEY",
        	KLAUS = "SCARY RED FIRST",
        },
        COOK =
        {
            GENERIC = "CAN'T DO IT",
            INUSE = "MUST WAIT",
            TOOFAR = "TOO FAR",
        },
        GIVE =
        {
            GENERIC = "CAN'T DO",
            DEAD = "IS TOO DEAD",
            SLEEPING = "YOU GET UP!",
            BUSY = "PIG TRY LATER",
            ABIGAILHEART = "GHOST NO LIVE",
            GHOSTHEART = "GHOST SHOULDN'T LIVE",
            NOTGEM = "PIG NOT DUMB",
            WRONGGEM = "GEM NO FIT",
            NOTSTAFF = "STAFF NO FIT",
            MUSHROOMFARM_NEEDSSHROOM = "NEED CAP FOOD",
            MUSHROOMFARM_NEEDSLOG = "NEED FACE LOG",
            SLOTFULL = "IT FULL",
            DUPLICATE = "PIG ALREADY SMART",
            NOTSCULPTABLE = "CAN'T SCULPT",
            NOTATRIUMKEY = "IT DON'T FIT",
            CANTSHADOWREVIVE = "WRONG PLACE",
            WRONGSHADOWFORM = "IS WRONG SKELETON",
        },
        GIVETOPLAYER =
        {
            FULL = "THEY TOO FULL",
            DEAD = "TOO DEAD",
            SLEEPING = "TOO SLEEPY",
            BUSY = "MAYBE LATER",
    	},
    	GIVEALLTOPLAYER = 
        {
        	FULL = "THEY TOO FULL",
            DEAD = "TOO DEAD",
            SLEEPING = "TOO SLEEPY",
            BUSY = "MAYBE LATER",
    	},
        WRITE =
        {
            GENERIC = "CAN'T DO IT",
            INUSE = "FRIEND SCRIBBLES FIRST",
        },
        DRAW =
        {
            NOIMAGE = "PIG NEEDS PICTURE",
        },
        CHANGEIN =
        {
            GENERIC = "NO!",
            BURNING = "TOO SCARY NOW!",
            INUSE = "WAIT TURN!",
        },
        ATTUNE =
        {
            NOHEALTH = "NOT FEEL WELL",
        },
        MOUNT =
        {
            TARGETINCOMBAT = "TOO ANGRY!",
            INUSE = "ME TOO LATE!",
        },
        SADDLE =
        {
            TARGETINCOMBAT = "IS TOO ANGRY",
        },
        TEACH =
        {
            --Recipes/Teacher
            KNOWN = "PIG TOO SMART",
            CANTLEARN = "PIG CAN'T LEARN",

            --MapRecorder/MapExplorer
            WRONGWORLD = "WRONG MAP",
        },
        WRAPBUNDLE =
        {
            EMPTY = "NO WRAP AIR!",
        },
		PICKUP =
        {
			RESTRICTION = "PIG HATE THAT!",
        },
	},
	ACTIONFAIL_GENERIC = "NO!",
	ANNOUNCE_DIG_DISEASE_WARNING = "SICK PLANT BETTER",
	ANNOUNCE_PICK_DISEASE_WARNING = "PLANT IS SICK",
	ANNOUNCE_ADVENTUREFAIL = "PIG DO BETTER",
    ANNOUNCE_MOUNT_LOWHEALTH = "IS HAIRY OKAY?",
	ANNOUNCE_BEES = "BUZZ BUGS!",
	ANNOUNCE_BOOMERANG = "WOOD JERK!",
	ANNOUNCE_CHARLIE = "MONSTER HERE!",
	ANNOUNCE_CHARLIE_ATTACK = "HURT! HELP PIG!",
	ANNOUNCE_COLD = "TOO COLD!",
	ANNOUNCE_HOT = "TOO HOT!",
	ANNOUNCE_CRAFTING_FAIL = "NOT ENOUGH",
	ANNOUNCE_DEERCLOPS = "PIG SCARED",
	ANNOUNCE_CAVEIN = "SKY IS FALLING!!!",
	ANNOUNCE_ANTLION_SINKHOLE = 
	{
		"GROUND FEEL WEIRD",
		"OH NO",
		"DO NOT LIKE",
	},
	ANNOUNCE_ANTLION_TRIBUTE =
	{
        "PIG GIVE GIFT",
        "KING IS BETTER",
        "HE HAPPY NOW",
	},
	ANNOUNCE_SACREDCHEST_YES = "PIG IS BEST",
	ANNOUNCE_SACREDCHEST_NO = "STUPID CHEST",
	ANNOUNCE_DUSK = "WHERE IS SUN?",
	ANNOUNCE_EAT =
	{
		GENERIC = "I EAT FOOD!",
		PAINFUL = "FOOD HURTS",
		SPOILED = "FOOD NOT FOOD",
		STALE = "FOOD GET OLD",
		INVALID = "PIG HAVE STANDARDS",
		YUCKY = "ME REFUSE!",
	},
    ANNOUNCE_ENCUMBERED =
    {
        "PIG... IS... STRONG",
        "NO... GIVE UP...",
        "PIG... CAN'T MOVE",
        "ROCK... HEAVY...",
        "PIG... BEAT... ROCK!",
        "PIG... HATE ROCK...",
        "FOR... KING!",
        "PIG... SMASH ROCK...",
        "ROCK... STUPID",
    },
    ANNOUNCE_ATRIUM_DESTABILIZING = 
    {
		"PIG LEAVE NOW",
		"UUUHHH",
		"PIG WANT OUT!",
	},
    ANNOUNCE_RUINS_RESET = "MONSTERS ARE BACK",
    ANNOUNCE_SNARED = "OINK!",
    ANNOUNCE_REPELLED = "PIG CAN'T SMASH?",
	ANNOUNCE_ENTER_DARK = "TOO DARK!",
	ANNOUNCE_ENTER_LIGHT = "LIGHT GOOD",
	ANNOUNCE_FREEDOM = "PIG IS FREE!",
	ANNOUNCE_HIGHRESEARCH = "PIG IS SMART",
	ANNOUNCE_HOUNDS = "DANGER NOISES",
	ANNOUNCE_WORMS = "ME FEEL SOMETHING",
	ANNOUNCE_HUNGRY = "ME HUNGRY",
	ANNOUNCE_HUNT_BEAST_NEARBY = "PIG FIND MEAT!",
	ANNOUNCE_HUNT_LOST_TRAIL = "PIG LOSE TRAIL",
	ANNOUNCE_HUNT_LOST_TRAIL_SPRING = "TRAIL IS WET",
	ANNOUNCE_INV_FULL = "PIG TOO FULL",
	ANNOUNCE_KNOCKEDOUT = "PIG'S HEAD HURT",
	ANNOUNCE_LOWRESEARCH = "PIG IS LEARNING",
	ANNOUNCE_MOSQUITOS = "BUG ANNOYING!",
    ANNOUNCE_NOWARDROBEONFIRE = "TOO HOT!",
    ANNOUNCE_NODANGERGIFT = "FIGHT DANGER FIRST!",
    ANNOUNCE_NOMOUNTEDGIFT = "PIG TOO HIGH",
	ANNOUNCE_NODANGERSLEEP = "TOO SCARY",
	ANNOUNCE_NODAYSLEEP = "NOT SLEEPTIME",
	ANNOUNCE_NODAYSLEEP_CAVE = "PIG NOT TIRED",
	ANNOUNCE_NOHUNGERSLEEP = "PIG WANT SNACK",
	ANNOUNCE_NOSLEEPONFIRE = "IS ON FIRE!",
	ANNOUNCE_NODANGERSIESTA = "DANGER CLOSE",
	ANNOUNCE_NONIGHTSIESTA = "TOO DARK!",
	ANNOUNCE_NONIGHTSIESTA_CAVE = "CAVE TOO SCARY!",
	ANNOUNCE_NOHUNGERSIESTA = "PIG IS HUNGRY!",
	ANNOUNCE_NODANGERAFK = "PIG KILL DANGER",
	ANNOUNCE_NO_TRAP = "TOO EASY",
	ANNOUNCE_PECKED = "BAD BIRD!",
	ANNOUNCE_QUAKE = "GROUND TOO ANGRY!",
	ANNOUNCE_RESEARCH = "PIG WANT SMARTS",
	ANNOUNCE_SHELTER = "SHELTER HELP PIG!",
	ANNOUNCE_THORNS = "PLANT IS MAD!",
	ANNOUNCE_BURNT = "PIG IS BURNT",
	ANNOUNCE_TORCH_OUT = "LIGHT STICK GONE!",
	ANNOUNCE_THURIBLE_OUT = "NEEDS MORE SCARE",
	ANNOUNCE_FAN_OUT = "FAN GONE",
    ANNOUNCE_COMPASS_OUT = "ARROW CIRCLE GONE",
	ANNOUNCE_TRAP_WENT_OFF = "...PIG DO THAT?",
	ANNOUNCE_UNIMPLEMENTED = "IS NOT READY",
	ANNOUNCE_WORMHOLE = "HOLE SCARE PIG",
	ANNOUNCE_TOWNPORTALTELEPORT = "AAAAAAH!",
	ANNOUNCE_CANFIX = "\nME MAY HELP",
	ANNOUNCE_ACCOMPLISHMENT = "ME DID IT!",
	ANNOUNCE_ACCOMPLISHMENT_DONE = "PIG SHOW VILLAGE!",	
	ANNOUNCE_INSUFFICIENTFERTILIZER = "PLANT IS HUNGRY?",
	ANNOUNCE_TOOL_SLIP = "COME BACK TOOL!",
	ANNOUNCE_LIGHTNING_DAMAGE_AVOIDED = "PIG IS LUCKY!",
	ANNOUNCE_TOADESCAPING = "FROG GETTING BORED",
	ANNOUNCE_TOADESCAPED = "FROG IS GONE",

	ANNOUNCE_DAMP = "PIG WET",
	ANNOUNCE_WET = "PIG TOO WET",
	ANNOUNCE_WETTER = "ME HATE WET",
	ANNOUNCE_SOAKED = "ME TOO WET",
	
	ANNOUNCE_BECOMEGHOST = "ooOOooOOOooOOooink...",
	ANNOUNCE_GHOSTDRAIN = "PIG LOSE MIND",
	ANNOUNCE_PETRIFED_TREES = "TREES IS ALIVE!!!",
	ANNOUNCE_KLAUS_ENRAGE = "RED IS ANGRY",
	ANNOUNCE_KLAUS_UNCHAINED = "RED IS FREE!",
	ANNOUNCE_KLAUS_CALLFORHELP = "RED CALL FRIENDS",
	
	--boarlord event
	ANNOUNCE_REVIVING_CORPSE = "PIG HELP FRIEND",
	ANNOUNCE_REVIVED_OTHER_CORPSE = "PIG HELPING",
	ANNOUNCE_REVIVED_FROM_CORPSE = "PIG MUCH BETTER",

    ANNOUNCE_ROYALTY = 
    {
    	"YOU NOT KING",
    	"NOT MY KING",
    	"PIG NOT BOW",
    },

	BATTLECRY =
	{
		GENERIC = "YOU GO SMASH!",
		PIG = "SORRY!",
		PREY = "ME HUNGRY!",
		SPIDER = "PIG SMASH SPIDER!",
		SPIDER_WARRIOR = "DIE EVIL SPIDER!",
		DEER = "YOU IS WEAK",
	},
	COMBAT_QUIT =
	{
		GENERIC = "PIG COME BACK",
		PIG = "FORGIVE PIG!",
		PREY = "PIG WIN!",
		SPIDER = "NOT WORTH IT",
		SPIDER_WARRIOR = "PIG KILL LATER",
	},
	DESCRIBE =
	{
		MULTIPLAYER_PORTAL = "WHERE PIG NOW?",
		ANTLION = 
		{
			GENERIC = "IT WANT STUFF",
			VERYHAPPY = "IT LIKE PIG",
			UNHAPPY = "IT NO HAPPY",
		},
		ANTLIONTRINKET = "KING DON'T LIKE",
		SANDSPIKE = "CAN HURT PIG",
        SANDBLOCK = "DUMB CASTLE",
        GLASSSPIKE = "PIG KILLED SPIKE!",
        GLASSBLOCK = "PIG MADE THIS",
		ABIGAIL_FLOWER = 
		{ 
			GENERIC ="IS CLOSED FLOWER",
			LONG = "FLOWER IS BLOOMING",
			MEDIUM = "SOMETHING HAPPENING?",
			SOON = "IS ALMOST OPEN",
			HAUNTED_POCKET = "PIG NO WANT!",
			HAUNTED_GROUND = "IS FLOATING! AAAH!",
		},

		BALLOONS_EMPTY = "PIG BLOW BALLOON",
		BALLOON = "HAH! PIG LIKE",

		BERNIE_INACTIVE =
		{
			BROKEN = "POOR TOY",
			GENERIC = "TOY IS BURNT",
		},

		BERNIE_ACTIVE = "TOY IS MOVING!",
		
		BOOK_BIRDS = "BOOK OF BIRDS",
		BOOK_TENTACLES = "BOOK OF MONSTERS",
		BOOK_GARDENING = "BOOK OF PLANT",
		BOOK_SLEEP = "SLEEPING BOOK",
		BOOK_BRIMSTONE = "IS BAD BOOK",

        PLAYER =
        {
            GENERIC = "%s IS FRIEND!",
            ATTACKER = "NO TRUST %s...",
            MURDERER = "%s NOT FRIEND!",
            REVIVER = "%s IS GOOD!",
            GHOST = "%s IS SPOOKY",
			FIRESTARTER = "%s START FIRE",
        },
		WILSON = 
		{
			GENERIC = "FUNNY HAIR MAN!",
			ATTACKER = "%s SEEM FUNNY...",
			MURDERER = "MUST KILL %s!",
			REVIVER = "%s IS FRIEND!",
			GHOST = "PIG SAVE %s!",
			FIRESTARTER = "IS FIRE MAN?",
		},
		WOLFGANG = 
		{
			GENERIC = "PIG IS STRONGER!",
            ATTACKER = "NO TRUST %s",
            MURDERER = "PIG KILL %s!",
            REVIVER = "PIG LIKE %!",
            GHOST = "%s NEEDS HEART",
			FIRESTARTER = "FLAMING STRONG MAN",
		},
		WAXWELL = 
		{
			GENERIC = "IS GOOD NOW?",
            ATTACKER = "PIG TRUSTED %s...",
            MURDERER = "PIG KILL NOW!",
            REVIVER = "SEE? %s GOOD!",
            GHOST = "%s IS DEAD",
			FIRESTARTER = "DAPPER FIRE MAN?",
		},
		WX78 = 
		{
			GENERIC = "WHY SKIN HARD?",
            ATTACKER = "%s DISLIKES PIG",
            MURDERER = "YOU NO HUMAN!",
            REVIVER = "%s SOFT INSIDE!",
            GHOST = "SKIN STILL HARD?",
			FIRESTARTER = "%s SKIN BURNING!",
		},
		WILLOW = 
		{
			GENERIC = "DON'T BURN PIG",
			ATTACKER = "NO COOK PIG",
			MURDERER = "PIG COOK %s!",
			REVIVER = "%s's HEART WARM!",
			GHOST = "%s COLD NOW...",
			FIRESTARTER = "%s START FIRES!",
		},
		WENDY = 
		{
			GENERIC = "SCARY GIRL",
			ATTACKER = "SCARIER GIRL",
			MURDERER = "KILLER GIRL! AAAAAAH!",
			REVIVER = "GIRL HELP US",
			GHOST = "PIG HELP GIRL",
			FIRESTARTER = "%s BURN STUFF",
		},
		WOODIE = 
		{
			GENERIC = "WOOD MAN!",--nice.
			ATTACKER = "%s...",
			MURDERER = "%s NOT FRIEND!",
			REVIVER = "%s GOOD FRIEND!",
			GHOST = "PIG HELP %s!",
			BEAVER = "LOOKS LIKE ME",
			BEAVERGHOST = "UH...",
			FIRESTARTER = "WOOD BURNING MAN",
		},
		WICKERBOTTOM = 
		{
			GENERIC = "BOOK LADY %s",
            ATTACKER = "NO TRUST %s",
            MURDERER = "%s NOT FRIEND!",
            REVIVER = "%s IS GOOD!",
            GHOST = "%s IS SPOOKY.",
			FIRESTARTER = "BOOK BURNING BAD!",
		},
		WES = 
		{
			GENERIC = "IS BROKEN?",
			ATTACKER = "PIG SHOULD HELP?",
			MURDERER = "IS BREAKING OTHERS!",
			REVIVER = "NOT BROKEN",
			GHOST = "PIG GO SAVE",
			FIRESTARTER = "BURNING IS BAD",
		},
		WEBBER = 
		{
			GENERIC = "STAY AWAY!",
			ATTACKER = "PIG WARNING YOU!",
			MURDERER = "PIG CRUSH SPIDER!",
			REVIVER = "WHY IS GOOD?",
			GHOST = "WHY HELP?",
			FIRESTARTER = "PIG SQUASH BURNS!",
		},
		WATHGRITHR = 
		{
			GENERIC = "IS STRONG %s!",
			ATTACKER = "NO TRUST %s...",
			MURDERER = "PIG STOP %s!",
			REVIVER = "PIG LIKE %s",
			GHOST = "WHAT HAPPEN, %s?",
			FIRESTARTER = "NO FIRES!",
		},
		WINONA =
        {
            GENERIC = "FIX-Y WOMAN %s!",
            ATTACKER = "%s SEEMS OFF",
            MURDERER = "PIG STOP %s!",
            REVIVER = "%s IS GOOD",
            GHOST = "PIG FIX %s!",
            FIRESTARTER = "STOP FIRE!",
        },
        MIGRATION_PORTAL = 
        {
            GENERIC = "FRIENDS?",
            OPEN = "SHOULD PIG GO?",
            FULL = "IS TOO FULL",
        },
		GLOMMER = "FLYING SPIDER! AAAAAAH!",
		GLOMMERFLOWER = 
		{
			GENERIC = "SMELLY FLOWER",
			DEAD = "DEAD. STILL SMELLY",
		},
		GLOMMERWINGS = "PIG WILL FLY?",
		GLOMMERFUEL = "DO NOT WANT",
		BELL = "FOOT BELL",
		STATUEGLOMMER = 
		{	
			GENERIC = "GROSS SPIDER STATUE",
			EMPTY = "IS BETTER NOW",
		},

        LAVA_POND_ROCK = "BURNT ROCK",

		WEBBERSKULL = "IS... SPIDER?",
		WORMLIGHT = "PIG EAT SHINY",
		WORMLIGHT_LESSER = "IS WRINKLY FOOD",
		WORM =
		{
		    PLANT = "GLOWY FOOD",
		    DIRT = "PIG CRUSH GROUND!",
		    WORM = "PIG SAVE FOOD!",
		},
        WORMLIGHT_PLANT = "GLOWY FOOD",
		MOLE =
		{
			HELD = "HELLO DIGGY FRIEND",
			UNDERGROUND = "HE FIND ROCKS",
			ABOVEGROUND = "THERE HE IS",
		},
		MOLEHILL = "IS MOLE HOLE",
		MOLEHAT = "PIG SEE DARK!",

		EEL = "WEIRD WORM FISH",
		EEL_COOKED = "STILL WEIRD WORM FISH",
		UNAGI = "LOOK FANCY",
		EYETURRET = "EYE HELP PIG?",
		EYETURRET_ITEM = "EYE NEED PLACE",
		MINOTAURHORN = "BEAST HORN",
		MINOTAURCHEST = "BIG CHEST!",
		THULECITE_PIECES = "SHINY PIECES",
		POND_ALGAE = "",
		GREENSTAFF = "PIG GET MORE FROM BREAKING",
		GIFT = "FOR PIG?",
        GIFTWRAP = "PIG MAKE PRESENT",
		POTTEDFERN = "PIG LIKE",
        SUCCULENT_POTTED = "PIG LIKE",
		SUCCULENT_PLANT = "LOOK YUMMY?",
		SUCCULENT_PICKED = "DESERT FOOD",
		SENTRYWARD = "",
        TOWNPORTAL =
        {
			GENERIC = "WHAT THIS?",
			ACTIVE = "PIG CURIOUS",
		},
        TOWNPORTALTALISMAN = 
        {
			GENERIC = "",
			ACTIVE = "",
		},
        WETPAPER = "",
        WETPOUCH = "",
        MOONROCK_PIECES = "",
        MOONBASE =
        {
            GENERIC = "",
            BROKEN = "",
            STAFFED = "",
            WRONGSTAFF = "",
            MOONSTAFF = "",
        },
        MOONDIAL = 
        {
			GENERIC = "",
			NIGHT_NEW = "",
			NIGHT_WAX = "",
			NIGHT_FULL = "UH OH",
			NIGHT_WANE = "",
			CAVE = "WHY NO MOON?",
        },
 		--MOWER = "",
		--MACHETE = "",
		--GOLDENMACHETE = "",
		--OBSIDIANMACHETE = "",
		--BOOK_METEOR = "",
		THULECITE = "SHINY ROCK",
		ARMORRUINS = "SHINY ARMOR FOR PIG",
		ARMORSKELETON = "BONE MAN PROTECT PIG",
		SKELETONHAT = "PIG NO LIKE THIS",
		RUINS_BAT = "PIG CRUSH BEASTS",
		RUINSHAT = "PIG IS KING NOW",
		NIGHTMARE_TIMEPIECE =
		{
            CALM = "IT SAFE",
            WARN = "IT NOT AS SAFE",
            WAXING = "OH NO",
            STEADY = "IT NOT SAFE",
            WANING = "IT SAFE SOON",
            DAWN = "IT SAFE",
            NOMAGIC = "NO EVIL",
		},
		BISHOP_NIGHTMARE = "PIG FEEL BAD, ALMOST",
		ROOK_NIGHTMARE = "PIG WISH IT NO HERE",
		KNIGHT_NIGHTMARE = "YOU HERE TOO?",
		MINOTAUR = "BIG BEAST!",
		SPIDER_DROPPER = "CREEPY SPIDER!",
		--[[
		NIGHTMARELIGHT = "CREEPY LIGHT",
		NIGHTSTICK = "SPOOKY STICK",
		GREENGEM = "PIG LIKE GREEN",
		MULTITOOL_AXE_PICKAXE = "PIG DO MANY JOBS WITH TOOL",
		ORANGESTAFF = "PIG NO NEED WALK NOW",
		YELLOWAMULET = "",
		GREENAMULET = "",
		SLURPERPELT = "",	

		SLURPER = "",
		SLURPER_PELT = "",
		ARMORSLURPER = "",
		ORANGEAMULET = "",
		YELLOWSTAFF = "",
		YELLOWGEM = "",
		ORANGEGEM = "",
        OPALSTAFF = "",
        OPALPRECIOUSGEM = "",
        TELEBASE = 
		{
			VALID = "",
			GEMS = "",
		},
		GEMSOCKET = 
		{
			VALID = "",
			GEMS = "",
		},
		STAFFLIGHT = "",
        STAFFCOLDLIGHT = "",

        ANCIENT_ALTAR = "",

        ANCIENT_ALTAR_BROKEN = "",

        ANCIENT_STATUE = "",

        LICHEN = "",
		CUTLICHEN = "",

		CAVE_BANANA = "",
		CAVE_BANANA_COOKED = "",
		CAVE_BANANA_TREE = "",
		ROCKY = "",
		
		COMPASS =
		{
			GENERIC="GOLD-Y POINT-Y THING",
			N = "UP",
			S = "DOWN",
			E = "RIGHT",
			W = "LEFT",
			NE = "UP RIGHT",
			SE = "DOWN RIGHT",
			NW = "UP LEFT",
			SW = "DOWN LEFT",
		},

        HOUNDSTOOTH = "",
        ARMORSNURTLESHELL = "",
        BAT = "",
        BATBAT = "",
        BATWING = "",
        BATWING_COOKED = "",
        BATCAVE = "",
        BEDROLL_FURRY = "",
        BUNNYMAN = "",
        FLOWER_CAVE = "",
        GUANO = "",
        LANTERN = "",
        LIGHTBULB = "",
        MANRABBIT_TAIL = "",
        MUSHROOMHAT = "",
        MUSHROOM_LIGHT2 =
        {
            ON = "",
            OFF = "",
            BURNT = "",
        },
        MUSHROOM_LIGHT =
        {
            ON = "",
            OFF = "",
            BURNT = "",
        },
        SLEEPBOMB = "",
        MUSHROOMBOMB = "",
        SHROOM_SKIN = "",
        TOADSTOOL_CAP =
        {
            EMPTY = "",
            INGROUND = "",
            GENERIC = "",
        },
        TOADSTOOL =
        {
            GENERIC = "",
            RAGE = "",
        },
        MUSHROOMSPROUT =
        {
            GENERIC = "",
            BURNT = "",
        },
        MUSHTREE_TALL =
        {
            GENERIC = "",
            BLOOM = "",
        },
        MUSHTREE_MEDIUM =
        {
            GENERIC = "",
            BLOOM = "",
        },
        MUSHTREE_SMALL =
        {
            GENERIC = "",
            BLOOM = "",
        },
        MUSHTREE_TALL_WEBBED = "",
        SPORE_TALL =
        {
            GENERIC = "",
            HELD = "",
        },
        SPORE_MEDIUM =
        {
            GENERIC = "",
            HELD = "",
        },
        SPORE_SMALL =
        {
            GENERIC = "",
            HELD = "",
        },
        RABBITHOUSE =
        {
            GENERIC = "",
            BURNT = "",
        },
        SLURTLE = "",
        SLURTLE_SHELLPIECES = "",
        SLURTLEHAT = "",
        SLURTLEHOLE = "",
        SLURTLESLIME = "",
        SNURTLE = "",
        SPIDER_HIDER = "",
        SPIDER_SPITTER = "",
        SPIDERHOLE = "",
        SPIDERHOLE_ROCK = "",
        STALAGMITE = "",
        STALAGMITE_TALL = "",
        TREASURECHEST_TRAP = "",

        TURF_CARPETFLOOR = "",
        TURF_CHECKERFLOOR = "",
        TURF_DIRT = "",
        TURF_FOREST = "",
        TURF_GRASS = "",
        TURF_MARSH = "",
        TURF_ROAD = "",
        TURF_ROCKY = "",
        TURF_SAVANNA = "",
        TURF_WOODFLOOR = "",

		TURF_CAVE = "",
		TURF_FUNGUS = "",
		TURF_SINKHOLE = "",
		TURF_UNDERROCK = "",
		TURF_MUD = "",

		TURF_DECIDUOUS = "",
		TURF_SANDY = "",
		TURF_BADLANDS = "",
		TURF_DESERTDIRT = "",
		TURF_FUNGUS_GREEN = "",
		TURF_FUNGUS_RED = "",
		TURF_DRAGONFLY = "",

		POWCAKE = "",
        CAVE_ENTRANCE = "",
        CAVE_ENTRANCE_RUINS = "",
       
       	CAVE_ENTRANCE_OPEN = 
        {
            GENERIC = "",
            OPEN = "",
            FULL = "",
        },
        CAVE_EXIT = 
        {
            GENERIC = "",
            OPEN = "",
            FULL = "",
        },

		MAXWELLPHONOGRAPH = "",
		BOOMERANG = "",
		PIGGUARD = "",
		ABIGAIL = "GHOST IS GOOD?",
		ADVENTURE_PORTAL = "",
		AMULET = "",
		ANIMAL_TRACK = "",
		ARMORGRASS = "",
		ARMORMARBLE = "",
		ARMORWOOD = "",
		ARMOR_SANITY = "",
		ASH =
		{
			GENERIC = "",
			REMAINS_GLOMMERFLOWER = "",
			REMAINS_EYE_BONE = "",
			REMAINS_THINGIE = "",
		},
		AXE = "",
		BABYBEEFALO = 
		{
			GENERIC = "",
		    SLEEPING = "",
        },
        BUNDLE = "",
        BUNDLEWRAP = "",
		BACKPACK = "",
		BACONEGGS = "",
		BANDAGE = "",
		BASALT = "",
		BEARDHAIR = "",
		BEARGER = "",
		BEARGERVEST = "",
		ICEPACK = "",
		BEARGER_FUR = "",
		BEDROLL_STRAW = "",
		BEEQUEEN = "",
		BEEQUEENHIVE = 
		{
			GENERIC = "",
			GROWING = "",
		},
        BEEQUEENHIVEGROWN = "",
        BEEGUARD = "",
        HIVEHAT = "",
        MINISIGN =
        {
            GENERIC = "",
            UNDRAWN = "",
        },
        MINISIGN_ITEM = "",
		BEE =
		{
			GENERIC = "",
			HELD = "",
		},
		BEEBOX =
		{
			READY = "",
			FULLHONEY = "",
			GENERIC = "",
			NOHONEY = "",
			SOMEHONEY = "",
			BURNT = "",
		},
		MUSHROOM_FARM =
		{
			STUFFED = "",
			LOTS = "",
			SOME = "",
			EMPTY = "",
			ROTTEN = "",
			BURNT = "",
			SNOWCOVERED = "",
		},
		BEEFALO =
		{
			FOLLOWER = "",
			GENERIC = "",
			NAKED = "",
			SLEEPING = "",
            --Domesticated states:
            DOMESTICATED = "",
            ORNERY = "",
            RIDER = "",
            PUDGY = "",
		},

		BEEFALOHAT = "",
		BEEFALOWOOL = "",
		BEEHAT = "",
        BEESWAX = "",
		BEEHIVE = "",
		BEEMINE = "",
		BEEMINE_MAXWELL = "",
		BERRIES = "",
		BERRIES_COOKED = "",
        BERRIES_JUICY = "",
        BERRIES_JUICY_COOKED = "",
		BERRYBUSH =
		{
			BARREN = "",
			WITHERED = "",
			GENERIC = "",
			PICKED = "",
			DISEASED = "",
			DISEASING = "",
			BURNING = "",
		},
		BERRYBUSH_JUICY =
		{
			BARREN = "",
			WITHERED = "",
			GENERIC = "",
			PICKED = "",
			DISEASED = "",
			DISEASING = "",
			BURNING = "",
		},
		BIGFOOT = "",
		BIRDCAGE =
		{
			GENERIC = "",
			OCCUPIED = "",
			SLEEPING = "",
			HUNGRY = "",
			STARVING = "",
			DEAD = "",
			SKELETON = "",
		},
		BIRDTRAP = "",
		CAVE_BANANA_BURNT = "",
		BIRD_EGG = "",
		BIRD_EGG_COOKED = "",
		BISHOP = "",
		BLOWDART_FIRE = "",
		BLOWDART_SLEEP = "",
		BLOWDART_PIPE = "",
		BLOWDART_YELLOW = "",
		BLUEAMULET = "",
		BLUEGEM = "",
		BLUEPRINT = 
		{ 
            COMMON = "",
            RARE = "",
        },
        SKETCH = "",
		--BELL_BLUEPRINT = "",
		BLUE_CAP = "",
		BLUE_CAP_COOKED = "",
		BLUE_MUSHROOM =
		{
			GENERIC = "",
			INGROUND = "",
			PICKED = "",
		},
		BOARDS = "",
		BONESHARD = "",
		BONESTEW = "",
		BUGNET = "",
		BUSHHAT = "",
		BUTTER = "",
		BUTTERFLY =
		{
			GENERIC = "",
			HELD = "",
		},
		BUTTERFLYMUFFIN = "",
		BUTTERFLYWINGS = "",
		BUZZARD = "",

		SHADOWDIGGER = "",

		CACTUS = 
		{
			GENERIC = "",
			PICKED = "",
		},
		CACTUS_MEAT_COOKED = "",
		CACTUS_MEAT = "",
		CACTUS_FLOWER = "",

		COLDFIRE =
		{
			EMBERS = "",
			GENERIC = "",
			HIGH = "",
			LOW = "",
			NORMAL = "",
			OUT = "",
		},
		CAMPFIRE =
		{
			EMBERS = "",
			GENERIC = "",
			HIGH = "",
			LOW = "",
			NORMAL = "",
			OUT = "",
		},
		CANE = "",
		CATCOON = "",
		CATCOONDEN = 
		{
			GENERIC = "",
			EMPTY = "",
		},
		CATCOONHAT = "",
		COONTAIL = "",
		CARROT = "",
		CARROT_COOKED = "",
		CARROT_PLANTED = "",
		CARROT_SEEDS = "",
		CARTOGRAPHYDESK =
		{
			GENERIC = "",
			BURNING = "",
			BURNT = "",
		},
		WATERMELON_SEEDS = "",
		CAVE_FERN = "",
		CHARCOAL = "",
        CHESSPIECE_PAWN = "",
        CHESSPIECE_ROOK =
        {
            GENERIC = "",
            STRUGGLE = "",
        },
        CHESSPIECE_KNIGHT =
        {
            GENERIC = "",
            STRUGGLE = "",
        },
        CHESSPIECE_BISHOP =
        {
            GENERIC = "",
            STRUGGLE = "",
        },
        CHESSPIECE_MUSE = "",
        CHESSPIECE_FORMAL = "",
        CHESSPIECE_HORNUCOPIA = "",
        CHESSPIECE_PIPE = "",
        CHESSPIECE_DEERCLOPS = "",
        CHESSPIECE_BEARGER = "",
        CHESSPIECE_MOOSEGOOSE = "",
        CHESSPIECE_DRAGONFLY = "",
        CHESSJUNK1 = "",
        CHESSJUNK2 = "",
        CHESSJUNK3 = "",
		CHESTER = "",
		CHESTER_EYEBONE =
		{
			GENERIC = "",
			WAITING = "",
		},
		COOKEDMANDRAKE = "",
		COOKEDMEAT = "",
		COOKEDMONSTERMEAT = "",
		COOKEDSMALLMEAT = "",
		COOKPOT =
		{
			COOKING_LONG = "",
			COOKING_SHORT = "",
			DONE = "",
			EMPTY = "",
			BURNT = "",
		},
		CORN = "",
		CORN_COOKED = "",
		CORN_SEEDS = "",
        CANARY =
		{
			GENERIC = "",
			HELD = "",
		},
        CANARY_POISONED = "",

		CRITTERLAB = "",
        CRITTER_GLOMLING = "",
        CRITTER_DRAGONLING = "",
		CRITTER_LAMB = "",
        CRITTER_PUPPY = "",
        CRITTER_KITTEN = "",
        CRITTER_PERDLING = "",

		CROW =
		{
			GENERIC = "",
			HELD = "",
		},
		CUTGRASS = "",
		CUTREEDS = "",
		CUTSTONE = "",
		DEADLYFEAST = "",
		DEER =
		{
			GENERIC = "",
			ANTLER = "",
		},
        DEER_ANTLER = "",
        DEER_GEMMED = "",
		DEERCLOPS = "",
		DEERCLOPS_EYEBALL = "",
		EYEBRELLAHAT =	"",
		DEPLETED_GRASS =
		{
			GENERIC = "",
		},
        GOGGLESHAT = "",
        DESERTHAT = "",
		DEVTOOL = "",
		DEVTOOL_NODEV = "",
		DIRTPILE = "",
		DIVININGROD =
		{
			COLD = "",
			GENERIC = "",
			HOT = "",
			WARM = "",
			WARMER = "",
		},
		DIVININGRODBASE =
		{
			GENERIC = "",
			READY = "",
			UNLOCKED = "",
		},
		DIVININGRODSTART = "",
		DRAGONFLY = "",
		ARMORDRAGONFLY = "",
		DRAGON_SCALES = "",
		DRAGONFLYCHEST = "",
		DRAGONFLYFURNACE = 
		{
			HAMMERED = "",
			GENERIC = "", --no gems
			NORMAL = "", --one gem
			HIGH = "", --two gems
		},
        
        HUTCH = "",
        HUTCH_FISHBOWL =
        {
            GENERIC = "",
            WAITING = "",
        },
		LAVASPIT = 
		{
			HOT = "",
			COOL = "",
		},
		LAVA_POND = "",
		LAVAE = "",
		LAVAE_COCOON = "",
		LAVAE_PET = 
		{
			STARVING = "",
			HUNGRY = "",
			CONTENT = "",
			GENERIC = "",
		},
		LAVAE_EGG = 
		{
			GENERIC = "",
		},
		LAVAE_EGG_CRACKED =
		{
			COLD = "",
			COMFY = "",
		},
		LAVAE_TOOTH = "",

		DRAGONFRUIT = "",
		DRAGONFRUIT_COOKED = "",
		DRAGONFRUIT_SEEDS = "",
		DRAGONPIE = "",
		DRUMSTICK = "",
		DRUMSTICK_COOKED = "",
		DUG_BERRYBUSH = "",
		DUG_BERRYBUSH_JUICY = "",
		DUG_GRASS = "",
		DUG_MARSH_BUSH = "",
		DUG_SAPLING = "",
		DURIAN = "",
		DURIAN_COOKED = "",
		DURIAN_SEEDS = "",
		EARMUFFSHAT = "",
		EGGPLANT = "",
		EGGPLANT_COOKED = "",
		EGGPLANT_SEEDS = "",
		
		ENDTABLE = 
		{
			BURNT = "",
			GENERIC = "",
			EMPTY = "",
			WILTED = "",
			FRESHLIGHT = "",
			OLDLIGHT = "", -- will be wilted soon, light radius will be very small at this point
		},
		DECIDUOUSTREE = 
		{
			BURNING = "",
			BURNT = "",
			CHOPPED = "",
			POISON = "",
			GENERIC = "",
		},
		ACORN = "",
        ACORN_SAPLING = "",
		ACORN_COOKED = "",
		BIRCHNUTDRAKE = "",
		EVERGREEN =
		{
			BURNING = "",
			BURNT = "",
			CHOPPED = "",
			GENERIC = "",
		},
		EVERGREEN_SPARSE =
		{
			BURNING = "",
			BURNT = "",
			CHOPPED = "",
			GENERIC = "",
		},
		TWIGGYTREE = 
		{
			BURNING = "",
			BURNT = "",
			CHOPPED = "",
			GENERIC = "",			
			DISEASED = "",
		},
		TWIGGY_NUT_SAPLING = "",
        TWIGGY_OLD = "",
		TWIGGY_NUT = "",
		EYEPLANT = "",
		INSPECTSELF = "",
		FARMPLOT =
		{
			GENERIC = "",
			GROWING = "",
			NEEDSFERTILIZER = "",
			BURNT = "",
		},
		FEATHERHAT = "",
		FEATHER_CROW = "",
		FEATHER_ROBIN = "",
		FEATHER_ROBIN_WINTER = "",
		FEATHER_CANARY = "",
		FEATHERPENCIL = "",
		FEM_PUPPET = "",
		FIREFLIES =
		{
			GENERIC = "",
			HELD = "",
		},
		FIREHOUND = "",
		FIREPIT =
		{
			EMBERS = "",
			GENERIC = "",
			HIGH = "",
			LOW = "",
			NORMAL = "",
			OUT = "",
		},
		COLDFIREPIT =
		{
			EMBERS = "",
			GENERIC = "",
			HIGH = "",
			LOW = "",
			NORMAL = "",
			OUT = "",
		},
		FIRESTAFF = "",
		FIRESUPPRESSOR = 
		{	
			ON = "",
			OFF = "",
			LOWFUEL = "",
		},

		FISH = "",
		FISHINGROD = "",
		FISHSTICKS = "",
		FISHTACOS = "",
		FISH_COOKED = "",
		FLINT = "",
		FLOWER = 
		{
            GENERIC = "",
            ROSE = "",
        },
        FLOWER_WITHERED = "",
		FLOWERHAT = "",
		FLOWER_EVIL = "",
		FOLIAGE = "",
		FOOTBALLHAT = "",
        FOSSIL_PIECE = "",
        FOSSIL_STALKER =
        {
			GENERIC = "",
			FUNNY = "",
			COMPLETE = "",
        },
        STALKER = "",
        STALKER_ATRIUM = "",
        STALKER_MINION = "",
        THURIBLE = "",
        ATRIUM_OVERGROWTH = "",
		FROG =
		{
			DEAD = "",
			GENERIC = "",
			SLEEPING = "",
		},
		FROGGLEBUNWICH = "",
		FROGLEGS = "",
		FROGLEGS_COOKED = "",
		FRUITMEDLEY = "",
		FURTUFT = "", 
		GEARS = "",
		GHOST = "",
		GOLDENAXE = "",
		GOLDENPICKAXE = "",
		GOLDENPITCHFORK = "",
		GOLDENSHOVEL = "",
		GOLDNUGGET = "",
		GRASS =
		{
			BARREN = "",
			WITHERED = "",
			BURNING = "",
			GENERIC = "",
			PICKED = "",
			DISEASED = "",
			DISEASING = "",
		},
		GRASSGEKKO = 
		{
			GENERIC = "",	
			DISEASED = "",
		},
		GREEN_CAP = "",
		GREEN_CAP_COOKED = "",
		GREEN_MUSHROOM =
		{
			GENERIC = "",
			INGROUND = "",
			PICKED = "",
		},
		GUNPOWDER = "",
		HAMBAT = "",
		HAMMER = "",
		HEALINGSALVE = "",
		HEATROCK =
		{
			FROZEN = "",
			COLD = "",
			GENERIC = "",
			WARM = "",
			HOT = "",
		},
		HOME = "",
		HOMESIGN =
		{
			GENERIC = "",
            UNWRITTEN = "",
			BURNT = "",
		},
		ARROWSIGN_POST =
		{
			GENERIC = "",
            UNWRITTEN = "",
			BURNT = "",
		},
		ARROWSIGN_PANEL =
		{
			GENERIC = "",
            UNWRITTEN = "",
			BURNT = "",
		},
		HONEY = "",
		HONEYCOMB = "",
		HONEYHAM = "",
		HONEYNUGGETS = "",
		HORN = "",
		HOUND = "",
		HOUNDBONE = "",
		HOUNDMOUND = "",
		ICEBOX = "",
		ICEHAT = "",
		ICEHOUND = "",
		INSANITYROCK =
		{
			ACTIVE = "",
			INACTIVE = "",
		},
		JAMMYPRESERVES = "",

		KABOBS = "",
		KILLERBEE =
		{
			GENERIC = "",
			HELD = "",
		},
		KNIGHT = "",
		KOALEFANT_SUMMER = "",
		KOALEFANT_WINTER = "",
		KRAMPUS = "",
		KRAMPUS_SACK = ".",
		LEIF = "",
		LEIF_SPARSE = "",
		LIGHTER  = "",
		LIGHTNING_ROD =
		{
			CHARGED = "",
			GENERIC = "",
		},
		LIGHTNINGGOAT = 
		{
			GENERIC = "",
			CHARGED = "",
		},
		LIGHTNINGGOATHORN = "",
		GOATMILK = "",
		LITTLE_WALRUS = "",
		LIVINGLOG = "",
		LOG =
		{
			BURNING = "",
			GENERIC = "",
		},
		LUCY = "",
		LUREPLANT = "",
		LUREPLANTBULB = "",
		MALE_PUPPET = "",

		MANDRAKE_ACTIVE = "",
		MANDRAKE_PLANTED = "",
		MANDRAKE = "",

        MANDRAKESOUP = "",
        MANDRAKE_COOKED = "",
        MAPSCROLL = "",
        MARBLE = "",
        MARBLEBEAN = "",
        MARBLEBEAN_SAPLING = "",
        MARBLESHRUB = "",
        MARBLEPILLAR = "",
        MARBLETREE = "",
        MARSH_BUSH =
        {
            BURNING = "",
            GENERIC = "",
            PICKED = "",
        },
        BURNT_MARSH_BUSH = "",
        MARSH_PLANT = "",
        MARSH_TREE =
        {
            BURNING = "",
            BURNT = "",
            CHOPPED = "",
            GENERIC = "",
        },
        MAXWELL = "",
        MAXWELLHEAD = "",
        MAXWELLLIGHT = "",
        MAXWELLLOCK = "",
        MAXWELLTHRONE = "",
        MEAT = "",
        MEATBALLS = "",
        MEATRACK =
        {
            DONE = "",
            DRYING = "",
            DRYINGINRAIN = "",
            GENERIC = "",
            BURNT = "",
        },
        MEAT_DRIED = "",
        MERM = "",
        MERMHEAD =
        {
            GENERIC = "",
            BURNT = "",
        },
        MERMHOUSE =
        {
            GENERIC = "",
            BURNT = "",
        },
        MINERHAT = "",
        MONKEY = "",
        MONKEYBARREL = "",
        MONSTERLASAGNA = "",
        FLOWERSALAD = "",
        ICECREAM = "",
        WATERMELONICLE = "",
        TRAILMIX = "",
        HOTCHILI = "",
        GUACAMOLE = "",
        MONSTERMEAT = "",
        MONSTERMEAT_DRIED = "",
        MOOSE = "",
        MOOSE_NESTING_GROUND = "",
        MOOSEEGG = "",
        MOSSLING = "",
        FEATHERFAN = "",
        MINIFAN = "",
        GOOSE_FEATHER = "",
        STAFF_TORNADO = "",
        MOSQUITO =
        {
            GENERIC = "",
            HELD = "",
        },
        MOSQUITOSACK = "",
        MOUND =
        {
            DUG = "",
            GENERIC = "",
        },
        NIGHTLIGHT = "",
        NIGHTMAREFUEL = "",
        NIGHTSWORD = "",
        NITRE = "",
        ONEMANBAND = "",
        OASISLAKE = "",
        PANDORASCHEST = "",
        PANFLUTE = "",
        PAPYRUS = "",
        WAXPAPER = "",
        PENGUIN = "",
        PERD = "",
        PEROGIES = "",
        PETALS = "",
        PETALS_EVIL = "",
        PHLEGM = "",
        PICKAXE = "",
        PIGGYBACK = "",
        PIGHEAD =
        {
            GENERIC = "",
            BURNT = "",
        },
        PIGHOUSE =
        {
            FULL = "",
            GENERIC = "",
            LIGHTSOUT = "",
            BURNT = "",
        },
        PIGKING = "",
        PIGMAN =
        {
            DEAD = "",
            FOLLOWER = "",
            GENERIC = "",
            GUARD = "",
            WEREPIG = "",
        },
        PIGSKIN = "",
        PIGTENT = "",
        PIGTORCH = "",
        PINECONE = "",
        PINECONE_SAPLING = "",
        LUMPY_SAPLING = "",
        PITCHFORK = "",
        PLANTMEAT = "",
        PLANTMEAT_COOKED = "",
        PLANT_NORMAL =
        {
            GENERIC = "",
            GROWING = "",
            READY = "",
            WITHERED = "",
        },
        POMEGRANATE = "",
        POMEGRANATE_COOKED = "",
        POMEGRANATE_SEEDS = "",
        POND = "",
        POOP = "",
        --PORTABLECOOKPOT_ITEM = "",
        FERTILIZER = "",
        PUMPKIN = "",
        PUMPKINCOOKIE = "",
        PUMPKIN_COOKED = "",
        PUMPKIN_LANTERN = "",
        PUMPKIN_SEEDS = "",
        PURPLEAMULET = "",
        PURPLEGEM = "",
        RABBIT =
        {
            GENERIC = "",
            HELD = "",
        },
        RABBITHOLE =
        {
            GENERIC = "",
            SPRING = "",
        },
        RAINOMETER =
        {
            GENERIC = "",
            BURNT = "",
        },
        RAINCOAT = "",
        RAINHAT = "",
        RATATOUILLE = "",
        RAZOR = "",
        REDGEM = "",
        RED_CAP = "",
        RED_CAP_COOKED = "",
        RED_MUSHROOM =
        {
            GENERIC = "",
            INGROUND = "",
            PICKED = "",
        },
        REEDS =
        {
            BURNING = "",
            GENERIC = "",
            PICKED = "",
        },
        RELIC = "",
        RUINS_RUBBLE = "",
        RUBBLE = "",
        --RUINSRELIC_PLATE = "",
        RESEARCHLAB =
        {
            GENERIC = "",
            BURNT = "",
        },
        RESEARCHLAB2 =
        {
            GENERIC = "",
            BURNT = "",
        },
        RESEARCHLAB3 =
        {
            GENERIC = "",
            BURNT = "",
        },
        RESEARCHLAB4 =
        {
            GENERIC = "",
            BURNT = "",
        },
        RESURRECTIONSTATUE =
        {
            GENERIC = "",
            BURNT = "",
        },
        RESURRECTIONSTONE = "",
        ROBIN =
        {
            GENERIC = "",
            HELD = "",
        },
        ROBIN_WINTER =
        {
            GENERIC = "",
            HELD = "",
        },
        ROBOT_PUPPET = "",
        ROCK_LIGHT =
        {
            GENERIC = "",
            OUT = "",
            LOW = "",
            NORMAL = "",
        },
        CAVEIN_BOULDER =
        {
            GENERIC = "",
            RAISED = "",
        },
        ROCK = "",
        PETRIFIED_TREE = "",
        ROCK_PETRIFIED_TREE = "",
        ROCK_PETRIFIED_TREE_OLD = "",
        ROCK_ICE =
        {
            GENERIC = "",
            MELTED = "",
        },
        ROCK_ICE_MELTED = "",
        ICE = "",
        ROCKS = "",
        ROOK = "",
        ROPE = "",
        ROTTENEGG = "",
        ROYAL_JELLY = "",
        JELLYBEAN = "",
        SADDLE_BASIC = "",
        SADDLE_RACE = "",
        SADDLE_WAR = "",
        SADDLEHORN = "",
        SALTLICK = "",
        BRUSH = "",
		SANITYROCK =
		{
			ACTIVE = "",
			INACTIVE = "",
		},
		SAPLING =
		{
			BURNING = "",
			WITHERED = "",
			GENERIC = "",
			PICKED = "",
			DISEASED = "",
			DISEASING = "",
		},
   		SCARECROW = 
   		{
			GENERIC = "",
			BURNING = "",
			BURNT = "",
   		},
   		SCULPTINGTABLE=
   		{
			EMPTY = "",
			BLOCK = "",
			SCULPTURE = "",
			BURNT = "",
   		},
        SCULPTURE_KNIGHTHEAD = "",
		SCULPTURE_KNIGHTBODY = 
		{
			COVERED = "",
			UNCOVERED = "",
			FINISHED = "",
			READY = "",
		},
        SCULPTURE_BISHOPHEAD = "",
		SCULPTURE_BISHOPBODY = 
		{
			COVERED = "",
			UNCOVERED = "",
			FINISHED = "",
			READY = "",
		},
        SCULPTURE_ROOKNOSE = "",
		SCULPTURE_ROOKBODY = 
		{
			COVERED = "",
			UNCOVERED = "",
			FINISHED = "",
			READY = "",
		},
        GARGOYLE_HOUND = "",
        GARGOYLE_WEREPIG = "",
		SEEDS = "",
		SEEDS_COOKED = "",
		SEWING_KIT = "",
		SEWING_TAPE = "STICKY PAPER",
		SHOVEL = "",
		SILK = "",
		SKELETON = "",
		SCORCHED_SKELETON = "",
		SKULLCHEST = "",
		SMALLBIRD =
		{
			GENERIC = "",
			HUNGRY = "",
			STARVING = "",
		},
		SMALLMEAT = "",
		SMALLMEAT_DRIED = "",
		SPAT = "",
		SPEAR = "",
		SPEAR_WATHGRITHR = "",
		WATHGRITHRHAT = "",
		SPIDER =
		{
			DEAD = "",
			GENERIC = "",
			SLEEPING = "",
		},
		SPIDERDEN = "",
		SPIDEREGGSACK = "",
		SPIDERGLAND = "",
		SPIDERHAT = "",
		SPIDERQUEEN = "",
		SPIDER_WARRIOR =
		{
			DEAD = "",
			GENERIC = "",
			SLEEPING = "",
		},
		SPOILED_FOOD = "",
        STAGEHAND =
        {
			AWAKE = "",
			HIDING = "",
        },
        STATUE_MARBLE = 
        {
            GENERIC = "",
            TYPE1 = "",
            TYPE2 = "",
        },
		STATUEHARP = "",
		STATUEMAXWELL = "",
		STEELWOOL = "",
		STINGER = "",
		STRAWHAT = "",
		STUFFEDEGGPLANT = "",
		--SUNKBOAT = "",
		SWEATERVEST = "",
		REFLECTIVEVEST = "",
		HAWAIIANSHIRT = "",
		TAFFY = "",
		TALLBIRD = "",
		TALLBIRDEGG = "",
		TALLBIRDEGG_COOKED = "",
		TALLBIRDEGG_CRACKED =
		{
			COLD = "",
			GENERIC = "",
			HOT = "",
			LONG = "",
			SHORT = "",
		},
		TALLBIRDNEST =
		{
			GENERIC = "",
			PICKED = "",
		},
		TEENBIRD =
		{
			GENERIC = "",
			HUNGRY = "",
			STARVING = "",
		},
		TELEPORTATO_BASE =
		{
			ACTIVE = "",
			GENERIC = "",
			LOCKED = "",
			PARTIAL = "",
		},
		TELEPORTATO_BOX = "",
		TELEPORTATO_CRANK = "",
		TELEPORTATO_POTATO = "",
		TELEPORTATO_RING = "",
		TELESTAFF = "",
		TENT = 
		{
			GENERIC = "",
			BURNT = "",
		},
		SIESTAHUT = 
		{
			GENERIC = "",
			BURNT = "",
		},
		TENTACLE = "",
		TENTACLESPIKE = "",
		TENTACLESPOTS = "",
		TENTACLE_PILLAR = "",
        TENTACLE_PILLAR_HOLE = "",
		TENTACLE_PILLAR_ARM = "",
		TENTACLE_GARDEN = "",
		TOPHAT = "",
		TORCH = "",
		TRANSISTOR = "",
		TRAP = "",
		TRAP_TEETH = "",
		TRAP_TEETH_MAXWELL = "",
		TREASURECHEST = 
		{
			GENERIC = "",
			BURNT = "",
		},
		TREASURECHEST_TRAP = "",
		SACRED_CHEST = 
		{
			GENERIC = "",
			LOCKED = "",
		},
		TREECLUMP = "",
		
		TRINKET_1 = "", --Melted Marbles
		TRINKET_2 = "", --Fake Kazoo
		TRINKET_3 = "", --Gord's Knot
		TRINKET_4 = "", --Gnome
		TRINKET_5 = "", --Toy Rocketship
		TRINKET_6 = "", --Frazzled Wires
		TRINKET_7 = "", --Ball and Cup
		TRINKET_8 = "", --Rubber Bung
		TRINKET_9 = "", --Mismatched Buttons
		TRINKET_10 = "", --Dentures
		TRINKET_11 = "", --Lying Robot
		TRINKET_12 = "", --Dessicated Tentacle
		TRINKET_13 = "", --Gnomette
		TRINKET_14 = "", --Leaky Teacup
		TRINKET_15 = "", --Pawn
		TRINKET_16 = "", --Pawn
		TRINKET_17 = "", --Bent Spork
		TRINKET_18 = "", --Trojan Horse
		TRINKET_19 = "", --Unbalanced Top
		TRINKET_20 = "", --Backscratcher
		TRINKET_21 = "", --Egg Beater
		TRINKET_22 = "", --Frayed Yarn
		TRINKET_23 = "", --Shoehorn
		TRINKET_24 = "", --Lucky Cat Jar
		TRINKET_25 = "", --Air Unfreshener
		TRINKET_26 = "", --Potato Cup
		TRINKET_27 = "", --Coat Hanger
		TRINKET_28 = "", --Rook
        TRINKET_29 = "", --Rook
        TRINKET_30 = "", --Knight
        TRINKET_31 = "", --Knight
        TRINKET_32 = "", --Cubic Zirconia Ball
        TRINKET_33 = "", --Spider Ring
        TRINKET_34 = "", --Monkey Paw
        TRINKET_35 = "", --Empty Elixir
        TRINKET_36 = "", --Faux Fangs
        TRINKET_37 = "", --Broken Stake
		TRINKET_38 = "I think it came from another world. One with grifts.", -- Binoculars Griftlands trinket
        TRINKET_39 = "I wonder where the other one is?", -- Lone Glove Griftlands trinket
        TRINKET_40 = "Holding it makes me feel like bartering.", -- Snail Scale Griftlands trinket
        TRINKET_41 = "It's a little warm to the touch.", -- Goop Canister Hot Lava trinket
        TRINKET_42 = "It's full of someone's childhood memories.", -- Toy Cobra Hot Lava trinket
        TRINKET_43= "It's not very good at jumping.", -- Crocodile Toy Hot Lava trinket
        TRINKET_44 = "It's some sort of plant specimen.", -- Broken Terrarium ONI trinket
        TRINKET_45 = "It's picking up frequencies from another world.", -- Odd Radio ONI trinket
        TRINKET_46 = "Maybe a tool for testing aerodynamics?", -- Hairdryer ONI trinket

        HALLOWEENCANDY_1 = "", -- Candy Apple
        HALLOWEENCANDY_2 = "", -- Candy Corn
        HALLOWEENCANDY_3 = "", -- Not-So-Candy Corn
        HALLOWEENCANDY_4 = "", -- Gummy Spider
        HALLOWEENCANDY_5 = "", -- Catcoon Candy
        HALLOWEENCANDY_6 = "", -- "Raisins"
        HALLOWEENCANDY_7 = "", -- Raisins
        HALLOWEENCANDY_8 = "", -- Ghost Pop
        HALLOWEENCANDY_9 = "", -- Jelly Worm
        HALLOWEENCANDY_10 = "", -- Tentacle Lolli
        HALLOWEENCANDY_11 = "", -- Choco Pigs
		HALLOWEENCANDY_12 = "Did that candy just move?", --ONI meal lice candy
        HALLOWEENCANDY_13 = "Oh, my poor jaw.", --Griftlands themed candy
        HALLOWEENCANDY_14 = "I don't do well with spice.", --Hot Lava pepper candy
        CANDYBAG = "",

        DRAGONHEADHAT = "",
        DRAGONBODYHAT = "",
        DRAGONTAILHAT = "",
        PERDSHRINE =
        {
            GENERIC = "",
            EMPTY = "",
            BURNT = "",
        },
        REDLANTERN = "",
        LUCKY_GOLDNUGGET = "",
        FIRECRACKERS = "",
        PERDFAN = "",
        REDPOUCH = "",

		BISHOP_CHARGE_HIT = "",
		TRUNKVEST_SUMMER = "",
		TRUNKVEST_WINTER = "",
		TRUNK_COOKED = "",
		TRUNK_SUMMER = "",
		TRUNK_WINTER = "",
		TUMBLEWEED = "",
		TURKEYDINNER = "",
		TWIGS = "",
		UMBRELLA = "",
		GRASS_UMBRELLA = "",
		UNIMPLEMENTED = "",
		WAFFLES = "",
		WALL_HAY = 
		{	
			GENERIC = "",
			BURNT = "",
		},
		WALL_HAY_ITEM = "",
		WALL_STONE = "",
		WALL_STONE_ITEM = "",
		WALL_RUINS = "",
		WALL_RUINS_ITEM = "",
		WALL_WOOD = 
		{
			GENERIC = "",
			BURNT = "",
		},
		WALL_WOOD_ITEM = "",
		WALL_MOONROCK = "",
		WALL_MOONROCK_ITEM = "",
		FENCE = "",
        FENCE_ITEM = "",
        FENCE_GATE = "",
        FENCE_GATE_ITEM = "",
		WALRUS = "",
		WALRUSHAT = "",
		WALRUS_CAMP =
		{
			EMPTY = "",
			GENERIC = "",
		},
		WALRUS_TUSK = "",
		WARDROBE = 
		{
			GENERIC = "",
            BURNING = "",
			BURNT = "",
		},
		WARG = "",
		WASPHIVE = "",
		WATERBALLOON = "",
		WATERMELON = "",
		WATERMELON_COOKED = "",
		WATERMELONHAT = "",
		WAXWELLJOURNAL = "",
		WETGOOP = "",
        WHIP = "",
		WINTERHAT = "",
		WINTEROMETER = 
		{
			GENERIC = "",
			BURNT = "",
		},

        WINTER_TREE =
        {
            BURNT = "",
            BURNING = "",
            CANDECORATE = "",
            YOUNG = "",
        },
		WINTER_TREESTAND = 
		{
			GENERIC = "",
            BURNT = "",
		},
        WINTER_ORNAMENT = "",
        WINTER_ORNAMENTLIGHT = "",
        WINTER_ORNAMENTBOSS = "",

        WINTER_FOOD1 = "", --gingerbread cookie
        WINTER_FOOD2 = "", --sugar cookie
        WINTER_FOOD3 = "", --candy cane
        WINTER_FOOD4 = "", --fruitcake
        WINTER_FOOD5 = "", --yule log cake
        WINTER_FOOD6 = "", --plum pudding
        WINTER_FOOD7 = "", --apple cider
        WINTER_FOOD8 = "", --hot cocoa
        WINTER_FOOD9 = "", --eggnog

        KLAUS = "",
        KLAUS_SACK = "",
		KLAUSSACKKEY = "",
		WORMHOLE =
		{
			GENERIC = "",
			OPEN = "",
		},
		WORMHOLE_LIMITED = "",
		ACCOMPLISHMENT_SHRINE = "",        
		LIVINGTREE = "",
		ICESTAFF = "",
		REVIVER = "",
		SHADOWHEART = "",
        ATRIUM_RUBBLE = 
        {
			LINE_1 = "",
			LINE_2 = "",
			LINE_3 = "",
			LINE_4 = "",
			LINE_5 = "",
		},
        ATRIUM_STATUE = "",
        ATRIUM_LIGHT = 
        {
			ON = "",
			OFF = "",
		},
        ATRIUM_GATE =
        {
			ON = "",
			OFF = "",
			CHARGING = "",
			DESTABILIZING = "",
			COOLDOWN = "",
        },
        ATRIUM_KEY = "",
		LIFEINJECTOR = "",
		SKELETON_PLAYER =
		{
			MALE = "",
			FEMALE = "",
			ROBOT = "",
			DEFAULT = "",
		},
		HUMANMEAT = "",
		HUMANMEAT_COOKED = "",
		HUMANMEAT_DRIED = "",
		ROCK_MOON = "",
		MOONROCKNUGGET = "",
		MOONROCKCRATER = "",

        REDMOONEYE = "",
        PURPLEMOONEYE = "",
        GREENMOONEYE = "",
        ORANGEMOONEYE = "",
        YELLOWMOONEYE = "",
        BLUEMOONEYE = "",
		]]
		--Arena Event
        LAVAARENA_BOARLORD = "BAD PIG KING!",
        BOARRIOR = "BIG PIG SCARY!",
        BOARON = "TINY PIG WEAK",
        PEGHOOK = "PIG NO LIKE GOO!",
        TRAILS = "THAT NO PIG",
        TURTILLUS = "MUST CRACK SHELL",
        SNAPPER = "PLEASE DON'T BITE PIG",
        
        LAVAARENA_PORTAL = 
        {
            ON = "PIG GO HOME",
            GENERIC = "HOW GO HOME?",
        },
        LAVAARENA_KEYHOLE = "PIG NEEDS KEY",
		LAVAARENA_KEYHOLE_FULL = "KEY FIT GOOD",
        LAVAARENA_BATTLESTANDARD = "BAD UGLY STICK, PIG MUST DESTROY",
        LAVAARENA_SPAWNER = "HOW BAD PIGS GET HERE?",

        HEALINGSTAFF = "FEEL GOOD STICK",
        FIREBALLSTAFF = "VERY HOT STICK",
        HAMMER_MJOLNIR = "PIG LIKE THIS TOOL",
        SPEAR_GUNGNIR = "PIG STAB BAD PIG WITH SHARP STICK",
        BLOWDART_LAVA = "WINTER PIG LOVES THIS PIG THINKS",
        BLOWDART_LAVA2 = "SHOULD GIVE TO WINTER PIGS",
        LAVAARENA_LUCY = "WHY COME BACK ONLY FOR NOT PIG?",
        WEBBER_SPIDER_MINION = "PIG THINKS YOU CUTE, BUT PIG HATES YOU",
        BOOK_FOSSIL = "BOOK MAKE BAD PIGS ROCKY",
		LAVAARENA_BERNIE = "CREEPY TOY IS FRIEND",
		SPEAR_LANCE = "PIG WANT TO USE THIS!",
		BOOK_ELEMENTAL = "SUMMON HOT ROCK TO HELP PIG",
		LAVAARENA_ELEMENTAL = "HI HOT ROCK",

   		LAVAARENA_ARMORLIGHT = "WEIRD PIG SKIRT",
		LAVAARENA_ARMORLIGHTSPEED = "IT LIKE PIG WEAR NOTHING AT ALL",
		LAVAARENA_ARMORMEDIUM = "PIG SAFE FROM BAD PIGS WITH THIS",
		LAVAARENA_ARMORMEDIUMDAMAGER = "IT MAKE PIG HURT MORE BAD PIGS",
		LAVAARENA_ARMORMEDIUMRECHARGER = "USELESS JUNK",
		LAVAARENA_ARMORHEAVY = "PIG WANT BE ROCKY PIG",
		LAVAARENA_ARMOREXTRAHEAVY = "SUPER ROCKY",

		LAVAARENA_FEATHERCROWNHAT = "MAKE PIG WANNA FLY!",
        LAVAARENA_HEALINGFLOWERHAT = "STUPID HAT",
        LAVAARENA_LIGHTDAMAGERHAT = "PIG LIKE HAT!",
        LAVAARENA_STRONGDAMAGERHAT = "PIG REALLY LIKE HAT!",
        LAVAARENA_TIARAFLOWERPETALSHAT = "PIG HATE HAT!",
        LAVAARENA_EYECIRCLETHAT = "CREEPY HAT NOT FOR PIG",
        LAVAARENA_RECHARGERHAT = "SHINY HAT IS OKAY PIG GUESS",
        LAVAARENA_HEALINGGARLANDHAT = "PIG LIKE FEEL GOOD HAT",
        LAVAARENA_CROWNDAMAGERHAT = "PIG BE BAD PIG ENDER WITH HAT",
	},
	DESCRIBE_GENERIC = "PID NOT KNOW THIS",
	DESCRIBE_TOODARK = "PIG SCARED OF DARK!",
	DESCRIBE_SMOLDERING = "IT BURNING!",
	EAT_FOOD =
	{
		TALLBIRDEGG_CRACKED = "PIG FEEL THING IN FOOD",
	},
}
