local MakePlayerCharacter = require "prefabs/player_common"

---------------------------
----------==Notes==--------
--
---------------------------

local prefabname = "shadow2player"

local assets = 
{
	Asset("ANIM", "anim/shadow_oceanhorror.zip"),
}

local prefabs = {
	"shadow_teleport_in",
    "shadow_teleport_out",
    "nightmarefuel",
}

local start_inv = {
}

local start_inv2 = 
{

}

local getskins = {}

if MOBHOUSE == "Enable1" or MOBHOUSE == "Enable3" then
	start_inv = start_inv2
end
-----------------------
--Stats--
local mob = 
{
	health = TUNING.CRAWLINGHORROR_HEALTH,
	hunger = 150,
	hungerrate = 0, --Can't starve
	sanity = 300,
	runspeed = TUNING.TERRORBEAK_SPEED,
	walkspeed = TUNING.TERRORBEAK_SPEED,
	damage = TUNING.TERRORBEAK_DAMAGE*2,
	attackperiod = TUNING.TERRORBEAK_ATTACK_PERIOD,
	range = 4,
	hit_range = 4,
	bank = "shadowcreature2",
	build = "shadow_insanity2_basic",
	scale = 1,
	stategraph = "SGshadowp",
	minimap = prefabname..".tex",
}

--Loot that drops when you die, duh.
SetSharedLootTable('shadow2player',
-----Prefab---------------------Chance------------
{
    {'nightmarefuel',			1.00},
	{'nightmarefuel',			1.00},   
})

local sounds_ocean =
{
    attack = "dontstarve/sanity/creature1/attack",
    attack_grunt = "dontstarve/sanity/creature1/attack_grunt",
    death = "dontstarve/sanity/creature1/die",
    idle = "dontstarve/sanity/creature1/idle",
    taunt = "dontstarve/sanity/creature1/taunt",
    appear = "dontstarve/sanity/creature1/appear",
    disappear = "dontstarve/sanity/creature1/dissappear",
}

local sounds =
{
    attack = "dontstarve/sanity/creature2/attack",
    attack_grunt = "dontstarve/sanity/creature2/attack_grunt",
    death = "dontstarve/sanity/creature2/die",
    idle = "dontstarve/sanity/creature2/idle",
    taunt = "dontstarve/sanity/creature2/taunt",
    appear = "dontstarve/sanity/creature2/appear",
    disappear = "dontstarve/sanity/creature2/dissappear",
}


local FORGE_STATS = PP_FORGE.TERRORBEAK
--==============================================
--					Mob Functions
--==============================================
--Ocean Horror

local function Transition(inst, isocean)
	local x,y,z = inst.Transform:GetWorldPosition()
    local fx = SpawnPrefab("shadow_teleport_"..(isocean and "in" or "out")) 
    fx.Transform:SetPosition(x,y,z) 
	if isocean then
		inst.sounds = sounds_ocean
		inst.components.locomotor.runspeed = 2
		inst.components.locomotor.walkspeed = 2
		inst.AnimState:SetBank("oceanhorror")
		inst.AnimState:SetBuild("shadow_oceanhorror")
		inst._ripples = SpawnPrefab("oceanhorror_ripples")
		inst._ripples.entity:SetParent(inst.entity)
		inst:SetStateGraph("SGoceanshadowp")
	else
		inst.sounds = sounds
		inst.components.locomotor.runspeed = mob.runspeed
		inst.components.locomotor.walkspeed = mob.walkspeed
		inst.AnimState:SetBank(mob.bank)
		inst.AnimState:SetBuild(mob.build)
		if inst._ripples then
			inst._ripples:Remove()
			inst._ripples = nil
		end
		inst:SetStateGraph(mob.stategraph)
	end
end
--==============================================
--				Custom Common Functions
--==============================================	
local function OnDeath(inst)
	inst:RemoveComponent("transparentonsanity") --makes the ghost visible.
end
--==============================================
--					Loading/Saving
--==============================================
 
local function OnLoad(inst, data)
	if data ~= nil then
		inst.mobteleported = data.mobteleported or false
		inst.isshiny = data.isshiny or 0
	end
end

local function OnSave(inst, data)
	data.mobteleported = inst.mobteleported or false
	data.isshiny = inst.isshiny or 0
end

--==============================================
--					Forged Forge
--==============================================

local ex_fns = require "prefabs/player_common_extensions"

local function SetForge(inst)
	PlayablePets.SetForgeStats(inst, FORGE_STATS)
	
	inst:DoTaskInTime(0, function(inst)
	inst:AddComponent("itemtyperestrictions")
	inst.components.itemtyperestrictions:SetRestrictions({"melees", "books", "staves", "darts"})
	end)
	
	inst.components.combat:SetDamageType(1)
	
	inst:RemoveEventCallback("respawnfromcorpse", ex_fns.OnRespawnFromPlayerCorpse)
	inst:ListenForEvent("respawnfromcorpse", PlayablePets.OnRespawnFromMobCorpse)
end

--==============================================
--					Common/Master
--==============================================

local common_postinit = function(inst) 
	inst.MiniMapEntity:SetIcon(mob.minimap)

	inst:DoTaskInTime(0, function() 
   if ThePlayer then
      inst:EnableMovementPrediction(false) --PP doesn't work with movement prediction enabled, due to use of custom stategraphs
   end
end)

	
	----------------------------------
	--Tags--
    inst:AddTag("monster")
    inst:AddTag("hostile")
    inst:AddTag("notraptrigger")
	----------------------------------
	inst:WatchWorldState( "isday", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "isdusk", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "isnight", function() PlayablePets.SetNightVision(inst)  end)
	inst:WatchWorldState( "iscaveday", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "iscavedusk", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "iscavenight", function() PlayablePets.SetNightVision(inst)  end)
	
	PlayablePets.SetNightVision(inst)
end



local master_postinit = function(inst)
	--Stats--
	inst.sounds = sounds
	PlayablePets.SetCommonStats(inst, mob) --mob table, ishuman, ignorepvpmultiplier
	PlayablePets.SetCommonWeatherResistances(inst, 60, 20, 1) --heat, cold, wetness
	PlayablePets.SetCommonStatResistances(inst, 0, 0, 0, 9999) --fire, acid, poison
	----------------------------------
	--Variables	
	inst.taunt = true
	inst.mobplayer = true
	inst.specialsleep = true
	inst.isshiny = 0
	inst.getskins = getskins
	
	inst.sounds = sounds
	
	inst.sanityreward = 50
	
	inst.debuffimmune = true
	
	local body_symbol = "chest"
	inst.poisonsymbol = body_symbol
	MakeLargeBurnableCharacter(inst, body_symbol)
    MakeLargeFreezableCharacter(inst)
	inst.components.debuffable:SetFollowSymbol(body_symbol, 0, 0, 0)
	PlayablePets.SetStormImmunity(inst)
	----------------------------------
	--Components
	PlayablePets.SetCommonLootdropper(inst, prefabname) --inst, prefaboverride
	----------------------------------
	--Eater--
	inst.components.hunger:Pause()
	---------------------------------
	--Physics and Shadows--
	MakeCharacterPhysics(inst, 10, 1.5)
	RemovePhysicsColliders(inst)
	inst.Physics:SetCollisionGroup(COLLISION.SANITY)
    inst.Physics:CollidesWith(COLLISION.SANITY)
    inst.DynamicShadow:Enable(false)
    inst.Transform:SetFourFaced()
	PlayablePets.SetAmphibious(inst, mob.bank, "oceanhorror", nil, nil, true)
	if inst.components.amphibiouscreature then
		inst.components.amphibiouscreature:SetEnterWaterFn(
			function(inst)
				if not inst:HasTag("playerghost") then
					Transition(inst, true)				
					inst.sg:GoToState("appear")
				end	
			end)
		inst.components.amphibiouscreature:SetExitWaterFn(
			function(inst)
				if not inst:HasTag("playerghost") then
					Transition(inst, false)					
					inst.sg:GoToState("appear")
				end
			end)	
	end	
	---------------------------------
	--Listeners--
	inst:ListenForEvent("equip", PlayablePets.CommonOnEquip) --Shows head when hats make heads disappear.
	---------------------------------
	--Forge--
	if TheNet:GetServerGameMode() == "lavaarena" then
		inst.forge_fn = SetForge(inst)
	end
    ------------------------------------------------------
	--Respawning and Initializing functions--
	
	inst:ListenForEvent("respawnfromghost", function(inst) PlayablePets.RevRestore(inst, mob, true, true, true) end) --(inst, mob, isflying, iswebimmune, noshadow, ishuman)

    inst:DoTaskInTime(0, function(inst) PlayablePets.CommonSetChar(inst, mob) end)
	inst:DoTaskInTime(3, function(inst) inst.AnimState:SetMultColour(1, 1, 1, 0.5) end)
    inst:ListenForEvent("respawnfromghost", function() 
        inst:DoTaskInTime(5, function(inst) PlayablePets.CommonSetChar(inst, mob) end)
		inst:DoTaskInTime(5.1, function(inst) PlayablePets.SetSkin(inst, mob) end)
		inst:DoTaskInTime(5, function(inst) PlayablePets.RevRestore(inst, mob, true, true, true) inst.components.hunger:Pause() inst.AnimState:SetMultColour(1, 1, 1, 0.5) end)
    end)
	
	inst.OnSave = OnSave
    inst.OnLoad = OnLoad
	
    return inst	
end

return MakePlayerCharacter(prefabname, prefabs, assets, common_postinit, master_postinit, start_inv)