local MakePlayerCharacter = require "prefabs/player_common"

---------------------------


---------------------------


local assets = 
{
	Asset("ANIM", "anim/dragonfly_build.zip"),
    Asset("ANIM", "anim/dragonfly_fire_build.zip"),
    Asset("ANIM", "anim/dragonfly_basic.zip"),
    Asset("ANIM", "anim/dragonfly_actions.zip"),
    Asset("ANIM", "anim/dragonfly_yule_build.zip"),
    Asset("ANIM", "anim/dragonfly_fire_build.zip"),
	
	Asset("ANIM", "anim/dragonfly_shiny_build_01.zip"),
	Asset("ANIM", "anim/dragonfly_fire_shiny_build_01.zip"),
	Asset("ANIM", "anim/dragonfly_shiny_build_02.zip"),
	Asset("ANIM", "anim/dragonfly_fire_shiny_build_02.zip"),
}

local prefabs = 
{	
	"firesplash_fx",
    "tauntfire_fx",
    "attackfire_fx",
    "vomitfire_fx",
    "firering_fx",

    --loot:
    "dragon_scales",
    "lavae_egg",
    "meat",
    "goldnugget",
    "redgem",
    "bluegem",
    "purplegem",
    "orangegem",
    "yellowgem",
    "greengem",
    "dragonflyfurnace_blueprint",
    "chesspiece_dragonfly_sketch",
}
	
	
local getskins = {"1"}
--Note: Becareful when adding starting items.
--People are likely to spam changecharacter to get more of them.	
local start_inv = 
{
	--"prefab",
}

local start_inv2 = 
{
	--"prefabhome",
}

if MOBHOUSE == "Enable1" or MOBHOUSE == "Enable3" then
	start_inv = start_inv2
end
-----------------------
local mob = 
{
	health = TUNING.DRAGONPLAYER_HEALTH,
	hunger = TUNING.DRAGONPLAYER_HUNGER,
	hungerrate = TUNING.WILSON_HUNGER_RATE,
	sanity = TUNING.DRAGONPLAYER_SANITY,
	runspeed = TUNING.DRAGONFLY_SPEED,
	walkspeed = TUNING.DRAGONFLY_SPEED,
	damage = 75*2,
	damage2 = 90*2,
	attackperiod = TUNING.DRAGONFLY_ATTACK_PERIOD,
	range = TUNING.DRAGONFLY_ATTACK_RANGE,
	hit_range = 6,
	bank = "dragonfly",
	build = "dragonfly_build",
	build2 = "dragonfly_fire_build",
	shiny = "dragonfly",
	scale = 1.3,
	--build2 = "alternate build here",
	stategraph = "SGdragonplayer",
	minimap = "dragonplayer.tex",
	
}
-----------------------
--Loot that drops when you die, duh.
SetSharedLootTable( 'dragonflyp',
{
    {'dragon_scales',    1.00},
	{'lavae_egg',    	 0.33},
    {'meat',             1.00},
    {'meat',             1.00},
    {'meat',             1.00},
    {'meat',             1.00},
    {'meat',             1.00},
    {'meat',             1.00},
})


local MIN_HEAT = 60
local MAX_HEAT = 60

local function onupdate(inst, dt)
    inst.charge_time = inst.charge_time - dt
    if inst.charge_time <= 0 then
        inst.charge_time = 0
		if inst.charge_task ~= nil then
            inst.charge_task:Cancel()
            inst.charge_task = nil
        end
        inst.specialatk = true
		inst.sg:GoToState("special_atk1")
    else
    --    
    end
end

local function TransformNormal(inst)
    inst.enraged = false
	inst.components.ppskin_manager:LoadSkin(mob, true)
    --Set normal stats
	inst.Light:Enable(true)
	inst.Light:SetRadius(0)
	inst.Light:SetFalloff(2)
	inst.Light:SetIntensity(4)
	inst.Light:SetColour(245/255,40/255,0/255)
    inst.components.locomotor.walkspeed = mob.walkspeed
    inst.components.combat:SetDefaultDamage(mob.damage)

    inst.components.propagator:StopSpreading()
end

local function TransformFire(inst)
    inst.enraged = true
	inst.components.ppskin_manager:LoadSkin(mob, true)
    inst.can_ground_pound = true
    --Set fire stats
    inst.components.locomotor.walkspeed = TUNING.DRAGONFLY_FIRE_SPEED
    inst.components.combat:SetDefaultDamage(mob.damage2)
	
	inst.Light:Enable(true)
	inst.Light:SetRadius(5)
	inst.Light:SetFalloff(0.9)
	inst.Light:SetIntensity(0.6)
	if inst.components.ppskin_manager:GetSkin() then
		--TODO just make a table for this
		inst.Light:SetColour(0/255,157/255,255/255)
	else
		inst.Light:SetColour(180/255,195/255,150/255)
	end
	if MOBFIRE== "Disable" then 
		inst.components.propagator:StartSpreading()
	end
end

local function SetSkinDefault(inst, data)
	if data then
		inst.AnimState:SetBuild(inst.enraged and (data.rage_build or mob.build2) or data.build)
	else
		inst.AnimState:SetBuild(inst.enraged and mob.build2 or mob.build)
	end	
end

------------------------------------------------------
 
local function OnLoad(inst, data)
	if data ~= nil then
		inst.mobteleported = data.mobteleported or false
		inst.isshiny = data.isshiny or 0
	end
end

local function OnSave(inst, data)
	data.mobteleported = inst.mobteleported or false
	data.isshiny = inst.isshiny or 0
end

------------------------------------------------------

local common_postinit = function(inst) 
	--inst.soundsname = "beefalo"

	inst.MiniMapEntity:SetIcon(mob.minimap)
	--inst.AnimState:SetMultColour(1, 1, 1, 0.5)

	inst:DoTaskInTime(0, function() 
   if ThePlayer then
      inst:EnableMovementPrediction(false)
   end
end)
	--Night Vision that toggles, might be a better way to do this.
	--Enables at night, disables during the day.
	inst:WatchWorldState( "isday", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "isdusk", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "isnight", function() PlayablePets.SetNightVision(inst)  end)
	inst:WatchWorldState( "iscaveday", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "iscavedusk", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "iscavenight", function() PlayablePets.SetNightVision(inst)  end)
	
	PlayablePets.SetNightVision(inst) --This is here to make sure the NightVision is enabled when a player rejoins.
	
end

--==============================================
--					Forged Forge
--==============================================
local ex_fns = require "prefabs/player_common_extensions"

local function OnHitOther_Forge(inst, target)
	if inst.is_doing_special and target then 
		target:PushEvent("flipped", {flipper = inst})
	end
end

local function SetForge(inst)
	PlayablePets.SetForgeStats(inst, PP_FORGE.DRAGONFLY)
	
	inst.mobsleep = false
	
	inst:DoTaskInTime(0, function(inst)
		inst.components.combat:AddDamageBuff("calm_dragon_buff", 1.25, true)
		
		inst:AddComponent("itemtyperestrictions")
		inst.components.itemtyperestrictions:SetRestrictions({"books", "staves", "melees", "darts"})
	end)
	
	inst.knockbackimmune = true
	
	inst.components.combat:SetDamageType(1)
	
	inst.components.revivablecorpse.revivespeedmult = 2
	
	inst:RemoveEventCallback("respawnfromcorpse", ex_fns.OnRespawnFromPlayerCorpse)
	inst:ListenForEvent("respawnfromcorpse", PlayablePets.OnRespawnFromMobCorpse)
end
----------------------------------------------

local master_postinit = function(inst) 
	------------------------------------------
	--Stats--
    PlayablePets.SetCommonStats(inst, mob) --mob table, ishuman, ignorepvpmultiplier
	PlayablePets.SetCommonWeatherResistances(inst, 60, 20, 1) --heat, cold, wetness
	PlayablePets.SetCommonStatResistances(inst, 0, nil, nil, TUNING.DRAGONFLY_FREEZE_THRESHOLD) --fire, acid, poison, freeze (flat value, not a multiplier)
	inst.components.combat:SetHurtSound("dontstarve_DLC001/creatures/dragonfly/hurt")	
	----------------------------------
	--Loot drops--
	inst:AddComponent("lootdropper")
    inst.components.lootdropper:SetChanceLootTable('dragonflyp')
	----------------------------------
	--Tags--
	inst.poisonimmune = true
	inst:AddTag("dragonplayer")
	inst:AddTag("flying")
	inst:AddTag("epic")
	
	inst.can_ground_pound = true
	inst.altattack = true
	inst.isdragonrage = false
	inst.taunt2 = true
	inst.mobsleep = true
	
	inst.getskins = getskins
	inst.components.ppskin_manager:SetSkinDefaultFn(SetSkinDefault)

	inst.isshiny = 0
	inst.level = 0
	inst.hit_recovery = 0.75
	
	inst.TransformFire = TransformFire
    inst.TransformNormal = TransformNormal
	inst.SoundEmitter:PlaySound("dontstarve_DLC001/creatures/dragonfly/fly", "flying")
	
	local body_symbol = "swap_fire"
	inst.poisonsymbol = body_symbol
    MakeHugeFreezableCharacter(inst, body_symbol)
	inst.components.debuffable:SetFollowSymbol(body_symbol, 0, 0, 0)
	MakeLargeBurnableCharacter(inst, body_symbol)
	
    inst:AddComponent("healthtrigger")
	PlayablePets.SetStormImmunity(inst)
	----------------------------------
	--Eater--
	
	inst.components.eater:SetDiet({ FOODTYPE.MEAT, FOODTYPE.BURNT }, { FOODTYPE.MEAT, FOODTYPE.BURNT }) 
    inst.components.eater:SetAbsorptionModifiers(2,1.5,1.5) --This might multiply food stats.
	inst.components.eater:SetCanEatHorrible() --check to make them eat monster meat.
	inst.components.eater.strongstomach = true -- can eat monster meat!
	inst.components.eater.eatwholestack = true --should eat the entire stack and get all the benefits.
	inst.components.eater.ignoresspoilage = true -- Can eat spoiled food without consequence. Might keep food stats as if fresh.
	---------------------------------
	--Physics and Scale--
	
	MakeFlyingGiantCharacterPhysics(inst, 500, 1.4)
    inst.DynamicShadow:SetSize(6, 3.5)
    inst.Transform:SetSixFaced()
	PlayablePets.SetAmphibious(inst, nil, nil, true)	
	---------------------------------
	--Ground Pound--
	inst:AddComponent("groundpounder")
	inst.components.groundpounder.numRings = 2
	inst.components.groundpounder.damageRings = 0
	if MOBFIRE ~= "Enable" then
		inst.components.groundpounder.burner = true
	end    
    inst.components.groundpounder.groundpoundfx = "firesplash_fx"
    inst.components.groundpounder.groundpounddamagemult = 1.25
    inst.components.groundpounder.groundpoundringfx = "firering_fx"
	inst.components.groundpounder.noTags = {"lavae"}
	---------------------------------
	--Listeners--
	
	inst:ListenForEvent("equip", PlayablePets.CommonOnEquip) --Shows head when hats make heads disappear.
    ------------------------------------------------------
	--Light and Character Functions. Don't Touch.--
    local light = inst.entity:AddLight()
  	inst.Light:Enable(false)
  	inst.Light:SetRadius(0)
  	inst.Light:SetFalloff(0.9)
  	inst.Light:SetIntensity(0.6)
  	inst.Light:SetColour(180/255,195/255,150/255)
	
	------------------------------------------------------
	--Forge--
	if TheNet:GetServerGameMode() == "lavaarena" then
		inst.forge_fn = SetForge(inst)
	end
	------------------------------------------------------
	--Respawning and Initializing functions--
	
	inst:ListenForEvent("respawnfromghost", function(inst) PlayablePets.RevRestore(inst, mob, true, true) end) --(inst, mob, isflying, iswebimmune, noshadow, ishuman)

    inst:DoTaskInTime(0, function(inst) PlayablePets.CommonSetChar(inst, mob) end)
	inst:DoTaskInTime(3, function(inst) PlayablePets.SetSkin(inst, mob) end)
    inst:ListenForEvent("respawnfromghost", function() 
        inst:DoTaskInTime(5, function(inst) PlayablePets.CommonSetChar(inst, mob) end)
		inst:DoTaskInTime(5.1, function(inst) PlayablePets.SetSkin(inst, mob) end)
		inst:DoTaskInTime(5, function(inst) PlayablePets.RevRestore(inst, mob, true, true) end)
    end)
	
	inst.OnSave = OnSave
    inst.OnLoad = OnLoad
	
    return inst
	
end


return MakePlayerCharacter("dragonplayer", prefabs, assets, common_postinit, master_postinit, start_inv)
