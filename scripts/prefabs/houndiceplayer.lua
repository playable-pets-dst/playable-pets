local MakePlayerCharacter = require "prefabs/player_common"

---------------------------
----------==Notes==--------
--
---------------------------

local prefabname = "houndiceplayer"

local assets = 
{
	Asset("ANIM", "anim/hound_basic.zip"),
    Asset("ANIM", "anim/hound_basic_water.zip"),
    Asset("ANIM", "anim/hound.zip"),
    Asset("ANIM", "anim/hound_ocean.zip"),
    Asset("ANIM", "anim/hound_red.zip"),
    Asset("ANIM", "anim/hound_red_ocean.zip"),
    Asset("ANIM", "anim/hound_ice.zip"),
    Asset("ANIM", "anim/hound_ice_ocean.zip"),
    Asset("ANIM", "anim/hound_mutated.zip"),
	Asset("ANIM", "anim/ghost_monster_build.zip"),
}

local prefabs = 
{	

}

local start_inv = 
{
	--'prefab',
	--"spear",
}

local start_inv2 =
{
   --"beehome",
}

local getskins = {}

if MOBHOUSE == "Enable1" or MOBHOUSE == "Enable3" then
	start_inv = start_inv2
end
-----------------------
--Stats--
local mob = 
{
	health = TUNING.HOUNDICEPLAYER_HEALTH,
	hunger = TUNING.HOUNDICEPLAYER_HUNGER,
	hungerrate = TUNING.WILSON_HUNGER_RATE, 
	sanity = TUNING.HOUNDICEPLAYER_SANITY,
	
	runspeed = TUNING.HOUND_SPEED,
	walkspeed = TUNING.HOUND_SPEED/2,
	
	attackperiod = 0,
	damage = 30*2,
	range = 3,
	hit_range = 3,
	
	bank = "hound",
	build = "hound_ice_ocean",
	shiny = "hound_ice",
	
	scale = 1,
	stategraph = "SGhoundplayer",
	minimap = prefabname..".tex",	
}

--Loot that drops when you die, duh.
SetSharedLootTable( 'houndiceplayer',
{
    {'monstermeat',			 1.00},
	{'houndstooth',			 0.50},
	{'houndstooth',			 0.50},
	{'bluegem',			 	 0.25},
})

local FORGE_STATS = PP_FORGE.HOUND
--==============================================
--					Mob Functions
--==============================================
local sounds =
{
    pant = "dontstarve/creatures/hound/pant",
    attack = "dontstarve/creatures/hound/attack",
    bite = "dontstarve/creatures/hound/bite",
    bark = "dontstarve/creatures/hound/bark",
    death = "dontstarve/creatures/hound/death",
    sleep = "dontstarve/creatures/hound/sleep",
    growl = "dontstarve/creatures/hound/growl",
    howl = "dontstarve/creatures/together/clayhound/howl",
    hurt = "dontstarve/creatures/hound/hurt",
}

local NO_TAGS = { "FX", "NOCLICK", "DECOR", "INLIMBO" }
local FREEZABLE_TAGS = { "freezable" }

local function DoIceExplosion(inst)
    inst.components.freezable:SpawnShatterFX()
    local x, y, z = inst.Transform:GetWorldPosition()
    local ents = TheSim:FindEntities(x, y, z, 4, FREEZABLE_TAGS, NO_TAGS)
    for i, v in pairs(ents) do
        if v ~= inst and v.components.freezable ~= nil then
            v.components.freezable:AddColdness(2)
        end
    end

    inst.SoundEmitter:PlaySound("dontstarve/creatures/hound/icehound_explo")
end
--==============================================
--				Custom Common Functions
--==============================================
local function OnHitOther(inst, data)
	local other = data.target
	if other and other.components.freezable and TheNet:GetServerGameMode() ~= "lavaarena" then
		other.components.freezable:AddColdness(1)
		other.components.freezable:SpawnShatterFX()
	end
end
--==============================================
--					Loading/Saving
--==============================================
 
local function OnLoad(inst, data)
	if data ~= nil then
		inst.mobteleported = data.mobteleported or false
		inst.isshiny = data.isshiny or 0
	end
end

local function OnSave(inst, data)
	data.mobteleported = inst.mobteleported or false
	data.isshiny = inst.isshiny or 0
end

--==============================================
--					Forged Forge
--==============================================

local ex_fns = require "prefabs/player_common_extensions"

local function SetForge(inst)
	PlayablePets.SetForgeStats(inst, PP_FORGE.HOUND)
	
	inst.mobsleep = false	
	
	inst:DoTaskInTime(0, function(inst)
	inst:AddComponent("itemtyperestrictions")
	inst.components.itemtyperestrictions:SetRestrictions({"books", "staves"})
	end)
	
	inst.components.combat:SetDamageType(1)
	
	inst:RemoveEventCallback("respawnfromcorpse", ex_fns.OnRespawnFromPlayerCorpse)
	inst:ListenForEvent("respawnfromcorpse", PlayablePets.OnRespawnFromMobCorpse)
end

--==============================================
--					Common/Master
--==============================================

local common_postinit = function(inst) 
	inst.MiniMapEntity:SetIcon(mob.minimap)

	inst:DoTaskInTime(0, function() 
   if ThePlayer then
      inst:EnableMovementPrediction(false) --PP doesn't work with movement prediction enabled, due to use of custom stategraphs
   end
end)
	----------------------------------
	--Tags--
    inst:AddTag("monster")
	inst:AddTag("hound")
	----------------------------------

	inst:WatchWorldState( "isday", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "isdusk", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "isnight", function() PlayablePets.SetNightVision(inst)  end)
	inst:WatchWorldState( "iscaveday", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "iscavedusk", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "iscavenight", function() PlayablePets.SetNightVision(inst)  end)
	
	PlayablePets.SetNightVision(inst)
end



local master_postinit = function(inst)
	--Stats--
	PlayablePets.SetCommonStats(inst, mob) --mob table, ishuman, ignorepvpmultiplier
	PlayablePets.SetCommonWeatherResistances(inst, nil, 20) --heat, cold, wetness
	PlayablePets.SetCommonStatResistances(inst) --fire, acid, poison
	inst.components.combat:SetHurtSound(sounds.hurt)	
	----------------------------------
	--Variables	
	inst.mobsleep = true
	inst.taunt = true
	inst.taunt2 = true
	
	inst.sounds = sounds
	
	inst.getskins = getskins
	
	local body_symbol = "hound_body"
	inst.poisonsymbol = body_symbol
	MakeLargeBurnableCharacter(inst, body_symbol)
    MakeHugeFreezableCharacter(inst, body_symbol)
	inst.components.debuffable:SetFollowSymbol(body_symbol, 0, 0, 0)
	PlayablePets.SetStormImmunity(inst)
	----------------------------------
	--Components
	PlayablePets.SetCommonLootdropper(inst, prefabname) --inst, prefaboverride
	----------------------------------
	--Eater--
	inst.components.eater:SetDiet({ FOODTYPE.MEAT }, { FOODTYPE.MEAT })
    inst.components.eater:SetAbsorptionModifiers(1,1,1) --This multiplies food stats.
	inst.components.eater:SetCanEatHorrible() --check to make them eat monster meat.
	inst.components.eater.strongstomach = true -- can eat monster meat!
	---------------------------------
	--Physics and Shadows--
	MakeCharacterPhysics(inst, 10, .5)
	inst.Transform:SetFourFaced()
	
	inst.DynamicShadow:SetSize(2.5, 1.5)
	
	PlayablePets.SetAmphibious(inst, "hound", "hound_water")
	---------------------------------
	--Listeners--
	inst:ListenForEvent("equip", PlayablePets.CommonOnEquip) --Shows head when hats make heads disappear.
	inst:ListenForEvent("death", DoIceExplosion)
	inst:ListenForEvent("onhitother", OnHitOther)
	---------------------------------
	--Forge--
	if TheNet:GetServerGameMode() == "lavaarena" then
		inst.forge_fn = SetForge(inst)
	end
    ------------------------------------------------------
	--Respawning and Initializing functions--
	
	inst:ListenForEvent("respawnfromghost", function(inst) PlayablePets.RevRestore(inst, mob) end) --(inst, mob, isflying, iswebimmune, noshadow, ishuman)

    inst:DoTaskInTime(0, function(inst) PlayablePets.CommonSetChar(inst, mob) end)
	inst:DoTaskInTime(3, function(inst) PlayablePets.SetSkin(inst, mob) end)
    inst:ListenForEvent("respawnfromghost", function() 
        inst:DoTaskInTime(5, function(inst) PlayablePets.CommonSetChar(inst, mob) end)
		inst:DoTaskInTime(5.1, function(inst) PlayablePets.SetSkin(inst, mob) end)
		inst:DoTaskInTime(5, function(inst) PlayablePets.RevRestore(inst, mob) end)
    end)
	
	inst.OnSave = OnSave
    inst.OnLoad = OnLoad
	
    return inst	
end

return MakePlayerCharacter(prefabname, prefabs, assets, common_postinit, master_postinit, start_inv)