local MakePlayerCharacter = require "prefabs/player_common"

---------------------------


---------------------------


local assets = 
{
	Asset("ANIM", "anim/walrus_shiny_build_01.zip"),	
	Asset("ANIM", "anim/walrus_actions.zip"),
    Asset("ANIM", "anim/walrus_attacks.zip"),
    Asset("ANIM", "anim/walrus_basic.zip"),
    Asset("ANIM", "anim/walrus_build.zip"),
    Asset("ANIM", "anim/walrus_baby_build.zip"),
    Asset("SOUND", "sound/mctusky.fsb"),
}

local getskins = {"1"}

local prefabs = 
{	

}
	
--Note: Becareful when adding starting items.
--People are likely to spam changecharacter to get more of them.	
local start_inv = 
{
	--"prefab",
}

local start_inv2 = 
{
	--"prefabhome",
}

if MOBHOUSE == "Enable1" or MOBHOUSE == "Enable3" then
	start_inv = start_inv2
end
-----------------------
local mob = 
{
	health = TUNING.WALRUSPLAYER_HEALTH,
	hunger = TUNING.WALRUSPLAYER_HUNGER,
	hungerrate = TUNING.WILSON_HUNGER_RATE,
	sanity = TUNING.WALRUSPLAYER_SANITY,
	runspeed = 4,
	walkspeed = 2,
	damage = 20,
	attackperiod = 0,
	range = 2,
	hitrange = 3,
	bank = "walrus",
	shiny = "walrus",
	build = "walrus_build",
	scale = 1.5,
	--build2 = "alternate build here",
	stategraph = "SGwalrusplayer",
	minimap = "walrusplayer.tex",
	
}
-----------------------
--Loot that drops when you die, duh.
SetSharedLootTable( 'walrusplayer',
{
	{'meat',			 1.00},
	{'walrushat',		 0.50},
})

local function Unequip(inst, data)
	inst.AnimState:Show("HAT")
	local hands = inst.components.inventory:GetEquippedItem(EQUIPSLOTS.HANDS) or nil
	if data.item and data.eslot == EQUIPSLOTS.HANDS and data.item.components.rechargeable then
		data.item.components.rechargeable.cooldownrate = 1
	end
end
 
local function OnLoad(inst, data)
	if data ~= nil then
		inst.mobteleported = data.mobteleported or false
		inst.isshiny = data.isshiny or 0
	end
end

local function OnSave(inst, data)
	data.mobteleported = inst.mobteleported or false
	data.isshiny = inst.isshiny or 0
end

local ex_fns = require "prefabs/player_common_extensions"

local function RetargetForgeFn(inst)
    if (inst.components.follower.leader and inst.components.follower.leader.components.health:IsDead()) or (inst.daddy and inst.daddy.components.health:IsDead()) then
		return FindEntity(inst, 30, function(guy) return inst.components.combat:CanTarget(guy) and not guy.components.health:IsDead() end, nil, { "structure", "player", "companion" })
	elseif inst.components.follower.leader and inst.components.follower.leader.components.combat.target then	
		return inst.components.follower.leader.components.combat.target
	end
end

local function KeepForgeTarget(inst, target)
    return target:IsValid() and 
		target.sg ~= nil and not 
		((target.sg:HasStateTag("sleeping") or target:HasTag("_isinheals")) or target.sg:HasStateTag("fossilized")) and not target:HasTag("player")
end

local function RelinktoLeader(inst)
	if inst.daddy and inst.daddy.components.leader then
		inst.daddy.components.leader:AddFollower(inst)
	end
end

local function NoDmgFromPlayers(inst, amount, overtime, cause, ignore_invincible, afflicter, ignore_absorb)
    return afflicter ~= nil and (afflicter:HasTag("player") or afflicter:HasTag("companion"))
end

local function DoBuff(inst, data)
	--I'll test to see if the data comes in later
	--if data and data.buff then
		inst.components.combat.defaultdamage = 15 * 2
		inst.components.combat:SetAttackPeriod(0)
		inst.Transform:SetScale(1, 1, 1)
		inst.components.locomotor.runspeed = TUNING.HOUND_SPEED
		inst.sg:GoToState("taunt")
	--end
end

local function UnBuff(inst, data)
	--if data and data.buff then
		inst.components.combat.defaultdamage = 15
		inst.components.combat:SetAttackPeriod(TUNING.HOUND_ATTACK_PERIOD)
		inst.Transform:SetScale(0.5, 0.5, 0.5)
		inst.components.locomotor.runspeed = TUNING.HOUND_SPEED * 2
		inst.sg:GoToState("taunt")
	--end
end

local function SetForge(inst)
	PlayablePets.SetForgeStats(inst, PP_FORGE.PIG)
	
	inst.mobsleep = false
	
	inst.components.revivablecorpse.revivespeedmult = 1
	
	inst.components.petleash.maxpets = 2
	inst.components.petleash.petprefab = "hound"
		
	inst:DoTaskInTime(0, function(inst) 
		for i = 1, 2 or 1 do
			local theta = math.random() * 2 * _G.PI
			local pt = inst:GetPosition()
			local radius = math.random(3, 6)
			local offset = _G.FindWalkableOffset(pt, theta, radius, 3, true, true)
				if offset ~= nil then
					pt.x = pt.x + offset.x
					pt.z = pt.z + offset.z
				end
			local pet = inst.components.petleash:SpawnPetAt(pt.x, 0, pt.z)
			pet:AddTag("companion")
			pet:AddTag("notarget")
			pet.components.health:SetInvincible(true)
			pet.daddy = inst
			pet.components.health.redirect = NoDmgFromPlayers
			pet.Transform:SetScale(0.5, 0.5, 0.5)
			pet.components.locomotor.runspeed = TUNING.HOUND_SPEED * 2
			
			pet.components.follower:KeepLeaderOnAttacked()
			pet.components.follower.keepleader = true
			
			pet.AnimState:SetBuild("hound_ice")
			
			pet.components.combat:SetDamageType(1)	
			pet.components.combat:SetDefaultDamage(15)
			pet.components.combat:SetRetargetFunction(1, RetargetForgeFn)
			pet.components.combat:SetKeepTargetFunction(KeepForgeTarget)
			
			pet:AddComponent("debuffable")
			pet.components.debuffable:IsEnabled(true)
			pet:AddComponent("colouradder")
			pet.components.lootdropper:SetChanceLootTable({})
			
			pet:ListenForEvent("buffpets", DoBuff)
			pet:ListenForEvent("debuffpets", UnBuff)
		end
	end)
	
	inst:DoTaskInTime(0, function(inst)
	inst:AddComponent("itemtyperestrictions")
	inst.components.itemtyperestrictions:SetRestrictions({"books"}, {"staves"}, {"melee"})
	end)
	
	inst:RemoveEventCallback("respawnfromcorpse", ex_fns.OnRespawnFromPlayerCorpse)
	inst:ListenForEvent("respawnfromcorpse", PlayablePets.OnRespawnFromMobCorpse)
end

local common_postinit = function(inst) 
	--inst.soundsname = "beefalo"

	inst.MiniMapEntity:SetIcon(mob.minimap)
	--inst.AnimState:SetMultColour(1, 1, 1, 0.5)

	inst:DoTaskInTime(0, function() 
   if ThePlayer then
      inst:EnableMovementPrediction(false)
   end
end)
	
end

local master_postinit = function(inst) 
	------------------------------------------
	--Stats--
    PlayablePets.SetCommonStats(inst, mob, true, true) --mob table, ishuman, ignorepvpmultiplier
	PlayablePets.SetCommonWeatherResistances(inst, nil, 20, 0.5) --heat, cold, wetness
	PlayablePets.SetCommonStatResistances(inst, nil, 0.75, nil, 3) --fire, acid, poison, freeze (flat value, not a multiplier)
	----------------------------------
	--Loot drops--
	inst:AddComponent("lootdropper")
    inst.components.lootdropper:SetChanceLootTable('walrusplayer')
	----------------------------------
	--Tags--
	
	inst:AddTag("walrus")
	inst:AddTag("walrusplayer")
	inst:AddTag("houndfriend")
	
	inst.taunt = true
	inst.taunt2 = true
	inst.mobsleep = true
	inst.soundgroup = "mctusk"
	
	inst.isshiny = 0
	inst.getskins = getskins
	
	local body_symbol = "pig_torso"
	inst.poisonsymbol = body_symbol
	MakeMediumBurnableCharacter(inst, body_symbol)
    MakeMediumFreezableCharacter(inst, body_symbol)
	inst.components.debuffable:SetFollowSymbol(body_symbol, 0, 0, 0)	
	
	inst.AnimState:Show("HAT")
	----------------------------------
	--PvP Mode Stuff--
	inst.mobteleported = false
	----------------------------------
	--Eater--
    inst.components.eater:SetAbsorptionModifiers(1,1,1) --This might multiply food stats.
	---------------------------------
	--Physics and Scale--
	MakeCharacterPhysics(inst, 50, .5)
    inst.DynamicShadow:SetSize(2.5, 1.5)
	MakeCharacterPhysics(inst, 50, .5)
    inst.Transform:SetFourFaced()
	---------------------------------
	--Listeners--
	inst:ListenForEvent("equip", PlayablePets.CommonOnEquip) --Shows head when hats make heads disappear.
	inst:ListenForEvent("unequip", Unequip)
	------------------------------------------------------
	--Forge--
	if TheNet:GetServerGameMode() == "lavaarena" then
		inst.forge_fn = SetForge(inst)
	end
	------------------------------------------------------
	--Respawning and Initializing functions--
	
	inst:ListenForEvent("respawnfromghost", function(inst) PlayablePets.RevRestore(inst, mob, nil, nil, nil, true) end) --(inst, mob, isflying, iswebimmune, noshadow, ishuman)

    inst:DoTaskInTime(0, function(inst) PlayablePets.CommonSetChar(inst, mob) end)
	inst:DoTaskInTime(3, function(inst) PlayablePets.SetSkin(inst, mob) end)
    inst:ListenForEvent("respawnfromghost", function() 
        inst:DoTaskInTime(5, function(inst) PlayablePets.CommonSetChar(inst, mob) end)
		inst:DoTaskInTime(5.1, function(inst) PlayablePets.SetSkin(inst, mob) end)
		inst:DoTaskInTime(5, function(inst) PlayablePets.RevRestore(inst, mob, nil, nil, nil, true) end)
    end)
	
	inst.OnSave = OnSave
    inst.OnLoad = OnLoad
	
    return inst	
end

return MakePlayerCharacter("walrusplayer", prefabs, assets, common_postinit, master_postinit, start_inv)
