local MakePlayerCharacter = require "prefabs/player_common"

---------------------------
----------==Notes==--------
--
---------------------------

local prefabname = "queenspiderplayer"

local assets = 
{
	Asset("ANIM", "anim/spider_queen_build.zip"),
	Asset("ANIM", "anim/spider_queen_shiny_build_01.zip"),
    Asset("ANIM", "anim/spider_queen.zip"),
    Asset("ANIM", "anim/spider_queen_2.zip"),
}

local prefabs = {
}

local start_inv = {
}

local start_inv2 = 
{

}

local getskins = {"1"}

if MOBHOUSE == "Enable1" or MOBHOUSE == "Enable3" then
	start_inv = start_inv2
end
-----------------------
--Stats--
local mob = 
{
	health = TUNING.QUEENSPIDERPLAYER_HEALTH,
	hunger = TUNING.QUEENSPIDERPLAYER_HUNGER,
	hungerrate = TUNING.WILSON_HUNGER_RATE, 
	sanity = TUNING.QUEENSPIDERPLAYER_SANITY,
	
	runspeed = TUNING.SPIDERQUEEN_WALKSPEED,
	walkspeed = TUNING.SPIDERQUEEN_WALKSPEED,
	
	attackperiod = 3,
	damage = TUNING.SPIDERQUEEN_DAMAGE*2,
	range = 3.5,
	hit_range = 5,
	
	bank = "spider_queen",
	build = "spider_queen_build",
	shiny = "spider_queen",
	
	scale = 1,
	stategraph = "SGspider_queenp",
	minimap = prefabname..".tex",	
}

--Loot that drops when you die, duh.
SetSharedLootTable( 'queenspiderplayer',
{
	{'monstermeat',			 1.00},
	{'monstermeat',			 1.00},
	{'monstermeat',			 1.00},
	{'monstermeat',			 1.00},
	{'monstermeat',			 1.00},
	{'spiderhat',		 1.00},
})



local FORGE_STATS = PP_FORGE.SPIDER_QUEEN
--==============================================
--					Mob Functions
--==============================================
local function BabyCount(inst)
    return inst.components.leader.numfollowers
end

--Update the code to not rely on targets to make warriors as you lose the target when you spawn babies.
local function MakeBaby(inst)
    local angle = inst.Transform:GetRotation()/DEGREES
    local prefab = (math.random() < .2) and "spider_warrior" or "spider"
    local spider = inst.components.lootdropper:SpawnLootPrefab(prefab)
	--local warrior = TheSim:FindEntities(pt.x, pt.y, pt.z, TUNING.SPIDERQUEEN_NEARBYPLAYERSDIST, {"spider_warrior"}, {"playerghost"})
    local rad = spider.Physics:GetRadius()+inst.Physics:GetRadius()+.25;
    local pt = Vector3(inst.Transform:GetWorldPosition())
	--if prefab == "spider_warrior" and warrior > 8 then
		--prefab = "spider"
	--end
    if spider then	
        spider.Transform:SetPosition(pt.x + rad*math.cos(angle), pt.y, pt.z + rad*math.sin(angle))
        spider.sg:GoToState("taunt")
		if inst.isshiny and inst.isshiny ~= 0 then
			if prefab == "spider" then
			spider.AnimState:SetBuild("spider_shiny_build_0"..inst.isshiny)
			else
			spider.AnimState:SetBuild("spider_warrior_shiny_build_0"..inst.isshiny)
			end
		end
        inst.components.leader:AddFollower(spider)
        if inst.components.combat.target then
            spider.components.combat:SetTarget(inst.components.combat.target)
        end
    end
end

local function MaxBabies(inst)
    local pt = Vector3(inst.Transform:GetWorldPosition())
    local ents = TheSim:FindEntities(pt.x, pt.y, pt.z, TUNING.SPIDERQUEEN_NEARBYPLAYERSDIST, {"player"}, {"playerghost"})
    local spiders = #ents * 20

    return 15
end

local function AdditionalBabies(inst)
    local pt = Vector3(inst.Transform:GetWorldPosition())
    local ents = TheSim:FindEntities(pt.x, pt.y, pt.z, TUNING.SPIDERQUEEN_NEARBYPLAYERSDIST, {"player"}, {"playerghost"})
    local addspiders = #ents * 0.5

    return RoundBiasedUp(addspiders)
end
--==============================================
--				Custom Common Functions
--==============================================	

--==============================================
--					Loading/Saving
--==============================================
 
local function OnLoad(inst, data)
	if data ~= nil then
		inst.mobteleported = data.mobteleported or false
		inst.isshiny = data.isshiny or 0
	end
end

local function OnSave(inst, data)
	data.mobteleported = inst.mobteleported or false
	data.isshiny = inst.isshiny or 0
end

--==============================================
--					Forged Forge
--==============================================

local ex_fns = require "prefabs/player_common_extensions"

local function SetForge(inst)
	PlayablePets.SetForgeStats(inst, FORGE_STATS)
	
	inst.mobsleep = false	
	
	inst:DoTaskInTime(0, function(inst)
		inst:AddComponent("itemtyperestrictions")
		inst.components.itemtyperestrictions:SetRestrictions({"melees", "darts", "armors", "books", "staves"})
	end)
	
	inst.components.combat:SetDamageType(1)
	
	inst:RemoveEventCallback("respawnfromcorpse", ex_fns.OnRespawnFromPlayerCorpse)
	inst:ListenForEvent("respawnfromcorpse", PlayablePets.OnRespawnFromMobCorpse)
end

--==============================================
--					Common/Master
--==============================================

local common_postinit = function(inst) 
	inst.MiniMapEntity:SetIcon(mob.minimap)

	inst:DoTaskInTime(0, function() 
   if ThePlayer then
      inst:EnableMovementPrediction(false) --PP doesn't work with movement prediction enabled, due to use of custom stategraphs
   end
end)
	----------------------------------
	--Tags--
    inst:AddTag("spiderwhisperer")
	inst:AddTag("monster")
	inst:AddTag("epic")
    inst:AddTag("spiderqueen")
	----------------------------------

	inst:WatchWorldState( "isday", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "isdusk", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "isnight", function() PlayablePets.SetNightVision(inst)  end)
	inst:WatchWorldState( "iscaveday", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "iscavedusk", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "iscavenight", function() PlayablePets.SetNightVision(inst)  end)
	
	PlayablePets.SetNightVision(inst)
end



local master_postinit = function(inst)
	--Stats--
	PlayablePets.SetCommonStats(inst, mob) --mob table, ishuman, ignorepvpmultiplier
	PlayablePets.SetCommonWeatherResistances(inst, 60, 20, 1) --heat, cold, wetness
	PlayablePets.SetCommonStatResistances(inst) --fire, acid, poison
	----------------------------------
	--Variables	
	inst.mobsleep = true
	inst.taunt = true
	inst.taunt2 = true
	inst.getskins = getskins
	inst.followers = {}
	
	local body_symbol = "body"
	inst.poisonsymbol = body_symbol
	MakeLargeBurnableCharacter(inst, body_symbol)
    MakeLargeFreezableCharacter(inst, body_symbol)
	inst.components.debuffable:SetFollowSymbol(body_symbol, 0, 0, 0)
	
	inst.hit_recovery = 2
	----------------------------------
	--Components
	PlayablePets.SetCommonLootdropper(inst, prefabname) --inst, prefaboverride
	
	inst:AddComponent("shedder")
	inst.components.shedder.shedItemPrefab = "spidereggsack"
	inst.components.shedder.shedHeight = 0.5
	inst.components.shedder:StartShedding(4800)
	
	inst:AddComponent("incrementalproducer")
    inst.components.incrementalproducer.countfn = BabyCount
    inst.components.incrementalproducer.producefn = MakeBaby
    inst.components.incrementalproducer.maxcountfn = MaxBabies
    inst.components.incrementalproducer.incrementfn = AdditionalBabies
	
	----------------------------------
	--Eater--
	inst.components.eater:SetDiet({ FOODTYPE.MEAT }, { FOODTYPE.MEAT })
    inst.components.eater:SetCanEatHorrible()
	inst.components.eater.strongstomach = true -- can eat monster meat!
	---------------------------------
	--Physics and Shadows--
	MakeCharacterPhysics(inst, 1000, 1)
    inst.DynamicShadow:SetSize(7, 3)
    inst.Transform:SetFourFaced()
	---------------------------------
	--Listeners--
	inst:ListenForEvent("equip", PlayablePets.CommonOnEquip) --Shows head when hats make heads disappear.
	---------------------------------
	--Forge--
	if TheNet:GetServerGameMode() == "lavaarena" then
		inst.forge_fn = SetForge(inst)
	end
    ------------------------------------------------------
	--Respawning and Initializing functions--
	
	inst:ListenForEvent("respawnfromghost", function(inst) PlayablePets.RevRestore(inst, mob, nil, true) end) --(inst, mob, isflying, iswebimmune, noshadow, ishuman)

    inst:DoTaskInTime(0, function(inst) PlayablePets.CommonSetChar(inst, mob) end)
	inst:DoTaskInTime(3, function(inst) PlayablePets.SetSkin(inst, mob) end)
    inst:ListenForEvent("respawnfromghost", function() 
        inst:DoTaskInTime(5, function(inst) PlayablePets.CommonSetChar(inst, mob) end)
		inst:DoTaskInTime(5.1, function(inst) PlayablePets.SetSkin(inst, mob) end)
		inst:DoTaskInTime(5, function(inst) PlayablePets.RevRestore(inst, mob, nil, true) end)
    end)
	
	inst.OnSave = OnSave
    inst.OnLoad = OnLoad
	
    return inst	
end

return MakePlayerCharacter(prefabname, prefabs, assets, common_postinit, master_postinit, start_inv)