local MakePlayerCharacter = require "prefabs/player_common"

---------------------------


---------------------------


local assets = 
{
	Asset("ANIM", "anim/deerclops_basic.zip"),
    Asset("ANIM", "anim/deerclops_actions.zip"),
    Asset("ANIM", "anim/deerclops_build.zip"),
    Asset("ANIM", "anim/deerclops_yule.zip"),
	--Asset("ANIM", "anim/deerclops_yule_shiny_build_01.zip"),
	--Asset("ANIM", "anim/deerclops_yule_shiny_build_03.zip"),
	Asset("ANIM", "anim/deerclops_skeleton_build.zip"),
	Asset("ANIM", "anim/deerclops_shiny_build_01.zip"),
	Asset("ANIM", "anim/deerclops_shiny_build_02.zip"),
	Asset("ANIM", "anim/deerclops_shiny_build_03.zip"),
}

local prefabs = 
{	

}
	
--Note: Becareful when adding starting items.
--People are likely to spam changecharacter to get more of them.	
local start_inv = 
{
	--"prefab",
}

local start_inv2 = 
{
	--"prefabhome",
}

if MOBHOUSE == "Enable1" or MOBHOUSE == "Enable3" then
	start_inv = start_inv2
end
-----------------------
local mob = 
{
	health = TUNING.DEERPLAYER_HEALTH,
	hunger = TUNING.DEERPLAYER_HUNGER,
	hungerrate = TUNING.WILSON_HUNGER_RATE,
	sanity = TUNING.DEERPLAYER_SANITY,
	runspeed = 4,
	walkspeed = 4,
	attackperiod = TUNING.DEERCLOPS_ATTACK_PERIOD,
	damage = BOSS_STATS and 100*2 or 120*2,
	range = TUNING.DEERCLOPS_ATTACK_RANGE,
	bank = "deerclops",
	build = "deerclops_build",
	scale = 1.65,
	--build2 = "alternate build here",
	stategraph = "SGdeerplayer",
	minimap = "deerplayerp.tex",
	
}
-----------------------
--Loot that drops when you die, duh.
SetSharedLootTable('deerclopsp',
-----Prefab---------------------Chance------------
{
    {'deerclops_eyeball',			 1.00},
	{'meat',			 1.00},
	{'meat',			 1.00},
	{'meat',			 1.00},
	{'meat',			 1.00},
	{'meat',			 1.00},
	{'meat',			 1.00},
	{'meat',			 1.00},
	{'meat',			 1.00},
})

------------------------------------------------------
 
local function OnLoad(inst, data)
	if data ~= nil then
		inst.mobteleported = data.mobteleported or false
		inst.isshiny = data.isshiny or 0
	end
end

local function OnSave(inst, data)
	data.mobteleported = inst.mobteleported or false
	data.isshiny = inst.isshiny or 0
end

local common_postinit = function(inst) 
	--inst.soundsname = "beefalo"

	inst.MiniMapEntity:SetIcon(mob.minimap)
	--inst.AnimState:SetMultColour(1, 1, 1, 0.5)

	inst:DoTaskInTime(0, function() 
   if ThePlayer then
      inst:EnableMovementPrediction(false)
   end
end)
	--Night Vision that toggles, might be a better way to do this.
	--Enables at night, disables during the day.
	inst:WatchWorldState( "isday", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "isdusk", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "isnight", function() PlayablePets.SetNightVision(inst)  end)
	inst:WatchWorldState( "iscaveday", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "iscavedusk", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "iscavenight", function() PlayablePets.SetNightVision(inst)  end)
	
	PlayablePets.SetNightVision(inst) --This is here to make sure the NightVision is enabled when a player rejoins.
	
end

local function ClearRecentlyCharged(inst, other)
    inst.recentlycharged[other] = nil
end

local function oncollapse(inst, other)
	if other and other.components.workable ~= nil and other.components.workable.workleft > 0 then
		SpawnPrefab("collapse_small").Transform:SetPosition(other:GetPosition():Get())
		other.components.workable:Destroy(inst)
	end
end

local function oncollide(inst, other)
	if other == nil or not other:HasTag("tree") then
		return
	end
	local v1 = Vector3(inst.Physics:GetVelocity())
	if v1:LengthSq() < 1 then
		return
	end
	inst:DoTaskInTime(2*FRAMES, oncollapse, other)
end

local function OnNewState(inst, data)
    if not (inst.sg:HasStateTag("sleeping") or inst.sg:HasStateTag("waking")) then
        inst.Light:SetIntensity(.6)
        inst.Light:SetRadius(8)
        inst.Light:SetFalloff(3)
        inst.Light:SetColour(1, 0, 0)
    end
end

local function OnHitOther(inst, data)
	local other = data.target
	if other and other.components.freezable and TheNet:GetServerGameMode() ~= "lavaarena" then
		other.components.freezable:AddColdness(2)
		other.components.freezable:SpawnShatterFX()
	end
end

local ex_fns = require "prefabs/player_common_extensions"

local function SetForge(inst)
	PlayablePets.SetForgeStats(inst, PP_FORGE.DEERCLOPS)
	
	inst.mobsleep = false
	
	inst:DoTaskInTime(0, function(inst)
		inst:AddComponent("itemtyperestrictions")
		inst.components.itemtyperestrictions:SetRestrictions({"books", "staves", "darts", "melees"})
	end)
	
	inst.components.combat:SetDamageType(1)
	
	inst.components.revivablecorpse.revivespeedmult = 2
	
	inst:RemoveEventCallback("respawnfromcorpse", ex_fns.OnRespawnFromPlayerCorpse)
	inst:ListenForEvent("respawnfromcorpse", PlayablePets.OnRespawnFromMobCorpse)
end

local master_postinit = function(inst) 
	------------------------------------------
	--Stats--
    PlayablePets.SetCommonStats(inst, mob) --mob table, ishuman, ignorepvpmultiplier
	PlayablePets.SetCommonWeatherResistances(inst, 60, 20, 1) --heat, cold, wetness
	PlayablePets.SetCommonStatResistances(inst, nil, nil, nil, 10) --fire, acid, poison, freeze (flat value, not a multiplier)
	--Attack is done through ice fx now.
    --inst.components.combat:SetAreaDamage(TUNING.DEERCLOPS_AOE_RANGE, TUNING.DEERCLOPS_AOE_SCALE)
	----------------------------------
	--Loot drops--
	inst:AddComponent("lootdropper")
    inst.components.lootdropper:SetChanceLootTable('deerclopsp')
	----------------------------------
	--Tags--
	
	inst:AddTag("epic")
    inst:AddTag("monster")
    inst:AddTag("deerclops")
    inst:AddTag("largecreature")

	inst.AnimState:Hide("head_neutral")
	
	inst.altattack = true
	inst.taunt = true
	inst.mobsleep = true
	
	inst.isshiny = 0
	
	inst.recentlycharged = {}
	inst.Physics:SetCollisionCallback(oncollide)
	
	local body_symbol = "deerclops_body"
	inst.poisonsymbol = body_symbol
    MakeHugeFreezableCharacter(inst, body_symbol)
	inst.components.debuffable:SetFollowSymbol(body_symbol, 0, 0, 0)
	MakeLargeBurnableCharacter(inst, body_symbol)
	----------------------------------
	--Eater--
    inst.components.eater:SetAbsorptionModifiers(2,1.5,1.5) --This might multiply food stats.
	inst.components.eater:SetCanEatHorrible() --check to make them eat monster meat.
	inst.components.eater.strongstomach = true -- can eat monster meat!
	---------------------------------
	--Physics and Scale--
	
	MakeCharacterPhysics(inst, 1000, .5)
    inst.DynamicShadow:SetSize(6, 3.5)
    inst.Transform:SetFourFaced()
	
	inst.Physics:SetCollisionCallback(oncollide)
	---------------------------------
	--Listeners--
	inst:ListenForEvent("equip", PlayablePets.CommonOnEquip) --Shows head when hats make heads disappear.
	inst:ListenForEvent("newstate", OnNewState)
	inst:ListenForEvent("onhitother", OnHitOther)
	
	------------------------------------------------------
	--Forge--
	if TheNet:GetServerGameMode() == "lavaarena" then
		inst.forge_fn = SetForge(inst)
	end
	------------------------------------------------------
	--Respawning and Initializing functions--
	
	inst:ListenForEvent("respawnfromghost", function(inst) PlayablePets.RevRestore(inst, mob) end) --(inst, mob, isflying, iswebimmune, noshadow, ishuman)

    inst:DoTaskInTime(0, function(inst) PlayablePets.CommonSetChar(inst, mob) end)
	inst:DoTaskInTime(3, function(inst) PlayablePets.SetSkin(inst, mob) end)
    inst:ListenForEvent("respawnfromghost", function() 
        inst:DoTaskInTime(5, function(inst) PlayablePets.CommonSetChar(inst, mob) end)
		inst:DoTaskInTime(5.1, function(inst) PlayablePets.SetSkin(inst, mob) end)
		inst:DoTaskInTime(5, function(inst) PlayablePets.RevRestore(inst, mob) end)
    end)
	
	inst.OnSave = OnSave
    inst.OnLoad = OnLoad
	
    return inst
	
end

return MakePlayerCharacter("deerplayer", prefabs, assets, common_postinit, master_postinit, start_inv)
