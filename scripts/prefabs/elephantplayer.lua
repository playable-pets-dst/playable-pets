local MakePlayerCharacter = require "prefabs/player_common"

---------------------------
----------==Notes==--------
--
---------------------------
local prefabname = "elephantplayer"

local assets = 
{
	Asset("ANIM", "anim/koalefant_summer_shiny_build_01.zip"),
	Asset("ANIM", "anim/koalefant_winter_shiny_build_01.zip"),
	Asset("ANIM", "anim/koalefant_cat.zip"),
	Asset("ANIM", "anim/koalefant_dinosaur.zip"),
	Asset("ANIM", "anim/koalefant_goldhauberk.zip"),
	Asset("ANIM", "anim/koalefant_hauberk.zip"),
	Asset("ANIM", "anim/koalefant_moth.zip"),
	Asset("ANIM", "anim/koalefant_tapir.zip"),

}

local prefabs = 
{	

}
	
--Note: Becareful when adding starting items.
--People are likely to spam changecharacter to get more of them.	
local start_inv = 
{
	--'prefab',
	--"spear",
}

local start_inv2 =
{
   --"beehome",
}

if MOBHOUSE== "Enable1" or MOBHOUSE== "Enable3" then
	start_inv = start_inv2
end
-----------------------
--Stats--
local mob = 
{
	health = 1000,
	hunger = 200,
	hungerrate = TUNING.WILSON_HUNGER_RATE, --I think .25 is defualt.
	sanity = 100,
	runspeed = 7,
	walkspeed = 1.5,
	damage = 35,
	range = 2,
	hit_range = 3,
	attackperiod = 2,
	bank = "koalefant",
	build = "koalefant_summer_build",
	build2 = "koalefant_winter_build",
	scale = 1,	
	stategraph = "SGelephantplayer",
	minimap = "elephantplayer.tex",
	
}
-----------------------
--Loot that drops when you die, duh.

local loot_summer = {"meat","meat","meat","meat","meat","meat","meat","meat","trunk_summer"}
local loot_winter = {"meat","meat","meat","meat","meat","meat","meat","meat","trunk_winter"}

FORGE_STATS = PP_FORGE[string.upper(prefabname)] 
--==============================================
--					Mob Functions
--==============================================

--==============================================
--				Custom Common Functions
--==============================================	
local function OnIsWinter(inst, iswinter)
    if TheWorld.state.iswinter then
        inst.components.ppskin_manager:LoadSkin(mob, true)
		inst.components.lootdropper:SetLoot(loot_winter)
    else
		inst.components.ppskin_manager:LoadSkin(mob, true)
		inst.components.lootdropper:SetLoot(loot_summer)
    end
end

local function SetSkinDefault(inst, data)
	if data then
		inst.AnimState:SetBuild(TheWorld.state.iswinter and data.build2 or data.build)
	else
		inst.AnimState:SetBuild(TheWorld.state.iswinter and mob.build2 or mob.build)
	end
end
----PvP Teleport----

local function getwildposition(inst)
    local ground = TheWorld
    local centers = {}
    for i, node in ipairs(ground.topology.nodes) do
        if ground.Map:IsPassableAtPoint(node.x, 0, node.y) then
            table.insert(centers, {x = node.x, z = node.y})
        end
    end
    if #centers > 0 then
        local pos = centers[math.random(#centers)]
        return Point(pos.x, 0, pos.z)
    else
        return inst:GetPosition()
    end
end

local function PvPTeleport(inst)
if inst.mobteleported == nil or inst.mobteleported == false then
	local ground = TheWorld
	local pt = getwildposition(inst)--Point(inst.Transform:GetWorldPosition())
      if pt.y <= .1 then
         pt.y = 0
         inst.Physics:Stop()
         inst.Physics:Teleport(pt.x,pt.y,pt.z)
		 inst.mobteleported = true
      end
	end
end
--==============================================
--					Loading/Saving
--==============================================
 
local function OnLoad(inst, data)
	if data ~= nil then
		inst.mobteleported = data.mobteleported or false
		inst.isshiny = data.isshiny or 0
	end
end

local function OnSave(inst, data)
	data.mobteleported = inst.mobteleported or false
	data.isshiny = inst.isshiny or 0
end

--==============================================
--					Forged Forge
--==============================================

local ex_fns = require "prefabs/player_common_extensions"

local function SetForge(inst)
	PlayablePets.SetForgeStats(inst, FORGE_STATS)
	
	inst:DoTaskInTime(0, function(inst)
	inst:AddComponent("itemtyperestrictions")
	inst.components.itemtyperestrictions:SetRestrictions({"melees", "books", "staves", "darts"})
	end)
	
	inst.components.combat:SetDamageType(1)
	
	inst:RemoveEventCallback("respawnfromcorpse", ex_fns.OnRespawnFromPlayerCorpse)
	inst:ListenForEvent("respawnfromcorpse", PlayablePets.OnRespawnFromMobCorpse)
end

--==============================================
--					Common/Master
--==============================================

local common_postinit = function(inst) 
	inst.MiniMapEntity:SetIcon(mob.minimap)

	inst:DoTaskInTime(0, function() 
   if ThePlayer then
      inst:EnableMovementPrediction(false)
	  --ThePlayer.HUD.controls.status.brain:Hide()
		if MONSTERHUNGER== "Disable" then
			--ThePlayer.HUD.controls.status.stomach:Hide()
		end
   end
end)
	----------------------------------
	--Tags--
	----------------------------------
	inst:AddTag("koalefant")
    inst:AddTag("animal")
    inst:AddTag("largecreature")
	----------------------------------
	inst:WatchWorldState( "isday", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "isdusk", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "isnight", function() PlayablePets.SetNightVision(inst)  end)
	inst:WatchWorldState( "iscaveday", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "iscavedusk", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "iscavenight", function() PlayablePets.SetNightVision(inst)  end)
	
	PlayablePets.SetNightVision(inst)	
	
	--[[
	
	inst:WatchWorldState("issummer", function() PlayablePets.SetSandstormImmunity(inst) end) --makes immune to Sandstorm visual issues.
	
	PlayablePets.SetSandstormImmunity(inst)	
	]]
end

local master_postinit = function(inst)  
	--Stats--
	PlayablePets.SetCommonStats(inst, mob, nil, true) --mob table, ishuman, ignorepvpmultiplier
	PlayablePets.SetCommonWeatherResistances(inst, 60, 20, 1) --heat, cold, wetness
	PlayablePets.SetCommonStatResistances(inst, 0.5, 1, 1) --fire, acid, poison
	----------------------------------
	--Variables			
	inst.isshiny = 0
	
	inst.mobsleep = true
	inst.taunt = true
	inst.taunt2 = true
	inst.shouldwalk = false
	
	inst.mobplayer = true
	inst.ghostbuild = "ghost_monster_build"
	
	local body_symbol = "swap_fire"
	inst.poisonsymbol = body_symbol
	MakeLargeBurnableCharacter(inst, body_symbol)
    MakeLargeFreezableCharacter(inst)
	inst.components.debuffable:SetFollowSymbol(body_symbol, 0, 0, 0)
	----------------------------------
	--Components
	inst:AddComponent("lootdropper")
	inst.components.lootdropper:SetLoot(loot_summer)
	
	inst:AddComponent("periodicspawner")
    inst.components.periodicspawner:SetPrefab("poop")
    inst.components.periodicspawner:SetRandomTimes(120,240)
    inst.components.periodicspawner:SetDensityInRange(30, 2)
    inst.components.periodicspawner:SetMinimumSpacing(8)
    inst.components.periodicspawner:Start()
	----------------------------------
	--Eater--
	inst.components.eater:SetDiet({ FOODTYPE.VEGGIE, FOODTYPE.ROUGHAGE }, { FOODTYPE.VEGGIE, FOODTYPE.ROUGHAGE })
    inst.components.eater:SetAbsorptionModifiers(1.5,1.5,1.5) --This might multiply food stats.
	inst.components.eater:SetCanEatHorrible() --check to make them eat monster meat.
	inst.components.eater.strongstomach = true -- can eat monster meat!
	---------------------------------
	--Physics and Shadows--
	MakeCharacterPhysics(inst, 100, .75)
    inst.DynamicShadow:SetSize(4.5, 2)
    inst.Transform:SetSixFaced()
    inst.Transform:SetScale(mob.scale, mob.scale, mob.scale)
	---------------------------------
	--Listeners--
	inst:ListenForEvent("equip", PlayablePets.CommonOnEquip) --Shows head when hats make heads disappear.
	inst:WatchWorldState("isday", OnIsWinter)
	--inst:ListenForEvent("attacked", OnAttacked)
	---------------------------------
	--Forge--
	if PP_FORGE_ENABLED then
		inst.forge_fn = SetForge(inst)
	end
    ------------------------------------------------------
	--Respawning and Initializing functions--
	
	inst:ListenForEvent("respawnfromghost", function(inst) PlayablePets.RevRestore(inst, mob) end) --(inst, mob, isflying, iswebimmune, noshadow, ishuman)

    inst:DoTaskInTime(0, function(inst) PlayablePets.CommonSetChar(inst, mob) OnIsWinter(inst) end)
	inst:DoTaskInTime(3, function(inst) PlayablePets.SetSkin(inst, mob) end)
    inst:ListenForEvent("respawnfromghost", function() 
        inst:DoTaskInTime(5, function(inst) PlayablePets.CommonSetChar(inst, mob) end)
		inst:DoTaskInTime(5.1, function(inst) PlayablePets.SetSkin(inst, mob) end)
		inst:DoTaskInTime(5, function(inst) PlayablePets.RevRestore(inst, mob) OnIsWinter(inst) end)
    end)
	
	inst.OnSave = OnSave
    inst.OnLoad = OnLoad
	
    return inst	
end

return MakePlayerCharacter(prefabname, prefabs, assets, common_postinit, master_postinit, start_inv)
