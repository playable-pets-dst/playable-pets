local MakePlayerCharacter = require "prefabs/player_common"

---------------------------
----------==Notes==--------
--
---------------------------

local prefabname = "clockwork1player"

local assets = 
{
	Asset("ANIM", "anim/knight.zip"),
    Asset("ANIM", "anim/knight_build.zip"),
    Asset("ANIM", "anim/knight_nightmare.zip"),
    Asset("SOUND", "sound/chess.fsb"),
	Asset("ANIM", "anim/knight_shiny_build_01.zip"),
	Asset("ANIM", "anim/knight_nightmare_shiny_build_01.zip"),
}

local prefabs = {
}

local start_inv = {
}

local start_inv2 = 
{

}

local getskins = {"1"}

if MOBHOUSE == "Enable1" or MOBHOUSE == "Enable3" then
	start_inv = start_inv2
end
-----------------------
--Stats--
local mob = 
{
	health = TUNING.CLOCKWORK1PLAYER_HEALTH,
	hunger = TUNING.CLOCKWORK1PLAYER_HUNGER,
	hungerrate = TUNING.WILSON_HUNGER_RATE, 
	sanity = TUNING.CLOCKWORK1PLAYER_SANITY,
	
	runspeed = TUNING.KNIGHT_WALK_SPEED + 2,
	walkspeed = TUNING.KNIGHT_WALK_SPEED + 2,
	
	attackperiod = 2,
	damage = TUNING.KNIGHT_DAMAGE,
	range = 3,
	hit_range = 3,
	
	bank = "knight",
	build = "knight_build",
	shiny = "knight",
	
	scale = 1,
	stategraph = "SG"..prefabname,
	minimap = prefabname..".tex",	
}

--Loot that drops when you die, duh.
SetSharedLootTable( 'clockwork1player',
{
    {'gears',             1.00},
    {'gears',             1.00},
})


local FORGE_STATS = PP_FORGE.KNIGHT
--==============================================
--					Mob Functions
--==============================================

--==============================================
--				Custom Common Functions
--==============================================

local function SetSkinDefault(inst, data)
	--Default
	if data and data.kind then
		inst.kind = data.kind
		inst.AnimState:SetBuild(data.build)
	else
		inst.kind = ""
		inst.AnimState:SetBuild(mob.build)	
	end	
end
--==============================================
--					Loading/Saving
--==============================================
 
local function OnLoad(inst, data)
	if data ~= nil then
		inst.mobteleported = data.mobteleported or false
		inst.isshiny = data.isshiny or 0
	end
end

local function OnSave(inst, data)
	data.mobteleported = inst.mobteleported or false
	data.isshiny = inst.isshiny or 0
end

--==============================================
--					Forged Forge
--==============================================

local ex_fns = require "prefabs/player_common_extensions"

local function SetForge(inst)
	PlayablePets.SetForgeStats(inst, FORGE_STATS)
	
	inst.mobsleep = false	
	
	inst:DoTaskInTime(0, function(inst)
		inst:AddComponent("itemtyperestrictions")
		inst.components.itemtyperestrictions:SetRestrictions({"melees", "darts", "armors", "books", "staves"})
	end)
	
	inst.components.health:SetAbsorptionAmount(0.9)
	
	inst.components.combat:SetDamageType(1)
	
	inst:RemoveEventCallback("respawnfromcorpse", ex_fns.OnRespawnFromPlayerCorpse)
	inst:ListenForEvent("respawnfromcorpse", PlayablePets.OnRespawnFromMobCorpse)
end

--==============================================
--					Common/Master
--==============================================

local common_postinit = function(inst) 
	inst.MiniMapEntity:SetIcon(mob.minimap)

	inst:DoTaskInTime(0, function() 
   if ThePlayer then
      inst:EnableMovementPrediction(false) --PP doesn't work with movement prediction enabled, due to use of custom stategraphs
   end
end)
	----------------------------------
	--Tags--
    inst:AddTag("monster")
    inst:AddTag("chess")
    inst:AddTag("knight")
	----------------------------------

	inst:WatchWorldState( "isday", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "isdusk", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "isnight", function() PlayablePets.SetNightVision(inst)  end)
	inst:WatchWorldState( "iscaveday", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "iscavedusk", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "iscavenight", function() PlayablePets.SetNightVision(inst)  end)
	
	PlayablePets.SetNightVision(inst)
end



local master_postinit = function(inst)
	--Stats--
	PlayablePets.SetCommonStats(inst, mob, nil, true) --mob table, ishuman, ignorepvpmultiplier
	PlayablePets.SetCommonWeatherResistances(inst, 60, 20, 1) --heat, cold, wetness
	PlayablePets.SetCommonStatResistances(inst, nil, nil, 0) --fire, acid, poison
	----------------------------------
	--Variables	
	inst.mobsleep = true
	inst.taunt = true
	--inst.taunt2 = true
	inst.kind = ""
	
	inst.getskins = getskins
	inst.components.ppskin_manager:SetSkinDefaultFn(SetSkinDefault)
	
	local body_symbol = "spring"
	inst.poisonsymbol = body_symbol
	MakeMediumBurnableCharacter(inst, body_symbol, Vector3(1,0,1))
	MakeLargeFreezableCharacter(inst)
	inst.components.debuffable:SetFollowSymbol(body_symbol, 0, 0, 0)
	PlayablePets.SetStormImmunity(inst)
	----------------------------------
	--Components
	PlayablePets.SetCommonLootdropper(inst, prefabname) --inst, prefaboverride
	----------------------------------
	--Eater--
	inst.components.eater:SetCanEatHorrible() --check to make them eat monster meat.
	inst.components.eater.strongstomach = true -- can eat monster meat!
	inst.components.eater.ignoresspoilage = true
	---------------------------------
	--Physics and Shadows--
	inst.DynamicShadow:SetSize(2, .75)
	inst.Transform:SetFourFaced()
	MakeCharacterPhysics(inst, 1, 0.5)
	---------------------------------
	--Listeners--
	inst:ListenForEvent("equip", PlayablePets.CommonOnEquip) --Shows head when hats make heads disappear.
	---------------------------------
	--Forge--
	if TheNet:GetServerGameMode() == "lavaarena" then
		inst.forge_fn = SetForge(inst)
	end
    ------------------------------------------------------
	--Respawning and Initializing functions--
	
	inst:ListenForEvent("respawnfromghost", function(inst) PlayablePets.RevRestore(inst, mob) end) --(inst, mob, isflying, iswebimmune, noshadow, ishuman)

    inst:DoTaskInTime(0, function(inst) PlayablePets.CommonSetChar(inst, mob) end)
	inst:DoTaskInTime(3, function(inst) PlayablePets.SetSkin(inst, mob) end)
    inst:ListenForEvent("respawnfromghost", function() 
        inst:DoTaskInTime(5, function(inst) PlayablePets.CommonSetChar(inst, mob) end)
		inst:DoTaskInTime(5.1, function(inst) PlayablePets.SetSkin(inst, mob) end)
		inst:DoTaskInTime(5, function(inst) PlayablePets.RevRestore(inst, mob) end)
    end)
	
	inst.OnSave = OnSave
    inst.OnLoad = OnLoad
	
    return inst	
end

return MakePlayerCharacter(prefabname, prefabs, assets, common_postinit, master_postinit, start_inv)