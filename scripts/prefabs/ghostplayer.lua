local MakePlayerCharacter = require "prefabs/player_common"

---------------------------
----------==Notes==--------
--
---------------------------

local assets = 
{
	Asset("ANIM", "anim/player_ghost_withhat.zip"),
    Asset("ANIM", "anim/ghost_build.zip"),
	Asset("ANIM", "anim/ghost_shiny_build_01.zip"),
}

local prefabs = 
{	

}

local start_inv = 
{
	--'prefab',
	--"spear",
}

local start_inv2 =
{
   --"beehome",
}

if MOBHOUSE == "Enable1" or MOBHOUSE == "Enable3" then
	start_inv = start_inv2
end

local prefabname = "ghostplayer"
-----------------------
--Stats--
local mob = 
{
	health = TUNING.GHOSTPLAYER_HEALTH,
	hunger = TUNING.GHOSTPLAYER_HUNGER,
	hungerrate = 0, 
	sanity = TUNING.GHOSTPLAYER_SANITY,
	
	runspeed = 6,
	walkspeed = 3,
	
	attackperiod = 0,
	damage = 10 * 2,
	range = 3,
	hit_range = 3,
	
	bank = "ghost",
	build = "ghost_build",
	--build2 = "bee_guard_puffy_build",
	shiny = "ghost",
	
	scale = 1,
	stategraph = "SG"..prefabname,
	minimap = prefabname..".tex",	
}

--Loot that drops when you die, duh.
SetSharedLootTable( prefabname,
{

})


--==============================================
--					Mob Functions
--==============================================


--==============================================
--				Custom Common Functions
--==============================================
local function updatedamage(inst, phase)
    if phase == "day" then
        inst.components.combat.defaultdamage = 10 * 2 
    elseif phase == "night" then
        inst.components.combat.defaultdamage = 20 * 2
    elseif phase == "dusk" then
        inst.components.combat.defaultdamage = 15 * 2
    end
end

local function OnHitOther(inst, other, damage)
    if inst.components.health and not inst.components.health:IsDead() then
		inst.components.health:DoDelta(TheNet:GetServerGameMode() == "lavaarena" and 0.33 or (TheWorld.state.isday and 3 or 6), false)
	end
end

local function GetExcludeTags(inst)
	if TheNet:GetPVPEnabled() then
		return {"INLIMBO", "playerghost", "notarget", "shadow", "ghost"}
	elseif TheNet:GetServerGameMode() == "lavaarena" then
		return {"player", "companion", "playerghost", "INLIMBO", "notarget", "ghost"}
	else	
		return {"player", "companion", "INLIMBO", "playerghost", "notarget", "shadow", "ghost"}
	end
end
--==============================================
--					Loading/Saving
--==============================================
 
local function OnLoad(inst, data)
	if data ~= nil then
		inst.mobteleported = data.mobteleported or false
		inst.isshiny = data.isshiny or 0
	end
end

local function OnSave(inst, data)
	data.mobteleported = inst.mobteleported or false
	data.isshiny = inst.isshiny or 0
end

--==============================================
--					Forged Forge
--==============================================


local ex_fns = require "prefabs/player_common_extensions"

local function SetForge(inst)
	PlayablePets.SetForgeStats(inst, PP_FORGE.GHOST)
	
	inst:RemoveTag("noplayerindicator")
	
	inst.specialsleep = nil
	inst.taunt2 = false
	inst.healmult = 0.2
	inst.ismad = true
	--inst.acidmult = 1.25
	
	inst:DoTaskInTime(0, function(inst)
	inst:AddComponent("itemtyperestrictions")
	inst.components.itemtyperestrictions:SetRestrictions({})
	end)
	
	--because FF forces character physics on us on spawn
	inst:DoTaskInTime(3, function(inst)
		inst.Physics:SetCollisionGroup(COLLISION.FLYERS)
		inst.Physics:CollidesWith(COLLISION.FLYERS)	
	end)
	
	inst.revive_delay = 1
	
	--inst:ListenForEvent("attacked", OnAttacked_Forge) --Shows head when hats make heads disappear.
	inst.components.health:StopRegen()
	--inst.components.combat.onhitotherfn = OnForgeHitOther
	
	inst.components.combat:SetDamageType(2)
	
	inst:AddComponent("corpsereviver")
	inst.components.corpsereviver:SetReviverSpeedMult(0.1)
	inst.components.corpsereviver:SetAdditionalReviveHealthPercent(0.15)
	
	inst.components.revivablecorpse.revivespeedmult = 999
	
	inst:RemoveEventCallback("respawnfromcorpse", ex_fns.OnRespawnFromPlayerCorpse)
	inst:ListenForEvent("respawnfromcorpse", PlayablePets.OnRespawnFromMobCorpse)
end

--==============================================
--					Common/Master
--==============================================

local common_postinit = function(inst) 
	inst.MiniMapEntity:SetIcon(mob.minimap)

	inst:DoTaskInTime(0, function() 
   if ThePlayer then
      inst:EnableMovementPrediction(false) --PP doesn't work with movement prediction enabled, due to use of custom stategraphs
   end
end)

	inst.AnimState:SetBloomEffectHandle("shaders/anim_bloom_ghost.ksh")
    inst.AnimState:SetLightOverride(TUNING.GHOST_LIGHT_OVERRIDE)

	inst:WatchWorldState( "isday", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "isdusk", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "isnight", function() PlayablePets.SetNightVision(inst)  end)
	inst:WatchWorldState( "iscaveday", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "iscavedusk", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "iscavenight", function() PlayablePets.SetNightVision(inst)  end)
	
	PlayablePets.SetNightVision(inst)
	
end



local master_postinit = function(inst)
	--Stats--
	PlayablePets.SetCommonStats(inst, mob, nil, true) --mob table, ishuman, ignorepvpmultiplier
	inst.components.health:StartRegen(2, 3)
	PlayablePets.SetCommonWeatherResistances(inst, 60, 20, 1) --heat, cold, wetness
	PlayablePets.SetCommonStatResistances(inst, 0, 0, 0) --fire, acid, poison
	----------------------------------
	--Tags--
	inst:AddTag("flying")
	inst:AddTag("noplayerindicator")
	inst:AddTag("ghost")
	
	inst.AnimState:Hide("HAT")
    inst.AnimState:Hide("HatFX")
	----------------------------------
	--Variables	
	--inst.mobsleep = true
	inst.specialsleep = true
	inst.taunt2 = true
	inst.debuffimmune = true
	
	inst.ismad = false
	PlayablePets.SetStormImmunity(inst)
	----------------------------------
	--Components
	PlayablePets.SetCommonLootdropper(inst, prefabname) --prefaboverride
	
	inst:AddComponent("aura")
    inst.components.aura.radius = 3
    inst.components.aura.tickperiod = 1.4
	inst.components.aura:Enable(false)
	inst.components.aura.auraexcludetags = GetExcludeTags(inst)
	
	local light = inst.entity:AddLight()
  	inst.Light:Enable(true)
  	inst.Light:SetRadius(0.5)
  	inst.Light:SetFalloff(0.6)
  	inst.Light:SetIntensity(0.6)
  	inst.Light:SetColour(180/255,195/255,255/255)
	
	inst.SoundEmitter:PlaySound("dontstarve/ghost/ghost_howl_LP", "howl")
	
	inst.AnimState:SetBloomEffectHandle("shaders/anim_bloom_ghost.ksh")
	----------------------------------
	--Eater--

    inst.components.eater:SetAbsorptionModifiers(1,1,1) --This multiplies food stats.
	inst.components.eater:SetCanEatHorrible() --check to make them eat monster meat.
	inst.components.eater.strongstomach = true -- can eat monster meat!
	inst.components.eater.ignoresspoilage = true -- Can eat spoiled food without consequence. Might keep food stats as if fresh.
	---------------------------------
	--Physics and Shadows--
	MakeGhostPhysics(inst, .5, .5)
	inst.Transform:SetTwoFaced()
	
	inst.DynamicShadow:SetSize(0, 0)
	--inst.DynamicShadow:Enable(false)
    
	inst.Physics:SetCollisionGroup(COLLISION.WORLD)
	inst.Physics:CollidesWith(COLLISION.WORLD)

	PlayablePets.SetAmphibious(inst, nil, nil, true)
	---------------------------------
	--Listeners--
	inst:ListenForEvent("equip", PlayablePets.CommonOnEquip) --Shows head when hats make heads disappear.
	inst.components.combat.onhitotherfn = OnHitOther
	inst:WatchWorldState("phase", updatedamage)
	inst:ListenForEvent("ondeath", function(inst) 
		if TheNet:GetServerGameMode() ~= "lavaarena" then
			inst.ismad = false
		end
		inst.components.aura:Enable(false)
	end)

    updatedamage(inst, TheWorld.state.phase)
	---------------------------------
	--Functions

    
	---------------------------------
	--Forge--
	if TheNet:GetServerGameMode() == "lavaarena" then
		inst.forge_fn = SetForge(inst)
	end
    ------------------------------------------------------
	--Respawning and Initializing functions--
	
	inst:DoTaskInTime(0, function(inst) PlayablePets.CommonSetChar(inst, mob, true, true, true) end) --(inst, mob, isflying, iswebimmune, noshadow, ishuman)
	inst:DoTaskInTime(3, function(inst) PlayablePets.SetSkin(inst, mob) end)
    inst:ListenForEvent("respawnfromghost", function() 
		if inst.ismad then
			inst.components.aura:Enable(true)
		end
        inst:DoTaskInTime(5, function(inst) PlayablePets.CommonSetChar(inst, mob, true, true, true) end)
		inst:DoTaskInTime(5.1, function(inst) PlayablePets.SetSkin(inst, mob) end)
		inst:DoTaskInTime(5, function(inst) PlayablePets.RevRestore(inst, mob, true, true, true, nil, true) end) --(inst, mob, isflying, iswebimmune, noshadow, ishuman, amphibious)
    end)
	
	inst.OnSave = OnSave
    inst.OnLoad = OnLoad
	
    return inst	
end

return MakePlayerCharacter(prefabname, prefabs, assets, common_postinit, master_postinit, start_inv)