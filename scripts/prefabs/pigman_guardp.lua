local MakePlayerCharacter = require "prefabs/player_common"

---------------------------


---------------------------


local assets = 
{
	Asset("ANIM", "anim/pig_build.zip"),
    Asset("ANIM", "anim/pig_guard_build.zip"),
    Asset("ANIM", "anim/ds_pig_boat_jump.zip"),
    Asset("ANIM", "anim/ds_pig_basic.zip"),
    Asset("ANIM", "anim/ds_pig_actions.zip"),
    Asset("ANIM", "anim/ds_pig_attacks.zip"),
    Asset("SOUND", "sound/merm.fsb"),
	Asset("ANIM", "anim/pig_shiny_build_01.zip"),
}

local getskins = {"1"}

local prefabs = 
{	

}
	
--Note: Becareful when adding starting items.
--People are likely to spam changecharacter to get more of them.	
local start_inv = 
{
	--"prefab",
}

local start_inv2 = 
{
	--"prefabhome",
}

if MOBHOUSE == "Enable1" or MOBHOUSE == "Enable3" then
	start_inv = start_inv2
end
-----------------------
local mob = 
{
	health = TUNING.PIGMAN_GUARDP_HEALTH,
	hunger = TUNING.PIGMAN_GUARDP_HUNGER,
	hungerrate = TUNING.WILSON_HUNGER_RATE,
	sanity = TUNING.PIGMAN_GUARDP_SANITY,
	runspeed = 6,
	walkspeed = TUNING.PIG_WALK_SPEED,
	damage = 35,
	attackperiod = 0,
	range = 2,
	hitrange = 3,
	bank = "pigman",
	shiny = "pigman_guard",
	build = "pig_guard_build",
	scale = 1,
	--build2 = "alternate build here",
	stategraph = "SGpig_commonp",
	minimap = "pigman_guardp.tex",
	
}

local werepig = 
{
	health = 750,
	hunger = 150,
	hungerrate = TUNING.WILSON_HUNGER_RATE,
	sanity = 150,
	runspeed = 9,
	walkspeed = 3,
	damage = 100*2,
	attackperiod = 0,
	range = 2,
	hitrange = 3,
	bank = "pigman",
	shiny = "pig",
	build = "werepig_build",
	scale = 1,
	--build2 = "alternate build here",
	stategraph = "SGwerepigplayer",
	minimap = "pigmanplayer.tex",
	
}
-----------------------
--Loot that drops when you die, duh.
SetSharedLootTable('pigmanp',
-----Prefab---------------------Chance------------
{
    {'meat',		1.00},
	{'pigskin',		1.00},
})

local sounds = {
	grunt = "dontstarve/pig/grunt",
    attack = "dontstarve/pig/attack",
    hit = "dontstarve/pig/hurt",
    death = "dontstarve/pig/death",
    talk = "dontstarve/pig/grunt",
} 

local function SetNormalPig(inst)
    inst:RemoveTag("werepig")
    inst:AddTag("guard")

    inst:SetStateGraph(mob.stategraph)
	inst.sg:GoToState("transformNormal")
	--inst.AnimState:SetBuild("pig_build")
	
	inst.components.sanity:DoDelta(-75, false)
    inst.components.combat:SetDefaultDamage(TheNet:GetServerGameMode() == "lavaarena" and PP_FORGE.PIG.DAMAGE or mob.damage)
	inst.components.combat.pvp_damagemod = 1
    inst.components.locomotor.runspeed = mob.runspeed
    inst.components.locomotor.walkspeed = mob.walkspeed

    PlayablePets.SetCommonWeatherResistances(inst)
	
	inst.curselvl = 0
	
	local hper = inst.components.health:GetPercent()
    inst.components.health:SetMaxHealth(TheNet:GetServerGameMode() == "lavaarena" and PP_FORGE.PIG.HEALTH or mob.health)
	inst.components.health:SetPercent(hper)
    inst.components.talker:StopIgnoringAll("becamewerepig")
	inst:StopWatchingWorldState("startday")
end

local CURSE_TIMER = 45

local function SetWerePig(inst, force)
	if (TheWorld.state.isnight and TheWorld.state.isfullmoon) or force then
	
    --inst:RemoveTag("guard")
		if inst.cursetask then
			inst.cursetask:Cancel()
			inst.cursetask = nil
		end
	
		if not inst:HasTag("werepig") then
			inst:AddTag("werepig")
			inst:RemoveTag("guard")
	
			inst:SetStateGraph(werepig.stategraph)
			inst.sg:GoToState("transformWere")
			if inst.components.inventory ~= nil then
				inst.components.inventory:DropEverything(true)
			end

			inst.components.combat:SetDefaultDamage(TheNet:GetServerGameMode() == "lavaarena" and PP_FORGE.WEREPIG.DAMAGE or werepig.damage)
			inst.components.combat.pvp_damagemod = 0.5
			inst.components.locomotor.runspeed = werepig.runspeed
			inst.components.locomotor.walkspeed = werepig.walkspeed
			inst.components.talker:IgnoreAll("becamewerepig")
	
			PlayablePets.SetCommonWeatherResistances(inst, 60, 20) --heat, cold, wetness
			inst.components.temperature:SetTemp(30)
		end
		if force then
			inst.cursetask = inst:DoTaskInTime(TheNet:GetServerGameMode() == "lavaarena" and PP_FORGE.PIG.CURSE_TIMER or CURSE_TIMER, function(inst) 
				if not TheWorld.state.isnight and not TheWorld.state.isfullmoon then 
					SetNormalPig(inst) 
				end 
			end)
		else
			inst.components.health:SetMaxHealth(werepig.health)
			inst:WatchWorldState("startday", SetNormalPig)
		end
	end
end

local function ontalk(inst, script)
    inst.SoundEmitter:PlaySound(inst.sounds.talk)
end

local function Equip(inst)
    local head = inst.components.inventory:GetEquippedItem(EQUIPSLOTS.HEAD)
	local hands = inst.components.inventory:GetEquippedItem(EQUIPSLOTS.HANDS)
	if head then
		inst.AnimState:Show("HEAD")
	end 
	if hands then
		inst.AnimState:Show("swap_object")
	end 	
end

local function Unequip(inst)
	local hands = inst.components.inventory:GetEquippedItem(EQUIPSLOTS.HANDS)
	if not hands then
		inst.AnimState:Hide("swap_object")
	end 
end 
 
local function OnLoad(inst, data)
	if data ~= nil then
		inst.mobteleported = data.mobteleported or false
		inst.isshiny = data.isshiny or 0
	end
end

local function OnSave(inst, data)
	data.mobteleported = inst.mobteleported or false
	data.isshiny = inst.isshiny or 0
end

local ex_fns = require "prefabs/player_common_extensions"

local function OnEat(inst, food)
	if food and food:HasTag("monstermeat") then
		inst.curselvl = inst.curselvl + 1
		--inst.components.sanity:DoDelta(-10, false)
		if inst.curselvl >= 4 then
			SetWerePig(inst, true)
		end	
	end
end

local function SetForge(inst)
	PlayablePets.SetForgeStats(inst, PP_FORGE.PIG)
	
	inst.mobsleep = false
	--inst.acidmult = 1.25
	inst.components.combat:AddDamageBuff("atk_banner", 1.20, false)
	
	inst:DoTaskInTime(0, function(inst)
	inst:AddComponent("itemtyperestrictions")
	inst.components.itemtyperestrictions:SetRestrictions({"books"}, {"staves"}, {"darts"})
	end)
	
	inst.components.revivablecorpse.revivespeedmult = 1
	
	--Werepig forge functions
	inst:ListenForEvent("healthdelta", function(inst)
		local health = inst.components.health:GetPercent()
		if health >= 0.5 then
			inst.becamewerewolf = false
		elseif health <= 0.2 and (not inst.becamewerewolf or inst.becamewerewolf == false) then
			SetWerePig(inst, true)
		end	
	end)
	
	inst:RemoveEventCallback("respawnfromcorpse", ex_fns.OnRespawnFromPlayerCorpse)
	inst:ListenForEvent("respawnfromcorpse", PlayablePets.OnRespawnFromMobCorpse)
end

local common_postinit = function(inst) 
	--inst.soundsname = "beefalo"

	inst.MiniMapEntity:SetIcon(mob.minimap)
	--inst.AnimState:SetMultColour(1, 1, 1, 0.5)

	inst:DoTaskInTime(0, function() 
   if ThePlayer then
      inst:EnableMovementPrediction(false)
   end
end)
	
end

local master_postinit = function(inst) 
	------------------------------------------
	--Stats--
    PlayablePets.SetCommonStats(inst, mob, true, true) --mob table, ishuman, ignorepvpmultiplier
	PlayablePets.SetCommonWeatherResistances(inst) --heat, cold, wetness
	PlayablePets.SetCommonStatResistances(inst) --fire, acid, poison, freeze (flat value, not a multiplier)
	----------------------------------
	--Loot drops--
	inst:AddComponent("lootdropper")
    inst.components.lootdropper:SetChanceLootTable('pigmanp')
	----------------------------------
	--Tags--
	
	inst:AddTag("pig")
	inst:AddTag("guard")
	inst.taunt = true
	inst.taunt2 = true
	inst.mobsleep = true
	
	inst.isshiny = 0
	inst.curselvl = 0
	inst.getskins = getskins
	inst.sounds = sounds
	
	local body_symbol = "pig_torso"
	inst.poisonsymbol = body_symbol
	MakeMediumBurnableCharacter(inst, body_symbol)
    MakeMediumFreezableCharacter(inst, body_symbol)
	inst.components.debuffable:SetFollowSymbol(body_symbol, 0, 0, 0)
	
	PlayablePets.SetSkeleton(inst, "pp_skeleton_pig")
	----------------------------------
	--PvP Mode Stuff--
	inst.mobteleported = false
	----------------------------------
	--Eater--
    inst.components.eater:SetAbsorptionModifiers(1,1,1.5) --This might multiply food stats.
	inst.components.eater:SetOnEatFn(OnEat)
	---------------------------------
	--Physics and Scale--
	
	MakeCharacterPhysics(inst, 50, .5)
    inst.Transform:SetScale(mob.scale, mob.scale, mob.scale)
    inst.DynamicShadow:SetSize(1.5, .75)
    inst.Transform:SetFourFaced()
	---------------------------------
	--Listeners--
	inst:ListenForEvent("equip", PlayablePets.CommonOnEquipPig) --Shows head when hats make heads disappear.
	inst:ListenForEvent("unequip", PlayablePets.CommonOnUnequip)
	inst:WatchWorldState("isfullmoon", function(inst) SetWerePig(inst) end)
	------------------------------------------------------
	--Forge--
	if TheNet:GetServerGameMode() == "lavaarena" then
		inst.forge_fn = SetForge(inst)
	end
	------------------------------------------------------
	--Respawning and Initializing functions--
	inst.components.talker.ontalk = ontalk
	inst:DoTaskInTime(2, SetWerePig)
	
	inst:ListenForEvent("respawnfromghost", function(inst) PlayablePets.RevRestore(inst, mob, nil, nil, nil, true) end) --(inst, mob, isflying, iswebimmune, noshadow, ishuman)

    inst:DoTaskInTime(0, function(inst) PlayablePets.CommonSetChar(inst, mob) end)
	inst:DoTaskInTime(3, function(inst) PlayablePets.SetSkin(inst, mob) end)
    inst:ListenForEvent("respawnfromghost", function() 
        inst:DoTaskInTime(5, function(inst) PlayablePets.CommonSetChar(inst, mob) end)
		inst:DoTaskInTime(5.1, function(inst) PlayablePets.SetSkin(inst, mob) end)
		inst:DoTaskInTime(5, function(inst) PlayablePets.RevRestore(inst, mob, nil, nil, nil, true) end)
    end)
	
	inst.OnSave = OnSave
    inst.OnLoad = OnLoad
	
    return inst	
end

return MakePlayerCharacter("pigman_guardp", prefabs, assets, common_postinit, master_postinit, start_inv)
