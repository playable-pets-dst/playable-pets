local MakePlayerCharacter = require "prefabs/player_common"

---------------------------
----------==Notes==--------
--
---------------------------
local prefabname = "birdp"

local assets = 
{
	Asset("ANIM", "anim/robin_shiny_build_01.zip"),
	Asset("ANIM", "anim/robin_winter_shiny_build_01.zip"),
}

local getskins = {"1"}

local prefabs = 
{	

}
	
--Note: Becareful when adding starting items.
--People are likely to spam changecharacter to get more of them.	
local start_inv = 
{
	--'prefab',
	--"spear",
}

local start_inv2 =
{
   --"beehome",
}

if MOBHOUSE== "Enable1" or MOBHOUSE== "Enable3" then
	start_inv = start_inv2
end
-----------------------
--Stats--
local mob = 
{
	
	health = 50,
	hunger = 100,
	hungerrate = TUNING.WILSON_HUNGER_RATE, --I think .15 is defualt.
	sanity = 100,
	runspeed = 6,
	walkspeed = 6,
	damage = 1,
	range = 1.5,
	hit_range = 2,
	attackperiod = 0,
	bank = "crow",
	build = "robin_build",
	build2 = "robin_winter_build",
	scale = 1,	
	stategraph = "SGbirdp",
	minimap = "birdp.tex",
	
}
-----------------------
--Loot that drops when you die, duh.
SetSharedLootTable('birdp',
-----Prefab---------------------Chance------------
{
    {'smallmeat',			1.00}, --we want the "mandrake" corpse. So we have him spawn at the exact location of death instead of dropping as loot. 
	--{'feather_crow',        0.50},
})

FORGE_STATS = PP_FORGE[string.upper(prefabname)] 
--==============================================
--					Mob Functions
--==============================================
local sounds =
{
    takeoff = "dontstarve/birds/takeoff_robin",
    chirp = "dontstarve/birds/chirp_robin",
    flyin = "dontstarve/birds/flyin",
}

local sounds_winter =
{
    takeoff = "dontstarve/birds/takeoff_junco",
    chirp = "dontstarve/birds/chirp_junco",
    flyin = "dontstarve/birds/flyin",
}
--==============================================
--				Custom Common Functions
--==============================================	

local function OnIsWinter(inst, iswinter)
    if TheWorld.state.iswinter then
        if inst.isshiny ~= 0 then
			inst.AnimState:SetBuild("robin_winter_shiny_build_0"..inst.isshiny)
		else
			inst.AnimState:SetBuild(mob.build2)
		end
		inst.sounds = sounds_winter
		inst.soundsname = "junco"
    else
		if inst.isshiny ~= 0 then
			inst.AnimState:SetBuild("robin_shiny_build_0"..inst.isshiny)
		else
			inst.AnimState:SetBuild(mob.build)
		end
		inst.sounds = sounds
		inst.soundsname = "robin"
    end
end

local function SetSkinDefault(inst, num)
	if num ~= 0 then
		inst.AnimState:SetBuild(TheWorld.state.iswinter and "robin_winter_shiny_build_0"..num or "robin_shiny_build_0"..num)
	else
		inst.AnimState:SetBuild(TheWorld.state.iswinter and mob.build2 or mob.build)
	end
end

----PvP Teleport----

local function getwildposition(inst)
    local ground = TheWorld
    local centers = {}
    for i, node in ipairs(ground.topology.nodes) do
        if ground.Map:IsPassableAtPoint(node.x, 0, node.y) then
            table.insert(centers, {x = node.x, z = node.y})
        end
    end
    if #centers > 0 then
        local pos = centers[math.random(#centers)]
        return Point(pos.x, 0, pos.z)
    else
        return inst:GetPosition()
    end
end

local function PvPTeleport(inst)
if inst.mobteleported == nil or inst.mobteleported == false then
	local ground = TheWorld
	local pt = getwildposition(inst)--Point(inst.Transform:GetWorldPosition())
      if pt.y <= .1 then
         pt.y = 0
         inst.Physics:Stop()
         inst.Physics:Teleport(pt.x,pt.y,pt.z)
		 inst.mobteleported = true
      end
	end
end
--==============================================
--					Loading/Saving
--==============================================
 
local function OnLoad(inst, data)
	if data ~= nil then
		inst.mobteleported = data.mobteleported or false
		inst.isshiny = data.isshiny or 0
	end
end

local function OnSave(inst, data)
	data.mobteleported = inst.mobteleported or false
	data.isshiny = inst.isshiny or 0
end

--==============================================
--					Forged Forge
--==============================================

local ex_fns = require "prefabs/player_common_extensions"

local function SetForge(inst)
	PlayablePets.SetForgeStats(inst, FORGE_STATS)
	
	inst:DoTaskInTime(0, function(inst)
	inst:AddComponent("itemtyperestrictions")
	inst.components.itemtyperestrictions:SetRestrictions({"melees", "books", "staves", "darts"})
	end)
	
	inst.components.combat:SetDamageType(1)
	
	inst:RemoveEventCallback("respawnfromcorpse", ex_fns.OnRespawnFromPlayerCorpse)
	inst:ListenForEvent("respawnfromcorpse", PlayablePets.OnRespawnFromMobCorpse)
end

--==============================================
--					Common/Master
--==============================================

local common_postinit = function(inst) 
	inst.MiniMapEntity:SetIcon(mob.minimap)

	inst:DoTaskInTime(0, function() 
   if ThePlayer then
      inst:EnableMovementPrediction(false)
	  --ThePlayer.HUD.controls.status.brain:Hide()
		if MONSTERHUNGER== "Disable" then
			--ThePlayer.HUD.controls.status.stomach:Hide()
		end
   end
end)
	----------------------------------
	--Tags--
	----------------------------------
	inst:AddTag("bird")
    inst:AddTag("robin")
    inst:AddTag("smallcreature")	
	----------------------------------
	
	--[[
	
	inst:WatchWorldState("issummer", function() PlayablePets.SetSandstormImmunity(inst) end) --makes immune to Sandstorm visual issues.
	
	PlayablePets.SetSandstormImmunity(inst)	
	]]
end

local master_postinit = function(inst)  
	--Stats--
	inst.sounds = sounds
	inst.soundsname = "robin"
	PlayablePets.SetCommonStats(inst, mob, nil, true) --mob table, ishuman, ignorepvpmultiplier
	PlayablePets.SetCommonWeatherResistances(inst, 60, 20, 1) --heat, cold, wetness
	PlayablePets.SetCommonStatResistances(inst, 1, 1, 1) --fire, acid, poison
	----------------------------------
	--Variables		
	
	inst.isshiny = 0
	inst.getskins = getskins
	inst.setskin_defaultfn = SetSkinDefault
	
	inst.mobsleep = true
	inst.taunt = true
	inst.taunt2 = true
	inst._isflying = false
	
	inst.mobplayer = true
	inst.ghostbuild = "ghost_monster_build"
	
	local body_symbol = "swap_fire"
	inst.poisonsymbol = body_symbol
	MakeLargeBurnableCharacter(inst, body_symbol)
    MakeLargeFreezableCharacter(inst)
	inst.components.debuffable:SetFollowSymbol(body_symbol, 0, 0, 0)
	----------------------------------
	--Components
	PlayablePets.SetCommonLootdropper(inst, prefabname) --inst, prefaboverride
	----------------------------------
	--Eater--
	inst.components.eater:SetAbsorptionModifiers(1,1,1) --This might multiply food stats.
	---------------------------------
	--Physics and Shadows--
	MakeCharacterPhysics(inst, 1, .5)
    inst.DynamicShadow:SetSize(1, .75)
    inst.Transform:SetTwoFaced()
    inst.Transform:SetScale(mob.scale, mob.scale, mob.scale)
	---------------------------------
	--Listeners--
	inst:ListenForEvent("equip", PlayablePets.CommonOnEquip) --Shows head when hats make heads disappear.
	--inst:ListenForEvent("attacked", OnAttacked)
	inst:WatchWorldState("iswinter", OnIsWinter) 
	inst:WatchWorldState("isspring", OnIsWinter) --ensures winter build is removed
	---------------------------------
	--Forge--
	if PP_FORGE_ENABLED then
		inst.forge_fn = SetForge(inst)
	end
    ------------------------------------------------------
	--Respawning and Initializing functions--
	
	inst:ListenForEvent("respawnfromghost", function(inst) PlayablePets.RevRestore(inst, mob) end) --(inst, mob, isflying, iswebimmune, noshadow, ishuman)

    inst:DoTaskInTime(0, function(inst) PlayablePets.CommonSetChar(inst, mob)  end)
	inst:DoTaskInTime(3, function(inst) PlayablePets.SetSkin(inst, mob) OnIsWinter(inst) inst.components.locomotor:SetAllowPlatformHopping(false) end)
    inst:ListenForEvent("respawnfromghost", function() 
        inst:DoTaskInTime(5, function(inst) PlayablePets.CommonSetChar(inst, mob) end)
		inst:DoTaskInTime(5.1, function(inst) PlayablePets.SetSkin(inst, mob) end)
		inst:DoTaskInTime(5, function(inst) PlayablePets.RevRestore(inst, mob) OnIsWinter(inst) inst.components.locomotor:SetAllowPlatformHopping(false) end)
    end)
	
	inst.OnSave = OnSave
    inst.OnLoad = OnLoad
	
    return inst	
end

return MakePlayerCharacter(prefabname, prefabs, assets, common_postinit, master_postinit, start_inv)
