local MakePlayerCharacter = require "prefabs/player_common"

---------------------------
----------==Notes==--------
--
---------------------------

local prefabname = "clockwork2player"

local assets = 
{
	Asset("ANIM", "anim/bishop.zip"),
    Asset("ANIM", "anim/bishop_build.zip"),
    Asset("ANIM", "anim/bishop_nightmare.zip"),
    Asset("SOUND", "sound/chess.fsb"),
	Asset("ANIM", "anim/bishop_shiny_build_01.zip"),
	Asset("ANIM", "anim/bishop_nightmare_shiny_build_01.zip"),
}

local prefabs = {
}

local start_inv = {
}

local start_inv2 = 
{

}

local getskins = {"1"}

if MOBHOUSE == "Enable1" or MOBHOUSE == "Enable3" then
	start_inv = start_inv2
end
-----------------------
--Stats--
local mob = 
{
	health = TUNING.CLOCKWORK2PLAYER_HEALTH,
	hunger = TUNING.CLOCKWORK2PLAYER_HUNGER,
	hungerrate = TUNING.WILSON_HUNGER_RATE, 
	sanity = TUNING.CLOCKWORK2PLAYER_SANITY,
	
	runspeed = TUNING.BISHOP_WALK_SPEED + 1,
	walkspeed = TUNING.BISHOP_WALK_SPEED + 1,
	
	attackperiod = 4,
	damage = TUNING.BISHOP_DAMAGE,
	range = 15,
	hit_range = 30,
	
	bank = "bishop",
	build = "bishop_build",
	shiny = "bishop",
	
	scale = 1,
	stategraph = "SG"..prefabname,
	minimap = prefabname..".tex",	
}

--Loot that drops when you die, duh.
SetSharedLootTable( 'clockwork2player',
{
    {'gears',             1.00},
    {'gears',             1.00},
	{'purplegem',  		  1.0},
})


local FORGE_STATS = PP_FORGE.BISHOP
--==============================================
--					Mob Functions
--==============================================

--==============================================
--				Custom Common Functions
--==============================================

local function FireProjectile(inst, target)
    if target then
		local pos = inst:GetPosition()
		local proj = SpawnPrefab("bishop_charge")
		proj.Transform:SetPosition(pos.x, 0, pos.z)
		inst:DoTaskInTime(0, function(inst) proj.components.projectile.owner = inst end)
		proj.components.projectile.owner = inst
		proj.components.projectile:Throw(inst, target)
    end
end

local function SetSkinDefault(inst, data)
	--Default
	if data then
		--print("DEBUG:SetSkinDefault shinizer ran")
		if inst.kind ~= nil and inst.kind == "" then
			inst.AnimState:SetBuild(data.build)
		else	
			inst.AnimState:SetBuild(data.build2)			
		end
	else
		if inst.kind ~= nil and inst.kind == "" then
			inst.AnimState:SetBuild(mob.build)
		else	
			inst.AnimState:SetBuild("bishop_nightmare")		
		end		
	end	
end

local function SetSkinDefault(inst, data)
	--Default
	if data and data.kind then
		inst.kind = data.kind
		inst.AnimState:SetBuild(data.build)
	else
		inst.kind = ""
		inst.AnimState:SetBuild(mob.build)	
	end	
end
--==============================================
--					Loading/Saving
--==============================================
 
local function OnLoad(inst, data)
	if data ~= nil then
		inst.mobteleported = data.mobteleported or false
		inst.isshiny = data.isshiny or 0
	end
end

local function OnSave(inst, data)
	data.mobteleported = inst.mobteleported or false
	data.isshiny = inst.isshiny or 0
end

--==============================================
--					Forged Forge
--==============================================

local ex_fns = require "prefabs/player_common_extensions"

local function SetForge(inst)
	PlayablePets.SetForgeStats(inst, FORGE_STATS)
	
	inst.mobsleep = false	
	
	inst:DoTaskInTime(0, function(inst)
		inst:AddComponent("itemtyperestrictions")
		inst.components.itemtyperestrictions:SetRestrictions({"melees", "darts", "armors", "books", "staves"})
	end)
	
	inst.components.health:SetAbsorptionAmount(0.8)
	
	inst.components.combat:SetDamageType(1)
	
	inst:RemoveEventCallback("respawnfromcorpse", ex_fns.OnRespawnFromPlayerCorpse)
	inst:ListenForEvent("respawnfromcorpse", PlayablePets.OnRespawnFromMobCorpse)
end

--==============================================
--					Common/Master
--==============================================

local common_postinit = function(inst) 
	inst.MiniMapEntity:SetIcon(mob.minimap)

	inst:DoTaskInTime(0, function() 
   if ThePlayer then
      inst:EnableMovementPrediction(false) --PP doesn't work with movement prediction enabled, due to use of custom stategraphs
   end
end)
	----------------------------------
	--Tags--
    inst:AddTag("monster")
    inst:AddTag("chess")
    inst:AddTag("bishop")
	----------------------------------

	inst:WatchWorldState( "isday", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "isdusk", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "isnight", function() PlayablePets.SetNightVision(inst)  end)
	inst:WatchWorldState( "iscaveday", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "iscavedusk", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "iscavenight", function() PlayablePets.SetNightVision(inst)  end)
	
	PlayablePets.SetNightVision(inst)
end



local master_postinit = function(inst)
	--Stats--
	PlayablePets.SetCommonStats(inst, mob, nil, true) --mob table, ishuman, ignorepvpmultiplier
	PlayablePets.SetCommonWeatherResistances(inst, 60, 20, 1) --heat, cold, wetness
	PlayablePets.SetCommonStatResistances(inst, nil, nil, 0) --fire, acid, poison
	----------------------------------
	--Variables	
	inst.mobsleep = true
	inst.taunt = true
	inst._ismonster = true
	--inst.taunt2 = true
	inst.kind = ""
    inst.soundpath = "dontstarve/creatures/bishop/"
    inst.effortsound = "dontstarve/creatures/bishop/idle"
	
	inst.getskins = getskins
	inst.components.ppskin_manager:SetSkinDefaultFn(SetSkinDefault)
	inst.FireProjectile = FireProjectile
	
	local body_symbol = "waist"
	inst.poisonsymbol = body_symbol
	MakeMediumBurnableCharacter(inst, body_symbol, Vector3(1,0,1))
	MakeLargeFreezableCharacter(inst)
	inst.components.debuffable:SetFollowSymbol(body_symbol, 0, 0, 0)
	PlayablePets.SetStormImmunity(inst)
	----------------------------------
	--Components
	PlayablePets.SetCommonLootdropper(inst, prefabname) --inst, prefaboverride
	----------------------------------
	--Eater--
	inst.components.eater:SetCanEatHorrible() --check to make them eat monster meat.
	inst.components.eater.strongstomach = true -- can eat monster meat!
	inst.components.eater.ignoresspoilage = true
	---------------------------------
	--Physics and Shadows--
	MakeCharacterPhysics(inst, 50, .5)
    inst.DynamicShadow:SetSize(1.5, .75)
	inst.Transform:SetFourFaced()
	---------------------------------
	--Listeners--
	inst:ListenForEvent("equip", PlayablePets.CommonOnEquip) --Shows head when hats make heads disappear.
	---------------------------------
	--Forge--
	if TheNet:GetServerGameMode() == "lavaarena" then
		inst.forge_fn = SetForge(inst)
	end
    ------------------------------------------------------
	--Respawning and Initializing functions--
	
	inst:ListenForEvent("respawnfromghost", function(inst) PlayablePets.RevRestore(inst, mob) end) --(inst, mob, isflying, iswebimmune, noshadow, ishuman)

    inst:DoTaskInTime(0, function(inst) PlayablePets.CommonSetChar(inst, mob) end)
	inst:DoTaskInTime(3, function(inst) PlayablePets.SetSkin(inst, mob) end)
    inst:ListenForEvent("respawnfromghost", function() 
        inst:DoTaskInTime(5, function(inst) PlayablePets.CommonSetChar(inst, mob) end)
		inst:DoTaskInTime(5.1, function(inst) PlayablePets.SetSkin(inst, mob) end)
		inst:DoTaskInTime(5, function(inst) PlayablePets.RevRestore(inst, mob) end)
    end)
	
	inst.OnSave = OnSave
    inst.OnLoad = OnLoad
	
    return inst	
end

return MakePlayerCharacter(prefabname, prefabs, assets, common_postinit, master_postinit, start_inv)