local MakePlayerCharacter = require "prefabs/player_common"

---------------------------
----------==Notes==--------
--
---------------------------

--Don't add assets unless absolutely needed.
local assets = 
{
	Asset("ANIM", "anim/lightning_goat_shiny_build_01.zip"),
	Asset("ANIM", "anim/lightning_goat_shocked_shiny_build_01.zip"),
}

local getskins = {"1"}
--Might be completely pointless here. I don't know.
local prefabs = 
{	

}
	
--Note: Becareful when adding starting items.
--People are likely to spam changecharacter to get more of them.	
local start_inv = 
{
	--'prefab',
	--"spear",
}

local start_inv2 = {

}

if MOBHOUSE == "Enable1" or MOBHOUSE == "Enable3" then
	start_inv = start_inv2
end
-----------------------
local mob = 
{
	health = TUNING.GOATP_HEALTH,
	hunger = TUNING.GOATP_HUNGER,
	hungerrate = TUNING.WILSON_HUNGER_RATE, 
	sanity = TUNING.GOATP_SANITY,
	runspeed = TUNING.LIGHTNING_GOAT_RUN_SPEED,
	walkspeed = TUNING.LIGHTNING_GOAT_WALK_SPEED,
	damage = 30*2,
	range = TUNING.LIGHTNING_GOAT_ATTACK_RANGE,
	hit_range = TUNING.LIGHTNING_GOAT_ATTACK_RANGE,
	attackperiod = 0,
	bank = "lightning_goat",
	build = "lightning_goat_build",
	shiny = "lightning_goat",
	scale = 1,
	--build2 = "alternate build here",
	stategraph = "SGgoatp",
	minimap = "goatp.tex",
	
}
-----------------------
--Loot that drops when you die, duh.
SetSharedLootTable('goatp',
-----Prefab---------------------Chance------------
{
    {'meat',			1.00},
	{'meat',			1.00},
	{'lightninggoathorn',			0.25},
    
   
})

SetSharedLootTable('goat2p',
{
    {'meat',              1.00},
    {'meat',              1.00},
    {'goatmilk',          1.00},  
    {'lightninggoathorn', 0.25},
})

local function discharge(inst)
    inst:RemoveTag("charged")
    inst.components.lootdropper:SetChanceLootTable('goatp') 
    inst.sg:GoToState("discharge")
    inst.AnimState:ClearBloomEffectHandle()
    inst.charged = false
    inst.Light:Enable(false)
    inst.chargeleft = nil
	inst.components.combat:SetDefaultDamage(mob.damage)
	inst.components.locomotor.runspeed = (mob.runspeed)
	inst.components.locomotor.walkspeed = (mob.walkspeed)
	
end

local function ReduceCharges(inst)
    if inst.chargeleft then
        inst.chargeleft = inst.chargeleft - 1
        if inst.chargeleft <= 0 then
            discharge(inst)
        end
    end
end

local function setcharged(inst, instant)
    inst:AddTag("charged")
    inst.components.lootdropper:SetChanceLootTable('goat2p')
	if inst.isshiny and inst.isshiny ~= 0 then
	inst.AnimState:SetBuild("lightning_goat_shocked_shiny_build_0"..inst.isshiny)
	else
    inst.AnimState:SetBuild("lightning_goat_shocked_build")
	end
    inst.AnimState:Show("fx") 
    if not instant then
        inst.sg:GoToState("shocked")
    end
    inst.AnimState:SetBloomEffectHandle("shaders/anim.ksh")
    inst.charged = true
    inst.chargeleft = 3
    inst.Light:Enable(true)
	
	inst.components.combat:SetDefaultDamage(mob.damage*1.5)
	inst.components.locomotor.runspeed = (mob.runspeed + 2)
	inst.components.locomotor.walkspeed = (mob.walkspeed + 2)
	
    inst:WatchWorldState("cycles", ReduceCharges)
end

local function OnAttacked(inst, data)
    inst.components.combat:SetTarget(data.attacker)

    if inst.charged then
        if data.attacker.components.health then
            if (data.weapon == nil or (not data.weapon:HasTag("projectile") and data.weapon.projectile == nil))
                and not (data.attacker.components.inventory and data.attacker.components.inventory:IsInsulated()) then

                data.attacker.components.health:DoDelta(-TUNING.LIGHTNING_GOAT_DAMAGE)
                if data.attacker:HasTag("player") then
                    data.attacker.sg:GoToState("electrocute")
                end
            end
        end
    end

    if not inst.charged and data and data.weapon and data.weapon.components.weapon and data.weapon.components.weapon.stimuli == "electric" then
        setcharged(inst)
    end

    local attacker = data and data.attacker
    inst.components.combat:SetTarget(attacker)
    inst.components.combat:ShareTarget(attacker, 20, function(dude) return dude:HasTag("lightninggoat") and dude:HasTag("charged") end, 3)
end

local function getwildposition(inst)
    local ground = TheWorld
    local centers = {}
    for i, node in ipairs(ground.topology.nodes) do
        if ground.Map:IsPassableAtPoint(node.x, 0, node.y) then
            table.insert(centers, {x = node.x, z = node.y})
        end
    end
    if #centers > 0 then
        local pos = centers[math.random(#centers)]
        return Point(pos.x, 0, pos.z)
    else
        return inst:GetPosition()
    end
end

local function PvPTeleport(inst)
if inst.mobteleported == nil or inst.mobteleported == false then
	local ground = TheWorld
	local pt = getwildposition(inst)--Point(inst.Transform:GetWorldPosition())
      if pt.y <= .1 then
         pt.y = 0
         inst.Physics:Stop()
         inst.Physics:Teleport(pt.x,pt.y,pt.z)
		 inst.mobteleported = true
      end
	end
end

------------------------------------------------------
 local function OnLoad(inst, data)
	if data ~= nil then
		inst.mobteleported = data.mobteleported or false
		inst.isshiny = data.isshiny or 0
		if data and data.charged and data.chargeleft then
        setcharged(inst, true)
        inst.chargeleft = data.chargeleft
		end
	end
end

local function OnSave(inst, data)
	data.mobteleported = inst.mobteleported or false
	data.isshiny = inst.isshiny or 0
	if inst.charged then
        data.charged = inst.charged
        data.chargeleft = inst.chargeleft
    end
end

local function SetSkinDefault(inst, num)
	--Default
	if num ~= nil and num ~= 0 then
		--print("DEBUG:SetSkinDefault shinizer ran")
			if inst.charged ~= nil and inst.charged == true then
				inst.AnimState:SetBuild("lightning_goat_shocked_shiny_build_0"..num)
			else	
				inst.AnimState:SetBuild("lightning_goat_shiny_build_0"..num)			
			end
	else
		--print("DEBUG:SetSkinDefault normalizer ran")
		if inst.charged ~= nil and inst.charged == true then
			inst.AnimState:SetBuild(mob.build2)
		else	
			inst.AnimState:SetBuild(mob.build)			
		end
	end	
end

local common_postinit = function(inst) 
	--inst.soundsname = "beefalo"

	inst.MiniMapEntity:SetIcon(mob.minimap)
	--inst.AnimState:SetMultColour(1, 1, 1, 0.5)

	inst:DoTaskInTime(0, function() 
   if ThePlayer then
      inst:EnableMovementPrediction(false)
   end
end)
	--Night Vision that toggles, might be a better way to do this.
	--Enables at night, disables during the day.
	inst:WatchWorldState( "isday", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "isdusk", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "isnight", function() PlayablePets.SetNightVision(inst)  end)
	inst:WatchWorldState( "iscaveday", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "iscavedusk", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "iscavenight", function() PlayablePets.SetNightVision(inst)  end)
	
	PlayablePets.SetNightVision(inst) --This is here to make sure the NightVision is enabled when a player rejoins.
	
	inst:WatchWorldState( "issummer", function() PlayablePets.SetSandstormImmunity(inst) end) --makes immune to Sandstorm visual issues.
	
	PlayablePets.SetSandstormImmunity(inst)
	
end


local master_postinit = function(inst) 
	--Stats--
	PlayablePets.SetCommonStats(inst, mob, nil, true) --mob table, ishuman, ignorepvpmultiplier
	PlayablePets.SetCommonWeatherResistances(inst, 60, 20, 1) --heat, cold, wetness
	PlayablePets.SetCommonStatResistances(inst) --fire, acid, poison
	PlayablePets.SetSandstormImmune(inst)
	----------------------------------
	local body_symbol = "lightning_goat_body"
	inst.poisonsymbol = body_symbol
	MakeMediumBurnableCharacter(inst, body_symbol)
    MakeMediumFreezableCharacter(inst)
	inst.components.debuffable:SetFollowSymbol(body_symbol, 0, 0, 0)
	----------------------------------
	--Loot drops--
	inst:AddComponent("lootdropper")
    inst.components.lootdropper:SetChanceLootTable('goatp')
	----------------------------------
	--Tags--
	inst.mobplayer = true --lets mod know that you're a mob.
	inst:AddTag("lightninggoat")
    inst:AddTag("animal")
    inst:AddTag("lightningrod")
	
	inst.AnimState:Hide("fx")
	
	inst.mobsleep = true
	inst.taunt = true
	inst.ghostbuild = "ghost_monster_build"
	inst.mobteleported = false
	inst.taunt2 = true
	inst.isshiny = 0
	inst.getskins = getskins
	inst.setskin_defaultfn = SetSkinDefault
	
	inst:ListenForEvent("lightningstrike", setcharged)
    inst.setcharged = setcharged
	----------------------------------
	inst.components.eater:SetDiet({ FOODTYPE.VEGGIE, FOODTYPE.ROUGHAGE }, { FOODTYPE.VEGGIE, FOODTYPE.ROUGHAGE })
    inst.components.eater:SetAbsorptionModifiers(4,1,1)
	inst.components.eater:SetCanEatHorrible() --check to make them eat monster meat.
	inst.components.eater.strongstomach = true -- can eat monster meat!
	---------------------------------
	--Physics and Scale--
	MakeCharacterPhysics(inst, 10, .5)
    inst.DynamicShadow:SetSize(1.5, .5)
    inst.Transform:SetFourFaced()
	---------------------------------
	--Shedder--
	
	inst:AddComponent("shedder")
	inst.components.shedder.shedItemPrefab = "goatmilk"
	inst.components.shedder.shedHeight = 0.1
	inst.components.shedder:StartShedding(1200) --Note: 480 is 1 day. 480 x n = n amount of days.
	---------------------------------
	inst:ListenForEvent("attacked", OnAttacked)
	---------------------------------
	--Functions that saves and loads data.
    inst.OnSave = OnSave
	inst.OnLoad = OnLoad
    ------------------------------------------------------
	--Light and Character Functions. Don't Touch.--
    
	---------------------------------
	--Forge--
	if TheNet:GetServerGameMode() == "lavaarena" then
		inst.forge_fn = SetForge(inst)
	end
    ------------------------------------------------------
	--Respawning and Initializing functions--
	
	inst:ListenForEvent("respawnfromghost", function(inst) PlayablePets.RevRestore(inst, mob) end) --(inst, mob, isflying, iswebimmune, noshadow, ishuman)

    inst:DoTaskInTime(0, function(inst) PlayablePets.CommonSetChar(inst, mob) end)
	inst:DoTaskInTime(3, function(inst) PlayablePets.SetSkin(inst, mob) end)
    inst:ListenForEvent("respawnfromghost", function() 
        inst:DoTaskInTime(5, function(inst) PlayablePets.CommonSetChar(inst, mob) end)
		inst:DoTaskInTime(5.1, function(inst) PlayablePets.SetSkin(inst, mob) end)
		inst:DoTaskInTime(5, function(inst) PlayablePets.RevRestore(inst, mob) end)
    end)
	
    return inst
	
end

return MakePlayerCharacter("goatp", prefabs, assets, common_postinit, master_postinit, start_inv)
