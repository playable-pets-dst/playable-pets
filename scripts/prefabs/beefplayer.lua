local MakePlayerCharacter = require "prefabs/player_common"

---------------------------
----------==Notes==--------
--
---------------------------

local prefabname = "beefplayer"

local assets = 
{
	Asset("ANIM", "anim/beefalo_basic.zip"),
    Asset("ANIM", "anim/beefalo_actions.zip"),
    Asset("ANIM", "anim/beefalo_actions_domestic.zip"),
    Asset("ANIM", "anim/beefalo_build.zip"),
    Asset("ANIM", "anim/beefalo_shaved_build.zip"),
    Asset("ANIM", "anim/beefalo_baby_build.zip"),
	
    Asset("ANIM", "anim/beefalo_domesticated.zip"),
    Asset("ANIM", "anim/beefalo_personality_docile.zip"),
    Asset("ANIM", "anim/beefalo_personality_ornery.zip"),
    Asset("ANIM", "anim/beefalo_personality_pudgy.zip"),

    Asset("ANIM", "anim/beefalo_fx.zip"),

    Asset("SOUND", "sound/beefalo.fsb"),
}

local prefabs = 
{	

}

local start_inv = 
{
	"beefalowool",
	"beefalowool",
	"beefalowool",
	"beefalowool",
}

local getskins = {}
-----------------------
--Stats--
local mob = 
{
	health = TUNING[string.upper(prefabname.."_HEALTH")],
	hunger = TUNING[string.upper(prefabname.."_HUNGER")],
	sanity = TUNING[string.upper(prefabname.."_SANITY")],
	
	runspeed = TUNING.BEEFALO_RUN_SPEED.DEFAULT,
	walkspeed = TUNING.BEEFALO_WALK_SPEED,
	
	attackperiod = 0,
	damage = TUNING.BEEFALO_DAMAGE.DEFAULT,
	range = 2,
	hit_range = 3,
	
	bank = "beefalo",
	build = "beefalo_build",
	shiny = "beefalo",
	
	scale = 1,
	stategraph = "SGbeefplayer",
	minimap = prefabname..".tex",	
}

--Loot that drops when you die, duh.
SetSharedLootTable( 'beefplayer',
{
    {'meat',			 1.00},
	{'meat',			 1.00},
	{'meat',			 1.00},
	{'meat',			 1.00},
	{'beefalowool',     1.00},
    {'beefalowool',     1.00},
    {'beefalowool',     1.00},
    {'horn',            0.33},
})

local FORGE_STATS = PP_FORGE.BEEFALO
--==============================================
--					Mob Functions
--==============================================
local sounds = 
{
    walk = "dontstarve/beefalo/walk",
    grunt = "dontstarve/beefalo/grunt",
    yell = "dontstarve/beefalo/yell",
    swish = "dontstarve/beefalo/tail_swish",
    curious = "dontstarve/beefalo/curious",
    angry = "dontstarve/beefalo/angry",
    sleep = "dontstarve/beefalo/sleep",
}


local function OnResetBeard(inst)
	inst.isshaved = true
	PlayablePets.SetCommonWeatherResistances(inst) --heat, cold, wetness
    if inst.isshiny and inst.isshiny ~= 0 then
		inst.AnimState:SetBuild("beefalo_shaved_shiny_build_0"..inst.isshiny)
	else
		inst.AnimState:SetBuild("beefalo_shaved_build")
	end
end

local function CanShaveTest(inst)
    return not inst.components.health:IsDead() and not inst.sg:HasStateTag("busy")
end

local function OnShaved(inst)
	inst.isshaved = true
	PlayablePets.SetCommonWeatherResistances(inst) --heat, cold, wetness
    if inst.isshiny and inst.isshiny ~= 0 then
		inst.AnimState:SetBuild("beefalo_shaved_shiny_build_0"..inst.isshiny)
	else
		inst.AnimState:SetBuild("beefalo_shaved_build")
	end
end

local function OnHairGrowth(inst)
    if inst.components.beard.bits == 0 then
        inst.hairGrowthPending = true
    end
end
--==============================================
--				Custom Common Functions
--==============================================
local function SetSkinDefault(inst, num)
	--Default
	if num ~= nil and num ~= 0 then
		if inst.isshaved then
			inst.AnimState:SetBuild(mob.shiny.."_shaved_shiny_build_0"..num)
		else
			inst.AnimState:SetBuild(mob.shiny.."_shiny_build_0"..num)
		end
	else
		if inst.isshiny and inst.isshiny ~= 0 then
			inst.AnimState:SetBuild("beefalo_shaved_build")
		else
			inst.AnimState:SetBuild(mob.build)
		end		
	end	
end
--==============================================
--					Loading/Saving
--==============================================
 
local function OnLoad(inst, data)
	if data ~= nil then
		inst.mobteleported = data.mobteleported or false
		inst.isshiny = data.isshiny or 0
		inst.isshaved = data.isshaved or false
		if inst.isshaved == true then
			OnResetBeard(inst)
		end
	end
end

local function OnSave(inst, data)
	data.mobteleported = inst.mobteleported or false
	data.isshiny = inst.isshiny or 0
	data.isshaved = inst.isshaved or false
end

--==============================================
--					Forged Forge
--==============================================

local ex_fns = require "prefabs/player_common_extensions"

local function SetForge(inst)
	PlayablePets.SetForgeStats(inst, FORGE_STATS)
	
	inst.mobsleep = false	
	
	inst:DoTaskInTime(0, function(inst)
	inst:AddComponent("itemtyperestrictions")
	inst.components.itemtyperestrictions:SetRestrictions({"books", "staves"})
	end)
	
	inst.components.combat:SetDamageType(1)
	
	inst:RemoveEventCallback("respawnfromcorpse", ex_fns.OnRespawnFromPlayerCorpse)
	inst:ListenForEvent("respawnfromcorpse", PlayablePets.OnRespawnFromMobCorpse)
end

--==============================================
--					Common/Master
--==============================================

local common_postinit = function(inst) 
	inst.MiniMapEntity:SetIcon(mob.minimap)

	inst:DoTaskInTime(0, function() 
   if ThePlayer then
      inst:EnableMovementPrediction(false) --PP doesn't work with movement prediction enabled, due to use of custom stategraphs
   end
end)
	----------------------------------
	--Tags--
    inst:AddTag("beefalo")
	inst:AddTag("animal")
	----------------------------------

	inst:WatchWorldState( "isday", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "isdusk", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "isnight", function() PlayablePets.SetNightVision(inst)  end)
	inst:WatchWorldState( "iscaveday", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "iscavedusk", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "iscavenight", function() PlayablePets.SetNightVision(inst)  end)
	
	PlayablePets.SetNightVision(inst)
end



local master_postinit = function(inst)
	--Stats--
	PlayablePets.SetCommonStats(inst, mob, nil, true) --mob table, ishuman, ignorepvpmultiplier
	PlayablePets.SetCommonWeatherResistances(inst, 60, 20 , 1) --heat, cold, wetness
	PlayablePets.SetCommonStatResistances(inst) --fire, acid, poison
	----------------------------------
	--Variables	
	inst.mobsleep = true
	inst.taunt = true
	inst.taunt2 = true
	inst.shouldwalk = false
	
	inst.sounds = sounds
	
	inst.getskins = getskins
	inst.setskin_defaultfn = SetSkinDefault
	
	local body_symbol = "beefalo_body"
	inst.poisonsymbol = body_symbol
	MakeLargeBurnableCharacter(inst, body_symbol)
    MakeHugeFreezableCharacter(inst, body_symbol)
	inst.components.debuffable:SetFollowSymbol(body_symbol, 0, 0, 0)
	
	inst.AnimState:AddOverrideBuild("poop_cloud")
    inst.AnimState:AddOverrideBuild("beefalo_carrat_idles")
	
	inst.AnimState:Hide("HEAT")
	----------------------------------
	--Components
	PlayablePets.SetCommonLootdropper(inst, prefabname) --inst, prefaboverride
	
	inst:AddComponent("shedder")
	inst.components.shedder.shedItemPrefab = "poop"
	inst.components.shedder.shedHeight = 0.1
	inst.components.shedder:StartShedding(180)
	
	inst:AddComponent("periodicspawner")
    inst.components.periodicspawner:SetPrefab("babybeefalo")
    inst.components.periodicspawner:SetRandomTimes(480 * 12, 480 * 15)
    inst.components.periodicspawner:SetDensityInRange(20, 8)
    inst.components.periodicspawner:SetMinimumSpacing(1)
	--baby spawns with x
	
	inst:AddComponent("beard")
    -- assume the beefalo has already grown its hair
    inst.components.beard.bits = 3
    inst.components.beard.daysgrowth = TUNING.BEEFALO_HAIR_GROWTH_DAYS
    inst.components.beard.onreset = OnResetBeard
    inst.components.beard.canshavetest = CanShaveTest
    inst.components.beard.prize = "beefalowool"
    inst.components.beard:AddCallback(0, OnShaved)
    inst.components.beard:AddCallback(TUNING.BEEFALO_HAIR_GROWTH_DAYS, OnHairGrowth)
	----------------------------------
	--Eater--
	inst.components.eater:SetDiet({ FOODTYPE.VEGGIE, FOODTYPE.ROUGHAGE }, { FOODTYPE.VEGGIE, FOODTYPE.ROUGHAGE })
    inst.components.eater:SetAbsorptionModifiers(4,1,1)
	---------------------------------
	--Physics and Shadows--
	MakeCharacterPhysics(inst, 100, .5)
	inst.Transform:SetSixFaced()
	inst.DynamicShadow:SetSize(6, 2)
	---------------------------------
	--Listeners--
	inst:ListenForEvent("equip", PlayablePets.CommonOnEquip) --Shows head when hats make heads disappear.
	---------------------------------
	--Forge--
	if TheNet:GetServerGameMode() == "lavaarena" then
		inst.forge_fn = SetForge(inst)
	end
    ------------------------------------------------------
	--Respawning and Initializing functions--
	
	inst:ListenForEvent("respawnfromghost", function(inst) PlayablePets.RevRestore(inst, mob) end) --(inst, mob, isflying, iswebimmune, noshadow, ishuman)

    inst:DoTaskInTime(0, function(inst) PlayablePets.CommonSetChar(inst, mob) end)
	inst:DoTaskInTime(3, function(inst) PlayablePets.SetSkin(inst, mob) end)
    inst:ListenForEvent("respawnfromghost", function() 
        inst:DoTaskInTime(5, function(inst) PlayablePets.CommonSetChar(inst, mob) end)
		inst:DoTaskInTime(5.1, function(inst) PlayablePets.SetSkin(inst, mob) end)
		inst:DoTaskInTime(5, function(inst) PlayablePets.RevRestore(inst, mob) end)
    end)
	
	inst:DoTaskInTime(1, function(inst)
		if inst.isshaved then
			OnResetBeard(inst)
		end
	end)
	inst.OnSave = OnSave
    inst.OnLoad = OnLoad
	
    return inst	
end

return MakePlayerCharacter(prefabname, prefabs, assets, common_postinit, master_postinit, start_inv)