local MakePlayerCharacter = require "prefabs/player_common"

---------------------------
----------==Notes==--------
--
---------------------------

local prefabname = "maggotplayer"

local assets = 
{
	Asset("ANIM", "anim/lavae.zip"),
    Asset("SOUND", "sound/together.fsb"),	
}

local prefabs = {
}

local start_inv = {
}

local start_inv2 = 
{

}

local getskins = {"1"}

if MOBHOUSE == "Enable1" or MOBHOUSE == "Enable3" then
	start_inv = start_inv2
end
-----------------------
--Stats--
local mob = 
{
	health = TUNING.MAGGOTPLAYER_HEALTH,
	hunger = TUNING.MAGGOTPLAYER_HUNGER,
	hungerrate = TUNING.WILSON_HUNGER_RATE, 
	sanity = TUNING.MAGGOTPLAYER_SANITY,
	
	runspeed = 6,
	walkspeed = 6,
	
	attackperiod = 0,
	damage = 20,
	range = 3,
	hit_range = TUNING.TALLBIRD_ATTACK_RANGE,
	
	bank = "lavae",
	build = "lavae",
	shiny = "lavae",
	
	scale = 1,
	stategraph = "SGmaggotplayer",
	minimap = "maggotplayer.tex",	
}

local mob_teen = 
{
	health = TUNING.MAGGOTPLAYER_TEEN_HEALTH,
	hunger = TUNING.MAGGOTPLAYER_TEEN_HUNGER,
	hungerrate = TUNING.WILSON_HUNGER_RATE,
	sanity = TUNING.MAGGOTPLAYER_TEEN_SANITY,
	runspeed = TUNING.CRITTER_WALK_SPEED,
	walkspeed = TUNING.CRITTER_WALK_SPEED,
	damage = 30,
	attackperiod = 0,
	range = 2,
	hit_range = 3,
	bank = "dragonling", 
	build = "dragonling_build",
	shiny = "dragonling",
	scale = 1,
	--build2 = "alternate build here",
	stategraph = "SGbabydragonplayer",
	minimap = "babydragonplayer.tex",
	
}

local mob_adult = 
{
	health = TUNING.MAGGOTPLAYER_ADULT_HEALTH,
	hunger = TUNING.MAGGOTPLAYER_ADULT_HUNGER,
	hungerrate = TUNING.WILSON_HUNGER_RATE,
	sanity = TUNING.MAGGOTPLAYER_ADULT_SANITY,
	runspeed = TUNING.DRAGONFLY_SPEED,
	walkspeed = TUNING.DRAGONFLY_SPEED,
	damage = 75*2,
	attackperiod = TUNING.DRAGONFLY_ATTACK_PERIOD,
	range = TUNING.DRAGONFLY_ATTACK_RANGE,
	hit_range = 6,
	bank = "dragonfly",
	build = "dragonfly_build",
	build2 = "dragonfly_fire_build",
	shiny = "dragonfly",
	scale = 1.3,
	--build2 = "alternate build here",
	stategraph = "SGdragonplayer",
	minimap = "dragonplayer.tex",
	
}

SetSharedLootTable( 'maggotplayer',
{
	{'rocks',			 1.00},
	{'rocks',			 1.00},
	{'rocks',			 1.00},
	--{'bluegem',			 	 0.25},
})

SetSharedLootTable('babydragonplayer',
-----Prefab---------------------Chance------------
{
    {'meat',            1.00},
    {'charcoal',        0.50},
    
   
})

SetSharedLootTable( 'dragonplayers',
{
    {'dragon_scales',    1.00},
	{'lavae_egg',    	 0.33},
    {'meat',             1.00},
    {'meat',             1.00},
    {'meat',             1.00},
    {'meat',             1.00},
    {'meat',             1.00},
    {'meat',             1.00},
})
foodprefs = {"VEGGIE", "SEEDS"}
food2prefs = {"MEAT", "VEGGIE"}

local FORGE_STATS = PP_FORGE.LAVAE

local MIN_HEAT = 99
local MAX_HEAT = 100
local TEEN_REQ = 100
local ADULT_REQ = 250
--==============================================
--					Mob Functions
--==============================================

local function TransformNormal(inst)
	if inst.isshiny and inst.isshiny ~= 0 then
		inst.AnimState:SetBuild("dragonfly_shiny_build_0"..inst.isshiny)
	else
		inst.AnimState:SetBuild("dragonfly_build")
	end
    inst.enraged = false
    --Set normal stats
	inst.Light:Enable(false)
	inst.Light:SetRadius(0)
	inst.Light:SetFalloff(2)
	inst.Light:SetIntensity(4)
	inst.Light:SetColour(245/255,40/255,0/255)
    inst.components.locomotor.walkspeed = TUNING.DRAGONFLY_SPEED
    inst.components.combat:SetDefaultDamage(TheNet:GetServerGameMode() == "lavaarena" and PP_FORGE.DRAGONFLY.DAMAGE or 75*2)
	if inst.components.buffable then
		inst.components.combat:RemoveDamageBuff("rage_dragon_buff", false)
		inst.components.combat:RemoveDamageBuff("rage_dragon_debuff", true)
		inst.components.combat:AddDamageBuff("calm_dragon_buff", 1.25, true)
	end
    inst.components.combat:SetAttackPeriod(TUNING.DRAGONFLY_ATTACK_PERIOD)
    inst.components.combat:SetRange(TUNING.DRAGONFLY_ATTACK_RANGE, mob.range)


    inst.components.propagator:StopSpreading()
end

local function TransformFire(inst)
	
	if inst.isshiny and inst.isshiny ~= 0 then
		inst.AnimState:SetBuild("dragonfly_fire_shiny_build_0"..inst.isshiny)
	else
		inst.AnimState:SetBuild("dragonfly_fire_build")
	end
    inst.enraged = true
    inst.can_ground_pound = true
    --Set fire stats
    inst.components.locomotor.walkspeed = TUNING.DRAGONFLY_FIRE_SPEED
	if TheNet:GetServerGameMode() == "lavaarena" then
		if inst.components.buffable then
			inst.components.combat:RemoveDamageBuff("calm_dragon_buff", true)
			inst.components.combat:AddDamageBuff("rage_dragon_buff", 1.5, false)
			inst.components.combat:AddDamageBuff("rage_dragon_debuff", 0.75, true)
		end
	else
		inst.components.combat:SetDefaultDamage(BOSS_STATS and 300 or 90 * 2)
	end
    inst.components.combat:SetAttackPeriod(TUNING.DRAGONFLY_FIRE_ATTACK_PERIOD)
    inst.components.combat:SetRange(TUNING.DRAGONFLY_ATTACK_RANGE, mob.range)

	inst.Light:Enable(true)
	inst.Light:SetRadius(5)
	inst.Light:SetFalloff(0.9)
	inst.Light:SetIntensity(0.6)
	if inst.isshiny and inst.isshiny ~= 0 then
		inst.Light:SetColour(0/255,157/255,255/255)
	elseif inst.isshiny == 2 then
		inst.Light:SetColour(93/255,0/255,255/255)
	else
		inst.Light:SetColour(180/255,195/255,150/255)
	end

	if MOBFIRE ~= "Enable" and TheNet:GetServerGameMode() ~= "lavaarena" then
		inst.components.propagator:StartSpreading()
	end
	
    inst.components.moisture:DoDelta(-inst.components.moisture:GetMoisture())
end
--==============================================
--				Custom Common Functions
--==============================================	
local function SetAdult(inst)
	PlayablePets.SetCommonStats(inst, mob_adult) --mob table, ishuman, ignorepvpmultiplier
	PlayablePets.SetCommonWeatherResistances(inst, 60, 20, 1) --heat, cold, wetness
	PlayablePets.SetCommonStatResistances(inst, 0) --fire, acid, poison
	if inst.isshiny and inst.isshiny ~= 0 then
		inst.AnimState:SetBuild("dragonfly_shiny_build_0"..inst.isshiny)
	else
		inst.AnimState:SetBuild(mob_adult.build)
	end
	
	PlayablePets.SetCommonLootdropper(inst, "dragonplayers") --inst, prefaboverride

	inst:AddTag("dragonplayer")
	inst:AddTag("flying")
	inst:AddTag("epic")
	inst:RemoveTag("lavae")
	inst.can_ground_pound = true
	inst.altattack = true
	inst.isdragonrage = false
	inst.taunt2 = true
	inst.level = 0
	inst.hit_recovery = 0.75
	
	inst.TransformFire = TransformFire
    inst.TransformNormal = TransformNormal
	inst.SoundEmitter:PlaySound("dontstarve_DLC001/creatures/dragonfly/fly", "flying")
	
	inst:AddComponent("groundpounder")
    inst:AddComponent("healthtrigger")
	inst:AddComponent("groundpounder")
	inst.components.groundpounder.numRings = 2
	inst.components.groundpounder.damageRings = 0
	if MOBFIRE ~= "Enable" then
		inst.components.groundpounder.burner = true
	end    
    inst.components.groundpounder.groundpoundfx = "firesplash_fx"
    inst.components.groundpounder.groundpounddamagemult = 1.25
    inst.components.groundpounder.groundpoundringfx = "firering_fx"
	inst.components.groundpounder.noTags = {"lavae"}
	
	inst.Light:Enable(false)
  	inst.Light:SetRadius(0)
  	inst.Light:SetFalloff(0.9)
  	inst.Light:SetIntensity(0.6)
  	inst.Light:SetColour(180/255,195/255,150/255)
	
	inst.Transform:SetScale(mob_adult.scale, mob_adult.scale, mob_adult.scale)

	inst.components.eater:SetAbsorptionModifiers(1,1.5,1)
	
	local body_symbol = "swap_fire"
	inst.poisonsymbol = body_symbol
    MakeHugeFreezableCharacter(inst, body_symbol)
	inst.components.debuffable:SetFollowSymbol(body_symbol, 0, 0, 0)
	MakeLargeBurnableCharacter(inst, body_symbol)
	
	MakeFlyingGiantCharacterPhysics(inst, 500, 1.4)
    inst.DynamicShadow:SetSize(6, 3.5)
    inst.Transform:SetSixFaced()
	PlayablePets.SetAmphibious(inst, nil, nil, true)	
end

local function SetTeen(inst)
	PlayablePets.SetCommonStats(inst, mob_teen, nil, true) --mob table, ishuman, ignorepvpmultiplier
	PlayablePets.SetCommonWeatherResistances(inst, 60, 20, 1) --heat, cold, wetness
	PlayablePets.SetCommonStatResistances(inst, 0, nil, nil, 9999) --fire, acid, poison
	if inst.isshiny and inst.isshiny ~= 0 then
		inst.AnimState:SetBuild(mob_teen.shiny.."_shiny_build_0"..inst.isshiny)
	else
		inst.AnimState:SetBuild(mob_teen.build)
	end
	
	inst.components.eater:SetDiet({ FOODTYPE.MEAT, FOODTYPE.BURNT }, { FOODTYPE.MEAT, FOODTYPE.BURNT }) 
	
	inst:AddTag("lavae")
	inst:AddTag("flying")

	inst.Light:Enable(false)
	
	local body_symbol = "head"
	inst.poisonsymbol = body_symbol
	MakeLargeBurnableCharacter(inst, body_symbol)
    MakeHugeFreezableCharacter(inst, body_symbol)
	inst.components.debuffable:SetFollowSymbol(body_symbol, 0, 0, 0)
	
	MakeGhostPhysics(inst, 1, .5) --Special physics for flying characters(?)
    inst.DynamicShadow:SetSize(2, .75)
    inst.Transform:SetSixFaced()
	PlayablePets.SetAmphibious(inst, nil, nil, true, nil, true)
end

local function OnGrow(inst)
	inst.Transform:SetPosition(inst.Transform:GetWorldPosition())
	inst.sg:GoToState("growup")
end

local function applyupgrades(inst)
    local health_percent = inst.components.health:GetPercent()
	local hunger_percent = inst.components.hunger:GetPercent()
	--local sanity_percent = inst.components.sanity:GetPercent()
	
	if inst.dragonfood == 0 and inst.dragonfood < TEEN_REQ then
		inst.components.health.maxhealth = mob.health
		inst.components.hunger.max = mob.hunger
	end
	
    if inst.dragonfood >= TEEN_REQ and inst.dragonfood < ADULT_REQ then
		inst.components.health.maxhealth = mob_teen.health
		inst.components.hunger.max = mob_teen.hunger	
		OnGrow(inst)
    end
	
	if inst.dragonfood >= 90 then
		inst.components.health.maxhealth = mob_adult.health
		inst.components.hunger.max = mob_adult.hunger
		OnGrow(inst)
	end
	
    inst.components.health:SetPercent(health_percent)
end

local function OnEat(inst, food)
    if food and food.components.edible then
		if food.components.edible.foodtype == FOODTYPE.MEAT and MOB_DRAGONFLY then
			PlayablePets.DebugPrint("Lavae Debug: Food conditions met")
			--food helps you grow!
			if inst.dragonfood < 255 then
				inst.dragonfood = inst.dragonfood + 1
			end
			PlayablePets.DebugPrint("Lavae Debug: dragonfood is now "..tostring(inst.dragonfood))
			if inst.dragonfood >= TEEN_REQ and inst.dragonfood < ADULT_REQ and not inst:HasTag("flying") then
				OnGrow(inst)
				applyupgrades(inst)
			end
		
			if inst.dragonfood >= ADULT_REQ and inst:HasTag("lavae") then
				OnGrow(inst)
				applyupgrades(inst)
			end
		end
    end
end

local function SetSkinDefault(inst, num)
	--Default
	if num ~= nil and num ~= 0 then
		if inst:HasTag("epic") then
			if inst.isdragonrage then
				inst.AnimState:SetBuild(mob_adult.shiny.."_fire_shiny_build_0"..num)
			else
				inst.AnimState:SetBuild(mob_adult.shiny.."_shiny_build_0"..num)
			end
		elseif inst:HasTag("flying") and not inst:HasTag("epic") then	
			inst.AnimState:SetBuild(mob_teen.shiny.."_shiny_build_0"..num)
		else
			inst.AnimState:SetBuild(mob.shiny.."_shiny_build_0"..num)
		end
	else
		if inst:HasTag("epic") then
			if inst.isdragonrage then
				inst.AnimState:SetBuild(mob_adult.build2)
			else
				inst.AnimState:SetBuild(mob_adult.build)
			end
		elseif inst:HasTag("flying") and not inst:HasTag("epic") then
			inst.AnimState:SetBuild(mob_teen.build)
		else
			inst.AnimState:SetBuild(mob.build)			
		end		
	end	
end

local function ondeath(inst)
    inst.dragonfood = 0
    applyupgrades(inst)
	inst.amphibious = nil
	inst:RemoveTag("dragonplayer")
	inst:RemoveTag("flying")
	inst:RemoveTag("epic")
	inst:AddTag("lavae")
end
--==============================================
--					Loading/Saving
--==============================================
local function OnPreload(inst, data)
    if data ~= nil and data.dragonfood ~= nil then
        inst.dragonfood = data.dragonfood		
        applyupgrades(inst)
        --re-set these from the save data, because of load-order clipping issues
        if data.health and data.health.health then inst.components.health:SetCurrentHealth(data.health.health) end
        if data.hunger and data.hunger.hunger then inst.components.hunger.current = data.hunger.hunger end

        inst.components.health:DoDelta(0)
        inst.components.hunger:DoDelta(0)
        --inst.components.sanity:DoDelta(0)
    end
end
 
local function OnLoad(inst, data)
	if data ~= nil then
		inst.dragonfood = data.dragonfood or 0	
		inst.mobteleported = data.mobteleported or false
		inst.isshiny = data.isshiny or 0
		if data.charge_time ~= nil then
			StartTimer(inst, data.charge_time)
		end
		if inst.dragonfood then
			applyupgrades(inst) --for build reasons
		end
	end
end

local function OnSave(inst, data)
	data.dragonfood = inst.dragonfood > 0 and inst.dragonfood or nil
	 data.charge_time = inst.charge_time > 0 and inst.charge_time or nil
	 data.mobteleported = inst.mobteleported or false
	 data.isshiny = inst.isshiny or 0
end

--==============================================
--					Forged Forge
--==============================================

local ex_fns = require "prefabs/player_common_extensions"

local function SetForge(inst)
	PlayablePets.SetForgeStats(inst, FORGE_STATS)
	
	inst.mobsleep = false
	inst.taunt2 = false
	
	inst:DoTaskInTime(0, function(inst)
	inst:AddComponent("itemtyperestrictions")
	inst.components.itemtyperestrictions:SetRestrictions({"melees", "books", "staves", "darts"})
	end)
	
	inst.components.combat:SetDamageType(1)
	
	inst:RemoveEventCallback("respawnfromcorpse", ex_fns.OnRespawnFromPlayerCorpse)
	inst:ListenForEvent("respawnfromcorpse", PlayablePets.OnRespawnFromMobCorpse)
end

--==============================================
--					Common/Master
--==============================================

local common_postinit = function(inst) 
	inst.MiniMapEntity:SetIcon(mob.minimap)

	inst:DoTaskInTime(0, function() 
   if ThePlayer then
      inst:EnableMovementPrediction(false) --PP doesn't work with movement prediction enabled, due to use of custom stategraphs
   end
end)
	----------------------------------
	--Tags--
    inst:AddTag("lavae")
    inst:AddTag("monster")
    inst:AddTag("hostile")
	----------------------------------

	inst:WatchWorldState( "isday", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "isdusk", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "isnight", function() PlayablePets.SetNightVision(inst)  end)
	inst:WatchWorldState( "iscaveday", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "iscavedusk", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "iscavenight", function() PlayablePets.SetNightVision(inst)  end)
	
	PlayablePets.SetNightVision(inst)
end

local function LoadBuilds(inst) --awkward but blegh
	if inst.dragonfood >= TEEN_REQ and inst.dragonfood < ADULT_REQ then
		SetTeen(inst)
	elseif inst.dragonfood >= ADULT_REQ then
		SetAdult(inst)
	end
end

local master_postinit = function(inst)
	--Stats--
	PlayablePets.SetCommonStats(inst, mob, nil, true) --mob table, ishuman, ignorepvpmultiplier
	PlayablePets.SetCommonWeatherResistances(inst, 60, 20) --heat, cold, wetness
	PlayablePets.SetCommonStatResistances(inst, 0) --fire, acid, poison
	----------------------------------
	--Variables	
	inst.mobsleep = true
	inst.taunt = true
	inst.taunt2 = true
	inst.getskins = getskins
	inst.setskin_defaultfn = SetSkinDefault
	
	inst.userfunctions =
    {
        SetTeen = SetTeen,
        SetAdult = SetAdult,
    }
	
	inst.dragonfood = 0
	
	local body_symbol = "head"
	inst.poisonsymbol = body_symbol
	MakeLargeBurnableCharacter(inst, body_symbol)
    MakeLargeFreezableCharacter(inst)
	inst.components.debuffable:SetFollowSymbol(body_symbol, 0, 0, 0)
	
	inst.userfunctions =
	{
		SetTeen = SetTeen,
		SetAdult = SetAdult,
	}
	
	inst.charge_task = nil
    inst.charge_time = 0
	PlayablePets.SetStormImmunity(inst)
	----------------------------------
	--Components
	PlayablePets.SetCommonLootdropper(inst, prefabname) --inst, prefaboverride
	
	inst:AddComponent("timer")
	----------------------------------
	--Eater--
	inst.components.eater:SetDiet({ FOODTYPE.MEAT, FOODTYPE.BURNT }, { FOODTYPE.MEAT, FOODTYPE.BURNT }) 
    inst.components.eater:SetAbsorptionModifiers(1,1.5,1)
	inst.components.eater:SetOnEatFn(OnEat)
	---------------------------------
	--Physics and Shadows--
	MakeCharacterPhysics(inst, 50, .5)
    inst.DynamicShadow:SetSize(2, 1)
    inst.Transform:SetSixFaced()
	---------------------------------
	--Listeners--
	inst:ListenForEvent("equip", PlayablePets.CommonOnEquip) --Shows head when hats make heads disappear.
	inst:ListenForEvent("death", ondeath)
	---------------------------------
	--Forge--
	if TheNet:GetServerGameMode() == "lavaarena" then
		inst.forge_fn = SetForge(inst)
	end
    ------------------------------------------------------
	--Respawning and Initializing functions--
	inst.entity:AddLight()
	inst.Light:SetRadius(2)
    inst.Light:SetFalloff(0.5)
    inst.Light:SetIntensity(0.75)
    inst.Light:SetColour(235/255, 121/255, 12/255)
    inst.Light:Enable(true)
	
	inst:ListenForEvent("respawnfromghost", function(inst) PlayablePets.RevRestore(inst, mob) end) --(inst, mob, isflying, iswebimmune, noshadow, ishuman)

    inst:DoTaskInTime(0, function(inst) if inst.dragonfood and inst.dragonfood > TEEN_REQ then applyupgrades(inst) else PlayablePets.CommonSetChar(inst, mob) end end)
	inst:DoTaskInTime(3, function(inst) PlayablePets.SetSkin(inst, mob) end)
    inst:ListenForEvent("respawnfromghost", function() 
        inst:DoTaskInTime(5, function(inst) PlayablePets.CommonSetChar(inst, mob) end)
		inst:DoTaskInTime(5.1, function(inst) PlayablePets.SetSkin(inst, mob) end)
		inst:DoTaskInTime(5, function(inst) PlayablePets.RevRestore(inst, mob)
			inst.Transform:SetScale(1, 1, 1)
			inst.DynamicShadow:SetSize(2, 1)
			inst.components.locomotor:SetAllowPlatformHopping(true)
			inst.Light:Enable(true)
			inst.Light:SetRadius(2)
			inst.Light:SetFalloff(0.5)
			inst.Light:SetIntensity(0.75)
			inst.Light:SetColour(235/255, 121/255, 12/255)
		end)
    end)
	
	inst:DoTaskInTime(0.3, LoadBuilds)
	
	inst.OnPreload = inst.OnPreload
	inst.OnSave = OnSave
    inst.OnLoad = OnLoad
	
    return inst	
end

return MakePlayerCharacter(prefabname, prefabs, assets, common_postinit, master_postinit, start_inv)