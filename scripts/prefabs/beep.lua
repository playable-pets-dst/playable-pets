local MakePlayerCharacter = require "prefabs/player_common"

---------------------------
----------==Notes==--------
--
---------------------------
local prefabname = "beep"

local assets = 
{
	Asset("ANIM", "anim/bee_shiny_build_01.zip"),
	Asset("ANIM", "anim/bee_angry_shiny_build_01.zip"),
}

local getskins = {"1"}

local prefabs = 
{	

}
	
--Note: Becareful when adding starting items.
--People are likely to spam changecharacter to get more of them.	
local start_inv = 
{
	--'prefab',
	--"spear",
}

local start_inv2 =
{
   "beehome",
}

if MOBHOUSE== "Enable1" or MOBHOUSE== "Enable3" then
	start_inv = start_inv2
end
-----------------------
--Stats--
local mob = 
{
	health = TUNING[string.upper(prefabname.."_HEALTH")],
	hunger = TUNING[string.upper(prefabname.."_HUNGER")], 
	sanity = TUNING[string.upper(prefabname.."_SANITY")],
	runspeed = 5,
	walkspeed = 5,
	damage = 25,
	range = 1.3,
	hit_range = 1.8,
	attackperiod = 0,
	bank = "bee",
	build = "bee_build",
	scale = 1,
	shiny = "bee",
	build2 = "bee_angry_build",
	stategraph = "SGbeep",
	minimap = "beep.tex",
	
}
-----------------------
--Loot that drops when you die, duh.
SetSharedLootTable('beep',
-----Prefab---------------------Chance------------
{
    {'stinger',             1.00},  
	{'honey',               0.50}, 
   
})

FORGE_STATS = PP_FORGE[string.upper(prefabname)] 
--==============================================
--					Mob Functions
--==============================================
local workersounds =
{
    takeoff = "dontstarve/bee/bee_takeoff",
    attack = "dontstarve/bee/bee_attack",
    buzz = "dontstarve/bee/bee_fly_LP",
    hit = "dontstarve/bee/bee_hurt",
    death = "dontstarve/bee/bee_death",
}

local killersounds =
{
    takeoff = "dontstarve/bee/killerbee_takeoff",
    attack = "dontstarve/bee/killerbee_attack",
    buzz = "dontstarve/bee/killerbee_fly_LP",
    hit = "dontstarve/bee/killerbee_hurt",
    death = "dontstarve/bee/killerbee_death",
}

local function OnAttacked(inst, data)
    inst.components.combat:SetTarget(data.attacker)
    inst.components.combat:ShareTarget(data.attacker, 30, function(dude) return dude:HasTag("bee") and not dude.components.health:IsDead() end, 30)
end
--==============================================
--				Custom Common Functions
--==============================================	
local function SetAlt(inst)
	if inst.isalt then
		inst.sounds = killersounds
		inst.AnimState:SetBuild(inst.isshiny ~= 0 and mob.shiny.."_angry_shiny_build_0"..inst.isshiny or mob.build2)		
	else
		inst.sounds = workersounds
		inst.AnimState:SetBuild(inst.isshiny ~= 0 and mob.shiny.."_shiny_build_0"..inst.isshiny or mob.build2)	
	end
end

local function SetSkinDefault(inst, num)
	if num ~= 0 then
		if inst.isalt then
			inst.AnimState:SetBuild(mob.shiny.."_angry_shiny_build_0"..num)
		else
			inst.AnimState:SetBuild(mob.shiny.."_shiny_build_0"..num)
		end
	else
		inst.AnimState:SetBuild(inst.isalt and mob.build2 or mob.build)
	end
end
----PvP Teleport----

local function getwildposition(inst)
    local ground = TheWorld
    local centers = {}
    for i, node in ipairs(ground.topology.nodes) do
        if ground.Map:IsPassableAtPoint(node.x, 0, node.y) then
            table.insert(centers, {x = node.x, z = node.y})
        end
    end
    if #centers > 0 then
        local pos = centers[math.random(#centers)]
        return Point(pos.x, 0, pos.z)
    else
        return inst:GetPosition()
    end
end

local function PvPTeleport(inst)
if inst.mobteleported == nil or inst.mobteleported == false then
	local ground = TheWorld
	local pt = getwildposition(inst)--Point(inst.Transform:GetWorldPosition())
      if pt.y <= .1 then
         pt.y = 0
         inst.Physics:Stop()
         inst.Physics:Teleport(pt.x,pt.y,pt.z)
		 inst.mobteleported = true
      end
	end
end
--==============================================
--					Loading/Saving
--==============================================
 
local function OnLoad(inst, data)
	if data ~= nil then
		inst.mobteleported = data.mobteleported or false
		inst.isshiny = data.isshiny or 0
		inst.isalt = data.isalt or false
	end
end

local function OnSave(inst, data)
	data.mobteleported = inst.mobteleported or false
	data.isshiny = inst.isshiny or 0
	data.isalt = inst.isalt or false
end

--==============================================
--					Forged Forge
--==============================================

local ex_fns = require "prefabs/player_common_extensions"

local function SetForge(inst)
	PlayablePets.SetForgeStats(inst, FORGE_STATS)
	
	inst:DoTaskInTime(0, function(inst)
	inst:AddComponent("itemtyperestrictions")
	inst.components.itemtyperestrictions:SetRestrictions({"melees", "books", "staves", "darts"})
	end)
	
	inst.components.combat:SetDamageType(1)
	
	inst:RemoveEventCallback("respawnfromcorpse", ex_fns.OnRespawnFromPlayerCorpse)
	inst:ListenForEvent("respawnfromcorpse", PlayablePets.OnRespawnFromMobCorpse)
end

--==============================================
--					Common/Master
--==============================================

local common_postinit = function(inst) 
	inst.MiniMapEntity:SetIcon(mob.minimap)

	inst:DoTaskInTime(0, function() 
   if ThePlayer then
      inst:EnableMovementPrediction(false)
	  --ThePlayer.HUD.controls.status.brain:Hide()
		if MONSTERHUNGER== "Disable" then
			--ThePlayer.HUD.controls.status.stomach:Hide()
		end
   end
end)
	----------------------------------
	--Tags--
	----------------------------------
	inst:AddTag("bee")
    inst:AddTag("insect")
    inst:AddTag("smallcreature")
    inst:AddTag("flying")
	----------------------------------
end

local master_postinit = function(inst)  
	--Stats--
	inst.sounds = workersounds
	PlayablePets.SetCommonStats(inst, mob, nil, true) --mob table, ishuman, ignorepvpmultiplier
	PlayablePets.SetCommonWeatherResistances(inst, 60, 20, 1) --heat, cold, wetness
	PlayablePets.SetCommonStatResistances(inst, 3, 3, 1) --fire, acid, poison
	----------------------------------
	--Variables		
	inst.SoundEmitter:PlaySound(inst.sounds.buzz, "buzz")
	
	inst.isshiny = 0
	inst.getskins = getskins
	inst.setskin_defaultfn = SetSkinDefault
	
	inst.mobsleep = true
	inst.taunt = true
	
	inst.mobplayer = true
	inst.ghostbuild = "ghost_monster_build"
	
	local chance = math.random()
	if chance > 0.5 then
		inst.isalt = true
	else
		inst.isalt = false
	end
	SetAlt(inst)
	
	local body_symbol = "swap_fire"
	inst.poisonsymbol = body_symbol
	MakeLargeBurnableCharacter(inst, body_symbol)
    MakeLargeFreezableCharacter(inst)
	inst.components.debuffable:SetFollowSymbol(body_symbol, 0, 0, 0)
	----------------------------------
	--Components
	PlayablePets.SetCommonLootdropper(inst, prefabname) --inst, prefaboverride
	
	inst:AddComponent("shedder")
	inst.components.shedder.shedItemPrefab = "honey"
	inst.components.shedder.shedHeight = 0.1
	inst.components.shedder:StartShedding(480) --Note: 480 is 1 day. 480 x n = n amount of days.
	----------------------------------
	--Eater--
    inst.components.eater:SetAbsorptionModifiers(1,1.5,1.5) --This might multiply food stats.
	---------------------------------
	--Physics and Shadows--
	MakeFlyingCharacterPhysics(inst, 1, 0.5)
    inst.DynamicShadow:SetSize(.8, .5)
    inst.Transform:SetFourFaced()
    inst.Transform:SetScale(mob.scale, mob.scale, mob.scale)
	PlayablePets.SetAmphibious(inst, nil, nil, true, nil, true)
	---------------------------------
	--Listeners--
	inst:ListenForEvent("equip", PlayablePets.CommonOnEquip) --Shows head when hats make heads disappear.
	inst:ListenForEvent("attacked", OnAttacked)
	---------------------------------
	--Forge--
	if PP_FORGE_ENABLED then
		inst.forge_fn = SetForge(inst)
	end
    ------------------------------------------------------
	--Respawning and Initializing functions--
	
	inst:ListenForEvent("respawnfromghost", function(inst) PlayablePets.RevRestore(inst, mob, true, true) end) --(inst, mob, isflying, iswebimmune, noshadow, ishuman)

    inst:DoTaskInTime(0, function(inst) PlayablePets.CommonSetChar(inst, mob) SetAlt(inst) end)
	inst:DoTaskInTime(3, function(inst) PlayablePets.SetSkin(inst, mob) end)
    inst:ListenForEvent("respawnfromghost", function() 
        inst:DoTaskInTime(5, function(inst) PlayablePets.CommonSetChar(inst, mob) end)
		inst:DoTaskInTime(5.1, function(inst) PlayablePets.SetSkin(inst, mob) end)
		inst:DoTaskInTime(5, function(inst) PlayablePets.RevRestore(inst, mob, true, true) SetAlt(inst) end)
    end)
	
	inst.OnSave = OnSave
    inst.OnLoad = OnLoad
	
    return inst	
end

return MakePlayerCharacter(prefabname, prefabs, assets, common_postinit, master_postinit, start_inv)
