local MakePlayerCharacter = require "prefabs/player_common"

---------------------------
----------==Notes==--------
--
---------------------------
local getskins = {}

local prefabname = "smallghostp"

local assets = 
{
	Asset("ANIM", "anim/ghost_kid.zip"),
    Asset("ANIM", "anim/ghost_build.zip"),
    Asset("SOUND", "sound/ghost.fsb"),
}

local prefabs = 
{	

}

local start_inv = 
{
	--'prefab',
	--"spear",
}

local start_inv2 =
{
   --"beehome",
}

-----------------------
--Stats--
local mob = 
{
	health = 100,
	hunger = 150,
	hungerrate = TUNING.WILSON_HUNGER_RATE, 
	sanity = 150,
	
	runspeed = TUNING.GHOST_SPEED * 3,
	walkspeed = TUNING.GHOST_SPEED,
	
	attackperiod = 1,
	damage = 10,
	range = 2,
	hit_range = 3,
	
	bank = "ghost_kid",
	build = "ghost_kid",
	shiny = "ghost_kid",
	
	scale = 1,
	stategraph = "SG"..prefabname,
	minimap = prefabname..".tex",	
}

--Loot that drops when you die, duh.
SetSharedLootTable('smallghostp',
{
    
})

--==============================================
--					Mob Functions
--==============================================
local function sethairstyle(inst, hairstyle)
    inst._hairstyle = hairstyle or math.random(0, 3)
    if inst._hairstyle ~= 0 then
        inst.AnimState:OverrideSymbol("smallghost_hair", "ghost_kid", "smallghost_hair_"..tostring(inst._hairstyle))
    end
end
--==============================================
--				Custom Common Functions
--==============================================

--==============================================
--					Loading/Saving
--==============================================
 
local function OnLoad(inst, data)
	if data ~= nil then
		inst.mobteleported = data.mobteleported or false
		inst.isshiny = data.isshiny or 0
		if data.hairstyle then
			inst._hairstyle = data.hairstyle or 0
			sethairstyle(inst, inst._hairstyle)
		end
	end
end

local function OnSave(inst, data)
	data.mobteleported = inst.mobteleported or false
	data.isshiny = inst.isshiny or 0
	data.hairstyle = inst._hairstyle or 0
end

--==============================================
--					Forged Forge
--==============================================

local ex_fns = require "prefabs/player_common_extensions"

local function SetForge(inst)
	PlayablePets.SetForgeStats(inst, PP_FORGE.GHOST_KID)
	
	inst.mobsleep = false	
	inst.specialsleep = nil
	
	inst.revive_delay = 1
	
	--inst:ListenForEvent("attacked", OnAttacked_Forge) --Shows head when hats make heads disappear.
	inst.components.health:StopRegen()
	
	inst:DoTaskInTime(0, function(inst)
	inst:AddComponent("itemtyperestrictions")
	inst.components.itemtyperestrictions:SetRestrictions({"melees"})
	end)
	
	inst.components.combat:SetDamageType(2)
	
	inst:AddComponent("corpsereviver")
	inst.components.corpsereviver:SetReviverSpeedMult(0.1)
	inst.components.corpsereviver:SetAdditionalReviveHealthPercent(0)
	
	inst.components.revivablecorpse.revivespeedmult = 999
	
	inst:RemoveEventCallback("respawnfromcorpse", ex_fns.OnRespawnFromPlayerCorpse)
	inst:ListenForEvent("respawnfromcorpse", PlayablePets.OnRespawnFromMobCorpse)
end

--==============================================
--					Common/Master
--==============================================

local common_postinit = function(inst) 
	inst.MiniMapEntity:SetIcon(mob.minimap)

	inst:DoTaskInTime(0, function() 
		if ThePlayer then
			inst:EnableMovementPrediction(false) --PP doesn't work with movement prediction enabled, due to use of custom stategraphs
		end
	end)
	
	
	----------------------------------
	--Tags--
	inst:AddTag("ghost")
    inst:AddTag("ghostkid")
    inst:AddTag("girl")
    inst:AddTag("noauradamage")
	----------------------------------

	inst:WatchWorldState( "isday", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "isdusk", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "isnight", function() PlayablePets.SetNightVision(inst)  end)
	inst:WatchWorldState( "iscaveday", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "iscavedusk", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "iscavenight", function() PlayablePets.SetNightVision(inst)  end)
	
	PlayablePets.SetNightVision(inst)
end



local master_postinit = function(inst)
	--Stats--
	PlayablePets.SetCommonStats(inst, mob, nil, true) --mob table, ishuman, ignorepvpmultiplier
	inst.components.health:StartRegen(2, 1.5)
	PlayablePets.SetCommonWeatherResistances(inst, 60, 20, 1) --heat, cold, wetness
	PlayablePets.SetCommonStatResistances(inst, 0, 0, 0) --fire, acid, poison
	----------------------------------
	--Variables	
	inst.specialsleep = true
	inst.taunt = true
	inst.taunt2 = true
	inst.shouldwalk = false
	inst.debuffimmune = true
	
	inst.getskins = getskins
	
	local body_symbol = "smallghost_tail"
	inst.poisonsymbol = body_symbol
	MakeSmallBurnableCharacter(inst, body_symbol)
    MakeSmallFreezableCharacter(inst, body_symbol)
	PlayablePets.SetStormImmunity(inst)
	----------------------------------
	--Components
	PlayablePets.SetCommonLootdropper(inst, prefabname) --inst, prefaboverride
	----------------------------------
	--Eater--
    inst.components.eater:SetAbsorptionModifiers(1,1,1) --This multiplies food stats.
	inst.components.eater:SetCanEatHorrible() --check to make them eat monster meat.
	inst.components.eater.strongstomach = true -- can eat monster meat!
	inst.components.hunger:Pause()
	---------------------------------
	--Physics and Shadows--
	MakeTinyGhostPhysics(inst, 0.5, 0.5)
	inst.Transform:SetTwoFaced()
	inst.DynamicShadow:SetSize(0.75, 0.75)

	inst.SoundEmitter:PlaySound("dontstarve/ghost/ghost_girl_howl_LP", "howl")
	
    inst.AnimState:SetBloomEffectHandle("shaders/anim_bloom_ghost.ksh")
	
	PlayablePets.SetAmphibious(inst, nil, nil, true)
	--inst.DynamicShadow:Enable(false)  
	---------------------------------
	--Listeners--
	inst:ListenForEvent("equip", PlayablePets.CommonOnEquip) --Shows head when hats make heads disappear.
	---------------------------------
	--Forge--
	if TheNet:GetServerGameMode() == "lavaarena" then
		inst.forge_fn = SetForge(inst)
	end
    ------------------------------------------------------
	--Respawning and Initializing functions--
	
	inst:ListenForEvent("respawnfromghost", function(inst) PlayablePets.RevRestore(inst, mob, true, true) end) --(inst, mob, isflying, iswebimmune, noshadow, ishuman)

    inst:DoTaskInTime(0, function(inst) PlayablePets.CommonSetChar(inst, mob) end)
	inst:DoTaskInTime(3, function(inst) PlayablePets.SetSkin(inst, mob) end)
    inst:ListenForEvent("respawnfromghost", function() 
        inst:DoTaskInTime(5, function(inst) PlayablePets.CommonSetChar(inst, mob) end)
		inst:DoTaskInTime(5.1, function(inst) PlayablePets.SetSkin(inst, mob) end)
		inst:DoTaskInTime(5, function(inst) PlayablePets.RevRestore(inst, mob, true, true) inst.components.hunger:Pause() end)
    end)
	
	inst.OnSave = OnSave
    inst.OnLoad = OnLoad
	
    return inst	
end

return MakePlayerCharacter(prefabname, prefabs, assets, common_postinit, master_postinit, start_inv)