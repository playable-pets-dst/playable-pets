local MakePlayerCharacter = require "prefabs/player_common"

---------------------------
----------==Notes==--------
--
---------------------------

local prefabname = "smallbirdp"

local assets = 
{
	Asset("ANIM", "anim/smallbird_basic.zip"),
	Asset("ANIM", "anim/ds_tallbird_basic.zip"),
    Asset("ANIM", "anim/tallbird_teen_basic.zip"),
    Asset("ANIM", "anim/tallbird_teen_build.zip"),
	
	Asset("ANIM", "anim/smallbird_shiny_build_01.zip"),
	Asset("ANIM", "anim/teenbird_shiny_build_01.zip"),
	Asset("ANIM", "anim/tallbird_shiny_build_01.zip"),
}

local prefabs = {
}

local start_inv = {
}

local start_inv2 = 
{

}

local getskins = {"1"}

if MOBHOUSE == "Enable1" or MOBHOUSE == "Enable3" then
	start_inv = start_inv2
end
-----------------------
--Stats--
local mob = 
{
	health = TUNING.SMALLBIRDP_HEALTH ,
	hunger = TUNING.SMALLBIRDP_HUNGER,
	hungerrate = TUNING.WILSON_HUNGER_RATE, 
	sanity = TUNING.SMALLBIRDP_SANITY,
	
	runspeed = 6,
	walkspeed = 6,
	
	attackperiod = 0,
	damage = 10,
	range = 2,
	hit_range = 2,
	
	bank = "smallbird",
	build = "smallbird_basic",
	shiny = "smallbird",
	
	scale = 1,
	stategraph = "SGsmallbirdp",
	minimap = "smallbirdp.tex",	
}

local mob_teen = 
{
	health = TUNING.SMALLBIRDP_TEEN_HEALTH,
	hunger = TUNING.SMALLBIRDP_TEEN_HUNGER,
	hungerrate = TUNING.WILSON_HUNGER_RATE, 
	sanity = TUNING.SMALLBIRDP_TEEN_SANITY,
	
	runspeed = 8,
	walkspeed = 8,
	
	attackperiod = 2,
	damage = TUNING.TALLBIRD_DAMAGE,
	range = 3,
	hit_range = TUNING.TALLBIRD_ATTACK_RANGE,
	
	bank = "tallbird",
	build = "ds_tallbird_basic",
	shiny = "teenbird",
	
	scale = 0.8,
	stategraph = "SGtallbirdplayer",
	minimap = "tallbirdplayer.tex",	
}

local mob_adult = 
{
	health = TUNING.SMALLBIRDP_ADULT_HEALTH,
	hunger = TUNING.SMALLBIRDP_ADULT_HUNGER,
	hungerrate = TUNING.WILSON_HUNGER_RATE, 
	sanity = TUNING.SMALLBIRDP_ADULT_SANITY,
	
	runspeed = 7,
	walkspeed = 7,
	
	attackperiod = 2,
	damage = TUNING.TALLBIRD_DAMAGE,
	range = 3,
	hit_range = TUNING.TALLBIRD_ATTACK_RANGE,
	
	bank = "tallbird",
	build = "tallbird_teen_build",
	shiny = "tallbird",
	
	scale = 1,
	stategraph = "SGtallbirdplayer",
	minimap = "tallbirdplayer.tex",	
}

--Loot that drops when you die, duh.
SetSharedLootTable('tallbirdplayer',
{
    {'meat',             1.00},
    {'meat',             1.00},
})

foodprefs = {"VEGGIE", "SEEDS"}
food2prefs = {"MEAT", "VEGGIE"}

local FORGE_STATS = PP_FORGE.TALLBIRD



local TEEN_REQ = 30
local ADULT_REQ = 90
--==============================================
--					Mob Functions
--==============================================
local function GetPeepChance(inst)
    local peep_percent = 0.1
    if inst.components.hunger then
        if inst.components.hunger:IsStarving() then
            peep_percent = 1
        elseif inst.components.hunger:GetPercent() < .25 then
            peep_percent = 0.9
        elseif inst.components.hunger:GetPercent() < .5 then
            peep_percent = 0.75
        end
    end
    --print("smallbird - GetPeepChance", peep_percent)
    return peep_percent
end

local function onupdate(inst, dt)
    inst.charge_time = inst.charge_time - dt
    if inst.charge_time <= 0 then
        inst.charge_time = 0
		if inst.charge_task ~= nil then
            inst.charge_task:Cancel()
            inst.charge_task = nil
        end
        inst.specialatk = true
		inst.sg:GoToState("idle_peep")
    else
    --    
    end
end

local function OnLongUpdate(inst, dt)
    inst.charge_time = math.max(0, inst.charge_time - dt)
end

local function StartTimer(inst, duration)
    inst.charge_time = duration
	inst.specialatk = false

    if inst.charge_task == nil then
        inst.charge_task = inst:DoPeriodicTask(1, onupdate, nil, 1)
        onupdate(inst, 0)
    end
end
--==============================================
--				Custom Common Functions
--==============================================	
local function SetAdult(inst)
	inst.components.health:SetInvincible(false)
	PlayablePets.SetCommonStats(inst, mob_adult, nil, true) --mob table, ishuman, ignorepvpmultiplier
	PlayablePets.SetCommonWeatherResistances(inst, 60, 20, 1) --heat, cold, wetness
	PlayablePets.SetCommonStatResistances(inst) --fire, acid, poison
	if inst.isshiny and inst.isshiny ~= 0 then
		inst.AnimState:SetBuild("tallbird_shiny_build_0"..inst.isshiny)
	else
		inst.AnimState:SetBuild("ds_tallbird_basic")
	end
	inst.components.lootdropper:SetLoot({"meat", "meat"})
	inst.AnimState:Hide("beakfull")
	inst:AddTag("tallbird")
	inst:RemoveTag("smallbird")
	inst:RemoveTag("teenbird")
	inst:AddTag("mommy")
	
	inst.taunt2 = true
	
	inst.Transform:SetScale(1.0, 1.0, 1.0)
	
	inst.components.eater:SetDiet({ FOODGROUP.OMNI }, { FOODGROUP.OMNI })
	inst.components.eater:SetAbsorptionModifiers(1,1.5,1)
	
	local body_symbol = "head"
	inst.poisonsymbol = body_symbol
	MakeLargeBurnableCharacter(inst, body_symbol)
    MakeLargeFreezableCharacter(inst)
	inst.components.debuffable:SetFollowSymbol(body_symbol, 0, 0, 0)
	
	MakeCharacterPhysics(inst, 10, 1.33)
    inst.DynamicShadow:SetSize(2.75, 1)
end

local function SetTeen(inst)
	inst.components.health:SetInvincible(false)
	PlayablePets.SetCommonStats(inst, mob_teen, nil, true) --mob table, ishuman, ignorepvpmultiplier
	PlayablePets.SetCommonWeatherResistances(inst, 60, 20, 1) --heat, cold, wetness
	PlayablePets.SetCommonStatResistances(inst) --fire, acid, poison
	if inst.isshiny and inst.isshiny ~= 0 then
		inst.AnimState:SetBuild("teenbird_shiny_build_0"..inst.isshiny)
	else
		inst.AnimState:SetBuild("tallbird_teen_build")
	end
    inst.AnimState:Hide("beakfull")
	inst.components.lootdropper:SetLoot({"meat"})
	inst.components.eater:SetDiet({ FOODGROUP.OMNI }, { FOODGROUP.OMNI })
	inst.components.eater:SetAbsorptionModifiers(1,1.5,1)
	inst:AddTag("teenbird")
	inst:RemoveTag("smallbird")

	local body_symbol = "head"
	inst.poisonsymbol = body_symbol
	MakeLargeBurnableCharacter(inst, body_symbol)
    MakeLargeFreezableCharacter(inst)
	inst.components.debuffable:SetFollowSymbol(body_symbol, 0, 0, 0)
	
	inst.Transform:SetScale(.8, .8, .8)
	
	MakeCharacterPhysics(inst, 10, 0.25)
    inst.DynamicShadow:SetSize(2.75, 1)
end

local function OnGrow(inst)
	inst.Transform:SetPosition(inst.Transform:GetWorldPosition())
	inst.sg:GoToState("growup")
end

local function applyupgrades(inst)
    local health_percent = inst.components.health:GetPercent()
	local hunger_percent = inst.components.hunger:GetPercent()
	--local sanity_percent = inst.components.sanity:GetPercent()
	
	if inst.birdfood < TEEN_REQ then
		inst.components.health.maxhealth = mob.health
		inst.components.hunger.max = mob.hunger
	end
	
    if inst.birdfood >= TEEN_REQ and inst.birdfood < ADULT_REQ then
		inst.components.health.maxhealth = mob_teen.health
		inst.components.hunger.max = mob_teen.hunger	
		OnGrow(inst)
    end
	
	if inst.birdfood >= 90 then
		inst.components.health.maxhealth = mob_adult.health
		inst.components.hunger.max = mob_adult.hunger
		OnGrow(inst)
	end
	
    inst.components.health:SetPercent(health_percent)
end

local function OnEat(inst, food)
    if food and food.components.edible and MOB_TALLBIRD then
		if food.components.edible.foodtype == FOODTYPE.MEAT or food.components.edible.foodtype == FOODTYPE.VEGGIE or food.components.edible.foodtype == FOODTYPE.SEEDS  then
			PlayablePets.DebugPrint("Smallbird Debug: Food conditions met")
			--food helps you grow!
			if inst.birdfood < 255 then
				inst.birdfood = inst.birdfood + 1
			end
			PlayablePets.DebugPrint("Smallbird Debug: birdfood is now "..tostring(inst.birdfood))
			if inst.birdfood >= TEEN_REQ and inst.birdfood < ADULT_REQ and inst:HasTag("smallbird") then
				OnGrow(inst)
				applyupgrades(inst)
			end
		
			if inst.birdfood >= ADULT_REQ and not inst:HasTag("mommy") then
				OnGrow(inst)
				applyupgrades(inst)
			end
		end
    end
end

local function SetSkinDefault(inst, num)
	--Default
	if num ~= nil and num ~= 0 then
		if inst:HasTag("mommy") then
			inst.AnimState:SetBuild(mob_adult.shiny.."_shiny_build_0"..num)
		elseif inst:HasTag("teenbird") then	
			inst.AnimState:SetBuild(mob_teen.shiny.."_shiny_build_0"..num)
		else
			inst.AnimState:SetBuild(mob.shiny.."_shiny_build_0"..num)
		end
	else
		if inst:HasTag("mommy") then
			inst.AnimState:SetBuild(mob_adult.build)
		elseif inst:HasTag("teenbird") then
			inst.AnimState:SetBuild(mob_teen.build)
		else
			inst.AnimState:SetBuild(mob.build)			
		end		
	end	
end

local function ondeath(inst)
    inst.birdfood = 0
    applyupgrades(inst)
end
--==============================================
--					Loading/Saving
--==============================================
local function OnPreload(inst, data)
    if data ~= nil and data.birdfood ~= nil then
		--print("PreLoad birdfood setting - " .. data.birdfood or "ERROR")
        inst.birdfood = data.birdfood		
        applyupgrades(inst)
        --re-set these from the save data, because of load-order clipping issues
        if data.health and data.health.health then inst.components.health:SetCurrentHealth(data.health.health) end
        if data.hunger and data.hunger.hunger then inst.components.hunger.current = data.hunger.hunger end

        inst.components.health:DoDelta(0)
        inst.components.hunger:DoDelta(0)
        --inst.components.sanity:DoDelta(0)
    end
end
 
local function OnLoad(inst, data)
	if data ~= nil then
		--print("OnLoad birdfood setting - " .. data.birdfood or "ERROR") --This is coming up nil.... when the skin is on????????
		inst.birdfood = data.birdfood or 0			
		inst.mobteleported = data.mobteleported or false
		inst.isshiny = data.isshiny or 0
		if data.charge_time ~= nil then
			StartTimer(inst, data.charge_time)
		end
		if inst.birdfood then
			applyupgrades(inst) --for build reasons
		end
	end
end

local function OnSave(inst, data)
	data.birdfood = inst.birdfood > 0 and inst.birdfood or nil
	data.charge_time = inst.charge_time > 0 and inst.charge_time or nil
	data.mobteleported = inst.mobteleported or false
	data.isshiny = inst.isshiny or 0
end

--==============================================
--					Forged Forge
--==============================================

local ex_fns = require "prefabs/player_common_extensions"

local function SetForge(inst)
	PlayablePets.SetForgeStats(inst, FORGE_STATS)
	
	inst.mobsleep = false
	inst.taunt2 = false
	
	inst:DoTaskInTime(0, function(inst)
	inst:AddComponent("itemtyperestrictions")
	inst.components.itemtyperestrictions:SetRestrictions({"melees", "books", "staves", "darts"})
	end)
	
	inst.components.combat:SetDamageType(1)
	
	inst:RemoveEventCallback("respawnfromcorpse", ex_fns.OnRespawnFromPlayerCorpse)
	inst:ListenForEvent("respawnfromcorpse", PlayablePets.OnRespawnFromMobCorpse)
end

--==============================================
--					Common/Master
--==============================================

local common_postinit = function(inst) 
	inst.MiniMapEntity:SetIcon(mob.minimap)

	inst:DoTaskInTime(0, function() 
   if ThePlayer then
      inst:EnableMovementPrediction(false) --PP doesn't work with movement prediction enabled, due to use of custom stategraphs
   end
end)
	----------------------------------
	--Tags--
    inst:AddTag("smallcreature")
	inst:AddTag("smallbird")
	----------------------------------

	inst:WatchWorldState( "isday", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "isdusk", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "isnight", function() PlayablePets.SetNightVision(inst)  end)
	inst:WatchWorldState( "iscaveday", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "iscavedusk", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "iscavenight", function() PlayablePets.SetNightVision(inst)  end)
	
	PlayablePets.SetNightVision(inst)
end

local function LoadBuilds(inst) --awkward but blegh
	if inst.birdfood >= TEEN_REQ and inst.birdfood < ADULT_REQ then
		SetTeen(inst)
	elseif inst.birdfood >= ADULT_REQ then
		SetAdult(inst)
	end
end

local master_postinit = function(inst)
	--Stats--
	PlayablePets.SetCommonStats(inst, mob, nil, true) --mob table, ishuman, ignorepvpmultiplier
	PlayablePets.SetCommonWeatherResistances(inst) --heat, cold, wetness
	PlayablePets.SetCommonStatResistances(inst) --fire, acid, poison
	----------------------------------
	--Variables	
	inst.mobsleep = true
	inst.taunt = true
	inst.taunt2 = true
	inst.birdfood = 0
	inst.isshiny = 0
	inst.getskins = getskins
	inst.setskin_defaultfn = SetSkinDefault
	
	inst.userfunctions =
    {
        GetPeepChance = GetPeepChance,
        SetTeen = SetTeen,
        SetAdult = SetAdult,
		StartTimer = StartTimer,
    }
	
	inst.AnimState:Hide("beakfull")	
	
	local body_symbol = "head"
	inst.poisonsymbol = body_symbol
	MakeLargeBurnableCharacter(inst, body_symbol)
    MakeLargeFreezableCharacter(inst)
	inst.components.debuffable:SetFollowSymbol(body_symbol, 0, 0, 0)
	
	inst.userfunctions =
	{
		StartTimer = StartTimer,	
		SetAdult = SetAdult,
		SetTeen = SetTeen,
		GetPeepChance = GetPeepChance,
	}
	
	inst.charge_task = nil
    inst.charge_time = 0
	----------------------------------
	--Components
	inst:AddComponent("lootdropper")
	inst.components.lootdropper:SetLoot({"smallmeat"})
	
	inst:AddComponent("timer")
	----------------------------------
	--Eater--
	inst.components.eater:SetDiet({ FOODGROUP.BERRIES_AND_SEEDS }, { FOODGROUP.BERRIES_AND_SEEDS })
    inst.components.eater:SetAbsorptionModifiers(1,1.5,1)
	inst.components.eater:SetOnEatFn(OnEat)
	---------------------------------
	--Physics and Shadows--
	MakeCharacterPhysics(inst, 10, .5)
    inst.DynamicShadow:SetSize(1.25, .75)
    inst.Transform:SetFourFaced()
	---------------------------------
	--Listeners--
	inst:ListenForEvent("equip", PlayablePets.CommonOnEquip) --Shows head when hats make heads disappear..
	inst:ListenForEvent("death", ondeath)
	---------------------------------
	--Forge--
	if TheNet:GetServerGameMode() == "lavaarena" then
		inst.forge_fn = SetForge(inst)
	end
    ------------------------------------------------------
	--Respawning and Initializing functions--
	
	
	inst:ListenForEvent("respawnfromghost", function(inst) PlayablePets.RevRestore(inst, mob) end) --(inst, mob, isflying, iswebimmune, noshadow, ishuman)

    inst:DoTaskInTime(0, function(inst) PlayablePets.CommonSetChar(inst, mob) end)
	inst:DoTaskInTime(3, function(inst) PlayablePets.SetSkin(inst, mob) end)
    inst:ListenForEvent("respawnfromghost", function() 
        inst:DoTaskInTime(5, function(inst) PlayablePets.CommonSetChar(inst, mob) end)
		inst:DoTaskInTime(5.1, function(inst) PlayablePets.SetSkin(inst, mob) end)
		inst:DoTaskInTime(5, function(inst) PlayablePets.RevRestore(inst, mob) end)
    end)
	
	inst:DoTaskInTime(0.3, LoadBuilds)
	
	inst.OnPreload = OnPreload
	inst.OnSave = OnSave
    inst.OnLoad = OnLoad
	
    return inst	
end

return MakePlayerCharacter(prefabname, prefabs, assets, common_postinit, master_postinit, start_inv)