--WARNING, these buffs are primarily made for extra perks in the forged forge mod. Using any of these in base game will most likely crash you!
local function GetAllies(target, range, tags, notags, cap, onlyalive, countdead)
	if target and not target.components.health:IsDead() then
		local allies = {}
		local pos = target:GetPosition()
		local ents = TheSim:FindEntities(pos.x,0,pos.z, range and range or 255, tags and tags or nil, notags and notags or {"playerghost", "ghost", "battlestandard", "LA_mob" , "INLIMBO" } )
		if ents and #ents > 0 then
			for i, v in ipairs(ents) do
				if v and v.components.health and (onlyalive and not v.components.health:IsDead() or countdead) and v ~= target then
					table.insert(allies, v)
				elseif v and not onlyalive and v.components.health:IsDead() and v ~= target then
					table.insert(allies, v)
				end
				return #allies > 0 and 1 + #allies or 1
			end
		end	
	end		
end

local function OnAttached(inst, target)
	local colouraddspeed = 1 -- #seconds it takes to reach color
    inst.entity:SetParent(target.entity)
    inst:ListenForEvent("death", function()
        inst.components.debuff:Stop()
    end, target)
	if inst.playanim then
		inst.AnimState:PlayAnimation(inst.playanim)
	end
	if inst.colorindicator and target.components.colourfader then
		target.components.colourfader:StartFade(inst.colorindicator, .25)
	end
	if inst.type and inst.type == "atk" and (target and target.components.combat) then
		--target.components.combat.damagemultiplier = TUNING.FORGE.BATTLESTANDARD.ATK_BUFF
		target.components.combat:AddDamageBuff("atk_banner", inst.value, false)
		target.atkbuffed = true
	elseif inst.type and inst.type == "def" and (target and target.components.combat) then
		target.components.combat:AddDamageBuff("def_banner", inst.value, true)
		--[[if target.components.armorbreak_debuff ~= nil and target.components.armorbreak_debuff.debuffed == true then
			target.components.health.absorb = TUNING.FORGE.BATTLESTANDARD.DEF_BUFF - (0.02*target.components.armorbreak_debuff.debufflevel)
		else
			target.components.health.absorb = TUNING.FORGE.BATTLESTANDARD.DEF_BUFF 
		end]]
		target.defbuffed = true
	elseif inst.type and inst.type == "healer" and (target and target.components.health) then
		target.healbuffed = true
		target.components.health:StartRegen(inst.value and inst.value or 5, 1)
	elseif inst.type and inst.type == "wet" and (target and target.components.locomotor) then
		target.AnimState:SetMultColour(0.5, 0.6, 1, 1)
		target:AddTag("wet")
		target.components.locomotor:SetExternalSpeedMultiplier(inst, "wet_debuff", inst.value)
	end
end

local function OnTimerDone(inst, data)
    if data.name == inst.type.."buffover" then
        inst.components.debuff:Stop()
    end
end

local function OnExtended(inst, target)
    inst.components.timer:StopTimer(inst.type.."buffover")
    inst.components.timer:StartTimer(inst.type.."buffover", inst.duration)
end

local function OnDetached(inst, target)
	if not (target.components.debuffable and target.components.debuffable.debuffs["healingcircle_regenbuff"])
	and target.components.colourfader then
		target.components.colourfader:StartFade({0, 0, 0}, .25)
	end
	if inst.type and inst.type == "def" then
		if target and target.components.combat and not target.components.health:IsDead() then
			target.components.combat:RemoveDamageBuff("def_banner", true)
			--[[if not (target.sg:HasStateTag("hiding") or target.sg:HasStateTag("nobuff")) then
				target.components.health.absorb = -(0.02*target.components.armorbreak_debuff.debufflevel)
			end]]
			target.defbuffed = nil
		end
	elseif inst.type and inst.type == "atk" then
		if target and target.components.combat then
			target.components.combat:RemoveDamageBuff("atk_banner", false)
			--target.components.combat.damagemultiplier = 1
			target.atkbuffed = nil
		end
	elseif inst.type and inst.type == "healer" then
		if target and target.components.health then
			target.components.health:StopRegen()
			target.healbuffed = nil
		end
	elseif inst.type and inst.type == "wet" then
		if target then
			target:RemoveTag("wet")
			target.AnimState:SetMultColour(1, 1, 1, 1)
			target.components.locomotor:RemoveExternalSpeedMultiplier(target, "speed_banner")
		end
	end
	inst:Remove()
end

local function MakeBuff(name, bank, build, playanim ,type, value, duration, colorindicator)
	
	local function fn()
		local inst = CreateEntity()
		local trans = inst.entity:AddTransform()
		inst.entity:AddNetwork()
		inst.entity:AddAnimState()
		inst.entity:AddSoundEmitter()

		if bank and build then
        inst.AnimState:SetBank(bank)
        inst.AnimState:SetBuild(build)
		inst.AnimState:SetOrientation(ANIM_ORIENTATION.OnGround)
		inst.AnimState:SetLayer(LAYER_BACKGROUND)
		inst.AnimState:SetSortOrder(3)
		end
		
		inst.entity:SetPristine()

		if not TheWorld.ismastersim then
			return inst
		end
		
		inst:AddComponent("debuff")
		inst.components.debuff:SetAttachedFn(OnAttached)
		inst.components.debuff:SetDetachedFn(OnDetached)
		inst.components.debuff:SetExtendedFn(OnExtended)
		--inst.components.debuff.keepondespawn = true
	
		inst.duration = duration or TUNING.FORGE.BATTLESTANDARD.BUFF_WEAROFF_TIME
		inst.type = type or nil
		inst.value = value or nil
		inst.playanim = playanim or nil
		inst.colorindicator = colorindicator or nil

		inst:AddComponent("timer")
		inst:DoTaskInTime(0, function() -- in case we want to change duration
			inst.components.timer:StartTimer(inst.type.."buffover", inst.duration)
		end)
		inst:ListenForEvent("timerdone", OnTimerDone)
		return inst
	end

	return Prefab(name, fn)
end

------------------------------------------------------------------------------

return MakeBuff("hound_buff", "lavaarena_battlestandard", "lavaarena_battlestandard", "defend_fx", "def", 0.7, 3),
MakeBuff("pig_buff", "lavaarena_battlestandard", "lavaarena_battlestandard", "attack_fx3", "atk", 1.20, 3),
MakeBuff("spiderwarrior_buff", "lavaarena_battlestandard", "lavaarena_battlestandard", "defend_fx", "def", 0.6, 3),
MakeBuff("wspiderwarrior_buff", "lavaarena_battlestandard", "lavaarena_battlestandard", "attack_fx3", "atk", 1.25, 3),
MakeBuff("moose_debuff", nil, nil, nil, "atk", 0.5, 3, {0.5, 0.1, 0.1}),
MakeBuff("wet_debuff", nil, nil, nil, "wet", 0.9, 5)
