local MakePlayerCharacter = require "prefabs/player_common"
local BALLOONS = require "prefabs/balloons_common"
---------------------------
----------==Notes==--------
--
---------------------------

--Don't add assets unless absolutely needed.
local assets = 
{

}

--Might be completely pointless here. I don't know.
local prefabs = 
{	
	"balloonparty_confetti_balloon",
	"balloonparty_confetti_cloud",
	"balloonparty_buff",
}
	
--Note: Becareful when adding starting items.
--People are likely to spam changecharacter to get more of them.	
local start_inv = 
{

}
-----------------------
local prefabname = "balloon_partyp"
--Stats--
local mob = 
{
	health = TUNING[string.upper(prefabname.."_HEALTH")],
	hunger = TUNING[string.upper(prefabname.."_HUNGER")],
	hungerrate = 0, 
	sanity = TUNING[string.upper(prefabname.."_SANITY")],
	runspeed = 6*1.3,
	walkspeed = 6*1.3,
	damage = 5,
	range = 2,
	hit_range = 2,
	attackperiod = 0,
	bank = "balloon2",
	build = "balloon2",
	scale = 1,
	shiny = "balloon",
	--build2 = "snake_yellow_build",
	--build3 = "chester_snow_build",
	stategraph = "SGballoonp",
	minimap = "balloon_partyp.tex",
	
}
-----------------------
--Loot that drops when you die, duh.
SetSharedLootTable('balloonp',
-----Prefab---------------------Chance------------
{
   
})

local function OnLoad(inst, data)
	if data ~= nil then

    end
end

local function OnSave(inst, data)

end

local ex_fns = require "prefabs/player_common_extensions"

local function SetForge(inst)
	PlayablePets.SetForgeStats(inst, PP_FORGE.BALLOON)
	
	inst.mobsleep = nil
	
	inst:DoTaskInTime(0, function(inst)
	inst:AddComponent("itemtyperestrictions")
	inst.components.itemtyperestrictions:SetRestrictions({"darts", "melees", "books", "staves", "armors", "hats"})
	end)	

		--because FF forces character physics on us on spawn
	inst:DoTaskInTime(3, function(inst)
		inst.Physics:SetCollisionGroup(COLLISION.FLYERS)
		inst.Physics:CollidesWith(COLLISION.FLYERS)	
	end)
		
	inst.components.health:StopRegen()
	
	inst.components.combat:SetDamageType(1)
	
	inst.components.revivablecorpse.revivespeedmult = 0
	
	inst:RemoveEventCallback("respawnfromcorpse", ex_fns.OnRespawnFromPlayerCorpse)
	inst:ListenForEvent("respawnfromcorpse", PlayablePets.OnRespawnFromMobCorpse)
end

local common_postinit = function(inst) 
	--inst.soundsname = "beefalo"

	inst.MiniMapEntity:SetIcon(mob.minimap)
	--inst.AnimState:SetMultColour(1, 1, 1, 0.5)

	inst:DoTaskInTime(0, function() 
   if ThePlayer then
      inst:EnableMovementPrediction(false)
   end
end)	
end

local master_postinit = function(inst)
	------------------------------------------
	--Stats--
    PlayablePets.SetCommonStats(inst, mob, nil, true) --mob table, ishuman, ignorepvpmultiplier
	PlayablePets.SetCommonWeatherResistances(inst, 60, 20, 1) --heat, cold, wetness
	PlayablePets.SetCommonStatResistances(inst, 0, 1, 0) --fire, acid, poison
	inst.components.hunger:Pause()
	------------------------------------------
	BALLOONS.SetRopeShape(inst)
	----------------------------------
	--Loot drops--
	inst:AddComponent("lootdropper")
    inst.components.lootdropper:SetChanceLootTable('balloonp')
	----------------------------------
	--Tags--
	
	inst:RemoveTag("character") --This is removed to make certain mobs to not treat you like a player.
	inst:RemoveTag("scarytoprey")
	inst:AddTag("nopunch")
    inst:AddTag("cattoyairborne")
    inst:AddTag("balloon")
    inst:AddTag("noepicmusic")
	
	--inst.Physics:SetCollisionCallback(oncollide)

    inst.AnimState:OverrideSymbol("swap_balloon", "balloon_shapes_speed", "balloon_4")
	PlayablePets.SetStormImmunity(inst)
	---------------------------------
	--Physics and Scale--
	
	MakeCharacterPhysics(inst, 10, .25)
    inst.Physics:SetFriction(.3)
    inst.Physics:SetDamping(0)
    inst.Physics:SetRestitution(1)
    inst.DynamicShadow:SetSize(1, .5)
	--inst.DynamicShadow:Enable(false) --Disables shadows.
    inst.Transform:SetNoFaced()
	PlayablePets.SetAmphibious(inst, nil, nil, true)
	---------------------------------
	--Forge--
	if TheNet:GetServerGameMode() == "lavaarena" then
		inst.forge_fn = SetForge(inst)
	end
	---------------------------------
	--Listeners--
	
	inst:ListenForEvent("equip", PlayablePets.CommonOnEquip) --Shows head when hats make heads disappear.
	---------------------------------
	--Functions that saves and loads data.
	
	--inst.OnLongUpdate = onlongupdate
    inst.OnSave = OnSave
    inst.OnLoad = OnLoad
    --inst.OnPreLoad = onpreload
	------------------------------------------------------
	
    ------------------------------------------------------
	--Light and Character Functions. Don't Touch.--

	inst:DoTaskInTime(0, function(inst) PlayablePets.CommonSetChar(inst, mob, true) end)
	inst:DoTaskInTime(3, function(inst) PlayablePets.SetSkin(inst, mob) end)
    inst:ListenForEvent("respawnfromghost", function() 
        inst:DoTaskInTime(5, function(inst) PlayablePets.CommonSetChar(inst, mob, true) end)
		inst:DoTaskInTime(5.1, function(inst) PlayablePets.SetSkin(inst, mob) end)
		inst:DoTaskInTime(5, function(inst) PlayablePets.RevRestore(inst, mob, true) end) --(inst, mob, isflying, iswebimmune, noshadow, ishuman, amphibious)
    end)
	
    return inst
end

return MakePlayerCharacter("balloon_speedp", prefabs, assets, common_postinit, master_postinit, start_inv)
