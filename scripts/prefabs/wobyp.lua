local MakePlayerCharacter = require "prefabs/player_common"

---------------------------
----------==Notes==--------
--This is a template for nerds.
---------------------------

--Don't add assets unless absolutely needed.
local assets = 
{
	Asset("ANIM", "anim/pupington_build.zip"),
    Asset("ANIM", "anim/pupington_basic.zip"),
    Asset("ANIM", "anim/pupington_emotes.zip"),
    Asset("ANIM", "anim/pupington_traits.zip"),
    Asset("ANIM", "anim/pupington_jump.zip"),
    
    Asset("ANIM", "anim/pupington_woby_build.zip"),
    Asset("ANIM", "anim/pupington_transform.zip"),
    Asset("ANIM", "anim/woby_big_build.zip"),
}
--Might be completely pointless here. I don't know.
local prefabs = 
{	

}
	
--Note: Becareful when adding starting items.
--People are likely to spam changecharacter to get more of them.	
local start_inv = 
{
	--"prefab",
}

local start_inv2 = 
{
	--"prefabhome",
}

local prefabname = "wobyp"

-----------------------
local mob = 
{
	health = 150,
	hunger = 300,
	hungerrate = TUNING.WILSON_HUNGER_RATE,
	sanity = 100,
	runspeed = 6,
	walkspeed = 6,
	
	attackperiod = 0,
	damage = 20, --should only apply to big bernie.
	range = 2,
	hit_range = 2.5,
	
	bank = "pupington",
	build = "pupington_woby_build",
	scale = 1,
	shiny = "woby",
	--build2 = "alternate build here",
	stategraph = "SGwobyp",
	minimap = "wobyp.tex",
	
}

local mob_big = 
{
	health = 1000,
	hunger = 300,
	hungerrate = TUNING.WILSON_HUNGER_RATE,
	sanity = 100,
	runspeed = 6,
	walkspeed = TUNING.BERNIE_SPEED + 1,
	
	attackperiod = 0,
	damage = 50, --should only apply to big bernie.
	range = 30,
	hit_range = 0,
	
	bank = "wobybig",
	build = "woby_big_build",
	scale = 1,
	shiny = "bernie",
	--build2 = "alternate build here",
	stategraph = "SGwobybigp",
	minimap = "wobyp.tex",
	
}
-----------------------
--Loot that drops when you die, duh.
SetSharedLootTable('wobyp',
-----Prefab---------------------Chance------------
{
	
})


local function SetSmall(inst)
	inst.big = false
	inst.AnimState:SetBank(mob.bank)
	inst.AnimState:SetBank(mob.build)
	inst:RemoveTag("epic")
	inst.shouldwalk = true
	inst.Physics:SetMass(5)
	if TheNet:GetServerGameMode() == "lavaarena" then
		inst.components.itemtyperestrictions:SetRestrictions({"darts", "books", "staves"})
		inst.components.revivablecorpse.revivespeedmult = 0.5
	end
end

local function SetBig(inst)
	inst.big = true
	inst.Physics:SetMass(500)
	inst.AnimState:SetBank(mob_big.bank)
	inst.AnimState:SetBank(mob_big.build)
	inst:AddTag("epic")
	inst.shouldwalk = false
	if TheNet:GetServerGameMode() == "lavaarena" then
		inst.components.itemtyperestrictions:SetRestrictions({"darts", "melees", "books", "staves"})
		inst.components.revivablecorpse.revivespeedmult = 1.5
	end
end
-------------------------------------------------------
----PvP Teleport----

local function getwildposition(inst)
    local ground = TheWorld
    local centers = {}
    for i, node in ipairs(ground.topology.nodes) do
        if ground.Map:IsPassableAtPoint(node.x, 0, node.y) then
            table.insert(centers, {x = node.x, z = node.y})
        end
    end
    if #centers > 0 then
        local pos = centers[math.random(#centers)]
        return Point(pos.x, 0, pos.z)
    else
        return inst:GetPosition()
    end
end

local function PvPTeleport(inst)
if inst.mobteleported == nil or inst.mobteleported == false then
	local ground = TheWorld
	local pt = getwildposition(inst)--Point(inst.Transform:GetWorldPosition())
      if pt.y <= .1 then
         pt.y = 0
         inst.Physics:Stop()
         inst.Physics:Teleport(pt.x,pt.y,pt.z)
		 inst.mobteleported = true
      end
	end
end

------------------------------------------------------
 
local function OnLoad(inst, data)
	if data ~= nil then
		inst.mobteleported = data.mobteleported or false
		inst.isshiny = data.isshiny or 0
	end
end

local function OnSave(inst, data)
	data.mobteleported = inst.mobteleported or false
	data.isshiny = inst.isshiny or 0
end

--==============================================
--					Forged Forge
--==============================================


local ex_fns = require "prefabs/player_common_extensions"

local function SetForge(inst)
	PlayablePets.SetForgeStats(inst, PP_FORGE.WOBY)
	
	inst.mobsleep = nil
	
	inst:DoTaskInTime(0, function(inst)
	inst:AddComponent("itemtyperestrictions")
	inst.components.itemtyperestrictions:SetRestrictions({"darts", "melees", "books", "staves"})
	SetBig(inst)
	end)	
	
	inst.components.combat:SetDamageType(1)
	
	inst.components.revivablecorpse.revivespeedmult = 1.5
	
	inst:RemoveEventCallback("respawnfromcorpse", ex_fns.OnRespawnFromPlayerCorpse)
	inst:ListenForEvent("respawnfromcorpse", PlayablePets.OnRespawnFromMobCorpse)
end

local common_postinit = function(inst) 
	--inst.soundsname = "beefalo"

	inst.MiniMapEntity:SetIcon(mob.minimap)
	--inst.AnimState:SetMultColour(1, 1, 1, 0.5)

	inst:DoTaskInTime(0, function() 
   if ThePlayer then
      inst:EnableMovementPrediction(false)
   end
end)
	
end

local function OnDeath(inst)
	inst.big = false
	inst.foodcount = 0
end

local function OnHunger(inst)
	if inst.big and inst.components.hunger:GetPercent() < 0.5 then
		inst.foodcount = 0
		SetSmall(inst)
	end
end

local function OnEat(inst, food)
	if food.prefab == "monstermeat" and not inst.big and inst.components.hunger > 0.5 then
		inst.foodcount = inst.foodcount + 1
		if inst.foodcount >= 3 then
			SetBig(inst)
		end
	end
end

local master_postinit = function(inst)
	------------------------------------------
	--Stats--
    PlayablePets.SetCommonStats(inst, mob, nil, true) --mob table, ishuman, ignorepvpmultiplier
	PlayablePets.SetCommonWeatherResistances(inst, 60, 20) --heat, cold, wetness
	PlayablePets.SetCommonStatResistances(inst) --fire, acid, poison
	----------------------------------
	--Tags--
	inst:AddTag("smallcreature")
	
	inst.taunt = true
	inst.taunt2 = true
	inst.mobsleep = true
	inst.shouldwalk = true
	inst.big = false
	inst.foodcount = 0
	----------------------------------
	--Components
	PlayablePets.SetCommonLootdropper(inst, prefabname) --prefaboverride
	---------------------------------
	--Physics and Scale--
	
	MakeCharacterPhysics(inst, 1, .5)
    inst.DynamicShadow:SetSize(1, .33)
    inst.Transform:SetFourFaced()
	---------------------------------
	--Forge--
	if TheNet:GetServerGameMode() == "lavaarena" then
		inst.forge_fn = SetForge(inst)
	end
	---------------------------------
	--Listeners--
	inst:ListenForEvent("death", OnDeath) --Shows head when hats make heads disappear.
	inst:ListenForEvent("equip", PlayablePets.CommonOnEquip) --Shows head when hats make heads disappear.
	---------------------------------
	--Functions that saves and loads data.
	
	--inst.OnLongUpdate = onlongupdate
    inst.OnSave = OnSave
    inst.OnLoad = OnLoad
    --inst.OnPreLoad = onpreload
	------------------------------------------------------
	
    ------------------------------------------------------
	--Light and Character Functions. Don't Touch.--

	inst:DoTaskInTime(0, function(inst) PlayablePets.CommonSetChar(inst, mob) end)
	inst:DoTaskInTime(3, function(inst) PlayablePets.SetSkin(inst, mob) end)
    inst:ListenForEvent("respawnfromghost", function() 
        inst:DoTaskInTime(5, function(inst) PlayablePets.CommonSetChar(inst, mob) end)
		inst:DoTaskInTime(5.1, function(inst) PlayablePets.SetSkin(inst, mob) end)
		inst:DoTaskInTime(5, function(inst) PlayablePets.RevRestore(inst, mob) end) --(inst, mob, isflying, iswebimmune, noshadow, ishuman, amphibious)
    end)
	
    return inst
	
end

return MakePlayerCharacter("wobyp", prefabs, assets, common_postinit, master_postinit, start_inv)
