local MakePlayerCharacter = require "prefabs/player_common"

---------------------------


---------------------------


local assets = 
{
	Asset("ANIM", "anim/merm_build.zip"),
    Asset("ANIM", "anim/merm_guard_build.zip"),
    Asset("ANIM", "anim/merm_guard_small_build.zip"),
    Asset("ANIM", "anim/merm_actions.zip"),
    Asset("ANIM", "anim/merm_guard_transformation.zip"),    
    Asset("ANIM", "anim/ds_pig_boat_jump.zip"),
    Asset("ANIM", "anim/ds_pig_basic.zip"),
    Asset("ANIM", "anim/ds_pig_actions.zip"),
    Asset("ANIM", "anim/ds_pig_attacks.zip"),
    Asset("SOUND", "sound/merm.fsb"),
	Asset("ANIM", "anim/merm_shiny_build_01.zip"),
}

local getskins = {"1"}

local prefabs = 
{	

}
	
--Note: Becareful when adding starting items.
--People are likely to spam changecharacter to get more of them.	
local start_inv = 
{
	--"prefab",
}

local start_inv2 = 
{
	--"prefabhome",
}

if MOBHOUSE == "Enable1" or MOBHOUSE == "Enable3" then
	start_inv = start_inv2
end
-----------------------
local mob = 
{
	health = TUNING.MERMPLAYER_HEALTH,
	hunger = TUNING.MERMPLAYER_HUNGER,
	hungerrate = TUNING.WILSON_HUNGER_RATE,
	sanity = TUNING.MERMPLAYER_SANITY,
	runspeed = TUNING.MERM_RUN_SPEED,
	walkspeed = TUNING.MERM_WALK_SPEED,
	damage = 30,
	attackperiod = 0,
	range = 2,
	hitrange = 3,
	bank = "pigman",
	shiny = "merm",
	build = "merm_build",
	scale = 1,
	--build2 = "alternate build here",
	stategraph = "SGpig_commonp",
	minimap = "mermplayer.tex",
	
}
-----------------------
--Loot that drops when you die, duh.
SetSharedLootTable('mermp',
-----Prefab---------------------Chance------------
{
    {'fish',			1.00},
	{'froglegs',		1.00},
})

local sounds = {
	grunt = "dontstarve/creatures/merm/attack",
    attack = "dontstarve/creatures/merm/attack",
    hit = "dontstarve/creatures/merm/hurt",
    death = "dontstarve/creatures/merm/death",
    talk = "dontstarve/creatures/merm/attack",
} 


local function ontalk(inst, script)
    inst.SoundEmitter:PlaySound(inst.sounds.talk)
end

local function Equip(inst)
    local head = inst.components.inventory:GetEquippedItem(EQUIPSLOTS.HEAD)
	local hands = inst.components.inventory:GetEquippedItem(EQUIPSLOTS.HANDS)
	if head then
		inst.AnimState:Show("HEAD")
	end 
	if hands then
		inst.AnimState:Show("swap_object")
	end 	
end

local function Unequip(inst)
	local hands = inst.components.inventory:GetEquippedItem(EQUIPSLOTS.HANDS)
	if not hands then
		inst.AnimState:Hide("swap_object")
	end 
end 
 
local function OnLoad(inst, data)
	if data ~= nil then
		inst.mobteleported = data.mobteleported or false
		inst.isshiny = data.isshiny or 0
	end
end

local function OnSave(inst, data)
	data.mobteleported = inst.mobteleported or false
	data.isshiny = inst.isshiny or 0
end

local ex_fns = require "prefabs/player_common_extensions"

local function SetForge(inst)
	PlayablePets.SetForgeStats(inst, PP_FORGE.MERM)
	
	inst.mobsleep = false
	
	inst.components.combat:AddDamageBuff("atk_banner", 1.5, false)
	
	inst:DoTaskInTime(0, function(inst)
	inst:AddComponent("itemtyperestrictions")
		inst.components.itemtyperestrictions:SetRestrictions({"staves", "books"})
	end)
	
	inst.components.combat:SetDamageType(1)
	
	inst:RemoveEventCallback("respawnfromcorpse", ex_fns.OnRespawnFromPlayerCorpse)
	inst:ListenForEvent("respawnfromcorpse", PlayablePets.OnRespawnFromMobCorpse)
end

local common_postinit = function(inst) 
	--inst.soundsname = "beefalo"

	inst.MiniMapEntity:SetIcon(mob.minimap)
	--inst.AnimState:SetMultColour(1, 1, 1, 0.5)

	inst:DoTaskInTime(0, function() 
   if ThePlayer then
      inst:EnableMovementPrediction(false)
   end
end)
	
end

local master_postinit = function(inst) 
	------------------------------------------
	--Stats--
    PlayablePets.SetCommonStats(inst, mob, true, true) --mob table, ishuman, ignorepvpmultiplier
	PlayablePets.SetCommonWeatherResistances(inst, 60, 20) --heat, cold, wetness
	PlayablePets.SetCommonStatResistances(inst, nil, nil, 0.5) --fire, acid, poison, freeze (flat value, not a multiplier)
	----------------------------------
	--Loot drops--
	inst:AddComponent("lootdropper")
    inst.components.lootdropper:SetChanceLootTable('mermp')
	----------------------------------
	--Tags--
	
	inst:AddTag("merm")
	inst.taunt = true
	inst.taunt2 = true
	inst.mobsleep = true
	
	inst.isshiny = 0
	inst.getskins = getskins
	inst.sounds = sounds
	
	local body_symbol = "pig_torso"
	inst.poisonsymbol = body_symbol
	MakeMediumBurnableCharacter(inst, body_symbol)
    MakeMediumFreezableCharacter(inst, body_symbol)
	inst.components.debuffable:SetFollowSymbol(body_symbol, 0, 0, 0)
	
	PlayablePets.SetSkeleton(inst, "pp_skeleton_pig")
	----------------------------------
	--PvP Mode Stuff--
	inst.mobteleported = false
	----------------------------------
	--Eater--
    inst.components.eater:SetAbsorptionModifiers(1,1,1.5) --This might multiply food stats.
	inst.components.eater:SetDiet({ FOODTYPE.SEEDS, FOODTYPE.GENERIC ,FOODTYPE.VEGGIE }, { FOODTYPE.SEEDS, FOODTYPE.GENERIC, FOODTYPE.VEGGIE }) 
	---------------------------------
	--Physics and Scale--
	
	MakeCharacterPhysics(inst, 50, .5)
    inst.Transform:SetScale(mob.scale, mob.scale, mob.scale)
    inst.DynamicShadow:SetSize(1.5, .75)
    inst.Transform:SetFourFaced()
	---------------------------------
	--Listeners--
	inst:ListenForEvent("equip", PlayablePets.CommonOnEquipPig) --Shows head when hats make heads disappear.
	inst:ListenForEvent("unequip", PlayablePets.CommonOnUnequip)
	------------------------------------------------------
	--Forge--
	if TheNet:GetServerGameMode() == "lavaarena" then
		inst.forge_fn = SetForge(inst)
	end
	------------------------------------------------------
	--Respawning and Initializing functions--
	inst.components.talker.ontalk = ontalk
	
	inst:ListenForEvent("respawnfromghost", function(inst) PlayablePets.RevRestore(inst, mob, nil, nil, nil, true) end) --(inst, mob, isflying, iswebimmune, noshadow, ishuman)

    inst:DoTaskInTime(0, function(inst) PlayablePets.CommonSetChar(inst, mob) end)
	inst:DoTaskInTime(3, function(inst) PlayablePets.SetSkin(inst, mob) end)
    inst:ListenForEvent("respawnfromghost", function() 
        inst:DoTaskInTime(5, function(inst) PlayablePets.CommonSetChar(inst, mob) end)
		inst:DoTaskInTime(5.1, function(inst) PlayablePets.SetSkin(inst, mob) end)
		inst:DoTaskInTime(5, function(inst) PlayablePets.RevRestore(inst, mob, nil, nil, nil, true) end)
    end)
	
	inst.OnSave = OnSave
    inst.OnLoad = OnLoad
	
    return inst
	
end

return MakePlayerCharacter("mermplayer", prefabs, assets, common_postinit, master_postinit, start_inv)
