local MakePlayerCharacter = require "prefabs/player_common"

---------------------------


---------------------------


local assets =
{
    Asset("ANIM", "anim/mossling_build.zip"),
    Asset("ANIM", "anim/mossling_basic.zip"),
    Asset("ANIM", "anim/mossling_actions.zip"),
    Asset("ANIM", "anim/mossling_angry_build.zip"),
    Asset("ANIM", "anim/mossling_yule_build.zip"),
    Asset("ANIM", "anim/mossling_yule_angry_build.zip"),
	Asset("ANIM", "anim/mossling_angry_build.zip"),
	
	Asset("ANIM", "anim/mossling_shiny_build_01.zip"),
	Asset("ANIM", "anim/mossling_angry_shiny_build_01.zip"),
    -- Asset("SOUND", "sound/mossling.fsb"),
}

local prefabs =
{
    "mossling_spin_fx",
    "goose_feather",
    "drumstick",
}

local getskins = {"1"}
	
--Note: Becareful when adding starting items.
--People are likely to spam changecharacter to get more of them.	
local start_inv = 
{
	--"prefab",
}

local start_inv2 = 
{
	--"prefabhome",
}

if MOBHOUSE == "Enable1" or MOBHOUSE == "Enable3" then
	start_inv = start_inv2
end
-----------------------
local mob = 
{
	health = TUNING.BABYGOOSEPLAYER_HEALTH,
	hunger = TUNING.BABYGOOSEPLAYER_HUNGER,
	hungerrate = TUNING.WILSON_HUNGER_RATE,
	sanity = TUNING.BABYGOOSEPLAYER_SANITY,
	runspeed = TUNING.MOOSE_WALK_SPEED,
	walkspeed = TUNING.MOOSE_WALK_SPEED,
	damage = 30*2,
	attackperiod = 2,
	range = TUNING.MOSSLING_ATTACK_RANGE,
	bank = "mossling",
	build = "mossling_build",
	shiny = "mossling",
	scale = 1,
	--build2 = "alternate build here",
	stategraph = "SGbabygooseplayer",
	minimap = "babygooseplayer.tex",
	
}
-----------------------
--Loot that drops when you die, duh.
SetSharedLootTable( 'babygooseplayer',
{
    {'drumstick',        1.00},
    {'drumstick',        1.00},
    {'meat',   			 1.00},
    {'goose_feather',    1.00},
    {'goose_feather',    1.00},
    {'goose_feather',    0.33},
    {'goose_feather',    0.33},
})

local function OnLoad(inst, data)
	if data ~= nil then
		inst.mobteleported = data.mobteleported or false
		inst.isshiny = data.isshiny or 0
	end
end

local function OnSave(inst, data)
	data.mobteleported = inst.mobteleported or false
	data.isshiny = inst.isshiny or 0
end
-------------------------------------------------------

------------------------------------------------------

local common_postinit = function(inst) 
	--inst.soundsname = "beefalo"

	inst.MiniMapEntity:SetIcon(mob.minimap)
	--inst.AnimState:SetMultColour(1, 1, 1, 0.5)

	inst:DoTaskInTime(0, function() 
		if ThePlayer then
			inst:EnableMovementPrediction(false)
		end
	end)
	
end

local function OnHitOther(inst, data)
	local other = data.target
	if other and other.components.freezable then
		other.components.freezable:AddColdness(2)
		other.components.freezable:SpawnShatterFX()
	end
end

--==============================================
--					Forged Forge
--==============================================

local function ShockOnHit(inst, data)
	if data.attacker and data.attacker.components.combat and inst.sg:HasStateTag("spinning") then
		data.attacker.components.combat:GetAttacked(inst, 10, nil, "electric")
	end
end

local ex_fns = require "prefabs/player_common_extensions"

local function SetForge(inst)
	PlayablePets.SetForgeStats(inst, PP_FORGE.MOSSLING)
	
	inst.mobsleep = false	
	
	inst:DoTaskInTime(0, function(inst)
		inst:AddComponent("itemtyperestrictions")
		inst.components.itemtyperestrictions:SetRestrictions({"books", "staves", "darts", "melees"})
	end)
	
	inst.components.combat:SetDamageType(1)
	
	inst.components.revivablecorpse.revivespeedmult = 1.5
	
	inst:ListenForEvent("attacked", ShockOnHit)
	
	inst:RemoveEventCallback("respawnfromcorpse", ex_fns.OnRespawnFromPlayerCorpse)
	inst:ListenForEvent("respawnfromcorpse", PlayablePets.OnRespawnFromMobCorpse)
end
----------------------------------------------
local function SetSkinDefault(inst, num)
	--Default
	if num ~= nil and num ~= 0 then
		--print("DEBUG:SetSkinDefault shinizer ran")
		if inst.isangry ~= nil and inst.isangry == true then
			inst.AnimState:SetBuild(mob.shiny.."_angry_shiny_build_0"..num)	
		else
			inst.AnimState:SetBuild(mob.shiny.."_shiny_build_0"..num)
		end
	else
		if inst.isangry ~= nil and inst.isangry == true then
			inst.AnimState:SetBuild("mossling_angry_build")	
		else
			inst.AnimState:SetBuild(mob.build)
		end
	end	
end

local master_postinit = function(inst) 
	------------------------------------------
	--Stats--
    PlayablePets.SetCommonStats(inst, mob, nil, true) --mob table, ishuman, ignorepvpmultiplier
	PlayablePets.SetCommonWeatherResistances(inst, 60, 20, 1) --heat, cold, wetness
	PlayablePets.SetCommonStatResistances(inst) --fire, acid, poison, freeze (flat value, not a multiplier)
	----------------------------------
	--Loot drops--
	inst:AddComponent("lootdropper")
    inst.components.lootdropper:SetChanceLootTable('babygooseplayer')
	
	inst:AddComponent("sizetweener")
	----------------------------------
	--Tags--
	
	inst:AddTag("mossling")    
    inst:AddTag("animal")
	
	inst.isangry = false
	inst.altattack = true
	inst.taunt = true
	inst.taunt2 = true
	inst.mobsleep = true
	inst.isshiny = 0
	
	inst.getskins = getskins
	inst.setskin_defaultfn = SetSkinDefault
	
	
	local body_symbol = "swap_fire"
	inst.poisonsymbol = body_symbol
    MakeHugeFreezableCharacter(inst, body_symbol)
	inst.components.debuffable:SetFollowSymbol(body_symbol, 0, 0, 0)
	MakeLargeBurnableCharacter(inst, body_symbol)	
	----------------------------------
	--SanityAura--
	--inst:AddComponent("sanityaura")
	
	----------------------------------
	--PvP Mode Stuff--
	inst.mobteleported = false
	----------------------------------
	--Eater--
    inst.components.eater:SetAbsorptionModifiers(2,1.5,1.5) --This might multiply food stats.
	inst.components.eater:SetCanEatHorrible() --check to make them eat monster meat.
	inst.components.eater.strongstomach = true -- can eat monster meat!
	inst.components.eater.eatwholestack = true --should eat the entire stack and get all the benefits.
	---------------------------------
	--Physics and Scale--
	
	MakeGiantCharacterPhysics(inst, 50, 0.5)
    inst.DynamicShadow:SetSize(1.5, 1.25)
    inst.Transform:SetFourFaced()
	---------------------------------
	---------------------------------
	--Listeners--
	inst:ListenForEvent("equip", PlayablePets.CommonOnEquip) --Shows head when hats make heads disappear.
	------------------------------------------------------
	--Forge--
	if TheNet:GetServerGameMode() == "lavaarena" then
		inst.forge_fn = SetForge(inst)
	end
	------------------------------------------------------
	--Respawning and Initializing functions--
	
	inst:ListenForEvent("respawnfromghost", function(inst) PlayablePets.RevRestore(inst, mob) end) --(inst, mob, isflying, iswebimmune, noshadow, ishuman)

    inst:DoTaskInTime(0, function(inst) PlayablePets.CommonSetChar(inst, mob) end)
	inst:DoTaskInTime(3, function(inst) PlayablePets.SetSkin(inst, mob) end)
    inst:ListenForEvent("respawnfromghost", function() 
        inst:DoTaskInTime(5, function(inst) PlayablePets.CommonSetChar(inst, mob) end)
		inst:DoTaskInTime(5.1, function(inst) PlayablePets.SetSkin(inst, mob) end)
		inst:DoTaskInTime(5, function(inst) PlayablePets.RevRestore(inst, mob) end)
    end)
	
	inst.OnSave = OnSave
    inst.OnLoad = OnLoad
	
    return inst
	
end




return MakePlayerCharacter("babygooseplayer", prefabs, assets, common_postinit, master_postinit, start_inv)
