local MakePlayerCharacter = require "prefabs/player_common"

---------------------------
----------==Notes==--------
--
---------------------------

local prefabname = "catplayer"

local assets = 
{
	Asset("ANIM", "anim/catcoon_build.zip"),
	Asset("ANIM", "anim/catcoon_basic.zip"),
	Asset("ANIM", "anim/catcoon_actions.zip"),
	Asset("SOUND", "sound/catcoon.fsb"),
	--Asset("ANIM", "anim/ghost_waxwell_build.zip"),
	Asset("ANIM", "anim/catcoon_shiny_build_01.zip"),
	Asset("ANIM", "anim/catcoon_shiny_build_02.zip"),
}

local prefabs = {
	"mole",
	"rabbit",
	"flint",
	"tumbleweed",
	"cutgrass",
	"twigs",
	"berries",
	"goldnugget",
	"smallmeat",
	"silk",
	"coontail",
	"rocks",
	"bee",
	"mosquito",
	"cutreeds",
	"tentaclespots",
	"beefalowool",
	"feather_robin",
	"feather_robin_winter",
	"feather_crow",
	"boneshard",
	"red_cap",
	"blue_cap",
	"green_cap",
	"carrot_seeds",
	"corn_seeds",
	"pumpkin_seeds",
	"eggplant_seeds",
	"durian_seeds",
	"pomegranate_seeds",
	"dragonfruit_seeds",
	"watermelon_seeds",
	"butterfly",
	"robin",
	"robin_winter",
	"crow",
	"fish",
	"transistor",
	"froglegs",
	"batwing",
	"petals",
	"petals_evil",
	"ash",
	"acorn",
	"pinecone",
	"ice",
    }

local start_inv = {
}

local start_inv2 = 
{
	"cathome",
}

local getskins = {"1", "2"}

if MOBHOUSE == "Enable1" or MOBHOUSE == "Enable3" then
	start_inv = start_inv2
end
-----------------------
--Stats--
local mob = 
{
	health = TUNING.CATCOON_LIFE,
	hunger = 125,
	hungerrate = TUNING.WILSON_HUNGER_RATE, 
	sanity = 50,
	
	runspeed = TUNING.CATCOON_SPEED,
	walkspeed = TUNING.CATCOON_SPEED,
	
	attackperiod = 0,
	damage = TUNING.CATCOON_DAMAGE,
	range = TUNING.CATCOON_ATTACK_RANGE,
	hit_range = TUNING.CATCOON_MELEE_RANGE,
	
	bank = "catcoon",
	build = "catcoon_build",
	shiny = "hound",
	
	scale = 1,
	stategraph = "SG"..prefabname,
	minimap = prefabname..".tex",	
}

--Loot that drops when you die, duh.
SetSharedLootTable( 'catplayer',
{
	{'meat',             1.00},
    {'coontail',		 0.33},
})


local FORGE_STATS = PP_FORGE.CATCOON
--==============================================
--					Mob Functions
--==============================================

local function onupdate(inst, dt)
    inst.charge_time = inst.charge_time - dt
    if inst.charge_time <= 0 then
        inst.charge_time = 0
		if inst.charge_task ~= nil then
            inst.charge_task:Cancel()
            inst.charge_task = nil
        end
        inst.taunt2 = true
		
    else
    --    
    end
end

local function OnLongUpdate(inst, dt)
    inst.charge_time = math.max(0, inst.charge_time - dt)
end

local function StartTimer(inst, duration)
    inst.charge_time = duration
	inst.taunt2 = false

    if inst.charge_task == nil then
        inst.charge_task = inst:DoPeriodicTask(1, onupdate, nil, 1)
        onupdate(inst, 0)
    end
end

--==============================================
--				Custom Common Functions
--==============================================

--==============================================
--					Loading/Saving
--==============================================
 
local function OnLoad(inst, data)
	if data ~= nil and data.charge_time ~= nil then
        StartTimer(inst, data.charge_time)
    end
	if data ~= nil then
		data.charge_time = inst.charge_time > 0 and inst.charge_time or nil
		inst.mobteleported = data.mobteleported or false
		inst.isshiny = data.isshiny or 0
	end
end

local function OnSave(inst, data)
	data.mobteleported = inst.mobteleported or false
	data.isshiny = inst.isshiny or 0
	data.charge_time = inst.charge_time > 0 and inst.charge_time or nil
end

--==============================================
--					Forged Forge
--==============================================

local ex_fns = require "prefabs/player_common_extensions"

local function SetForge(inst)
	PlayablePets.SetForgeStats(inst, FORGE_STATS)
	
	inst.mobsleep = false	
	
	inst:DoTaskInTime(0, function(inst)
	inst:AddComponent("itemtyperestrictions")
	inst.components.itemtyperestrictions:SetRestrictions({"books", "staves"})
	end)
	
	inst.components.combat:SetDamageType(1)
	
	inst:RemoveEventCallback("respawnfromcorpse", ex_fns.OnRespawnFromPlayerCorpse)
	inst:ListenForEvent("respawnfromcorpse", PlayablePets.OnRespawnFromMobCorpse)
end

--==============================================
--					Common/Master
--==============================================

local common_postinit = function(inst) 
	inst.MiniMapEntity:SetIcon(mob.minimap)

	inst:DoTaskInTime(0, function() 
   if ThePlayer then
      inst:EnableMovementPrediction(false) --PP doesn't work with movement prediction enabled, due to use of custom stategraphs
   end
end)
	----------------------------------
	--Tags--
    inst:AddTag("smallcreature")
	inst:AddTag("animal")
	inst:AddTag("catcoon")
	inst:AddTag("catcoonp") --TODO investigate why this is here
	----------------------------------

	inst:WatchWorldState( "isday", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "isdusk", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "isnight", function() PlayablePets.SetNightVision(inst)  end)
	inst:WatchWorldState( "iscaveday", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "iscavedusk", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "iscavenight", function() PlayablePets.SetNightVision(inst)  end)
	
	PlayablePets.SetNightVision(inst)
end



local master_postinit = function(inst)
	--Stats--
	PlayablePets.SetCommonStats(inst, mob, nil, true) --mob table, ishuman, ignorepvpmultiplier
	PlayablePets.SetCommonWeatherResistances(inst, 60, 20, 1) --heat, cold, wetness
	PlayablePets.SetCommonStatResistances(inst) --fire, acid, poison
	inst.components.combat:SetHurtSound("dontstarve_DLC001/creatures/catcoon/hurt")	
	----------------------------------
	--Variables	
	inst.mobsleep = true
	inst.taunt = true
	inst.taunt2 = true
	inst.shouldwalk = true
	inst.numretches = 0
	
	inst.getskins = getskins
	
	inst.userfunctions =
	{
		StartTimer = StartTimer,	
	}
	
	inst.charge_task = nil
    inst.charge_time = 0
	
	local body_symbol = "catcoon_torso"
	inst.poisonsymbol = body_symbol
	MakeSmallBurnableCharacter(inst, body_symbol, Vector3(1,0,1))
	MakeSmallFreezableCharacter(inst)
	inst.components.debuffable:SetFollowSymbol(body_symbol, 0, 0, 0)
	----------------------------------
	--Components
	PlayablePets.SetCommonLootdropper(inst, prefabname) --inst, prefaboverride
	----------------------------------
	--Eater--
	
	---------------------------------
	--Physics and Shadows--
	inst.DynamicShadow:SetSize(2,0.75)
	inst.Transform:SetFourFaced()
	MakeCharacterPhysics(inst, 1, 0.5)
	---------------------------------
	--Listeners--
	inst:ListenForEvent("equip", PlayablePets.CommonOnEquip) --Shows head when hats make heads disappear.
	---------------------------------
	--Forge--
	if TheNet:GetServerGameMode() == "lavaarena" then
		inst.forge_fn = SetForge(inst)
	end
    ------------------------------------------------------
	--Respawning and Initializing functions--
	
	inst:ListenForEvent("respawnfromghost", function(inst) PlayablePets.RevRestore(inst, mob) end) --(inst, mob, isflying, iswebimmune, noshadow, ishuman)

    inst:DoTaskInTime(0, function(inst) PlayablePets.CommonSetChar(inst, mob) end)
	inst:DoTaskInTime(3, function(inst) PlayablePets.SetSkin(inst, mob) end)
    inst:ListenForEvent("respawnfromghost", function() 
        inst:DoTaskInTime(5, function(inst) PlayablePets.CommonSetChar(inst, mob) end)
		inst:DoTaskInTime(5.1, function(inst) PlayablePets.SetSkin(inst, mob) end)
		inst:DoTaskInTime(5, function(inst) PlayablePets.RevRestore(inst, mob) end)
    end)
	
	inst.OnLongUpdate = OnLongUpdate
	inst.OnSave = OnSave
    inst.OnLoad = OnLoad
	
    return inst	
end

return MakePlayerCharacter(prefabname, prefabs, assets, common_postinit, master_postinit, start_inv)