local MakePlayerCharacter = require "prefabs/player_common"

---------------------------
----------==Notes==--------
--
---------------------------
local prefabname = "chesterp"

local assets = 
{
	Asset("ANIM", "anim/chester_shiny_build_01.zip"),
	Asset("ANIM", "anim/chester_snow_shiny_build_01.zip"),
	Asset("ANIM", "anim/chester_shadow_shiny_build_01.zip"),
}

local getskins = {"1"}

local prefabs = 
{	

}
	
--Note: Becareful when adding starting items.
--People are likely to spam changecharacter to get more of them.	
local start_inv = 
{
	--'prefab',
	--"spear",
}

local start_inv2 =
{
   --"beehome",
}

if MOBHOUSE== "Enable1" or MOBHOUSE== "Enable3" then
	start_inv = start_inv2
end
-----------------------
--Stats--
local mob = 
{
	health = 350,
	hunger = 100,
	hungerrate = TUNING.WILSON_HUNGER_RATE, --I think .25 is defualt.
	sanity = 100,
	runspeed = 7,
	walkspeed = 7,
	damage = 10*2,
	range = 1.3,
	hit_range = 2,
	attackperiod = 0,
	bank = "chester",
	build = "chester_build",
	scale = 1,
	build2 = "chester_shadow_build",
	build3 = "chester_snow_build",
	stategraph = "SGchesterp",
	minimap = "chester.png",
	
}
-----------------------
--Loot that drops when you die, duh.
SetSharedLootTable('chesterp',
-----Prefab---------------------Chance------------
{
    {'meat',             1.00},  
})

FORGE_STATS = PP_FORGE[string.upper(prefabname)] 
--==============================================
--					Mob Functions
--==============================================
local sounds =
{
    hurt = "dontstarve/creatures/chester/hurt",
    pant = "dontstarve/creatures/chester/pant",
    death = "dontstarve/creatures/chester/death",
    open = "dontstarve/creatures/chester/open",
    close = "dontstarve/creatures/chester/close",
    pop = "dontstarve/creatures/chester/pop",
    boing = "dontstarve/creatures/chester/boing",
    lick = "dontstarve/creatures/chester/lick",
}

local function MorphShadowChester(inst)
	if inst.isshiny == 0 then
		inst.AnimState:SetBuild("chester_shadow_build")
	else
		inst.AnimState:SetBuild("chester_shadow_shiny_build_0"..inst.isshiny)
	end
    inst:AddTag("spoiler")
    inst.MiniMapEntity:SetIcon("chestershadow.png")
	inst:AddTag("monster")
	
	inst:AddComponent("shedder")
	inst.components.shedder.shedItemPrefab = "nightmarefuel"
	inst.components.shedder.shedHeight = 0.1
	inst.components.shedder:StartShedding(480 * 5) --Note: 480 is 1 day. 480 x n = n amount of days.
	
	inst.skinn = 1

    inst.ChesterState = "SHADOW"
end

local function MorphSnowChester(inst)
	if inst.isshiny == 0 then
		inst.AnimState:SetBuild("chester_snow_build")
	else
		inst.AnimState:SetBuild("chester_snow_shiny_build_0"..inst.isshiny)
	end
    inst:AddTag("fridge")
    inst.MiniMapEntity:SetIcon("chestersnow.png")

    inst.skinn = 2
end

local function IsIce(item)
	return item.prefab == "bluegem" or nil
end

local function IsNightmareFuel(item)
	return item.prefab == "nightmarefuel" or nil
end

local function OnTransform(inst)
	local fuel = inst.components.inventory:FindItem(IsNightmareFuel)
	local ice = inst.components.inventory:FindItem(IsIce)
	local req = 9
	if inst.skinn == 0 then
		if inst.components.inventory ~= nil and fuel ~= nil and fuel.components.stackable.stacksize >= req then
			if ice == nil or ice.components.stackable.stacksize <= req then -- or ice ~= nil and not ice.components.stackable.stacksize >= req then
			inst.components.inventory:ConsumeByName("nightmarefuel", 9)
			inst:StopWatchingWorldState("isfullmoon", OnTransform)
			inst.skinn = 1
			inst.sg:GoToState("transition")
			--MorphShadowChester(inst)
			end
		elseif inst.components.inventory ~= nil and ice ~= nil and ice.components.stackable.stacksize >= req then
			if fuel == nil or fuel.components.stackable.stacksize <= req then-- or fuel ~= nil and not fuel.components.stackable.stacksize >= req then
			inst.components.inventory:ConsumeByName("bluegem", 9)
			inst:StopWatchingWorldState("isfullmoon", OnTransform)
			inst.skinn = 2
			inst.sg:GoToState("transition")
			--MorphSnowChester(inst)
			end
		end
	end	
end
--==============================================
--				Custom Common Functions
--==============================================	
local function SkinSet(inst)
	if inst.skinn ~= nil and inst.skinn == 0 then
		inst.AnimState:SetBuild(mob.build)
	elseif inst.skinn ~= nil and inst.skinn == 1 then
		inst:StopWatchingWorldState("isfullmoon", OnTransform)
		MorphShadowChester(inst)
	elseif inst.skinn ~= nil and inst.skinn == 2 then	
		inst:StopWatchingWorldState("isfullmoon", OnTransform)
		MorphSnowChester(inst)
	end	
end

local function SetSkinDefault(inst, num)
	if num ~= 0 then
		if inst.skinn == 1 then
			inst.AnimState:SetBuild("chester_shadow_shiny_build_0"..num)
		elseif inst.skinn == 2 then
			inst.AnimState:SetBuild("chester_snow_shiny_build_0"..num)
		else
			inst.AnimState:SetBuild("chester_shiny_build_0"..num)
		end
	else
		if inst.skinn == 1 then
			inst.AnimState:SetBuild(mob.build2)
		elseif inst.skinn == 2 then
			inst.AnimState:SetBuild(mob.build3)
		else
			inst.AnimState:SetBuild(mob.build)
		end		
	end
end
----PvP Teleport----

local function getwildposition(inst)
    local ground = TheWorld
    local centers = {}
    for i, node in ipairs(ground.topology.nodes) do
        if ground.Map:IsPassableAtPoint(node.x, 0, node.y) then
            table.insert(centers, {x = node.x, z = node.y})
        end
    end
    if #centers > 0 then
        local pos = centers[math.random(#centers)]
        return Point(pos.x, 0, pos.z)
    else
        return inst:GetPosition()
    end
end

local function PvPTeleport(inst)
if inst.mobteleported == nil or inst.mobteleported == false then
	local ground = TheWorld
	local pt = getwildposition(inst)--Point(inst.Transform:GetWorldPosition())
      if pt.y <= .1 then
         pt.y = 0
         inst.Physics:Stop()
         inst.Physics:Teleport(pt.x,pt.y,pt.z)
		 inst.mobteleported = true
      end
	end
end
--==============================================
--					Loading/Saving
--==============================================
 
local function OnLoad(inst, data)
	if data ~= nil then
		inst.skinn = data.skinn or 0
		inst.mobteleported = data.mobteleported or false
		inst.isshiny = data.isshiny or 0
	end
end

local function OnSave(inst, data)
	data.skinn = inst.skinn or 0
	data.mobteleported = inst.mobteleported or false
	data.isshiny = inst.isshiny or 0
end

--==============================================
--					Forged Forge
--==============================================

local ex_fns = require "prefabs/player_common_extensions"

local function SetForge(inst)
	PlayablePets.SetForgeStats(inst, FORGE_STATS)
	
	inst:DoTaskInTime(0, function(inst)
	inst:AddComponent("itemtyperestrictions")
	inst.components.itemtyperestrictions:SetRestrictions({"melees", "books", "staves", "darts"})
	end)
	
	inst.components.combat:SetDamageType(1)
	
	inst:RemoveEventCallback("respawnfromcorpse", ex_fns.OnRespawnFromPlayerCorpse)
	inst:ListenForEvent("respawnfromcorpse", PlayablePets.OnRespawnFromMobCorpse)
end

--==============================================
--					Common/Master
--==============================================

local common_postinit = function(inst) 
	inst.MiniMapEntity:SetIcon(mob.minimap)

	inst:DoTaskInTime(0, function() 
   if ThePlayer then
      inst:EnableMovementPrediction(false)
	  --ThePlayer.HUD.controls.status.brain:Hide()
		if MONSTERHUNGER== "Disable" then
			--ThePlayer.HUD.controls.status.stomach:Hide()
		end
   end
end)
	----------------------------------
	--Tags--
	----------------------------------
	
	----------------------------------
	inst:WatchWorldState( "isday", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "isdusk", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "isnight", function() PlayablePets.SetNightVision(inst)  end)
	inst:WatchWorldState( "iscaveday", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "iscavedusk", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "iscavenight", function() PlayablePets.SetNightVision(inst)  end)
	
	PlayablePets.SetNightVision(inst)	
	
	--[[
	
	inst:WatchWorldState("issummer", function() PlayablePets.SetSandstormImmunity(inst) end) --makes immune to Sandstorm visual issues.
	
	PlayablePets.SetSandstormImmunity(inst)	
	]]
end

local master_postinit = function(inst)  
	--Stats--
	inst.sounds = sounds
	PlayablePets.SetCommonStats(inst, mob, nil, true) --mob table, ishuman, ignorepvpmultiplier
	PlayablePets.SetCommonWeatherResistances(inst, 60, 20, 1) --heat, cold, wetness
	PlayablePets.SetCommonStatResistances(inst, 1, 1, 1) --fire, acid, poison
	----------------------------------
	--Variables			
	inst.isshiny = 0
	inst.getskins = getskins
	inst.setskin_defaultfn = SetSkinDefault
	
	inst.mobsleep = true
	inst.skinn = 0
	inst.mobplayer = true
	inst.ghostbuild = "ghost_monster_build"
	
	local body_symbol = "swap_fire"
	inst.poisonsymbol = body_symbol
	MakeLargeBurnableCharacter(inst, body_symbol)
    MakeLargeFreezableCharacter(inst)
	inst.components.debuffable:SetFollowSymbol(body_symbol, 0, 0, 0)
	
	inst:WatchWorldState("isfullmoon", OnTransform)
	
	inst.userfunctions =
	{
		MorphShadowChester = MorphShadowChester,
		MorphSnowChester = MorphSnowChester,
	}
	----------------------------------
	--Components
	PlayablePets.SetCommonLootdropper(inst, prefabname) --inst, prefaboverride
	
	----------------------------------
	--Eater--
	inst.components.eater:SetAbsorptionModifiers(1,1,1) --This might multiply food stats.
	---------------------------------
	--Physics and Shadows--
	MakeCharacterPhysics(inst, 75, 0.5)
    inst.Transform:SetScale(mob.scale, mob.scale, mob.scale)
    inst.DynamicShadow:SetSize(2, 1.5)
    inst.Transform:SetFourFaced()
	---------------------------------
	--Listeners--
	inst:ListenForEvent("equip", PlayablePets.CommonOnEquip) --Shows head when hats make heads disappear.
	--inst:ListenForEvent("attacked", OnAttacked)
	---------------------------------
	--Forge--
	if PP_FORGE_ENABLED then
		inst.forge_fn = SetForge(inst)
	end
    ------------------------------------------------------
	--Respawning and Initializing functions--
	
	inst:ListenForEvent("respawnfromghost", function(inst) PlayablePets.RevRestore(inst, mob) end) --(inst, mob, isflying, iswebimmune, noshadow, ishuman)

    inst:DoTaskInTime(0, function(inst) PlayablePets.CommonSetChar(inst, mob)  end)
	inst:DoTaskInTime(3, function(inst) PlayablePets.SetSkin(inst, mob) end)
    inst:ListenForEvent("respawnfromghost", function() 
        inst:DoTaskInTime(5, function(inst) PlayablePets.CommonSetChar(inst, mob) end)
		inst:DoTaskInTime(5.1, function(inst) PlayablePets.SetSkin(inst, mob) end)
		inst:DoTaskInTime(5, function(inst) PlayablePets.RevRestore(inst, mob) SkinSet(inst) end)
    end)
	
	inst.OnSave = OnSave
    inst.OnLoad = OnLoad
	
    return inst	
end

return MakePlayerCharacter(prefabname, prefabs, assets, common_postinit, master_postinit, start_inv)
