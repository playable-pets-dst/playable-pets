local MakePlayerCharacter = require "prefabs/player_common"

---------------------------
----------==Notes==--------
--This is a template for nerds.
---------------------------

--Don't add assets unless absolutely needed.
local assets = 
{
	Asset("ANIM", "anim/bernie_big.zip"),
    Asset("ANIM", "anim/bernie_build.zip"),
	
	Asset("ANIM", "anim/bernie_shiny_build_06.zip"),
}
--Might be completely pointless here. I don't know.
local prefabs = 
{	

}
	
--Note: Becareful when adding starting items.
--People are likely to spam changecharacter to get more of them.	
local start_inv = 
{
	--"prefab",
}

local start_inv2 = 
{
	--"prefabhome",
}

local getskins = {"6"}

local prefabname = "berniep"

-----------------------
local mob = 
{
	health = 1000,
	hunger = 20,
	hungerrate = 0,
	sanity = 100,
	runspeed = 6,
	walkspeed = TUNING.BERNIE_SPEED + 1,
	
	attackperiod = 0,
	damage = 50, --should only apply to big bernie.
	range = 30,
	hit_range = 0,
	
	bank = TheNet:GetServerGameMode() == "lavaarena" and "bernie_big" or "bernie",
	build = "bernie_build",
	scale = 1,
	shiny = "bernie",
	--build2 = "alternate build here",
	stategraph = "SGberniep",
	minimap = "berniep.tex",
	
}
-----------------------
--Loot that drops when you die, duh.
SetSharedLootTable('berniep',
-----Prefab---------------------Chance------------
{

})


local function SetSmall(inst)
	inst.big = nil
	inst.AnimState:SetBank("bernie")
	if inst.isshiny ~= 0 then
		inst.AnimState:SetBuild(mob.shiny.."_shiny_build_0"..inst.isshiny)
	else
		inst.AnimState:SetBuild(mob.build)
	end
	inst:RemoveTag("epic")
	inst.Transform:SetScale(1, 1, 1)
	inst.components.locomotor.walkspeed = mob.walkspeed
    inst.components.locomotor.runspeed = mob.runspeed
	inst.shouldwalk = true
	inst.Physics:SetMass(50)
end

local function SetBig(inst)
	inst.big = true
	inst.Physics:SetMass(500)
	inst.AnimState:SetBank("bernie_big")
	inst.AnimState:SetBuild(mob.build)
	inst.Transform:SetScale(0.7, 0.7, 0.7)
	inst:AddTag("epic")
	inst.components.locomotor.walkspeed = TUNING.BERNIE_BIG_WALK_SPEED
    inst.components.locomotor.runspeed = TUNING.BERNIE_BIG_RUN_SPEED
	inst.shouldwalk = false
end
-------------------------------------------------------
----PvP Teleport----

local function getwildposition(inst)
    local ground = TheWorld
    local centers = {}
    for i, node in ipairs(ground.topology.nodes) do
        if ground.Map:IsPassableAtPoint(node.x, 0, node.y) then
            table.insert(centers, {x = node.x, z = node.y})
        end
    end
    if #centers > 0 then
        local pos = centers[math.random(#centers)]
        return Point(pos.x, 0, pos.z)
    else
        return inst:GetPosition()
    end
end

local function PvPTeleport(inst)
if inst.mobteleported == nil or inst.mobteleported == false then
	local ground = TheWorld
	local pt = getwildposition(inst)--Point(inst.Transform:GetWorldPosition())
      if pt.y <= .1 then
         pt.y = 0
         inst.Physics:Stop()
         inst.Physics:Teleport(pt.x,pt.y,pt.z)
		 inst.mobteleported = true
      end
	end
end

------------------------------------------------------
 
local function OnLoad(inst, data)
	if data ~= nil then
		inst.mobteleported = data.mobteleported or false
		inst.isshiny = data.isshiny or 0
	end
end

local function OnSave(inst, data)
	data.mobteleported = inst.mobteleported or false
	data.isshiny = inst.isshiny or 0
end

--==============================================
--					Forged Forge
--==============================================


local ex_fns = require "prefabs/player_common_extensions"

local function SetForge(inst)
	PlayablePets.SetForgeStats(inst, PP_FORGE.BERNIE)
	
	inst.mobsleep = nil
	
	inst:DoTaskInTime(0, function(inst)
	inst:AddComponent("itemtyperestrictions")
	inst.components.itemtyperestrictions:SetRestrictions({"darts", "melees", "books", "staves"})
	SetBig(inst)
	end)	
	
	inst.components.health:StopRegen()
	
	inst.components.combat:SetDamageType(1)
	
	inst.components.revivablecorpse.revivespeedmult = 1.5
	
	inst:RemoveEventCallback("respawnfromcorpse", ex_fns.OnRespawnFromPlayerCorpse)
	inst:ListenForEvent("respawnfromcorpse", PlayablePets.OnRespawnFromMobCorpse)
end

local function RemovePenalty(inst)
	inst.components.health:DeltaPenalty(-1.00) --Removes health penalty when reviving
	inst.components.health:DoDelta(20000, false) --Returns health to max health upon reviving
end

local common_postinit = function(inst) 
	--inst.soundsname = "beefalo"

	inst.MiniMapEntity:SetIcon(mob.minimap)
	--inst.AnimState:SetMultColour(1, 1, 1, 0.5)

	inst:DoTaskInTime(0, function() 
   if ThePlayer then
      inst:EnableMovementPrediction(false)
   end
end)
	
end

local function DontTriggerCreep(inst)
    inst.components.locomotor.runspeed = (mob.runspeed)
	inst.components.locomotor.walkspeed = (mob.walkspeed)
end

local function SetSkinDefault(inst, num)
	if not inst.big then
		if num and num ~= 0 then
			inst.AnimState(mob.shiny.."_shiny_build_0"..inst.isshiny)
		else
			inst.AnimState(mob.build)
		end
	end
end

local master_postinit = function(inst)
	------------------------------------------
	--Stats--
    PlayablePets.SetCommonStats(inst, mob, nil, true) --mob table, ishuman, ignorepvpmultiplier
	inst.components.health:StartRegen(5, 5)
	PlayablePets.SetCommonWeatherResistances(inst, 60, 20) --heat, cold, wetness
	PlayablePets.SetCommonStatResistances(inst, 0.5, 1, 0) --fire, acid, poison
	inst.components.hunger:Pause()
	----------------------------------
	--Tags--
	--inst:RemoveTag("character") --This is removed to make certain mobs to not treat you like a player.
	--inst:AddTag("special_atk1") --allows taunting.
	
	inst:AddTag("smallcreature")
	
	inst.taunt = true
	inst.taunt2 = true
	inst.mobsleep = true
	inst.shouldwalk = true
	inst.isshiny = 0
	inst.getskins = getskins
	inst.setskin_defualtfn = SetSkinDefault
	----------------------------------
	--Components
	PlayablePets.SetCommonLootdropper(inst, prefabname) --prefaboverride
	---------------------------------
	--Physics and Scale--
	
	MakeCharacterPhysics(inst, 50, .25)
    inst.DynamicShadow:SetSize(1, .5)
	--inst.DynamicShadow:Enable(false) --Disables shadows.
    inst.Transform:SetFourFaced()
	---------------------------------
	--Forge--
	if TheNet:GetServerGameMode() == "lavaarena" then
		inst.forge_fn = SetForge(inst)
	end
	---------------------------------
	--Listeners--
	
	inst:ListenForEvent("equip", PlayablePets.CommonOnEquip) --Shows head when hats make heads disappear.
	---------------------------------
	--Functions that saves and loads data.
	
	--inst.OnLongUpdate = onlongupdate
    inst.OnSave = OnSave
    inst.OnLoad = OnLoad
    --inst.OnPreLoad = onpreload
	------------------------------------------------------
	
    ------------------------------------------------------
	--Light and Character Functions. Don't Touch.--

	inst:DoTaskInTime(0, function(inst) PlayablePets.CommonSetChar(inst, mob) end)
	inst:DoTaskInTime(3, function(inst) PlayablePets.SetSkin(inst, mob) end)
    inst:ListenForEvent("respawnfromghost", function() 
        inst:DoTaskInTime(5, function(inst) PlayablePets.CommonSetChar(inst, mob) end)
		inst:DoTaskInTime(5.1, function(inst) PlayablePets.SetSkin(inst, mob) end)
		inst:DoTaskInTime(5, function(inst) PlayablePets.RevRestore(inst, mob) end) --(inst, mob, isflying, iswebimmune, noshadow, ishuman, amphibious)
    end)
	
    return inst
	
end




return MakePlayerCharacter("berniep", prefabs, assets, common_postinit, master_postinit, start_inv)
