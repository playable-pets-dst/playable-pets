local MakePlayerCharacter = require "prefabs/player_common"

---------------------------
----------==Notes==--------
--
---------------------------
local prefabname = "rabbitp"

local assets = 
{
	Asset("ANIM", "anim/ds_rabbit_basic.zip"),
	Asset("ANIM", "anim/rabbit_build.zip"),
	Asset("ANIM", "anim/rabbit_shiny_build_01.zip"),
	Asset("ANIM", "anim/rabbit_shiny_build_08.zip"),
}

local prefabs = 
{	

}
	
--Note: Becareful when adding starting items.
--People are likely to spam changecharacter to get more of them.	
local start_inv = 
{
	--'prefab',
	--"spear",
}

local start_inv2 =
{
   "rabbithome",
}

if MOBHOUSE== "Enable1" or MOBHOUSE== "Enable3" then
	start_inv = start_inv2
end
-----------------------
--Stats--
local mob = 
{
	health = 10,
	hunger = 50,
	hungerrate = 0.10, --I think .25 is defualt.
	sanity = 100,
	runspeed = 6,
	walkspeed = 6,
	damage = 5*2,
	range = 1,
	hit_range = 1.2, 
	attackperiod = 0,
	bank = "rabbit",
	build = "rabbit_build",
	scale = 1,
	build2 = "rabbit_winter_build",
	build3 = "beard_monster",
	stategraph = "SGrabbitp",
	minimap = "rabbitp.tex",
	
}
-----------------------
--Loot that drops when you die, duh.
SetSharedLootTable('rabbitp',
-----Prefab---------------------Chance------------
{
    {'smallmeat',			1.00},
    
   
})

local FORGE_STATS = PP_FORGE[string.upper(prefabname)] 
--==============================================
--					Mob Functions
--==============================================
local rabbitsounds =
{
    scream = "dontstarve/rabbit/scream",
    hurt = "dontstarve/rabbit/scream_short",
}

local beardsounds =
{
    scream = "dontstarve/rabbit/beardscream",
    hurt = "dontstarve/rabbit/beardscream_short",
}

local wintersounds =
{
    scream = "dontstarve/rabbit/winterscream",
    hurt = "dontstarve/rabbit/winterscream_short",
}
--==============================================
--				Custom Common Functions
--==============================================	

local function SetSkinDefault(inst, data)
	if data then
		inst.AnimState:SetBuild(data.build)
	else
		inst.AnimState:SetBuild(TheWorld.state.iswinter and mob.build2 or mob.build)
	end
end

local function OnIsWinter(inst, iswinter)
	print(TheWorld.state.iswinter)
    if TheWorld.state.iswinter or iswinter then
        inst.components.ppskin_manager:LoadSkin(mob, true)
		inst.sounds = wintersounds
    else
		inst.components.ppskin_manager:LoadSkin(mob, true)
		inst.sounds = rabbitsounds
    end
end
----PvP Teleport----

local function getwildposition(inst)
    local ground = TheWorld
    local centers = {}
    for i, node in ipairs(ground.topology.nodes) do
        if ground.Map:IsPassableAtPoint(node.x, 0, node.y) then
            table.insert(centers, {x = node.x, z = node.y})
        end
    end
    if #centers > 0 then
        local pos = centers[math.random(#centers)]
        return Point(pos.x, 0, pos.z)
    else
        return inst:GetPosition()
    end
end

local function PvPTeleport(inst)
if inst.mobteleported == nil or inst.mobteleported == false then
	local ground = TheWorld
	local pt = getwildposition(inst)--Point(inst.Transform:GetWorldPosition())
      if pt.y <= .1 then
         pt.y = 0
         inst.Physics:Stop()
         inst.Physics:Teleport(pt.x,pt.y,pt.z)
		 inst.mobteleported = true
      end
	end
end
--==============================================
--					Loading/Saving
--==============================================
 
local function OnLoad(inst, data)
	if data ~= nil then
		inst.mobteleported = data.mobteleported or false
	end
end

local function OnSave(inst, data)
	data.mobteleported = inst.mobteleported or false
end

--==============================================
--					Forged Forge
--==============================================

local ex_fns = require "prefabs/player_common_extensions"

local function SetForge(inst)
	PlayablePets.SetForgeStats(inst, FORGE_STATS)
	
	inst.mobsleep = nil
	
	inst:DoTaskInTime(0, function(inst)
	inst:AddComponent("itemtyperestrictions")
	inst.components.itemtyperestrictions:SetRestrictions({"melees", "books", "staves", "darts"})
	end)
	
	inst:AddTag("lessaggro")
	
	inst:DoTaskInTime(0, function(inst)	
	inst:AddComponent("itemtyperestrictions")
	inst.components.itemtyperestrictions:SetRestrictions({"books", "staves", "darts"})
	end)
	
	inst.components.combat:SetDamageType(1)
	
	inst.components.revivablecorpse.revivespeedmult = 0.3
	
	inst.components.combat:SetDamageType(1)
	
	inst:RemoveEventCallback("respawnfromcorpse", ex_fns.OnRespawnFromPlayerCorpse)
	inst:ListenForEvent("respawnfromcorpse", PlayablePets.OnRespawnFromMobCorpse)
end

--==============================================
--					Common/Master
--==============================================

local common_postinit = function(inst) 
	inst.MiniMapEntity:SetIcon(mob.minimap)

	inst:DoTaskInTime(0, function() 
   if ThePlayer then
      inst:EnableMovementPrediction(false)
	  --ThePlayer.HUD.controls.status.brain:Hide()
		if MONSTERHUNGER== "Disable" then
			--ThePlayer.HUD.controls.status.stomach:Hide()
		end
   end
end)
	----------------------------------
	--Tags--
	----------------------------------
	inst:RemoveTag("character") --This is removed to make certain mobs to not treat you like a player.
    inst:AddTag("rabbit")
	inst:RemoveTag("scarytoprey")
	----------------------------------
	
	--[[
	
	inst:WatchWorldState("issummer", function() PlayablePets.SetSandstormImmunity(inst) end) --makes immune to Sandstorm visual issues.
	
	PlayablePets.SetSandstormImmunity(inst)	
	]]
end

local master_postinit = function(inst)  
	--Stats--
	inst.sounds = rabbitsounds
	PlayablePets.SetCommonStats(inst, mob, nil, true) --mob table, ishuman, ignorepvpmultiplier
	PlayablePets.SetCommonWeatherResistances(inst, 60, 20, 1) --heat, cold, wetness
	PlayablePets.SetCommonStatResistances(inst, 2, 1, 1) --fire, acid, poison
	----------------------------------
	--Variables			
	inst.isshiny = 0
	inst.components.ppskin_manager:SetSkinDefaultFn(SetSkinDefault)
	
	inst.mobsleep = true
	inst.taunt2 = true
	inst.shouldwalk = false
	
	inst.mobplayer = true
	inst.ghostbuild = "ghost_monster_build"
	
	local body_symbol = "swap_fire"
	inst.poisonsymbol = body_symbol
	MakeLargeBurnableCharacter(inst, body_symbol)
    MakeLargeFreezableCharacter(inst)
	inst.components.debuffable:SetFollowSymbol(body_symbol, 0, 0, 0)
	----------------------------------
	--Components
	PlayablePets.SetCommonLootdropper(inst, prefabname) --inst, prefaboverride
	----------------------------------
	--Eater--
	inst.components.eater:SetDiet({ FOODTYPE.VEGGIE }, { FOODTYPE.VEGGIE })
    inst.components.eater:SetAbsorptionModifiers(1,2,1) --This might multiply food stats.
	---------------------------------
	--Physics and Shadows--
	MakeCharacterPhysics(inst, 10, .5)
    inst.Transform:SetScale(mob.scale, mob.scale, mob.scale)
    inst.DynamicShadow:SetSize(1.5, .5)
    inst.Transform:SetFourFaced()
	---------------------------------
	--Listeners--
	inst:ListenForEvent("equip", PlayablePets.CommonOnEquip) --Shows head when hats make heads disappear.
	--inst:ListenForEvent("attacked", OnAttacked)
	inst:WatchWorldState("iswinter", OnIsWinter) 
	inst:WatchWorldState("isspring", OnIsWinter) 
	---------------------------------
	--Forge--
	if PP_FORGE_ENABLED then
		inst.forge_fn = SetForge(inst)
	end
    ------------------------------------------------------
	--Respawning and Initializing functions--
	
	inst:ListenForEvent("respawnfromghost", function(inst) PlayablePets.RevRestore(inst, mob) end) --(inst, mob, isflying, iswebimmune, noshadow, ishuman)

    inst:DoTaskInTime(0, function(inst) PlayablePets.CommonSetChar(inst, mob)  end)
	inst:DoTaskInTime(3, function(inst) PlayablePets.SetSkin(inst, mob) OnIsWinter(inst) end)
    inst:ListenForEvent("respawnfromghost", function() 
        inst:DoTaskInTime(5, function(inst) PlayablePets.CommonSetChar(inst, mob) end)
		inst:DoTaskInTime(5.1, function(inst) PlayablePets.SetSkin(inst, mob) end)
		inst:DoTaskInTime(5, function(inst) PlayablePets.RevRestore(inst, mob) OnIsWinter(inst) end)
    end)
	
	inst.OnSave = OnSave
    inst.OnLoad = OnLoad
	
    return inst	
end

return MakePlayerCharacter(prefabname, prefabs, assets, common_postinit, master_postinit, start_inv)
