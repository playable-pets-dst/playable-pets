local MakePlayerCharacter = require "prefabs/player_common"
local easing = require("easing")
---------------------------
----------==Notes==--------
--
---------------------------
local function ShakeIfClose(inst)
    ShakeAllCameras(CAMERASHAKE.VERTICAL, .5, .03, 2, inst, 40)
end

local prefabname = "bigfootp"

local assets = 
{
	Asset("ANIM", "anim/foot_build.zip"),
	Asset("ANIM", "anim/foot_basic.zip"),
	Asset("ANIM", "anim/foot_print.zip"),
	Asset("ANIM", "anim/foot_shadow.zip"),
}

local prefabs = 
{
    "groundpound_fx",
    "groundpoundring_fx",
}

local start_inv = {
}

local start_inv2 = 
{

}

local getskins = {}

if MOBHOUSE == "Enable1" or MOBHOUSE == "Enable3" then
	start_inv = start_inv2
end
-----------------------
--Stats--
local mob = 
{
	health = TUNING.BIGFOOTP_HEALTH,
	hunger = TUNING.BIGFOOTP_HUNGER,
	hungerrate = 0, 
	sanity = TUNING.BIGFOOTP_SANITY,
	
	runspeed = 10,
	walkspeed = 15,
	
	attackperiod = 3,
	damage = BOSS_STATS and 1000 or 300*2,
	range = 20,
	hit_range = 8,
	
	bank = "foot",
	build = "foot_build",
	shiny = "foot",
	
	scale = 1,
	stategraph = "SGbigfootp",
	minimap = prefabname..".tex",	
}

--Loot that drops when you die, duh.
SetSharedLootTable('bigfootp',
{
    {'meat',             1.00},
	{'meat',             1.00},
	{'meat',             1.00},
	{'meat',             1.00},
	{'meat',             1.00},
	{'meat',             1.00},
	{'meat',             1.00},
	{'meat',             1.00},
	{'meat',             1.00},
	{'meat',             1.00},
})



local FORGE_STATS = PP_FORGE.BIGFOOT
--==============================================
--					Mob Functions
--==============================================
local ShadowWarnTime = 2

local function IsAboveLand(inst)
	local pos = inst:GetPosition()
	return not PlayablePets.CheckWaterTile(pos)
end

local function GetExcludeTags(inst)
	if TheNet:GetPVPEnabled() then
		return {"INLIMBO", "playerghost", "notarget"}
	elseif TheNet:GetServerGameMode() == "lavaarena" then
		return {"player", "companion", "playerghost", "INLIMBO"}
	else	
		return {"player", "companion", "INLIMBO", "playerghost", "notarget"}
	end
end

local function DoStep(inst)
	if IsAboveLand(inst) then
		inst.components.groundpounder:GroundPound()
		local pt = inst:GetPosition()
		local ents = TheSim:FindEntities(pt.x, pt.y, pt.z, mob.hit_range, nil, GetExcludeTags())
		inst.components.combat:StartAttack()
		for i,ent in ipairs(ents) do
			if ent.components.combat and not ent:HasTag("INLIMBO") and not ent:HasTag("playerghost") and ent ~= inst then
				ent.components.combat:GetAttacked(inst, mob.damage)
			end
		end
		inst.SoundEmitter:PlaySound("dontstarve_DLC001/creatures/glommer/foot_ground")
		--TheWorld:PushEvent("bigfootstep")
		ShakeIfClose(inst)
	end
end

local function SpawnPrint(inst)
	if IsAboveLand(inst) then
		local footprint = SpawnPrefab("bigfootprintp")
		footprint.Transform:SetPosition(inst:GetPosition():Get())
		footprint.Transform:SetRotation(inst.Transform:GetRotation() - 90)
	end
end

local function SimulateStep(inst)
	if IsAboveLand(inst) then
	inst:DoTaskInTime(ShadowWarnTime, function(inst) 
		inst:DoStep()
	end)
	end
end

local function StartStep(inst)
	if IsAboveLand(inst) then
		local shadow = SpawnPrefab("bigfootshadowp")
		shadow.Transform:SetPosition(inst:GetPosition():Get())
		shadow.Transform:SetRotation(inst.Transform:GetRotation() - 90)
		inst:Hide()
		inst:DoTaskInTime(ShadowWarnTime - (5*FRAMES), function(inst) inst.sg:GoToState("stomp") end)
	end
end

local function LeaveStep(inst)
	if IsAboveLand(inst) then
		local shadow = SpawnPrefab("bigfootshadowp")
		shadow._leaving = true
		shadow.Transform:SetPosition(inst:GetPosition():Get())
		shadow.Transform:SetRotation(inst.Transform:GetRotation() - 90)
		shadow.StartingScale = 1
	end
end

local function LerpOut(inst)
	local s = easing.inExpo(inst:GetTimeAlive(), 1, 2, 1.33)
	inst.Transform:SetScale(s,s,s)
	if s >= 2 then
		inst.sizeTask:Cancel()
		inst.sizeTask = nil
	end
end

local function LerpIn(inst)
	local s = easing.inExpo(inst:GetTimeAlive(), inst.StartingScale, 1 - inst.StartingScale, inst.TimeToImpact)
	inst.Transform:SetScale(s,s,s)
	if s <= 1 then
		inst.sizeTask:Cancel()
		inst.sizeTask = nil
	end
end
--==============================================
--				Custom Common Functions
--==============================================	

--==============================================
--					Loading/Saving
--==============================================
 
local function OnLoad(inst, data)
	if data ~= nil then
		inst.mobteleported = data.mobteleported or false
		inst.isshiny = data.isshiny or 0
	end
end

local function OnSave(inst, data)
	data.mobteleported = inst.mobteleported or false
	data.isshiny = inst.isshiny or 0
end

--==============================================
--					Forged Forge
--==============================================

local ex_fns = require "prefabs/player_common_extensions"

local function SetForge(inst)
	PlayablePets.SetForgeStats(inst, FORGE_STATS)
	
	inst.acidmult = 2
	inst.healmult = 3
	
	inst:RemoveComponent("shedder")
	
	inst:DoTaskInTime(0, function(inst)
	inst:AddComponent("itemtyperestrictions")
	inst.components.itemtyperestrictions:SetRestrictions({"melees", "books", "staves", "darts"})
	end)
	
	inst.components.combat:SetDamageType(1)
	
	inst:AddComponent("corpsereviver")
	inst.components.revivablecorpse.revivespeedmult = 1.5
	if inst.components.corpsereviver then
		inst.components.corpsereviver.reviver_speed_mult = 1.25
		inst.components.corpsereviver.additional_revive_health_percent = 0.6
	end
	
	inst.components.combat:SetDamageType(1)
	
	inst:RemoveEventCallback("respawnfromcorpse", ex_fns.OnRespawnFromPlayerCorpse)
	inst:ListenForEvent("respawnfromcorpse", PlayablePets.OnRespawnFromMobCorpse)
end

--==============================================
--					Common/Master
--==============================================

local common_postinit = function(inst) 
	inst.MiniMapEntity:SetIcon(mob.minimap)

	inst:DoTaskInTime(0, function() 
   if ThePlayer then
      inst:EnableMovementPrediction(false) --PP doesn't work with movement prediction enabled, due to use of custom stategraphs
   end
end)
	----------------------------------
	--Tags--
    inst:AddTag("epic")
    inst:AddTag("monster")
    inst:AddTag("largecreature")	
	----------------------------------

	inst:WatchWorldState( "isday", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "isdusk", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "isnight", function() PlayablePets.SetNightVision(inst)  end)
	inst:WatchWorldState( "iscaveday", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "iscavedusk", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "iscavenight", function() PlayablePets.SetNightVision(inst)  end)
	
	PlayablePets.SetNightVision(inst)
end

local master_postinit = function(inst)
	--Stats--
	PlayablePets.SetCommonStats(inst, mob) --mob table, ishuman, ignorepvpmultiplier
	PlayablePets.SetCommonWeatherResistances(inst, 60, 20, 1) --heat, cold, wetness
	PlayablePets.SetPassiveHeal(inst)
	----------------------------------
	--Variables	
	
	local body_symbol = "marker"
	inst.poisonsymbol = body_symbol
	MakeLargeBurnableCharacter(inst, body_symbol)
    MakeLargeFreezableCharacter(inst, body_symbol)
	inst.components.debuffable:SetFollowSymbol(body_symbol, 0, 0, 0)
	
	inst.hit_recovery = 2
	PlayablePets.SetStormImmunity(inst)
	
	inst.StartStep = StartStep
	inst.LeaveStep = LeaveStep
	inst.DoStep = DoStep
	inst.SpawnPrint = SpawnPrint
	
	PlayablePets.SetCommonStatResistances(inst, 0, 0, nil, 9999) --fire, acid, poison
	----------------------------------
	--Components
	PlayablePets.SetCommonLootdropper(inst, prefabname) --inst, prefaboverride	
	
	inst:AddComponent("groundpounder")
	inst.components.groundpounder.destroyer = MOBFIRE == "Disable" and true or false
	inst.components.groundpounder.damageRings = 0
	----------------------------------
	--Eater--

	---------------------------------
	--Physics and Shadows--
	MakeFlyingCharacterPhysics(inst, 1000, .5)
    inst.DynamicShadow:SetSize(4, 1.5)
	inst.DynamicShadow:Enable(false)
    inst.Transform:SetFourFaced()
	PlayablePets.SetAmphibious(inst, nil, nil, true, nil, true)
	---------------------------------
	--Listeners--
	inst:ListenForEvent("equip", PlayablePets.CommonOnEquip) --Shows head when hats make heads disappear.
	---------------------------------
	--Forge--
	if TheNet:GetServerGameMode() == "lavaarena" then
		inst.forge_fn = SetForge(inst)
	end
    ------------------------------------------------------
	--Respawning and Initializing functions--	
	inst:ListenForEvent("respawnfromghost", function(inst) PlayablePets.RevRestore(inst, mob, true, true, true) end) --(inst, mob, isflying, iswebimmune, noshadow, ishuman)

    inst:DoTaskInTime(0, function(inst) PlayablePets.CommonSetChar(inst, mob) end)
	inst:DoTaskInTime(3, function(inst) PlayablePets.SetSkin(inst, mob) end)
    inst:ListenForEvent("respawnfromghost", function() 
        inst:DoTaskInTime(5, function(inst) PlayablePets.CommonSetChar(inst, mob) end)
		inst:DoTaskInTime(5.1, function(inst) PlayablePets.SetSkin(inst, mob) end)
		inst:DoTaskInTime(5, function(inst) PlayablePets.RevRestore(inst, mob, true, true, true) end)
    end)
	
	inst.OnSave = OnSave
    inst.OnLoad = OnLoad
	
    return inst	
end

local function shadow_fn()
	local inst = CreateEntity()
	inst.entity:AddTransform()
	inst.entity:AddAnimState()
	inst.entity:AddNetwork()

	inst.AnimState:SetBank("foot_shadow")
	inst.AnimState:SetBuild("foot_shadow")
	inst.AnimState:PlayAnimation("idle")
	inst.AnimState:SetOrientation( ANIM_ORIENTATION.OnGround )
	inst.AnimState:SetLayer( LAYER_BACKGROUND )
	inst.AnimState:SetSortOrder( 3 )
	
	inst.entity:SetPristine()

    if not TheWorld.ismastersim then
        return inst
    end

	inst.persists = false
	
	local s = 2
	inst.StartingScale = s
	inst.Transform:SetScale(s,s,s)
	inst.TimeToImpact = ShadowWarnTime

	inst.AnimState:SetMultColour(0,0,0,0)

	inst:AddComponent("colourtweener")
	inst:DoTaskInTime(0, function(inst) 
		if inst._leaving then
			inst.Transform:SetScale(0.5,0.5,0.5)
			inst.AnimState:SetMultColour(0,0,0,1)
			inst.components.colourtweener:StartTween({0,0,0,0}, 1.33, inst.Remove)
			inst.sizeTask = inst:DoPeriodicTask(FRAMES, LerpOut)
		else
			inst.AnimState:SetMultColour(0,0,0,0)
			inst.components.colourtweener:StartTween({0,0,0,1}, inst.TimeToImpact, inst.Remove)
			inst.sizeTask = inst:DoPeriodicTask(FRAMES, LerpIn)
		end
	end)

	return inst
end

local function foot_fn()
	local inst = CreateEntity()
	inst.entity:AddTransform()
	inst.entity:AddAnimState()
	inst.entity:AddNetwork()

	inst.AnimState:SetBank("foot_print")
	inst.AnimState:SetBuild("foot_print")
	inst.AnimState:PlayAnimation("idle")
	inst.AnimState:SetOrientation( ANIM_ORIENTATION.OnGround )
	inst.AnimState:SetLayer( LAYER_BACKGROUND )
	inst.AnimState:SetSortOrder( 3 )
	
	inst:AddTag("scarytoprey")
	
	inst.entity:SetPristine()

    if not TheWorld.ismastersim then
        return inst
    end

	inst.persists = false
	inst:AddComponent("colourtweener")
	inst.components.colourtweener:StartTween({0,0,0,0}, 15, function(inst) inst:Remove() end)

	return inst
end

return MakePlayerCharacter(prefabname, prefabs, assets, common_postinit, master_postinit, start_inv),
	Prefab("bigfootshadowp", shadow_fn),
	Prefab("bigfootprintp", foot_fn)