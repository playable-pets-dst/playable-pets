local MakePlayerCharacter = require "prefabs/player_common"

---------------------------
----------==Notes==--------
--
---------------------------

local prefabname = "slurtlep"

local assets = 
{
	
}

local prefabs = {
}

local start_inv = {
}

local start_inv2 = 
{

}

local getskins = {}

if MOBHOUSE == "Enable1" or MOBHOUSE == "Enable3" then
	start_inv = start_inv2
end
-----------------------
--Stats--
local mob = 
{
	health = 500,
	hunger = 150,
	hungerrate = 0.05, 
	sanity = 200,
	runspeed = 3,
	walkspeed = 3,
	damage = 30,
	attackperiod = 0,
	range = 2,
	hit_range = 2,
	bank = "slurtle",
	build = "slurtle",
	scale = 1,
	bank2 = "snurtle",
	build2 = "slurtle_snaily",
	stategraph = "SGslurtlep",
	minimap = "slurtlep.tex",
}

--Loot that drops when you die, duh.
SetSharedLootTable('slurtlep',
-----Prefab---------------------Chance------------
{
    {'slurtleslime',			1.00},
	{'slurtleslime',			1.00},
	{'slurtle_shellpieces',		0.50},
})

local FORGE_STATS = PP_FORGE.SLURTLE
--==============================================
--					Mob Functions
--==============================================

--==============================================
--				Custom Common Functions
--==============================================	
local function RandomSkinSet(inst)
	if inst.skinn ~= nil and inst.skinn == 0 then
		inst.skinn = math.random(1,100)
		if inst.skinn >= 70 then
			inst.AnimState:SetBank(mob.bank2)
			inst.AnimState:SetBuild(mob.build2)
		else
			inst.AnimState:SetBank(mob.bank)
			inst.AnimState:SetBuild(mob.build)
		end	
	else
		if inst.skinn ~=nil and inst.skinn >= 70 then
			inst.AnimState:SetBank(mob.bank2)
			inst.AnimState:SetBuild(mob.build2)
		elseif inst.skinn ~= nil and inst.skinn <= 69 then
			inst.AnimState:SetBank(mob.bank)
			inst.AnimState:SetBuild(mob.build)
		end		
	end	
end

local function OnIgniteFn(inst)
    inst.SoundEmitter:PlaySound("dontstarve/creatures/slurtle/rattle", "rattle")
    inst.persists = false
end

local function OnExtinguishFn(inst)
    inst.SoundEmitter:KillSound("rattle")
    inst.persists = true
end

local function OnExplodeFn(inst)
    inst.SoundEmitter:KillSound("rattle")
    SpawnPrefab("explode_small_slurtle").Transform:SetPosition(inst.Transform:GetWorldPosition())
	if inst.components.inventory then
		inst.components.inventory:DropEverything(true)
	end
	inst.sg:GoToState("exploded")
end

--==============================================
--					Loading/Saving
--==============================================
 
local function OnLoad(inst, data)
	if data ~= nil then
		inst.mobteleported = data.mobteleported or false
		inst.isshiny = data.isshiny or 0
		inst.skinn = data.skinn or 0
		RandomSkinSet(inst)
	end
end

local function OnSave(inst, data)
	data.mobteleported = inst.mobteleported or false
	data.isshiny = inst.isshiny or 0
	data.skinn = inst.skinn or 0
end

--==============================================
--					Forged Forge
--==============================================

local ex_fns = require "prefabs/player_common_extensions"

local function SetForge(inst)
	PlayablePets.SetForgeStats(inst, FORGE_STATS)
	
	inst:DoTaskInTime(0, function(inst)
	inst:AddComponent("itemtyperestrictions")
	inst.components.itemtyperestrictions:SetRestrictions({"melees", "books", "staves", "darts"})
	end)
	
	inst.components.combat:SetDamageType(1)
	
	inst:RemoveEventCallback("respawnfromcorpse", ex_fns.OnRespawnFromPlayerCorpse)
	inst:ListenForEvent("respawnfromcorpse", PlayablePets.OnRespawnFromMobCorpse)
end

--==============================================
--					Common/Master
--==============================================

local common_postinit = function(inst) 
	inst.MiniMapEntity:SetIcon(mob.minimap)

	inst:DoTaskInTime(0, function() 
   if ThePlayer then
      inst:EnableMovementPrediction(false) --PP doesn't work with movement prediction enabled, due to use of custom stategraphs
   end
end)
	----------------------------------
	--Tags--
    inst:AddTag("cavedweller")
    inst:AddTag("animal")
    inst:AddTag("explosive")
	----------------------------------
	inst:WatchWorldState( "isday", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "isdusk", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "isnight", function() PlayablePets.SetNightVision(inst)  end)
	inst:WatchWorldState( "iscaveday", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "iscavedusk", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "iscavenight", function() PlayablePets.SetNightVision(inst)  end)
	
	PlayablePets.SetNightVision(inst)
end

local master_postinit = function(inst)
	--Stats--
	PlayablePets.SetCommonStats(inst, mob, nil, true) --mob table, ishuman, ignorepvpmultiplier
	PlayablePets.SetCommonWeatherResistances(inst, 60, 20, 1) --heat, cold, wetness
	PlayablePets.SetCommonStatResistances(inst, nil, 0.5, nil) --fire, acid, poison
	----------------------------------
	--Variables	
	inst.mobsleep = true
	inst.taunt = true
	inst.mobplayer = true
	inst.isshiny = 0
	inst.getskins = getskins
	inst.canhide = true
	
	local body_symbol = "shell"
	inst.poisonsymbol = body_symbol
	MakeMediumBurnableCharacter(inst, body_symbol)
    MakeMediumFreezableCharacter(inst)
	inst.components.debuffable:SetFollowSymbol(body_symbol, 0, 0, 0)
	----------------------------------
	--Components
	PlayablePets.SetCommonLootdropper(inst, prefabname) --inst, prefaboverride
	
	inst:AddComponent("explosive")
    inst.components.explosive.explosiverange = 3
    inst.components.explosive.lightonexplode = false
    inst.components.explosive:SetOnExplodeFn(OnExplodeFn)
	
	inst:AddComponent("thief")
	
	inst:AddComponent("shedder")
	inst.components.shedder.shedItemPrefab = "slurtleslime"
	inst.components.shedder.shedHeight = 0.1
	inst.components.shedder:StartShedding(480) --Note: 480 is 1 day. 480 x n = n amount of days.
	----------------------------------
	--Eater--
	inst.components.eater:SetDiet({ FOODTYPE.ELEMENTAL, FOODTYPE.ELEMENTAL }, { FOODTYPE.ELEMENTAL, FOODTYPE.ELEMENTAL }) 
    inst.components.eater:SetAbsorptionModifiers(0.5,30,0) --This might multiply food stats.
	inst.components.eater:SetCanEatHorrible() --check to make them eat monster meat.
	inst.components.eater.strongstomach = true -- can eat monster meat!
	---------------------------------
	--Physics and Shadows--
	MakeCharacterPhysics(inst, 50, .5)
    inst.DynamicShadow:SetSize(2, 1.5)
	inst.Transform:SetFourFaced()
	
	inst.components.burnable:SetOnIgniteFn(OnIgniteFn)
    inst.components.burnable:SetOnExtinguishFn(OnExtinguishFn)
	---------------------------------
	--Listeners--
	inst:ListenForEvent("equip", PlayablePets.CommonOnEquip) --Shows head when hats make heads disappear.
	---------------------------------
	--Forge--
	if TheNet:GetServerGameMode() == "lavaarena" then
		inst.forge_fn = SetForge(inst)
	end
    ------------------------------------------------------
	--Respawning and Initializing functions--
	
	inst:ListenForEvent("respawnfromghost", function(inst) PlayablePets.RevRestore(inst, mob) end) --(inst, mob, isflying, iswebimmune, noshadow, ishuman)

    inst:DoTaskInTime(0, function(inst) PlayablePets.CommonSetChar(inst, mob) end)
	inst:DoTaskInTime(3, function(inst) PlayablePets.SetSkin(inst, mob) end)
    inst:ListenForEvent("respawnfromghost", function() 
        inst:DoTaskInTime(5, function(inst) PlayablePets.CommonSetChar(inst, mob) end)
		inst:DoTaskInTime(5.1, function(inst) PlayablePets.SetSkin(inst, mob) end)
		inst:DoTaskInTime(5, function(inst) PlayablePets.RevRestore(inst, mob) end)
    end)
	
	inst.OnSave = OnSave
    inst.OnLoad = OnLoad
	
    return inst	
end

return MakePlayerCharacter(prefabname, prefabs, assets, common_postinit, master_postinit, start_inv)