local BALLOONS = require "prefabs/balloons_common"

local MakePlayerCharacter = require "prefabs/player_common"

---------------------------
----------==Notes==--------
--
---------------------------

--Don't add assets unless absolutely needed.
local assets = 
{
	Asset("ANIM", "anim/balloon.zip"),
    Asset("ANIM", "anim/balloon_shapes.zip"),
    Asset("ANIM", "anim/balloon2.zip"),
    Asset("ANIM", "anim/balloon_shapes2.zip"),
    Asset("SCRIPT", "scripts/prefabs/balloons_common.lua"),
}

local NUM_BALLOON_SHAPES = 9

--Might be completely pointless here. I don't know.
local prefabs = 
{	

}
	
--Note: Becareful when adding starting items.
--People are likely to spam changecharacter to get more of them.	
local start_inv = 
{

}
-----------------------
local prefabname = "balloonp"
--Stats--
local mob = 
{
	health = TUNING[string.upper(prefabname.."_HEALTH")],
	hunger = TUNING[string.upper(prefabname.."_HUNGER")],
	hungerrate = 0, 
	sanity = TUNING[string.upper(prefabname.."_SANITY")],
	runspeed = 6,
	walkspeed = 6,
	damage = 5,
	range = 2,
	hit_range = 2,
	attackperiod = 0,
	bank = "balloon2",
	build = "balloon2",
	scale = 1,
	shiny = "balloon",
	--build2 = "snake_yellow_build",
	--build3 = "chester_snow_build",
	stategraph = "SGballoonp",
	minimap = "balloonp.tex",
	
}
-----------------------
--Loot that drops when you die, duh.
SetSharedLootTable('balloonp',
-----Prefab---------------------Chance------------
{
   
})

local function SetBalloonShape(inst, num)
    inst.balloon_num = num
    inst.AnimState:OverrideSymbol("swap_balloon", "balloon_shapes2", "balloon_"..tostring(num))
end

local function OnLoad(inst, data)
	if data ~= nil then
        if data.num ~= nil and inst.balloon_num ~= data.num then
			SetBalloonShape(inst, data.num)
        end
        if data.colour_idx ~= nil then
			inst.colour_idx = BALLOONS.SetColour(inst, data.colour_idx)
        end
    end
end

local function OnSave(inst, data)
	data.num = inst.balloon_num
    data.colour_idx = inst.colour_idx
end

local function oncollide(inst, other)    
    if (inst:IsValid() and Vector3(inst.Physics:GetVelocity()):LengthSq() > .1) or
        (other ~= nil and other:IsValid() and Vector3(other.Physics:GetVelocity()):LengthSq() > .1) then
        inst.AnimState:PlayAnimation("hit")
        inst.AnimState:PushAnimation("idle", true)
        --inst.SoundEmitter:PlaySound("dontstarve/common/balloon_bounce")
    end
end

local function updatemotorvel(inst, xvel, yvel, zvel, t0)
    local x, y, z = inst.Transform:GetWorldPosition()
    if y >= 35 then
        inst:Remove()
        return
    end
    local time = GetTime() - t0
    if time >= 15 then
        inst:Remove()
        return
    elseif time < 1 then
        local scale = easing.inQuad(time, 1, -1, 1)
        inst.DynamicShadow:SetSize(scale, .5 * scale)
    else
        inst.DynamicShadow:Enable(false)
    end
    local hthrottle = easing.inQuad(math.clamp(time - 1, 0, 3), 0, 1, 3)
    yvel = easing.inQuad(math.min(time, 3), 1, yvel - 1, 3)
    inst.Physics:SetMotorVel(xvel * hthrottle, yvel, zvel * hthrottle)
end

local function UnregisterBalloon(inst)
    if balloons[inst] == nil then
        return
    end
    balloons[inst] = nil
    num_balloons = num_balloons - 1
    inst.OnRemoveEntity = nil
end

local function flyoff(inst)
    UnregisterBalloon(inst)
    inst:AddTag("notarget")
    inst.Physics:SetCollisionCallback(nil)
    inst.persists = false

    local xvel = math.random() * 2 - 1
    local yvel = 5
    local zvel = math.random() * 2 - 1

    inst:DoPeriodicTask(FRAMES, updatemotorvel, nil, xvel, yvel, zvel, GetTime())
end

local function RegisterBalloon(inst)
    if balloons[inst] then
        return
    end
    if num_balloons >= TUNING.BALLOON_MAX_COUNT then
        local rand = math.random(num_balloons)
        for k, v in pairs(balloons) do
            if rand > 1 then
                rand = rand - 1
            else
                flyoff(k)
                break
            end
        end
    end
    balloons[inst] = true
    num_balloons = num_balloons + 1
    inst.OnRemoveEntity = UnregisterBalloon
end

local ex_fns = require "prefabs/player_common_extensions"

local function SetForge(inst)
	PlayablePets.SetForgeStats(inst, PP_FORGE.BALLOON)
	
	inst.mobsleep = nil
	
	inst:DoTaskInTime(0, function(inst)
	inst:AddComponent("itemtyperestrictions")
	inst.components.itemtyperestrictions:SetRestrictions({"darts", "melees", "books", "staves", "armors", "hats"})
	end)	

		--because FF forces character physics on us on spawn
	inst:DoTaskInTime(3, function(inst)
		inst.Physics:SetCollisionGroup(COLLISION.FLYERS)
		inst.Physics:CollidesWith(COLLISION.FLYERS)	
	end)
		
	inst.components.health:StopRegen()
	
	inst.components.combat:SetDamageType(1)
	
	inst.components.revivablecorpse.revivespeedmult = 0
	
	inst:RemoveEventCallback("respawnfromcorpse", ex_fns.OnRespawnFromPlayerCorpse)
	inst:ListenForEvent("respawnfromcorpse", PlayablePets.OnRespawnFromMobCorpse)
end

local common_postinit = function(inst) 
	--inst.soundsname = "beefalo"

	inst.MiniMapEntity:SetIcon(mob.minimap)
	--inst.AnimState:SetMultColour(1, 1, 1, 0.5)

	inst:DoTaskInTime(0, function() 
   if ThePlayer then
      inst:EnableMovementPrediction(false)
   end
end)	
end


local master_postinit = function(inst)
	------------------------------------------
	--Stats--
    PlayablePets.SetCommonStats(inst, mob, nil, true) --mob table, ishuman, ignorepvpmultiplier
	PlayablePets.SetCommonWeatherResistances(inst, 60, 20) --heat, cold, wetness
	PlayablePets.SetCommonStatResistances(inst, 0, 1, 0) --fire, acid, poison
	inst.components.hunger:Pause()
	------------------------------------------
	inst.balloon_build = "balloon_shapes2"
	----------------------------------
	--Loot drops--
	inst:AddComponent("lootdropper")
    inst.components.lootdropper:SetChanceLootTable('balloonp')
	----------------------------------
	--Tags--
	
	inst:RemoveTag("character") --This is removed to make certain mobs to not treat you like a player.
	inst:RemoveTag("scarytoprey")
	inst:AddTag("cattoyairborne")
	
	--inst.Physics:SetCollisionCallback(oncollide)
	
	SetBalloonShape(inst, math.random(NUM_BALLOON_SHAPES))

    inst.colour_idx = BALLOONS.SetColour(inst)
	BALLOONS.SetRopeShape(inst)
	PlayablePets.SetStormImmunity(inst)
	---------------------------------
	--Physics and Scale--
	
	MakeCharacterPhysics(inst, 10, .25)
    inst.Physics:SetFriction(.3)
    inst.Physics:SetDamping(0)
    inst.Physics:SetRestitution(1)
    inst.DynamicShadow:SetSize(1, .5)
	--inst.DynamicShadow:Enable(false) --Disables shadows.
    inst.Transform:SetNoFaced()
	PlayablePets.SetAmphibious(inst, nil, nil, true)
	---------------------------------
	--Forge--
	if TheNet:GetServerGameMode() == "lavaarena" then
		inst.forge_fn = SetForge(inst)
	end
	---------------------------------
	--Listeners--
	
	inst:ListenForEvent("equip", PlayablePets.CommonOnEquip) --Shows head when hats make heads disappear.
	---------------------------------
	--Functions that saves and loads data.
	
	--inst.OnLongUpdate = onlongupdate
    inst.OnSave = OnSave
    inst.OnLoad = OnLoad
    --inst.OnPreLoad = onpreload
	------------------------------------------------------
	
    ------------------------------------------------------
	--Light and Character Functions. Don't Touch.--

	inst:DoTaskInTime(0, function(inst) PlayablePets.CommonSetChar(inst, mob, true) end)
	inst:DoTaskInTime(3, function(inst) PlayablePets.SetSkin(inst, mob) BALLOONS.SetColour(inst, inst.colour_idx) end)
    inst:ListenForEvent("respawnfromghost", function() 
        inst:DoTaskInTime(5, function(inst) PlayablePets.CommonSetChar(inst, mob, true) end)
		inst:DoTaskInTime(5.1, function(inst) PlayablePets.SetSkin(inst, mob) end)
		inst:DoTaskInTime(5, function(inst) PlayablePets.RevRestore(inst, mob, true) end) --(inst, mob, isflying, iswebimmune, noshadow, ishuman, amphibious)
    end)
	
    return inst
end

return MakePlayerCharacter("balloonp", prefabs, assets, common_postinit, master_postinit, start_inv)
