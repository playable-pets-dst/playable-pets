local MakePlayerCharacter = require "prefabs/player_common"

---------------------------
----------==Notes==--------
--
---------------------------

local prefabname = "krampusp"

local assets = 
{
	Asset("ANIM", "anim/krampus_hawaiian_basic.zip"),
    Asset("ANIM", "anim/krampus_hawaiian_build.zip"),
	Asset("ANIM", "anim/krampus_shiny_build_01.zip"),
	Asset("ANIM", "anim/krampus_shiny_build_02.zip"),
	Asset("ANIM", "anim/krampus_shiny_build_06.zip"),
}

local prefabs = {
    }

local start_inv = {
}

local start_inv2 = 
{

}

local getskins = {"1", "2", "6"}

if MOBHOUSE == "Enable1" or MOBHOUSE == "Enable3" then
	start_inv = start_inv2
end
-----------------------
--Stats--
local mob = 
{
	health = TUNING.KRAMPUSP_HEALTH,
	hunger = TUNING.KRAMPUSP_HUNGER,
	hungerrate = TUNING.WILSON_HUNGER_RATE, 
	sanity = TUNING.KRAMPUSP_SANITY,
	runspeed = TUNING.KRAMPUS_SPEED,
	walkspeed = TUNING.KRAMPUS_SPEED,
	damage = 40*2,
	range = 3,
	hit_range = 3,
	attackperiod = 2,
	bank = "krampus",
	build = "krampus_build",
	scale = 1,
	build2 = "krampus_hawaiian_build",
	stategraph = "SGkrampusp",
	minimap = "krampusp.tex",	
}

SetSharedLootTable('krampusp',
-----Prefab---------------------Chance------------
{
    {'monstermeat',  1.0},
    {'charcoal',     1.0},
    {'charcoal',     1.0},
    {'krampus_sack', .005},  --:)
   
})


local FORGE_STATS = PP_FORGE.KRAMPUS
--==============================================
--					Mob Functions
--==============================================

--==============================================
--				Custom Common Functions
--==============================================
local function makebagfull(inst)
    inst.AnimState:Show("SACK")
    inst.AnimState:Hide("ARM")
end

local function makebagempty(inst)
    inst.AnimState:Hide("SACK")
    inst.AnimState:Show("ARM")
end

local function IsCurrency(item)
    return item:HasTag("currency")
end

local function onhitother(inst, other)
	if other and other.components.inventory then
		local money = other.components.inventory:FindItems(IsCurrency)
		for k,v in pairs(money) do
			local owner = v.components.inventoryitem.owner
			local dropchance = not v:HasTag("hightier") and math.random(1,100) or math.random(1,60)
			if dropchance > 50 or v:HasTag("commontier") then
				if owner and owner.components.inventory then
					owner.components.inventory:DropItem(v, false, true)
					if v.components.stackable and v.components.stackable.stacksize > 5 then
						if math.random(1, 2) == 1 then
							owner.components.inventory:DropItem(v, false, true)
						end
					
						if math.random(1, 2) == 1 then
							owner.components.inventory:DropItem(v, false, true)
						end
					
						if math.random(1, 2) == 1 and not v:HasTag("hightier") then
							owner.components.inventory:DropItem(v, false, true)
						end
					
						if math.random(1, 3) == 1 and not v:HasTag("hightier") then
							owner.components.inventory:DropItem(v, false, true)
						end
					end
				end
			end
		end
	end
end

--==============================================
--					Loading/Saving
--==============================================
local function onupdate(inst, dt)
    inst.charge_time = inst.charge_time - dt
    if inst.charge_time <= 0 then
        inst.charge_time = 0
		if inst.charge_task ~= nil then
            inst.charge_task:Cancel()
            inst.charge_task = nil
        end
        inst.taunt2 = true
		inst.sg:GoToState("special_atk1")
    end
end

local function OnLongUpdate(inst, dt)
    inst.charge_time = math.max(0, inst.charge_time - dt)
end

local function StartTimer(inst, duration)
    inst.charge_time = duration
	inst.specialatk = false

    if inst.charge_task == nil then
        inst.charge_task = inst:DoPeriodicTask(1, onupdate, nil, 1)
        onupdate(inst, 0)
    end
end

local function OnLoad(inst, data)
	if data ~= nil and data.charge_time ~= nil then
        StartTimer(inst, data.charge_time)
    end
	if data ~= nil then
		data.charge_time = inst.charge_time > 0 and inst.charge_time or nil
		inst.mobteleported = data.mobteleported or false
		inst.isshiny = data.isshiny or 0
	end
end

local function OnSave(inst, data)
	data.mobteleported = inst.mobteleported or false
	data.isshiny = inst.isshiny or 0
	data.charge_time = inst.charge_time > 0 and inst.charge_time or nil
end

--==============================================
--					Forged Forge
--==============================================

local function OnForgeHitOther(inst, target)
	if target and target.sg and not target.sg:HasStateTag("attack") then 
		if target.components.combat then
			target.components.combat:SetTarget(inst)
			target.aggrotimer = 1
		end
	end
end

local T3 =
{
	"bacontome",
	"splintmail",
	"steadfastarmor",
	"steadfastgrandarmor",
	"jaggedgrandarmor",
	"silkengrandarmor",
	"whisperinggrandarmor",
	"flowerheadband",
	"noxhelm",
	"resplendentnoxhelm",
	"blossomedwreath",
	"clairvoyantcrown",
	"spiralspear",
	"infernalstaff",
	"blacksmithsedge"

}

local T2 =
{
	"forginghammer",
	"moltendarts",
	"pithpike",
	"reedtunic",
	"featheredwreath",
	"jaggedarmor",
	"silkenarmor",	
	"barbedhelm",
	"wovengarland",
	"firebomb",
	
}

local T1 =
{
	"petrifyingtome",
	"forgedarts",
	"reedtunic",
	"crystaltiara",
	"forge_woodarmor",
}

local T1_CHANCE = 60
local T2_CHANCE = 90
local T3_CHANCE = 100

local function ForgeSpecialAttack(inst)
	local roll1 = math.random(1, 100)
	local roll2 = math.random(1, 100)
	local roll3 = math.random(1, 100)
	
	--Leo:okay, this is dumb. Why can't I think of the correct away to do this.
	if roll1 and math.random(1, 100) <= 75 then
		if roll1 <= T1_CHANCE then
			inst.item1 = T1[math.random(1, #T1)]
		elseif roll1 > T1_CHANCE and roll1 <= T2_CHANCE then
			inst.item1 = T2[math.random(1, #T2)]
		elseif roll1 > T2_CHANCE then
			inst.item1 = T3[math.random(1, #T3)]
		end	
	end	
	
	if roll2 and math.random(1, 100) <= 50 then
		if roll2 <= T1_CHANCE then
			inst.item2 = T1[math.random(1, #T1)]
		elseif roll2 > T1_CHANCE and roll1 <= T2_CHANCE then
			inst.item2 = T2[math.random(1, #T2)]
		elseif roll2 > T2_CHANCE then
			inst.item2 = T3[math.random(1, #T3)]
		end	
	end	
	
	if roll3 and math.random(1, 100) <= 10 then
		if roll3 <= T1_CHANCE then
			inst.item3 = T1[math.random(1, #T1)]
		elseif roll3 > T1_CHANCE and roll1 <= T2_CHANCE then
			inst.item3 = T2[math.random(1, #T2)]
		elseif roll3 > T2_CHANCE then
			inst.item3 = T3[math.random(1, #T3)]
		end	
	end	
	--print(inst.item1 or "FAILED"..", "..inst.item2 or "FAILED"..", "..inst.item3 or "FAILED")
	
	if inst.item1 then
		local item = SpawnPrefab(inst.item1)
		item.Transform:SetPosition(inst:GetPosition():Get())
		Launch(item, inst, 2)
	end
	if inst.item2 then
		local item = SpawnPrefab(inst.item2)
		item.Transform:SetPosition(inst:GetPosition():Get())
		Launch(item, inst, 2)
	end
	if inst.item3 then
		local item = SpawnPrefab(inst.item3)
		item.Transform:SetPosition(inst:GetPosition():Get())
		Launch(item, inst, 2)
	end
	
	inst.item1 = nil
	inst.item2 = nil
	inst.item3 = nil
end

local ex_fns = require "prefabs/player_common_extensions"

local function SetForge(inst)
	PlayablePets.SetForgeStats(inst, FORGE_STATS)
	
	inst.mobsleep = false
	inst.ForgeSpecialAttack = ForgeSpecialAttack
	
	inst:DoTaskInTime(0, function(inst)
	inst:AddComponent("itemtyperestrictions")
	inst.components.itemtyperestrictions:SetRestrictions({"melees"})
	end)
	
	inst.components.combat:SetDamageType(1)
	
	inst:RemoveEventCallback("respawnfromcorpse", ex_fns.OnRespawnFromPlayerCorpse)
	inst:ListenForEvent("respawnfromcorpse", PlayablePets.OnRespawnFromMobCorpse)
end

--==============================================
--					Common/Master
--==============================================

local common_postinit = function(inst) 
	inst.MiniMapEntity:SetIcon(mob.minimap)

	inst:DoTaskInTime(0, function() 
   if ThePlayer then
      inst:EnableMovementPrediction(false) --PP doesn't work with movement prediction enabled, due to use of custom stategraphs
   end
end)
	----------------------------------
	--Tags--
    inst:AddTag("hostile")
    inst:AddTag("scarytoprey")
    inst:AddTag("monster")
	----------------------------------

	inst:WatchWorldState( "isday", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "isdusk", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "isnight", function() PlayablePets.SetNightVision(inst)  end)
	inst:WatchWorldState( "iscaveday", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "iscavedusk", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "iscavenight", function() PlayablePets.SetNightVision(inst)  end)
	
	PlayablePets.SetNightVision(inst)
end



local master_postinit = function(inst)
	--Stats--
	PlayablePets.SetCommonStats(inst, mob, nil, true) --mob table, ishuman, ignorepvpmultiplier
	PlayablePets.SetCommonWeatherResistances(inst, 60, 20, 1) --heat, cold, wetness
	PlayablePets.SetCommonStatResistances(inst) --fire, acid, poison
	----------------------------------
	--Variables	
	inst.mobsleep = true
	inst.taunt = true
	inst.taunt2 = true
	inst.AnimState:Hide("ARM")
	
	inst.getskins = getskins
	
	inst:AddComponent("timer")
	
	inst.userfunctions =
    {
        StartTimer = StartTimer,
    }
	
	inst.charge_task = nil
    inst.charge_time = 0
	
	local body_symbol = "krampus_torso"
	inst.poisonsymbol = body_symbol
	MakeLargeBurnableCharacter(inst, body_symbol, Vector3(1,0,1))
	MakeMediumFreezableCharacter(inst)
	inst.components.debuffable:SetFollowSymbol(body_symbol, 0, 0, 0)
	----------------------------------
	--Components
	PlayablePets.SetCommonLootdropper(inst, prefabname) --inst, prefaboverride
	----------------------------------
	--Eater--
	inst.components.eater:SetAbsorptionModifiers(1,1,1) --This might multiply food stats.
	inst.components.eater:SetCanEatHorrible() --check to make them eat monster meat.
	inst.components.eater.strongstomach = true -- can eat monster meat!
	---------------------------------
	--Physics and Shadows--
	inst.DynamicShadow:SetSize(3, 1)
	inst.Transform:SetFourFaced()
	MakeCharacterPhysics(inst, 10, .5)
	---------------------------------
	--Listeners--
	inst:ListenForEvent("equip", PlayablePets.CommonOnEquip) --Shows head when hats make heads disappear.
	inst.components.combat.onhitotherfn = onhitother
	---------------------------------
	--Forge--
	if TheNet:GetServerGameMode() == "lavaarena" then
		inst.forge_fn = SetForge(inst)
	end
    ------------------------------------------------------
	--Respawning and Initializing functions--
	
	inst:ListenForEvent("respawnfromghost", function(inst) PlayablePets.RevRestore(inst, mob) end) --(inst, mob, isflying, iswebimmune, noshadow, ishuman)

    inst:DoTaskInTime(0, function(inst) PlayablePets.CommonSetChar(inst, mob) end)
	inst:DoTaskInTime(3, function(inst) PlayablePets.SetSkin(inst, mob) end)
    inst:ListenForEvent("respawnfromghost", function() 
        inst:DoTaskInTime(5, function(inst) PlayablePets.CommonSetChar(inst, mob) end)
		inst:DoTaskInTime(5.1, function(inst) PlayablePets.SetSkin(inst, mob) end)
		inst:DoTaskInTime(5, function(inst) PlayablePets.RevRestore(inst, mob) end)
    end)
	
	inst.OnLongUpdate = OnLongUpdate
	inst.OnSave = OnSave
    inst.OnLoad = OnLoad
	
    return inst	
end

return MakePlayerCharacter(prefabname, prefabs, assets, common_postinit, master_postinit, start_inv)