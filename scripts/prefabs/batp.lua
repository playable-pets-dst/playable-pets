local MakePlayerCharacter = require "prefabs/player_common"

---------------------------
----------==Notes==--------
--
---------------------------
local prefabname = "batp"
--Don't add assets unless absolutely needed.
local assets = 
{
	 Asset("ANIM", "anim/bat_basic.zip"),
	 Asset("ANIM", "anim/bat_shiny_build_01.zip"),
	 Asset("SOUND", "sound/bat.fsb"),
}
--Might be completely pointless here. I don't know.

local getskins = {"1"}
local prefabs = 
{	

}
	
--Note: Becareful when adding starting items.
--People are likely to spam changecharacter to get more of them.	
local start_inv = 
{
	--'prefab',
}

local start_inv2 = 
{
	"bathome",
}

if MOBHOUSE == "Enable1" or MOBHOUSE == "Enable3" then
	start_inv = start_inv2
end
-----------------------
--Stats--

local mob = 
{
	health = TUNING[string.upper(prefabname.."_HEALTH")],
	hunger = TUNING[string.upper(prefabname.."_HUNGER")],
	hungerrate = TUNING.WILSON_HUNGER_RATE, 
	sanity = TUNING[string.upper(prefabname.."_SANITY")],
	runspeed = 7,
	walkspeed = 7,
	damage = 30*2,
	attackperiod = 0,
	range = 2,
	hit_range = 3,
	bank = "bat",
	build = "bat_basic",
	scale = 0.75,
	--build2 = "alternate build here",
	stategraph = "SGbatp",
	minimap = "batp.tex",
	
}
-----------------------
--Loot that drops when you die, duh.
SetSharedLootTable('batp',
-----Prefab---------------------Chance------------
{
    {'batwing',    0.15},
    {'guano',      0.15},
    {'monstermeat',0.10},
    
   
})

local function onattacked(inst, data) --Get friends to come help you out when you're in trouble!
    if data.attacker then
        inst.components.combat:SetTarget(data.attacker)
        inst.components.combat:ShareTarget(data.attacker, 40, function(dude) return dude:HasTag("bat") and not dude.components.health:IsDead() end, 3)
    end
end


local function OnHitOther(inst)
	inst.components.health:DoDelta(10, false)
	inst.components.hunger:DoDelta(5, false)
end


--------------------------------------------------
local function OnLoad(inst, data)
	if data ~= nil then
		--inst.mobteleported = data.mobteleported or false
		inst.isshiny = data.isshiny or 0
		--RandomSkinSet(inst)
	end
end

local function OnSave(inst, data)
	--data.mobteleported = inst.mobteleported or false
	data.isshiny = inst.isshiny or 0
end
------------------------------------------------------


local common_postinit = function(inst) 
	--inst.soundsname = "beefalo"

	inst.MiniMapEntity:SetIcon(mob.minimap)
	--inst.AnimState:SetMultColour(1, 1, 1, 0.5)

	inst:DoTaskInTime(0, function() --Disabled because might cause sliding. Remove this if I never slide again.
  if ThePlayer then
      inst:EnableMovementPrediction(false)
   end
end)
	--Night Vision that toggles, might be a better way to do this.
	--Enables at night, disables during the day.
	inst:WatchWorldState( "isday", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "isdusk", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "isnight", function() PlayablePets.SetNightVision(inst)  end)
	inst:WatchWorldState( "iscaveday", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "iscavedusk", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "iscavenight", function() PlayablePets.SetNightVision(inst)  end)
	
	PlayablePets.SetNightVision(inst) --This is here to make sure the NightVision is enabled when a player rejoins.
	
end



local master_postinit = function(inst) 
    PlayablePets.SetCommonStats(inst, mob) --mob table, ishuman, ignorepvpmultiplier
    PlayablePets.SetCommonWeatherResistances(inst, 60, 20) --heat, cold, wetness
    PlayablePets.SetCommonStatResistances(inst) --fire, acid, poison, freeze
	----------------------------------
	--Loot drops--
	inst:AddComponent("lootdropper")
    inst.components.lootdropper:SetChanceLootTable('batp')
	----------------------------------

	----------------------------------
	--Tags--
	inst.mobsleep = true --allows mobs to sleep when z is pressed.
	inst.mobplayer = true --lets mod know that you're a mob.
    inst:AddTag("monster") --PIGS HATE HIM!
    inst:AddTag("bat")
    inst:AddTag("flying")
    inst.taunt2 = true
    inst._isflying = false
	inst.isshiny = 0
	inst.getskins = getskins
	local body_symbol = "bat_body"
    inst.poisonsymbol = body_symbol
    MakeSmallBurnableCharacter(inst, body_symbol)
    MakeTinyFreezableCharacter(inst, body_symbol)
    inst.components.debuffable:SetFollowSymbol(body_symbol, 0, 0, 0)
	
	inst.taunt = true
	inst.ghostbuild = "ghost_monster_build"

	--Eater--
	

	inst.components.eater:SetDiet({ FOODTYPE.MEAT, FOODTYPE.MEAT }, { FOODTYPE.MEAT, FOODTYPE.MEAT }) 

	inst.components.eater:SetCanEatHorrible() --check to make them eat monster meat.
	inst.components.eater.strongstomach = true -- can eat monster meat!

	MakeGhostPhysics(inst, 1, .5) --Special physics for flying characters(?)
    inst.Transform:SetScale(0.75, 0.75, 0.75)
    inst.DynamicShadow:SetSize(1.5, .75)
    PlayablePets.SetAmphibious(inst, nil, nil, true)

    inst.Transform:SetFourFaced()
	
	PlayablePets.SetStormImmunity(inst)
	---------------------------------
	--Shedder--
	
	--Periodic Spawner--
	
	inst:AddComponent("periodicspawner")
    inst.components.periodicspawner:SetPrefab("guano")
    inst.components.periodicspawner:SetRandomTimes(120,240)
    inst.components.periodicspawner:SetDensityInRange(30, 2)
    inst.components.periodicspawner:SetMinimumSpacing(8)
    inst.components.periodicspawner:Start()
	---------------------------------
	--Listeners--
	
	inst:ListenForEvent("equip", PlayablePets.CommonOnEquip) --Shows head when hats make heads disappear.
	inst:ListenForEvent("attacked", onattacked)
	inst:ListenForEvent("onhitother", OnHitOther)

	--Forge--
	if TheNet:GetServerGameMode() == "lavaarena" then
		inst.forge_fn = SetForge(inst)
	end
    ------------------------------------------------------
	--Respawning and Initializing functions--
	
	inst:ListenForEvent("respawnfromghost", function(inst) PlayablePets.RevRestore(inst, mob, true, true) end) --(inst, mob, isflying, iswebimmune, noshadow, ishuman)

    inst:DoTaskInTime(0, function(inst) PlayablePets.CommonSetChar(inst, mob) end)
	inst:DoTaskInTime(3, function(inst) PlayablePets.SetSkin(inst, mob) end)
    inst:ListenForEvent("respawnfromghost", function() 
        inst:DoTaskInTime(5, function(inst) PlayablePets.CommonSetChar(inst, mob) end)
		inst:DoTaskInTime(5.1, function(inst) PlayablePets.SetSkin(inst, mob) end)
		inst:DoTaskInTime(5, function(inst) PlayablePets.RevRestore(inst, mob, true, true) end)
    end)
	
	inst.OnSave = OnSave
    inst.OnLoad = OnLoad
    return inst
end

return MakePlayerCharacter("batp", prefabs, assets, common_postinit, master_postinit, start_inv)
